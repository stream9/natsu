# natsu
Yet another Chromium base Web browser

## motivation
There are many excellent Web browser out there but
- out of concern for securiy, customizability is rather restrictive
- as consequence of trying to be user friendly, they come with a lot of 
  functionality I will never use

I wanted the Web browser that is more
- simple
- hackable
- modular

so I wrote it for myself.

## requirement
- C++17 compiler
- boost
- cmake