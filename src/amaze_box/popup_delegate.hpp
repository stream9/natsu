#ifndef NATSU_AMAZE_BOX_POPUP_DELEGATE_HPP
#define NATSU_AMAZE_BOX_POPUP_DELEGATE_HPP

#include <QStyledItemDelegate>

class QModelIndex;
class QPainter;
class QSize;
class QStyleOptionViewItem;

namespace natsu::amaze_box {

class PopupDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    PopupDelegate();
    ~PopupDelegate() override;

    // override QStyledItemDelegate
    QSize sizeHint(QStyleOptionViewItem const&, QModelIndex const&) const override;
    void paint(QPainter*, QStyleOptionViewItem const&, QModelIndex const&) const override;

private:
    void paintBackground(QPainter&, QStyleOptionViewItem const&) const;
    void paintIcon(QPainter&, QIcon const&, QRect&) const;
    void paintText(QPainter&, QString const& title, QString const& url,
                   QString const& keyword,
                   QStyleOptionViewItem const&, QRect const&) const;

private:
    static int constexpr m_padding = 3;
};

} // namespace natsu::amaze_box

#endif // NATSU_AMAZE_BOX_POPUP_DELEGATE_HPP
