#ifndef NATSU_AMAZE_BOX_INLINE_COMPLETER_HPP
#define NATSU_AMAZE_BOX_INLINE_COMPLETER_HPP

#include "fwd/amaze_box.hpp"
#include "core/fwd/history_manager.hpp"

#include <QObject>
#include <QString>

namespace natsu::amaze_box {

class InlineCompleter : public QObject
{
    Q_OBJECT
public:
    InlineCompleter(AmazeBox&, HistoryManager&);
    ~InlineCompleter() override;

    // accessor
    AmazeBox& amazeBox() const { return m_amazeBox; }
    HistoryManager& history() const { return m_history; }

private:
    Q_SLOT void onTextEdited(QString const&);

    void completeHostName(QString const&);
    bool isKeyword(QString const&) const;

private:
    AmazeBox& m_amazeBox;
    HistoryManager& m_history;

    QString m_previous;
};

} // namespace natsu::amaze_box

#endif // NATSU_AMAZE_BOX_INLINE_COMPLETER_HPP
