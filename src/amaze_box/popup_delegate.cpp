#include "popup_delegate.hpp"

#include "popup_model.hpp"

#include "global.hpp"
#include "core/conv.hpp"
#include "misc/pointer.hpp"

#include <QModelIndex>
#include <QPainter>
#include <QSize>
#include <QStyleOptionViewItem>
#include <QTextLayout>

namespace natsu::amaze_box {

class TextLayout : public QTextLayout
{
public:
    TextLayout(QString const& title,
               QString const& url,
               QString const& keyword,
               QStyleOptionViewItem const& option)
        : m_option { option }
        , m_separator { QSL(" - ") }
        , m_keyword { keyword }
    {
        configureOption();

        m_title = elideTitle(title);
        m_url = elideUrl(url);

        this->setFont(m_option.font);
        this->setText(QSL("%1%2%3")
                .arg(m_title).arg(m_separator).arg(m_url));

        setColors();

        emphasizeKeyword();

        doLayout();
    }

private:
    QString elideTitle(QString const& title) const
    {
        auto const& fm = m_option.fontMetrics;
        auto const& rect = m_option.rect;

        return fm.elidedText(
            title,
            m_option.textElideMode,
            rect.width() / 2
        );
    }

    QString elideUrl(QString const& url) const
    {
        auto const& fm = m_option.fontMetrics;
        auto const& rect = m_option.rect;

        return fm.elidedText(
            url,
            m_option.textElideMode,
            rect.width() - fm.horizontalAdvance(m_title)
                         - fm.horizontalAdvance(m_separator)
        );
    }

    void configureOption()
    {
        QTextOption textOption;
        textOption.setWrapMode(QTextOption::NoWrap);
        textOption.setTextDirection(m_option.direction);
        textOption.setAlignment(m_option.displayAlignment);
        this->setTextOption(textOption);
    }

    void setColors()
    {
        auto const selected = m_option.state & QStyle::State_Selected;
        auto const& palette = m_option.palette;

        auto formats = this->formats();

        QTextLayout::FormatRange range1;
        range1.start = 0;
        range1.length = toInt(m_title.size() + m_separator.size());
        range1.format.setForeground(palette.color(
                    selected ? QPalette::HighlightedText : QPalette::Text));
        formats.push_back(range1);

        QTextLayout::FormatRange range2;
        range2.start = range1.start + range1.length;
        range2.length = toInt(m_url.size());
        range2.format.setForeground(palette.color(
                    selected ? QPalette::HighlightedText : QPalette::Link));
        formats.push_back(range2);

        this->setFormats(formats);
    }

    void emphasizeKeyword()
    {
        if (m_keyword.isEmpty()) return;

        auto const& text = this->text();
        auto const len = text.size();
        auto const keywordLen = m_keyword.size();
        auto formats = this->formats();

        for (qsizetype from = 0; from < len; from += keywordLen) {
            from = text.indexOf(m_keyword, from, Qt::CaseInsensitive);
            if (from == -1) break;

            QTextLayout::FormatRange range;
            range.start = toInt(from);
            range.length = toInt(keywordLen);
            range.format.setFontWeight(QFont::Bold);

            formats.push_back(range);
        }

        this->setFormats(formats);
    }

    void doLayout()
    {
        this->beginLayout();
        this->createLine();
        this->endLayout();
    }

private:
    QStyleOptionViewItem const& m_option;
    QString m_title;
    QString m_separator;
    QString m_url;
    QString const& m_keyword;
};

// PopupDelegate

PopupDelegate::
PopupDelegate()
{}

PopupDelegate::~PopupDelegate() = default;

QSize PopupDelegate::
sizeHint(QStyleOptionViewItem const& option, QModelIndex const&) const
{
    QFontMetrics const fm { option.font };

    QSize const result {
        100,
        fm.height() + m_padding * 2
    };

    return result;
}

void PopupDelegate::
paint(QPainter* const painter, QStyleOptionViewItem const& option,
                               QModelIndex const& index) const
{
    assert(painter);
    assert(index.isValid());

    auto rect = option.rect;
    auto const& model =
            to_ref(static_cast<PopupModel const*>(index.model()));
    auto const& title = index.data(PopupModel::TitleRole).toString();
    auto const& url = index.data(PopupModel::UrlRole).toString();
    auto const& icon = index.data(PopupModel::IconRole).value<QIcon>();
    auto const& keyword = model.keyword();

    paintBackground(*painter, option);

    paintIcon(*painter, icon, rect);

    paintText(*painter, title, url, keyword, option, rect);
}

void PopupDelegate::
paintBackground(QPainter& painter, QStyleOptionViewItem const& option) const
{
    option.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, &painter);
}

void PopupDelegate::
paintIcon(QPainter& painter, QIcon const& icon, QRect& rect) const
{
    auto const iconSize = 16;
    QRect iconRect { 0, 0, iconSize, iconSize };
    iconRect.moveCenter(rect.center());
    iconRect.moveLeft(m_padding);

    auto const& pixmap = icon.pixmap(rect.width());
    painter.drawPixmap(iconRect, pixmap);

    rect.moveLeft(iconRect.right() + m_padding);
}

void PopupDelegate::
paintText(QPainter& painter,
          QString const& title, QString const& url, QString const& keyword,
          QStyleOptionViewItem const& option, QRect const& rect) const
{
    TextLayout const layout { title, url, keyword, option };

    QPoint const point { rect.left() + m_padding, rect.top() + m_padding };
    layout.draw(&painter, point);
}

} // namespace natsu::amaze_box
