#include "actions.hpp"

#include "amaze_box.hpp"

#include "core/action_manager.hpp"

namespace natsu::amaze_box {

Actions::
Actions(AmazeBox& amazeBox)
    : m_amazeBox { amazeBox }
{
    m_setFocus.setText("Focus on Address Bar");
    m_setFocus.setShortcut(makeKeyCode(Qt::CTRL, Qt::Key_L));
    this->connect(&m_setFocus, &QAction::triggered,
                  this,        &Actions::onSetFocus);

    amazeBox.addAction(&m_setFocus);
}

Actions::~Actions() = default;

void Actions::
onSetFocus()
{
    m_amazeBox.selectAll();
    m_amazeBox.setFocus();
}

} // namespace natsu::amaze_box
