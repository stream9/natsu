#include "popup_model.hpp"

#include "core/bookmark_manager.hpp"
#include "core/get.hpp"
#include "core/history.hpp"
#include "core/history_manager.hpp"
#include "core/icon.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/variant.hpp"

#include <boost/numeric/conversion/cast.hpp>

#include <QVariant>

namespace natsu::amaze_box {

PopupModel::
PopupModel(Profile& profile)
    : m_profile { profile }
{}

PopupModel::~PopupModel() = default;

QString PopupModel::
url(QModelIndex const& index) const
{
    return data(index, UrlRole).toString();
}

void PopupModel::
setKeyword(QString const& keyword)
{
    m_keyword = keyword;

    auto& history = get::history(m_profile);
    auto& bookmark = get::bookmark(m_profile);

    this->beginResetModel();

    m_items.clear();
    auto oit = std::back_inserter(m_items);

    auto const& bookmarks = bookmark.search(keyword);
    std::copy(bookmarks.begin(), bookmarks.end(), oit);

    auto const& entries = history.search(keyword);
    std::copy(entries.begin(), entries.end(), oit);

    this->endResetModel();
}

int PopupModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (parent.isValid()) return 0;

    return boost::numeric_cast<int>(m_items.size());
}

QVariant PopupModel::
data(QModelIndex const& index, int const role/*= Qt::DisplayRole*/) const
{
    if (!index.isValid()) return {};
    assert(index.model() == this);

    auto const row = index.row();
    // row >= 0 is implied by index.isValid()
    assert(row < rowCount());

    auto const& item = m_items[static_cast<size_t>(row)];

    return std::visit(overloaded {
        [&](Bookmark const& item) -> QVariant {
            switch (role) {
            case TitleRole:
                return item.title;
            case UrlRole:
                return item.url;
            case Qt::DisplayRole:
                return item.url;
            case IconRole:
            case Qt::DecorationRole:
                return bookmark::icon::bookmarked();
            default:
                return {};
            }
        },
        [&](History const& item) -> QVariant {
            switch (role) {
            case TitleRole:
                return item.title;
            case UrlRole:
                return item.url;
            case Qt::DisplayRole:
                return item.url;
            case IconRole:
            case Qt::DecorationRole:
                return item.icon;
            default:
                return {};
            }
        }
    }, item);
}

bool PopupModel::
removeRows(int const row, int const count, QModelIndex const& parent/*= {}*/)
{
    assert(row >= 0);
    assert(static_cast<size_t>(row) < m_items.size());
    assert(count == 1);

    return std::visit(overloaded {
        [&](Bookmark const&) {
            (void)parent;
            return false;
        },
        [&](History const& item) {
            auto& history = get::history(m_profile);

            history.removeEntry(item.id);

            this->beginRemoveRows(parent, row, row + count - 1);

            m_items.erase(m_items.begin() + row);

            this->endRemoveRows();

            return true;
        }
    }, m_items[static_cast<size_t>(row)]);
}

} // namespace natsu::amaze_box
