#include "amaze_box.hpp"

#include "actions.hpp"
#include "inline_completer.hpp"
#include "popup.hpp"

#include "browser/browser_tab.hpp"
#include "browser/tab_browser.hpp"
#include "browser/web_view.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/search_engine_manager.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QApplication>
#include <QRegularExpression>
#include <QUrl>

namespace natsu {

static bool
hasScheme(QString const& text)
{
    static QRegularExpression const hasScheme {
        "^[a-z]+://.*"
    };

    return hasScheme.match(text).hasMatch();
}

static bool
isLookLikeHostName(QString const& text)
{
    static QRegularExpression const lookLikeHostName {
        R"(^[a-z]+\.[^ ]+$)"
    };

    return lookLikeHostName.match(text).hasMatch();
}

static bool
isLookLikeIPAddress(QString const& text)
{
    static QRegularExpression const regex {
        R"(^(?:[0-9]{1,3}\.){3}[0-9]{1,3}/?$)"
    };

    return regex.match(text).hasMatch();
}

QUrl AmazeBox::
makeUrl(QString const& text) const
{
    if (hasScheme(text)) {
        return text;
    }

    if (isLookLikeHostName(text) || isLookLikeIPAddress(text)) {
        return QSL("https://%1").arg(text);
    }

    auto& searchEngine = get::searchEngine(m_browserTab);
    return searchEngine.searchUrl(text);
}

// AmazeBox

AmazeBox::
AmazeBox(BrowserTab& tab)
    : QLineEdit { &tab }
    , m_browserTab { tab }
    , m_popup { std::make_unique<amaze_box::Popup>(*this, get::profile(tab)) }
    , m_inlineCompleter { std::make_unique<amaze_box::InlineCompleter>(*this, get::history(tab)) }
    , m_actions { std::make_unique<amaze_box::Actions>(*this) }
{
    this->connect(this, &QLineEdit::returnPressed,
                  this, &AmazeBox::onReturnPressed);

    auto& view = get::webView(m_browserTab);
    this->connect(&view, &WebView::urlChanged,
                  this,  &AmazeBox::onUrlChanged);
}

AmazeBox::~AmazeBox() = default;

void AmazeBox::
setUrl(QUrl const& url)
{
    this->setText(url.toDisplayString());
    this->setCursorPosition(0);
}

void AmazeBox::
onReturnPressed()
{
    auto const& url = makeUrl(this->text());

    auto const& modifiers = qApp->keyboardModifiers();

    if (modifiers == Qt::ControlModifier) {
            auto& browser = get::tabBrowser(m_browserTab);
            auto& tab = browser.newBackgroundTab();

            tab.load(url);
    }
    else if (modifiers == Qt::ShiftModifier) {
        auto& app = get::application(m_browserTab);
        auto& newWin = app.createWindow();
        auto& newTab = get::activeTab(newWin);

        newTab.load(url);
    }
    else {
        m_browserTab.load(url);
    }
}

void AmazeBox::
onUrlChanged(QUrl const& url)
{
    setUrl(url);
}

} // namespace natsu
