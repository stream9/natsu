#ifndef NATSU_CORE_AMAZE_BOX_ACTION_HPP
#define NATSU_CORE_AMAZE_BOX_ACTION_HPP

#include "fwd/amaze_box.hpp"

#include <QObject>
#include <QAction>

namespace natsu::amaze_box {

class Actions : public QObject
{
    Q_OBJECT
public:
    Actions(AmazeBox&);
    ~Actions() override;

    // accessor
    AmazeBox& amazeBox() const { return m_amazeBox; }

    QAction& setFocusAction() { return m_setFocus; }

private:
    Q_SLOT void onSetFocus();

private:
    AmazeBox& m_amazeBox;

    QAction m_setFocus;
};

} // namespace natsu

#endif // NATSU_CORE_AMAZE_BOX_ACTION_HPP
