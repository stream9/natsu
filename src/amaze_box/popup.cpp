#include "popup.hpp"

#include "amaze_box.hpp"
#include "popup_delegate.hpp"
#include "popup_model.hpp"

#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <algorithm>

#include <QKeyEvent>
#include <QObject>
#include <QString>

namespace natsu::amaze_box {

Popup::
Popup(AmazeBox& amazeBox, Profile& profile)
    : m_amazeBox { amazeBox }
    , m_profile { profile }
    , m_model { std::make_unique<PopupModel>(profile) }
    , m_delegate { std::make_unique<PopupDelegate>() }
{
    this->setWindowFlags(Qt::Popup); //TODO setParent
    this->setFocusPolicy(Qt::NoFocus);
    this->setFocusProxy(&m_amazeBox);
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->setSelectionMode(QAbstractItemView::SingleSelection);
    this->setUniformItemSizes(true);
    this->setModel(m_model.get());
    this->setItemDelegate(m_delegate.get());
    this->installEventFilter(this);

    this->connect(this, &QListView::clicked,
                  this, &Popup::onItemClicked);

    auto& selection = to_ref(this->selectionModel());

    this->connect(&selection, &QItemSelectionModel::currentChanged,
                  this,       &Popup::onCurrentChanged);

    this->connect(m_model.get(), &QAbstractItemModel::modelReset,
                  this,          &Popup::onModelReseted);

    this->connect(&m_amazeBox, &AmazeBox::textEdited,
                  this,        &Popup::onTextEdited);
}

Popup::~Popup() = default;

void Popup::
showEvent(QShowEvent* const)
{
    adjustGeometry();
}

void Popup::
keyPressEvent(QKeyEvent* const ev)
{
    auto& event = to_ref(ev);

    if (event.matches(QKeySequence::Cancel)) {
        this->hide();
        return;
    }

    switch (event.key()) {
        case Qt::Key_Up:
            if (handleKeyUp()) break;
            [[fallthrough]];
        case Qt::Key_Down:
        case Qt::Key_PageUp:
        case Qt::Key_PageDown:
            QListView::keyPressEvent(ev);
            break;
        case Qt::Key_Delete: {
            auto const& index = this->currentIndex();
            if (!index.isValid()) break;

            m_model->removeRow(index.row());
            adjustGeometry();
            break;
        }
        case Qt::Key_Enter:
        case Qt::Key_Return:
        case Qt::Key_Tab:
            this->hide();
            [[fallthrough]];
        default:
            m_amazeBox.event(ev);
            break;
    }
}

bool Popup::
eventFilter(QObject* const obj, QEvent* const ev)
{
    assert(obj);
    assert(ev);

    if (ev->type() == QEvent::MouseButtonPress && !this->underMouse()) {
        this->hide();
        return true;
    }

    return QListView::eventFilter(obj, ev);
}

void Popup::
onCurrentChanged(QModelIndex const& index)
{
    if (!index.isValid()) return;

    m_amazeBox.setText(m_model->url(index));
}

void Popup::
onItemClicked()
{
    this->hide();
    m_amazeBox.returnPressed();
}

void Popup::
onModelReseted()
{
    adjustGeometry();
}

void Popup::
onTextEdited(QString const& text)
{
    m_editedText = text;

    m_model->setKeyword(text);

    if (m_model->rowCount() == 0) {
        this->hide();
    }
    else {
        this->show();
    }
}

void Popup::
adjustGeometry()
{
    auto const pos = m_amazeBox.mapToGlobal(QPoint(0, 0));
    auto const rect = m_amazeBox.rect();

    auto const rowCount = std::min(m_model->rowCount(), m_maxVisibleRows);

    // magic number 3 is for suppressing scrollbar to appear when it has only one line
    auto const height = this->sizeHintForRow(0) * rowCount + 3;

    this->setGeometry(
        pos.x(),
        pos.y() + rect.height(),
        rect.width(),
        height
    );
}

bool Popup::
handleKeyUp()
{
    auto const& index = this->currentIndex();
    if (!index.isValid()) return false;

    if (index.row() == 0) {
        m_amazeBox.setText(m_editedText);
        this->hide();
        return true;
    }

    return false;
}

} // namespace natsu::amaze_box
