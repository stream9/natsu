#ifndef NATSU_AMAZE_BOX_POPUP_MODEL_HPP
#define NATSU_AMAZE_BOX_POPUP_MODEL_HPP

#include "core/fwd/bookmark.hpp"
#include "core/fwd/history.hpp"
#include "core/fwd/profile.hpp"

#include <variant>
#include <vector>

#include <QAbstractListModel>
#include <QIcon>
#include <QString>

namespace natsu::amaze_box {

class PopupModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum { TitleRole = Qt::UserRole, UrlRole, IconRole };

public:
    PopupModel(Profile&);
    ~PopupModel() override;

    // accessor
    Profile& profile() const { return m_profile; }
    QString const& keyword() const { return m_keyword; }

    // query
    QString url(QModelIndex const&) const;

    // modifier
    void setKeyword(QString const&);

    // override QAbstractItemModel
    int rowCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const& index, int role = Qt::DisplayRole) const override;
    bool removeRows(int row, int count, QModelIndex const& parent = {}) override;

private:
    Profile& m_profile;

    QString m_keyword;

    using Bookmark = bookmark::Bookmark;
    using History = history::Entry;
    using Item = std::variant<Bookmark, History>;
    std::vector<Item> m_items;
};

} // namespace natsu::amaze_box

#endif // NATSU_AMAZE_BOX_POPUP_MODEL_HPP
