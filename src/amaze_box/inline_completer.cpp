#include "inline_completer.hpp"

#include "amaze_box.hpp"

#include "core/conv.hpp"
#include "core/get.hpp"
#include "core/history_manager.hpp"
#include "core/search_engine_manager.hpp"
#include "global.hpp"

#include <QRegularExpression>

namespace natsu::amaze_box {

static bool
isLookLikeHostName(QString const& text)
{
    static QRegularExpression const lookLikeHostName {
        R"(^[0-9A-Za-z\-.]+$)"
    };

    return lookLikeHostName.match(text).hasMatch();
}

// InlineCompleter

InlineCompleter::
InlineCompleter(AmazeBox& amazeBox, HistoryManager& history)
    : m_amazeBox { amazeBox }
    , m_history { history }
{
    this->connect(&m_amazeBox, &AmazeBox::textEdited,
                  this,        &InlineCompleter::onTextEdited);
}

InlineCompleter::~InlineCompleter() = default;

void InlineCompleter::
onTextEdited(QString const& text)
{
    if (text.size() > m_previous.size()) {
        //TODO more
        if (isKeyword(text)) {
            // Nop
        }
        else if (isLookLikeHostName(text)) {
            completeHostName(text);
        }
    }

    m_previous = text;
}

void InlineCompleter::
completeHostName(QString const& prefix)
{
    auto const& candidate = m_history.completeHost(prefix);

    if (!candidate.isEmpty()) {
        auto const hostname = candidate + "/";
        auto const selStart = prefix.size();
        auto const selLength = hostname.size() - selStart;
        m_amazeBox.setText(hostname);
        m_amazeBox.setSelection(toInt(selStart), toInt(selLength));
    }
}

bool InlineCompleter::
isKeyword(QString const& text) const
{
    auto& searchEngineMgr = get::searchEngine(m_amazeBox.browserTab());
    auto* const engine = searchEngineMgr.searchEngine(text);

    return engine != nullptr;
}

} // namespace natsu::amaze_box
