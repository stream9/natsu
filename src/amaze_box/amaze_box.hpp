#ifndef NATSU_CORE_AMAZE_BOX_HPP
#define NATSU_CORE_AMAZE_BOX_HPP

#include "browser/fwd/browser_tab.hpp"

#include <memory>

#include <QLineEdit>

class QUrl;

namespace natsu::amaze_box {

class Actions;
class InlineCompleter;
class Popup;

} // namespace natsu::amaze_box

namespace natsu {

class AmazeBox : public QLineEdit
{
    Q_OBJECT
public:
    AmazeBox(BrowserTab&);
    ~AmazeBox() override;

    // accessor
    BrowserTab& browserTab() const { return m_browserTab; }
    amaze_box::Popup& popup() const { return *m_popup; }
    amaze_box::InlineCompleter& inlineCompleter() const { return *m_inlineCompleter; }
    amaze_box::Actions& amazeBoxActions() const { return *m_actions; }

    // modifier
    void setUrl(QUrl const&);

private:
    QUrl makeUrl(QString const& text) const;

    Q_SLOT void onReturnPressed();
    Q_SLOT void onUrlChanged(QUrl const&);

private:
    BrowserTab& m_browserTab;

    std::unique_ptr<amaze_box::Popup> const m_popup; // non-null
    std::unique_ptr<amaze_box::InlineCompleter> const m_inlineCompleter; // non-null
    std::unique_ptr<amaze_box::Actions> const m_actions; // non-null
};

} // namespace natsu

#endif // NATSU_CORE_AMAZE_BOX_HPP
