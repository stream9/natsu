#ifndef NATSU_AMAZE_BOX_POPUP_HPP
#define NATSU_AMAZE_BOX_POPUP_HPP

#include "fwd/amaze_box.hpp"
#include "core/fwd/profile.hpp"

#include <memory>

#include <QListView>

class QKeyEvent;
class QEvent;
class QObject;
class QShowEvent;
class QString;

namespace natsu::amaze_box {

class PopupDelegate;
class PopupModel;

class Popup : public QListView
{
    Q_OBJECT
public:
    Popup(AmazeBox&, Profile&);
    ~Popup() override;

    // accessor
    AmazeBox& amazeBox() const { return m_amazeBox; }
    Profile& profile() const { return m_profile; }

    // override QWidget
    bool eventFilter(QObject*, QEvent*) override;

protected:
    // override QWidget
    void showEvent(QShowEvent*) override;
    void keyPressEvent(QKeyEvent*) override;

private:
    Q_SLOT void onCurrentChanged(QModelIndex const&);
    Q_SLOT void onItemClicked();
    Q_SLOT void onModelReseted();
    Q_SLOT void onTextEdited(QString const&);

    void adjustGeometry();
    bool handleKeyUp();

private:
    AmazeBox& m_amazeBox;
    Profile& m_profile;

    std::unique_ptr<PopupModel> const m_model; // non-null
    std::unique_ptr<PopupDelegate> const m_delegate; // non-null
    QString m_editedText;

    static int constexpr m_maxVisibleRows = 10;
};

} // namespace natsu::amaze_box

#endif // NATSU_AMAZE_BOX_POPUP_HPP
