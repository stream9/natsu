#ifndef NATSU_GLOBAL_HPP
#define NATSU_GLOBAL_HPP

#include <cassert>

#include <QDebug>

#define QSL(str) QStringLiteral(str)

#define NATSU_VERSION QSL("0.0.1")

#endif // NATSU_GLOBAL_HPP
