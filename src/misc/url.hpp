#ifndef NATSU_MISC_URL_HPP
#define NATSU_MISC_URL_HPP

#include <QUrl>

namespace natsu {

QUrl normalizeUrl(QUrl const&);

} // namespace natsu

#endif // NATSU_MISC_URL_HPP
