#include "abstract_widget_list.hpp"

#include "global.hpp"

#include <cassert>

#include <QAbstractItemModel>
#include <QMouseEvent>
#include <QPalette>
#include <QModelIndex>

AbstractWidgetList::
AbstractWidgetList(QWidget* const parent/*= nullptr*/)
    : QScrollArea { parent }
    , m_layout { make_qmanaged<QVBoxLayout>() }
{
    auto container = make_qmanaged<QWidget>(this);
    auto layout = make_qmanaged<QVBoxLayout>(container.get());

    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    m_layout->setContentsMargins(0, 0, 0, 0);
    m_layout->setSpacing(0);

    layout->addLayout(m_layout);
    layout->addStretch();

    this->setWidget(container.get());
    this->setWidgetResizable(true);
}

AbstractWidgetList::~AbstractWidgetList() = default;

void AbstractWidgetList::
setModel(QAbstractItemModel* const model)
{
    clear();
    m_model = model;

    if (m_model == nullptr) return;

    auto const count = model->rowCount();
    for (auto row = 0; row < count; ++row) {
        insertWidget(row);
    }

    this->connect(m_model, &QAbstractItemModel::rowsInserted,
                  this,    &AbstractWidgetList::onRowsInserted);
    this->connect(m_model, &QAbstractItemModel::rowsRemoved,
                  this,    &AbstractWidgetList::onRowsRemoved);
    this->connect(m_model, &QAbstractItemModel::modelReset,
                  this,    &AbstractWidgetList::onModelReset);
}

void AbstractWidgetList::
select(index_type const index)
{
    auto& item = to_ref(m_layout->itemAt(index));
    auto& widget = to_ref(dynamic_cast<QWidget*>(item.widget()));

    select(widget);
}

QModelIndex AbstractWidgetList::
selection() const
{
    return m_selected ? index(*m_selected) : QModelIndex();
}

void AbstractWidgetList::
clearSelection()
{
    auto const& before = m_selected ? index(*m_selected) : QModelIndex();
    deselect();

    Q_EMIT selectionChanged(before, {});
}

void AbstractWidgetList::
keyPressEvent(QKeyEvent* const event)
{
    assert(event);

    switch (event->key()) {
        case Qt::Key_Up:
            moveCursor(-1);
            break;
        case Qt::Key_Down:
            moveCursor(1);
            break;
        case Qt::Key_Home:
            this->ensureVisible(0, 0);
            break;
        case Qt::Key_End: {
            auto& container = to_ref(this->widget());
            auto const& bottomLeft = container.geometry().bottomLeft();
            this->ensureVisible(bottomLeft.x(), bottomLeft.y());
            break;
        }
        default:
            QScrollArea::keyPressEvent(event);
            break;
    }
}

void AbstractWidgetList::
mousePressEvent(QMouseEvent* const event)
{
    assert(event);

    auto* const item = findItem(event->pos());
    if (!item) return;

    select(*item);
}

QModelIndex AbstractWidgetList::
index(QWidget &widget) const
{
    const auto row = m_layout->indexOf(&widget);
    assert(row != -1);

    assert(m_model);
    return m_model->index(row, 0);
}

void AbstractWidgetList::
select(QWidget &item)
{
    auto const& before = m_selected ? index(*m_selected) : QModelIndex();

    deselect();

    m_selected = &item;

    QModelIndex const& after = index(item);

    adjustBackgroundRole(item);
    item.setFocus();
    this->ensureWidgetVisible(&item);

    Q_EMIT selectionChanged(before, after);
}

void AbstractWidgetList::
deselect()
{
    if (!m_selected) return;

    auto &widget = *m_selected;
    m_selected = nullptr;
    adjustBackgroundRole(widget);
}

void AbstractWidgetList::
moveCursor(int const amount)
{
    if (!m_selected) {
        select(0);
        return;
    }

    const auto current = m_layout->indexOf(m_selected);
    const auto last = m_layout->count() - 1;

    auto next = current + amount;
    if (next < 0) {
        next = 0;
    }
    else if (next > last) {
        next = last;
    }
    assert(0 <= next && next <= last);

    select(static_cast<size_t>(next));
}

void AbstractWidgetList::
clear()
{
    for (auto i = 0, count = m_layout->count(); i < count; ++i) {
        auto& item = to_ref(m_layout->itemAt(0));

        auto& widget = to_ref(item.widget());

        widget.deleteLater();
        m_layout->removeItem(&item);
    }

    m_selected = nullptr;
}

void AbstractWidgetList::
onRowsInserted(QModelIndex const&/*parent*/, int const start, int const end)
{
    assert(m_model);
    assert(0 <= start && start < m_model->rowCount());
    assert(0 <= end   && end   < m_model->rowCount());

    for (auto row = start; row <= end; ++row) {
        insertWidget(row);
    }
    adjustBackgroundRole(start);
}

void AbstractWidgetList::
onRowsRemoved(QModelIndex const&/*parent*/, int const start, int const end)
{
    assert(0 <= start && start < m_model->rowCount() + 1);
    assert(0 <= end   && end   < m_model->rowCount() + 1);

    for (auto row = start; row <= end; ++row) {
        removeWidget(row);
    }
    adjustBackgroundRole(start);
}

void AbstractWidgetList::
onModelReset()
{
    clear();

    auto const count = m_model->rowCount();
    for (auto row = 0; row < count; ++row) {
        insertWidget(row);
    }
}

QWidget *AbstractWidgetList::
findItem(QPoint const& pos) const
{
    QWidget *result = nullptr;
    auto *child = this->childAt(pos);

    while (child != nullptr) {
        if (m_layout->indexOf(child) != -1) {
            result = child;
            break;
        }

        child = child->parentWidget();
    }

    return result;
}

void AbstractWidgetList::
insertWidget(index_type const row)
{
    assert(m_model);
    auto &&index = m_model->index(row, 0);
    assert(index.isValid());

    auto &item = createItemWidget(index);

    m_layout->insertWidget(row, &item);
    adjustBackgroundRole(item);

    item.show(); // This show() will recalculate geometry immediately.
}

void AbstractWidgetList::
removeWidget(index_type const row)
{
    auto& item = to_ref(m_layout->itemAt(row));
    auto& widget = to_ref(item.widget());

    if (&widget == m_selected) {
        auto const count = m_layout->count();
        assert(count > 0);
        if (count == 1) {
            deselect();
        }
        else {
            auto const newIndex = row != count - 1 ? row + 1 : row - 1;
            assert(0 <= newIndex && newIndex < count);
            select(newIndex);
        }
    }

    m_layout->removeWidget(&widget);
    widget.deleteLater();
}

void AbstractWidgetList::
adjustBackgroundRole(QWidget &widget)
{
    if (&widget == m_selected) {
        widget.setBackgroundRole(QPalette::Highlight);
    }
    else {
        auto const row = m_layout->indexOf(&widget);
        assert(row != -1);
        widget.setBackgroundRole(
            row % 2 ? QPalette::Base : QPalette::AlternateBase);
    }
}

void AbstractWidgetList::
adjustBackgroundRole(index_type const start)
{
    if (start >= m_layout->count()) return; // This occurs when last item is removed.

    for (auto row = start, count = m_layout->count(); row < count; ++row) {
        auto& item = to_ref(m_layout->itemAt(row));
        auto& widget = to_ref(item.widget());

        adjustBackgroundRole(widget);
    }
}

