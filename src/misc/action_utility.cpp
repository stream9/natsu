#include "action_utility.hpp"

#include <QAction>
#include <QString>
#include <QWidget>

namespace natsu::action {

QAction*
findAction(QWidget& widget, QString const& name)
{
    for (auto* const action: widget.actions()) {
        if (!action) continue;

        if (action->objectName() == name) return action;
    }

    return nullptr;
}

} // namespace natsu::action
