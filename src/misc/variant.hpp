#ifndef NATSU_MISC_VARIANT_HPP
#define NATSU_MISC_VARIANT_HPP

#include <variant>
#include <type_traits>

namespace natsu {

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

} // namespace natsu

#endif // NATSU_MISC_VARIANT_HPP
