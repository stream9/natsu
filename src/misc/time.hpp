#ifndef NATSU_MISC_TIME_HPP
#define NATSU_MISC_TIME_HPP

#include <chrono>

namespace natsu {

using time_type = std::chrono::system_clock::time_point;
using duration_type = std::chrono::system_clock::duration;

} // namespace natsu

#endif // NATSU_MISC_TIME_HPP
