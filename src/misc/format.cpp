#include "format.hpp"

#include "global.hpp"

namespace natsu {

QString
formatBytes(int64_t const bytes, int const precision/*= 0*/)
{
    auto constexpr fieldWidth = 0;
    auto constexpr format = 'f';

    if (bytes < 1024) {
        return QSL("%1 bytes").arg(bytes);
    }

    auto const kibis = static_cast<double>(bytes) / 1024;
    if (kibis < 1024) {
        return QSL("%1 KiB").arg(kibis, fieldWidth, format, precision);
    }

    auto const mebis = static_cast<double>(kibis) / 1024;
    if (mebis < 1024) {
        return QSL("%1 MiB").arg(mebis, fieldWidth, format, precision);
    }

    auto const gibis = static_cast<double>(mebis) / 1024;
    if (gibis < 1024) {
        return QSL("%1 GiB").arg(gibis, fieldWidth, format, precision);
    }

    auto const tebis = static_cast<double>(gibis) / 1024;
    return QSL("%1 TiB").arg(tebis, fieldWidth, format, precision);
}

namespace format {

static QString
doFormat(QString const& fmt, double const number, int const precision)
{
    auto constexpr fieldWidth = 0;
    auto constexpr format = 'f';

    return fmt.arg(number, fieldWidth, format, precision);
}

static QString
bytes(int64_t const bytes,
      int const precision)
{
    return doFormat(QSL("%1"),
                    static_cast<double>(bytes),
                    precision);
}

static QString
kibiBytes(int64_t const bytes,
          int const precision,
          bool const unit = true)
{
    return doFormat(unit ? QSL("%1 KiB") : QSL("%1"),
                    static_cast<double>(bytes) / 1024,
                    precision);
}

static QString
mebiBytes(int64_t const bytes,
          int const precision,
          bool const unit = true)
{
    return doFormat(unit ? QSL("%1 MiB") : QSL("%1"),
                    static_cast<double>(bytes) / (1024 * 1024),
                    precision);
}

static QString
gibiBytes(int64_t const bytes,
          int const precision,
          bool const unit = true)
{
    return doFormat(unit ? QSL("%1 GiB") : QSL("%1"),
                    static_cast<double>(bytes) / (1024 * 1024 * 1024),
                    precision);
}

static QString
tebiBytes(int64_t const bytes,
          int const precision,
          bool const unit = true)
{
    return doFormat(unit ? QSL("%1 TiB") : QSL("%1"),
                    static_cast<double>(bytes) / (1024L * 1024 * 1024 * 1024),
                    precision);
}

QString
binaryBytes(int64_t const b,
            int const precision/*= 0*/,
            bool const unit/*= true*/)
{
    if (b < 1024)
        return bytes(b, precision);
    else if (b < 1024 * 1024)
        return kibiBytes(b, precision, unit);
    else if (b < 1024 * 1024 * 1024)
        return mebiBytes(b, precision, unit);
    else if (b < 1024L * 1024 * 1024 * 1024)
        return gibiBytes(b, precision, unit);
    else
        return tebiBytes(b, precision, unit);
}

QString
binaryBytes(int64_t const b,
            Prefix const prefix,
            int const precision/*= 0*/,
            bool const unit/*= true*/)
{
    switch (prefix) {
        case Prefix::Byte: return bytes(b, precision);
        case Prefix::Kilo: return kibiBytes(b, precision, unit);
        case Prefix::Mega: return mebiBytes(b, precision, unit);
        case Prefix::Giga: return gibiBytes(b, precision, unit);
        default: return tebiBytes(b, precision);
    }
}

std::pair<QString, Prefix>
binaryBytes2(int64_t const b,
             int const precision/*= 0*/,
             bool const unit/*= false*/)
{
    if (b < 1024)
        return { bytes(b, precision), Byte };
    else if (b < 1024 * 1024)
        return { kibiBytes(b, precision, unit), Kilo };
    else if (b < 1024 * 1024 * 1024)
        return { mebiBytes(b, precision, unit), Mega };
    else if (b < 1024L * 1024 * 1024 * 1024)
        return { gibiBytes(b, precision, unit), Giga };
    else
        return { tebiBytes(b, precision, unit), Tera };
}

static QString
kiloBytes(int64_t const bytes,
          int const precision)
{
    return doFormat(QSL("%1 KB"),
                    static_cast<double>(bytes) / 1000,
                    precision);
}

static QString
megaBytes(int64_t const bytes,
          int const precision)
{
    return doFormat(QSL("%1 MB"),
                    static_cast<double>(bytes) / (1000 * 1000),
                    precision);
}

static QString
gigaBytes(int64_t const bytes,
          int const precision)
{
    return doFormat(QSL("%1 GB"),
                    static_cast<double>(bytes) / (1000 * 1000 * 1000),
                    precision);
}

static QString
teraBytes(int64_t const bytes,
          int const precision)
{
    return doFormat(QSL("%1 TB"),
                    static_cast<double>(bytes) / (1000L * 1000 * 1000 * 1000),
                    precision);
}

QString
decimalBytes(int64_t const b,
             int const precision/*= 0*/)
{
    if (b < 1000)
        return bytes(b, precision);
    else if (b < 1000 * 1000)
        return kiloBytes(b, precision);
    else if (b < 1000 * 1000 * 1000)
        return megaBytes(b, precision);
    else if (b < 1000L * 1000 * 1000 * 1000)
        return gigaBytes(b, precision);
    else
        return teraBytes(b, precision);
}

QString
duration(duration_type const d)
{
    namespace sc = std::chrono;

    auto const ds = sc::duration_cast<sc::seconds>(d);
    auto const secs = ds.count() % 60;
    auto const mins = ds.count() % (60 * 60) / 60;
    auto const hours = ds.count() % (60 * 60 * 24) / (60 * 60);
    auto const days = ds.count() / (60 * 60 * 24);

    if (days > 0) {
        return QSL("%1 days").arg(days);
    }
    else if (hours > 0) {
        qDebug() << hours << mins << secs;
        return QSL("%1h%2m%3s").arg(hours).arg(mins).arg(secs);
    }
    else if (mins > 0) {
        return QSL("%1m%2s").arg(mins).arg(secs);
    }
    else {
        return QSL("%1s").arg(secs);
    }
}

} // namespace format

} // namespace natsu
