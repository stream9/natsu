#ifndef NATSU_POINTER_HPP
#define NATSU_POINTER_HPP

#include "global.hpp"

#include "qmanaged_ptr.hpp"
#include "non_null_ptr.hpp"

template<typename T>
T&
to_ref(T* const ptr)
{
    assert(ptr);
    return *ptr;
}

#endif // NATSU_POINTER_HPP
