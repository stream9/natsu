#ifndef NON_NULL_PTR_HPP
#define NON_NULL_PTR_HPP

#include "global.hpp"

#include <cstddef>

template<typename T>
class non_null_ptr
{
public:
    // constructors
    template<typename U>
    constexpr non_null_ptr(U* const ptr)
        : m_ptr { ptr }
    {
        assert(m_ptr);
    }

    constexpr non_null_ptr(nullptr_t) = delete;

    // assignment
    template<typename U>
    constexpr non_null_ptr& operator=(U* const ptr)
    {
        assert(ptr);
        m_ptr = ptr;
    }

    constexpr non_null_ptr& operator=(nullptr_t) = delete;

    // accessor
    constexpr T* get() const
    {
        return m_ptr;
    }

    // operator
    constexpr T* operator->() const
    {
        return get();
    }

    constexpr T& operator*() const
    {
        return *get();
    }

private:
    T* m_ptr;
};

template<typename T>
constexpr bool
operator==(non_null_ptr<T> const lhs, non_null_ptr<T> const rhs)
{
    return lhs.get() == rhs.get();
}

template<typename T>
constexpr bool
operator!=(non_null_ptr<T> const lhs, non_null_ptr<T> const rhs)
{
    return !(lhs == rhs);
}

template<typename T>
constexpr bool
operator<(non_null_ptr<T> const lhs, non_null_ptr<T> const rhs)
{
    return lhs.get() < rhs.get();
}

template<typename T>
constexpr bool
operator>(non_null_ptr<T> const lhs, non_null_ptr<T> const rhs)
{
    return rhs < lhs;
}

template<typename T>
constexpr bool
operator<=(non_null_ptr<T> const lhs, non_null_ptr<T> const rhs)
{
    return !(rhs < lhs);
}

template<typename T>
constexpr bool
operator>=(non_null_ptr<T> const lhs, non_null_ptr<T> const rhs)
{
    return !(lhs < rhs);
}

#endif // NON_NULL_PTR_HPP
