#ifndef NATSU_MISC_ELIDED_LABEL_HPP
#define NATSU_MISC_ELIDED_LABEL_HPP

#include <QLabel>

class QPaintEvent;
class QString;
class QWidget;

namespace natsu {

class ElidedLabel : public QLabel
{
public:
    ElidedLabel(QWidget* parent = nullptr,
                Qt::WindowFlags f = Qt::WindowFlags(),
                Qt::TextElideMode = Qt::ElideMiddle);

    ElidedLabel(QString const& text,
                QWidget* parent = nullptr,
                Qt::WindowFlags f = Qt::WindowFlags(),
                Qt::TextElideMode = Qt::ElideMiddle);

    ~ElidedLabel();

    // query
    Qt::TextElideMode elideMode() const { return m_elideMode; }

    // modifier
    void setElideMode(Qt::TextElideMode);

    // override
    void paintEvent(QPaintEvent*) override; // QWidget
    bool event(QEvent*) override; // QObject

private:
    Qt::TextElideMode m_elideMode;
};

} // namespace natsu

#endif // NATSU_MISC_ELIDED_LABEL_HPP
