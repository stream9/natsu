#include "url.hpp"

#include <QFileInfo>

namespace natsu {

QUrl
normalizeUrl(QUrl const& url)
{
    if (url.isEmpty()) return url;

    if (!url.isRelative()) {
        return url;
    }

    QFileInfo const file { url.toString() };
    if (!file.exists()) {
        return QUrl();
    }

    return QUrl::fromLocalFile(file.absoluteFilePath());
}

} // namespace natsu

