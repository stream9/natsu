#include "misc/time.hpp"

#include <cstdint>

#include <QString>

namespace natsu::format {

// size
enum Prefix { Byte, Kilo, Mega, Giga, Tera };

QString binaryBytes(int64_t bytes, int precision = 0, bool unit = true);
QString binaryBytes(int64_t bytes, Prefix, int precision = 0, bool unit = true);
std::pair<QString, Prefix>
    binaryBytes2(int64_t bytes, int precision = 0, bool unit = false);

QString decimalBytes(int64_t bytes, int precision = 0);

// time
QString duration(duration_type);

} // namespace natsu::format
