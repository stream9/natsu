#include "link_action.hpp"

#include "browser/browser_window.hpp"
#include "core/application.hpp"
#include "core/get.hpp"

#include <QIcon>
#include <QObject>
#include <QString>
#include <QUrl>

namespace natsu {

LinkAction::
LinkAction(QString const& title, QUrl const& url,
           QIcon const& icon,
           BrowserWindow& window, QObject& parent)
    : QAction(title, &parent)
    , m_browserWindow { window }
    , m_url { url }
{
    auto const& toolTip = title.isEmpty()
                ? url.toDisplayString()
                : QSL("%1\n%2").arg(title).arg(url.toDisplayString());
    this->setToolTip(toolTip);
    this->setIcon(icon);

    this->connect(this, &QAction::triggered,
                  this, &LinkAction::openUrl);
}

void LinkAction::
openUrl() const
{
    auto& app = get::application(m_browserWindow);

    if (app.lastButtons() & Qt::MiddleButton) {
        natsu::openUrlInNewTab(m_browserWindow, m_url);
    }
    else {
        natsu::openUrlInActiveTab(m_browserWindow, m_url);
    }
}

} // namespace natsu
