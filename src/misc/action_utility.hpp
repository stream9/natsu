#ifndef NATSU_ACTION_UTILITY_HPP
#define NATSU_ACTION_UTILITY_HPP

class QAction;
class QString;
class QWidget;

namespace natsu::action {

QAction* findAction(QWidget&, QString const& name);

} // namespace natsu

#endif // NATSU_ACTION_UTILITY_HPP
