#include "elided_label.hpp"

#include "pointer.hpp"

#include "global.hpp"

#include <QFontMetrics>
#include <QPaintEvent>
#include <QPainter>
#include <QSizePolicy>
#include <QString>

namespace natsu {

ElidedLabel::
ElidedLabel(QString const& text,
            QWidget* const parent/*= nullptr*/,
            Qt::WindowFlags const f/*= Qt::WindowFlags()*/,
            Qt::TextElideMode const elideMode/*= Qt::ElideMiddle*/)
    : QLabel { text, parent, f }
    , m_elideMode { elideMode }
{
    this->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
}

ElidedLabel::
ElidedLabel(QWidget* const parent/*= nullptr*/,
            Qt::WindowFlags const f/*= Qt::WindowFlags()*/,
            Qt::TextElideMode const elideMode/*= Qt::ElideMiddle*/)
    : QLabel { parent, f }
    , m_elideMode { elideMode }
{
    this->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
}

ElidedLabel::~ElidedLabel() = default;

void ElidedLabel::
setElideMode(Qt::TextElideMode const mode)
{
    m_elideMode = mode;
}

void ElidedLabel::
paintEvent(QPaintEvent* const e)
{
    auto& event = to_ref(e);

    auto const& fm = this->fontMetrics();
    auto const& rect = event.rect();
    auto const& text = fm.elidedText(
            this->text(),
            m_elideMode,
            rect.width(),
            Qt::TextShowMnemonic
        );

    QPainter p { this };
    p.drawText(
        0, 0,
        rect.width(), rect.height(),
        static_cast<int>(this->alignment()),
        text
    );
}

bool ElidedLabel::
event(QEvent* const e)
{
    if (e && e->type() == QEvent::ToolTip) {
        this->setToolTip(this->text());
    }

    return QLabel::event(e);
}

} // namespace natsu
