#ifndef INTEGER_HPP
#define INTEGER_HPP

template<typename T>
using non_negative = T;

#endif // INTEGER_HPP
