#ifndef ABSTRACT_WIDGET_LIST_HPP
#define ABSTRACT_WIDGET_LIST_HPP

#include "pointer.hpp"
#include "integer.hpp"

#include <QScrollArea>
#include <QVBoxLayout>

class QAbstractItemModel;
class QModelIndex;
class QPoint;
class QWidget;

class AbstractWidgetList : public QScrollArea
{
    Q_OBJECT
public:
    using index_type = non_negative<int>;

public:
    AbstractWidgetList(QWidget* parent = nullptr);
    ~AbstractWidgetList() override;

    // accessor
    QAbstractItemModel* model() const { return m_model; }

    // modifier
    void setModel(QAbstractItemModel*);

    // query
    void select(index_type);
    QModelIndex selection() const;
    void clearSelection();

    // signal
    Q_SIGNAL void selectionChanged(QModelIndex const&, QModelIndex const&);

    // @override QWidget
    void keyPressEvent(QKeyEvent*) override;
    void mousePressEvent(QMouseEvent*) override;

private:
    virtual QWidget& createItemWidget(QModelIndex const&) = 0;

    Q_SLOT void onRowsInserted(QModelIndex const& parent, int start, int end);
    Q_SLOT void onRowsRemoved(QModelIndex const& parent, int start, int end);
    Q_SLOT void onModelReset();

    QModelIndex index(QWidget&) const;

    void select(QWidget&);
    void deselect();

    void moveCursor(int amount);
    void clear();
    QWidget *findItem(QPoint const&) const;
    void insertWidget(index_type row);
    void removeWidget(index_type row);
    void adjustBackgroundRole(index_type start);
    void adjustBackgroundRole(QWidget&);

private:
    qmanaged_ptr<QVBoxLayout> m_layout;
    QAbstractItemModel *m_model = nullptr;
    QWidget *m_selected = nullptr;
};

#endif // ABSTRACT_WIDGET_LIST_HPP
