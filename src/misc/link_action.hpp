#ifndef NATSU_MISC_LINK_ACTION_HPP
#define NATSU_MISC_LINK_ACTION_HPP

#include <QAction>
#include <QUrl>

class QIcon;
class QObject;
class QString;

namespace natsu {

class BrowserWindow;

class LinkAction : public QAction
{
    Q_OBJECT
public:
    LinkAction(QString const& title, QUrl const& url,
               QIcon const&,
               BrowserWindow&, QObject& parent);

private:
    QUrl const& url() const { return m_url; }

    Q_SLOT void openUrl() const;

private:
    BrowserWindow& m_browserWindow;

    QUrl m_url;
};

} // namespace natsu

#endif // NATSU_MISC_LINK_ACTION_HPP
