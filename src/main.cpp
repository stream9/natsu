#include "browser/browser_window.hpp"
#include "core/application.hpp"
#include "core/command_line_option.hpp"
#include "mime/scheme_register.hpp"

#include <csignal>

#include <QApplication>
#include <QUrl>

int main(int argc, char *argv[])
{
    natsu::mime::SchemeRegister schemes;

    QApplication app { argc, argv };

    Q_INIT_RESOURCE(browser);

    natsu::Application natsu;

    if (natsu.commandLineOption().disableSegVHandler()) {
        std::signal(SIGSEGV, SIG_DFL);
        std::signal(SIGABRT, SIG_DFL);
    }

    auto& window = natsu.createWindow();

    for (auto const& url: natsu.commandLineOption().urls()) {
        natsu::openUrlInNewTab(window, QUrl(url));
    }

    return app.exec();
}
