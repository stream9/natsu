#include "application_view.hpp"

#include "application.hpp"
#include "application_model.hpp"
#include "database.hpp"

#include "misc/pointer.hpp"

#include <QString>
#include <QStyledItemDelegate>

namespace natsu::mime {

// ItemDelegate

class ApplicationView::ItemDelegate : public QStyledItemDelegate
{
public:
    ItemDelegate(ApplicationModel& model)
        : m_model { model }
    {}

    // override QStyledItemDelegate
    void paint(QPainter* const painter,
               QStyleOptionViewItem const& option,
               QModelIndex const& index) const override
    {
        if (m_model.type(index) == ApplicationModel::Category) {
            auto copy = option;
            copy.font.setBold(true);

            QStyledItemDelegate::paint(painter, copy, index);
        }
        else {
            QStyledItemDelegate::paint(painter, option, index);
        }
    }

private:
    ApplicationModel& m_model;
};

// ApplicationView

ApplicationView::
ApplicationView(QString const& mimeType, QWidget& parent)
    : QTreeView { &parent }
    , m_model { std::make_unique<ApplicationModel>(mimeType) }
    , m_delegate { std::make_unique<ItemDelegate>(*m_model) }
{
    this->setModel(m_model.get());
    this->expandAll();
    this->setHeaderHidden(true);
    this->setRootIsDecorated(false);
    this->setExpandsOnDoubleClick(false);
    this->setItemDelegate(m_delegate.get());

    auto& selection = to_ref(this->selectionModel());
    this->connect(&selection, &QItemSelectionModel::currentChanged,
                  this,       &ApplicationView::onSelected);

}

ApplicationView::~ApplicationView() = default;

Application ApplicationView::
currentApplication() const
{
    auto const& index = this->currentIndex();

    return m_model->application(index);
}

void ApplicationView::
showAll()
{
    m_model->showAll(true);
    this->expandAll();
}

void ApplicationView::
onSelected(QModelIndex const& index) const
{
    if (m_model->type(index) == mime::ApplicationModel::Category) {
        Q_EMIT selected({});
    }
    else {
        Q_EMIT selected(index);
    }
}

} // namespace natsu::mime
