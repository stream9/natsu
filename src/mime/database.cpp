#include "database.hpp"

#include "application.hpp"

#include "core/conv.hpp"
#include "core/error.hpp"
#include "core/icon.hpp"
#include "global.hpp"

#include <algorithm>
#include <filesystem>

#include <QIcon>
#include <QString>

#include <stream9/strings/join.hpp>
#include <stream9/strings/stream.hpp>

namespace natsu::mime {

namespace xdg = stream9::xdg;

static Application
makeApplication(xdg::desktop_entry const& e)
{
    //TODO cmdLine should be parsed elements
    auto const& cmdLine = str::join(e.exec_raw(), " ");
    auto const& icon = icon::forName(toQString(e.icon()));

    return {
        toQString(e.name()),
        toQString(cmdLine),
        toQString(e.path()),
        icon
    };
}

// Database

Database::
Database()
    try : m_desktopDb { m_mimeDb }
{}
catch (...) {
    rethrow_error();
}

Database::~Database() = default;

QString Database::
string(QString const& name) const
{
    auto const& oMimeType =
        m_mimeDb.find_mime_type_for_name(toString(name));
    if (!oMimeType) {
        return QSL("unknown");
    }
    else {
        auto const& comment = oMimeType->comment();
        return toQString(comment);
    }
}

QString Database::
filterString(QString const& name) const
{
    auto const& oMimeType =
        m_mimeDb.find_mime_type_for_name(toString(name));
    if (!oMimeType) {
        return QSL("All type (*.*)");
    }
    else {
        auto const& comment = oMimeType->comment();
        auto const& globs = oMimeType->glob_patterns();

        std::string filter;
        str::ostream ss { filter };
        ss << comment << "(" << str::views::join(globs, " ") << ")";

        return toQString(filter);

    }
}

QIcon Database::
icon(QString const& name) const
{
    auto const& oMimeType = m_mimeDb.find_mime_type_for_name(toString(name));
    if (oMimeType) {
        auto const& iconName = oMimeType->icon_name();
        if (!iconName.empty()) {
            return icon::forName(toQString(iconName));
        }

        auto const& genericName = oMimeType->generic_icon_name();
        if (!genericName.empty()) {
            return icon::forName(toQString(genericName));
        }
    }

    return {};
}

std::optional<Application> Database::
defaultApplication(QString const& name) const
{
    std::optional<Application> result;

    auto const& oDesktopEntry =
        m_desktopDb.find_default_application(toString(name));
    if (oDesktopEntry) {
        result = makeApplication(*oDesktopEntry);
    }

    return result;
}

std::vector<Application> Database::
applications(QString const& name) const
{
    std::vector<Application> result;

    auto const& entry_set = m_desktopDb.find_applications(toString(name));

    for (auto const& entry: entry_set) {
        result.push_back(makeApplication(entry));
    }

    return result;
}

std::vector<Application> Database::
allApplications() const
{
    std::vector<Application> result;

    auto const& entry_set = m_desktopDb.all_applications();

    for (auto const& entry: entry_set) {
        result.push_back(makeApplication(entry));
    }

    return result;
}

con::flat_set<QString> Database::
mimeTypes(QString const& prefix) const
{
    con::flat_set<QString> result;

    auto const& mimeTypes =
        m_desktopDb.associated_mime_types(toString(prefix));
    for (auto const& m: mimeTypes) {
        result.insert(toQString(m));
    }

    return result;
}

} // namespace natsu::mime
