#ifndef NATSU_MIME_SCHEME_HANDLER_HPP
#define NATSU_MIME_SCHEME_HANDLER_HPP

#include "core/fwd/profile.hpp"
#include "mime/fwd/application.hpp"
#include "mime/fwd/scheme_handler.hpp"

#include <QProcess>
#include <QWebEngineUrlSchemeHandler>

class QUrl;
class QWebEngineUrlRequestJob;

namespace natsu::mime {

class SchemeHandler : public QWebEngineUrlSchemeHandler
{
    Q_OBJECT
public:
    SchemeHandler(Profile&);
    ~SchemeHandler() override;

    // accessor
    Profile& profile() const { return m_profile; }

    // override QWebEngineUrlSchemeHandler
    void requestStarted(QWebEngineUrlRequestJob*) override;

private:
    void launch(mime::Application const&, QUrl const&);

    Q_SLOT void onProcessFinished(int exitCode, QProcess::ExitStatus);

private:
    Profile& m_profile;
};

} // namespace natsu::mime

#endif // NATSU_MIME_SCHEME_HANDLER_HPP
