#include "scheme_register.hpp"

#include "core/application.hpp"
#include "global.hpp"
#include "mime/database.hpp"
#include "mime/scheme.hpp"

#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

namespace natsu::mime {

static QJsonDocument
loadConfigFile()
{
    auto const& filePath = natsu::Application::configPath()
        + QDir::separator()
        + QSL("schemes.json");

    QFile file { filePath };
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        return {};
    }

    auto const& buf = file.readAll();

    return QJsonDocument::fromJson(buf);
}

static void
setSyntax(QWebEngineUrlScheme& scheme, QJsonObject const& obj)
{
    auto const& syntax = obj[QSL("syntax")].toString();

    if (syntax == QSL("host_port_and_user_information")) {
        scheme.setSyntax(QWebEngineUrlScheme::Syntax::HostPortAndUserInformation);
    }
    else if (syntax == QSL("host_and_port")) {
        scheme.setSyntax(QWebEngineUrlScheme::Syntax::HostAndPort);
    }
    else if (syntax == QSL("host")) {
        scheme.setSyntax(QWebEngineUrlScheme::Syntax::Host);
    }
    else if (syntax == QSL("path")) {
        scheme.setSyntax(QWebEngineUrlScheme::Syntax::Path);
    }
}

static void
setDefaultPort(QWebEngineUrlScheme& scheme, QJsonObject const& obj)
{
    auto const& port = obj[QSL("port")].toInt();

    scheme.setDefaultPort(port);
}

static void
setFlags(QWebEngineUrlScheme& scheme, QJsonObject const& obj)
{
    auto const& arr = obj[QSL("flags")].toArray();

    using Flag = QWebEngineUrlScheme::Flag;
    QWebEngineUrlScheme::Flags flags {};

    for (auto const& v: arr) {
        auto const& flag = v.toString();

        if (flag == QSL("secure_scheme")) {
            flags |= Flag::SecureScheme;
        }
        else if (flag == QSL("local_scheme")) {
            flags |= Flag::LocalScheme;
        }
        else if (flag == QSL("local_access_allowed")) {
            flags |= Flag::LocalAccessAllowed;
        }
        else if (flag == QSL("no_access_allowed")) {
            flags |= Flag::NoAccessAllowed;
        }
        else if (flag == QSL("service_workers_allowed")) {
            flags |= Flag::ServiceWorkersAllowed;
        }
        else if (flag == QSL("view_source_allowed")) {
            flags |= Flag::ViewSourceAllowed;
        }
        else if (flag == QSL("content_security_policy_ignored")) {
            flags |= Flag::ContentSecurityPolicyIgnored;
        }
        else if (flag == QSL("cors_enabled")) {
            flags |= Flag::CorsEnabled;
        }
    }

    scheme.setFlags(flags);
}

static bool
isAlreadyRegistered(QByteArray const& name)
{
    using Scheme = QWebEngineUrlScheme;

    return Scheme::schemeByName(name) != Scheme();
}

// SchemeRegister

SchemeRegister::
SchemeRegister()
{
    registerFromConfigFile();
    registerFromMimeDatabase();
}

SchemeRegister::~SchemeRegister() = default;

void SchemeRegister::
registerFromConfigFile()
{
    auto const& doc = loadConfigFile();
    auto const& obj = doc.object();

    for (auto const& name: obj.keys()) {
        QWebEngineUrlScheme scheme { name.toUtf8() };

        auto const& schemeObj = obj[name].toObject();
        setSyntax(scheme, schemeObj);
        setFlags(scheme, schemeObj);
        setDefaultPort(scheme, schemeObj);

        QWebEngineUrlScheme::registerScheme(scheme);
        m_schemes.push_back(std::move(scheme));
    }
}

void SchemeRegister::
registerFromMimeDatabase()
{
    using Scheme = QWebEngineUrlScheme;

    mime::Database db;
    auto const& prefix = schemeMimeTypePrefix();

    for (auto const& mimeType: db.mimeTypes(prefix)) {
        auto const& name = mimeType.mid(prefix.size()).toUtf8();

        if (isStandardScheme(name)) continue;
        if (isAlreadyRegistered(name)) continue;

        Scheme scheme { name };
        scheme.setFlags(Scheme::Flag::LocalScheme);

        Scheme::registerScheme(scheme);
        m_schemes.push_back(std::move(scheme));
    }
}

} // namespace natsu::mime
