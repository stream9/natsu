#include "application_model.hpp"

#include "application.hpp"
#include "database.hpp"

#include "global.hpp"

#include <boost/numeric/conversion/cast.hpp>

#include <QString>
#include <QModelIndex>

namespace natsu::mime {

ApplicationModel::
ApplicationModel(QString const& mimeType)
{
    setMimeType(mimeType);
}

ApplicationModel::~ApplicationModel() = default;

void ApplicationModel::
setMimeType(QString const& mimeType)
{
    this->beginResetModel();

    Database const mimeDb;
    m_recommended = std::move(mimeDb.applications(mimeType));

    this->endResetModel();
}

void ApplicationModel::
showAll(bool const flag)
{
    this->beginResetModel();

    if (flag) {
        Database const mimeDb;
        m_all = std::move(mimeDb.allApplications());
    }
    else {
        m_all.clear();
    }

    this->endResetModel();
}

ApplicationModel::Type ApplicationModel::
type(QModelIndex const& index) const
{
    return static_cast<Type>(index.internalId());
}

Application ApplicationModel::
application(QModelIndex const& index) const
{
    if (!index.isValid()) return {};

    auto const row = static_cast<size_t>(index.row());

    switch (type(index)) {
    case Recommended:
        return m_recommended.at(row);
    case All:
        return m_all.at(row);
    default:
        return {};
    }
}

QModelIndex ApplicationModel::
index(int const row, int const column, QModelIndex const& parent/*= {}*/) const
{
    assert(row >= 0);
    assert(column == 0);

    if (!parent.isValid()) {
        return this->createIndex(row, column, Category);
    }
    else {
        assert(type(parent) == Category);

        switch (parent.row()) {
        case 0:
            return this->createIndex(row, column, Recommended);
        case 1:
            return this->createIndex(row, column, All);
        default:
            return {};
        }
    }
}

QModelIndex ApplicationModel::
parent(QModelIndex const& index) const
{
    assert(index.isValid());

    switch (type(index)) {
    default:
    case Category:
        return {};
    case Recommended:
        return this->index(0, 0);
    case All:
        return this->index(1, 0);
    }
}

int ApplicationModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (!parent.isValid()) {
        return m_all.empty() ? 1 : 2;
    }

    if (type(parent) != Category) return 0;

    switch (parent.row()) {
    case 0:
        return boost::numeric_cast<int>(m_recommended.size());
    default:
    case 1:
        return boost::numeric_cast<int>(m_all.size());
    }
}

int ApplicationModel::
columnCount(QModelIndex const&/*parent = {}*/) const
{
    return 1;
}

QVariant ApplicationModel::
data(QModelIndex const& index, int const role/*= Qt::DisplayRole*/) const
{
    if (!index.isValid()) return {};

    switch (role) {
    case Qt::DisplayRole:
        return displayData(index);
    case Qt::DecorationRole:
        return iconData(index);
    default:
        return {};
    }
}

QVariant ApplicationModel::
displayData(QModelIndex const& index) const
{
    auto const row = static_cast<size_t>(index.row());

    switch (type(index)) {
    case Category:
        switch (row) {
        case 0:
            return QSL("Recommended Applications");
        case 1:
            return QSL("Other Applications");
        default:
            assert(false);
            return {};
        }
        break;
    case Recommended:
        return m_recommended.at(row).name;
    case All:
        return m_all.at(row).name;
    default:
        assert(false);
        return {};
    }
}

QVariant ApplicationModel::
iconData(QModelIndex const& index) const
{
    auto const row = static_cast<size_t>(index.row());

    switch (type(index)) {
    default:
    case Category:
        return {};
    case Recommended:
        return m_recommended.at(row).icon;
    case All:
        return m_all.at(row).icon;
    }
}

} // namespace natsu::mime
