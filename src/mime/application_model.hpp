#ifndef NATSU_MIME_APPLICATION_MODEL_HPP
#define NATSU_MIME_APPLICATION_MODEL_HPP

#include <vector>

#include <QAbstractItemModel>

class QString;
class QModelIndex;

namespace natsu::mime {

class Application;

class ApplicationModel : public QAbstractItemModel
{
public:
    enum Type { Category, Recommended, All };

public:
    ApplicationModel(QString const& mimeType);
    ~ApplicationModel();

    // modifier
    void setMimeType(QString const&);
    void showAll(bool flag);

    // query
    Type type(QModelIndex const&) const;
    Application application(QModelIndex const&) const;

    // override QAbstractItemModel
    QModelIndex index(int row, int column, QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const& index) const override;
    int rowCount(QModelIndex const& parent = {}) const override;
    int columnCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const& index, int role = Qt::DisplayRole) const override;

private:
    QVariant displayData(QModelIndex const&) const;
    QVariant iconData(QModelIndex const&) const;

private:
    std::vector<Application> m_recommended;
    std::vector<Application> m_all;
};

} // namespace natsu::mime

#endif // NATSU_MIME_APPLICATION_MODEL_HPP
