#ifndef NATSU_MIME_SCHEME_REGISTER_HPP
#define NATSU_MIME_SCHEME_REGISTER_HPP

#include <vector>

#include <QWebEngineUrlScheme>

namespace natsu::mime {

class SchemeRegister
{
public:
    SchemeRegister();
    ~SchemeRegister();

private:
    void registerFromConfigFile();
    void registerFromMimeDatabase();

private:
    std::vector<QWebEngineUrlScheme> m_schemes;
};

} // namespace natsu::mime

#endif // NATSU_MIME_SCHEME_REGISTER_HPP
