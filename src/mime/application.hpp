#ifndef NATSU_MIME_APPLICATION_HPP
#define NATSU_MIME_APPLICATION_HPP

#include <QString>
#include <QIcon>

namespace natsu::mime {

struct Application {
    QString name;
    QString commandLine;
    QString workingDir;
    QIcon icon;
};

QString expandCommandLine(QString const& commandLine, QString const& filePath);

} // namespace natsu::mime

#endif // NATSU_MIME_APPLICATION_HPP
