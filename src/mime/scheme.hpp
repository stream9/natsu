#ifndef NATSU_MIME_SCHEME_HPP
#define NATSU_MIME_SCHEME_HPP

#include <QByteArray>

namespace natsu::mime {

inline bool
isStandardScheme(QByteArray const& name)
{
    return name == "http" || name == "https" || name == "ftp";
}

inline QByteArray
schemeMimeTypePrefix()
{
    return QByteArrayLiteral("x-scheme-handler/");
}

} // namespace natsu::mime

#endif // NATSU_MIME_SCHEME_HPP
