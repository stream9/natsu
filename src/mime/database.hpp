#ifndef NATSU_MIME_DATABASE_HPP
#define NATSU_MIME_DATABASE_HPP

#include "core/fwd/profile.hpp"
#include "core/namespace.hpp"

#include <optional>
#include <vector>

#include <boost/container/flat_set.hpp>

#include <stream9/xdg/application/database.hpp>
#include <stream9/xdg/mime/database.hpp>

class QIcon;
class QString;

namespace natsu::mime {

class Application;

class Database
{
public:
    Database();
    ~Database();

    // query
    QString string(QString const& name) const;
    QString filterString(QString const& name) const;
    QIcon icon(QString const& name) const;
    std::optional<Application> defaultApplication(QString const& name) const;
    std::vector<Application> applications(QString const& name) const;
    std::vector<Application> allApplications() const;
    con::flat_set<QString> mimeTypes(QString const& prefix) const;

private:
    stream9::xdg::mime::database m_mimeDb;
    stream9::xdg::applications::database m_desktopDb;
};

} // namespace natsu::mime

#endif // NATSU_MIME_DATABASE_HPP
