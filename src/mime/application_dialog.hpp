#ifndef NATSU_MIME_APPLICATION_DIALOG_HPP
#define NATSU_MIME_APPLICATION_DIALOG_HPP

#include <misc/pointer.hpp>

#include <QDialog>

class QDialogButtonBox;
class QPushButton;
class QString;

namespace natsu::mime {

class Application;
class ApplicationView;
class Database;

class ApplicationDialog : public QDialog
{
    Q_OBJECT
public:
    ApplicationDialog(QString const& mimeType, QWidget& parent);
    ~ApplicationDialog();

    // query
    Application selected() const;

private:
    Q_SLOT void showAll();
    Q_SLOT void onApplicationSelected(QModelIndex const&);

private:
    qmanaged_ptr<ApplicationView> m_applications;
    qmanaged_ptr<QPushButton> m_showAll;
    qmanaged_ptr<QDialogButtonBox> m_buttons;
};

} // namespace natsu::mime

#endif // NATSU_MIME_APPLICATION_DIALOG_HPP
