#include "scheme_handler.hpp"

#include "core/profile.hpp"
#include "global.hpp"
#include "mime/application.hpp"
#include "mime/database.hpp"
#include "mime/scheme.hpp"
#include "misc/pointer.hpp"

#include <QApplication>
#include <QMessageBox>
#include <QUrl>
#include <QWebEngineUrlRequestJob>
#include <QWebEngineUrlScheme>

namespace natsu::mime {

// SchemeHandler

SchemeHandler::
SchemeHandler(Profile& profile)
    : QWebEngineUrlSchemeHandler { &profile }
    , m_profile { profile }
{
    mime::Database db;

    auto const& prefix = schemeMimeTypePrefix();
    for (auto const& mimeType: db.mimeTypes(prefix)) {
        auto const& scheme = mimeType.mid(prefix.size()).toUtf8();
        if (isStandardScheme(scheme)) continue;

        profile.installUrlSchemeHandler(scheme, this);
    }
}

SchemeHandler::~SchemeHandler() = default;

void SchemeHandler::
requestStarted(QWebEngineUrlRequestJob* const request)
{
    assert(request);

    auto const& url = request->requestUrl();
    auto const& mimeType = schemeMimeTypePrefix() + url.scheme();

    mime::Database db;
    auto const& app = db.defaultApplication(mimeType);
    if (app) {
        launch(*app, url);
        request->fail(QWebEngineUrlRequestJob::NoError);
    }
    else {
        request->fail(QWebEngineUrlRequestJob::RequestFailed);
    }
}

void SchemeHandler::
launch(mime::Application const& app, QUrl const& url)
{
    auto process = make_qmanaged<QProcess>(this);

    process->setWorkingDirectory(app.workingDir);

    auto commandLine = app.commandLine;
    commandLine.replace(QSL("%f"), url.toEncoded()); //TODO should it be %u ?

    auto args = commandLine.split(' ');
    auto const& program = args.takeFirst();

    assert(!program.isEmpty());

    auto const finished =
                qOverload<int, QProcess::ExitStatus>(&QProcess::finished);
    this->connect(process.get(), finished,
                  this,          &SchemeHandler::onProcessFinished);

    process->start(program, args);
}

void SchemeHandler::
onProcessFinished(int const exitCode, QProcess::ExitStatus const exitStatus)
{
    if (exitCode != 0) {
        auto* const window = qApp->activeWindow();

        QMessageBox::critical(
            window,
            QSL("Scheme Handler"),
            QSL("External program exited abnormally."
                " exit code = %1, exit status = %2")
                                        .arg(exitCode).arg(exitStatus)
        );
    }

    auto& process = to_ref(dynamic_cast<QProcess*>(this->sender()));
    process.deleteLater();
}

} // namespace natsu::mime
