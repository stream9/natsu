#ifndef NATSU_MIME_APPLICATION_VIEW_HPP
#define NATSU_MIME_APPLICATION_VIEW_HPP

#include <memory>

#include <QTreeView>

class QString;
class QWidget;

namespace natsu::mime {

class Application;
class ApplicationModel;

class ApplicationView : public QTreeView
{
    Q_OBJECT
public:
    ApplicationView(QString const& mimeType, QWidget& parent);
    ~ApplicationView();

    // query
    Application currentApplication() const;

    // command
    void showAll();

    // signal
    Q_SIGNAL void selected(QModelIndex const&) const;

private:
    Q_SLOT void onSelected(QModelIndex const&) const;

private:
    std::unique_ptr<ApplicationModel> m_model; // non-null

    class ItemDelegate;
    std::unique_ptr<ItemDelegate> m_delegate; // non-null
};

} // namespace natsu::mime

#endif // NATSU_MIME_APPLICATION_VIEW_HPP
