#include "application.hpp"

#include "global.hpp"

#include <QUrl>

namespace natsu::mime {

static QString
unquote(QString const& s)
{
    auto result = s;

    result.replace(QSL("\\\""), QSL("\""));
    result.replace(QSL("\\`"), QSL("`"));
    result.replace(QSL("\\$"), QSL("$"));
    result.replace(QSL("\\\\"), QSL("\\"));

    return result;
}

QString
expandCommandLine(QString const& commandLine, QString const& filePath)
{
    auto line = unquote(commandLine);

    line.replace(QSL("%f"), filePath);
    line.replace(QSL("%F"), filePath);

    auto const url = QUrl::fromLocalFile(filePath).toEncoded();
    line.replace(QSL("%u"), url);
    line.replace(QSL("%U"), url);

    return line;
}

} // namespace natsu::mime
