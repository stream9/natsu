#include "application_dialog.hpp"

#include "application.hpp"
#include "application_view.hpp"

#include "global.hpp"

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QPushButton>
#include <QString>

namespace natsu::mime {

// ApplicationDialog

ApplicationDialog::
ApplicationDialog(QString const& mimeType, QWidget& parent)
    : QDialog { &parent }
    , m_applications { make_qmanaged<ApplicationView>(mimeType, *this) }
    , m_showAll { make_qmanaged<QPushButton>(this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    auto label = make_qmanaged<QLabel>(QSL("Choose Helper Application"), this);
    label->setAlignment(Qt::AlignCenter);
    layout->addWidget(label.get());

    layout->addWidget(m_applications.get());

    m_showAll->setText(QSL("View All Applications"));
    layout->addWidget(m_showAll.get());

    m_buttons->addButton(QDialogButtonBox::Ok)->setEnabled(false);
    m_buttons->addButton(QDialogButtonBox::Cancel);
    layout->addWidget(m_buttons.get());

    this->connect(m_applications.get(), &ApplicationView::selected,
                  this,                 &ApplicationDialog::onApplicationSelected);
    this->connect(m_showAll.get(), &QPushButton::clicked,
                  this,            &ApplicationDialog::showAll);
    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &QDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &QDialog::reject);

    this->setWindowTitle(QSL("Selecting Application"));
    this->resize(400, 300);
}

ApplicationDialog::~ApplicationDialog() = default;

Application ApplicationDialog::
selected() const
{
    return m_applications->currentApplication();
}

void ApplicationDialog::
showAll()
{
    m_applications->showAll();
    m_showAll->hide();
}

void ApplicationDialog::
onApplicationSelected(QModelIndex const& index)
{
    auto& button = to_ref(m_buttons->button(QDialogButtonBox::Ok));
    button.setEnabled(index.isValid());
}

} // namespace natsu::mime
