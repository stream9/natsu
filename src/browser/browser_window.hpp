#ifndef NATSU_BROWSER_BROWSER_WINDOW_HPP
#define NATSU_BROWSER_BROWSER_WINDOW_HPP

#include "misc/pointer.hpp"

#include <memory>
#include <vector>

#include <QMainWindow>

class QCloseEvent;

namespace natsu {

class Application;
class BrowserTab;
class Profile;
class TabBrowser;

namespace browser_window { class MenuBar; }

class BrowserWindow : public QMainWindow
{
    Q_OBJECT
public:
    BrowserWindow(Application&, Profile&);
    ~BrowserWindow() override;

    // accessor
    Application& application() const { return m_application; }
    Profile& profile() const { return m_profile; }
    TabBrowser& tabBrowser() const;
    browser_window::MenuBar& menuBar();

    // modifier
    void setTitle(QString const& title);

    // signal
    Q_SIGNAL void tabCreated(BrowserTab&);

protected:
    // override QWidget
    void closeEvent(QCloseEvent*) override;
    void changeEvent(QEvent*) override;

private:
    void loadSettings();
    void saveSettings();

private:
    Application& m_application;
    Profile& m_profile;

    qmanaged_ptr<TabBrowser> const m_tabBrowser;
};

void openUrlInActiveTab(BrowserWindow&, QUrl const&);
void openUrlInNewTab(BrowserWindow&, QUrl const&);

} // namespace natsu

#endif // NATSU_BROWSER_BROWSER_WINDOW_HPP
