#include "actions.hpp"

#include "../browser_tab.hpp"
#include "../browser_window.hpp"
#include "../navigation_bar.hpp"
#include "../tab_browser.hpp"
#include "../web_page.hpp"

#include "core/action_manager.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"
#include "global.hpp"

#include <QClipboard>
#include <QCursor>
#include <QGuiApplication>
#include <QTabBar>

namespace natsu::tab_browser {

Actions::
Actions(TabBrowser& tabBrowser)
    : m_tabBrowser { tabBrowser }
{
    this->connect(&m_tabBrowser, &TabBrowser::currentChanged,
                  this,          &Actions::onCurrentChanged);

    auto& manager = get::actionManager(m_tabBrowser);
    auto fn = [&](auto& action,
                  auto const& name,
                  auto const& text,
                  auto const& icon,
                  auto const slot,
                  QKeySequence const& shortcut)
    {
        action.setObjectName(name);
        action.setText(text);
        action.setIcon(icon);
        action.setShortcut(shortcut);
        this->connect(&action, &QAction::triggered, this, slot);

        manager.configure(QSL("TabBrowser"), action, shortcut);

        m_tabBrowser.addAction(&action);
    };

    fn(m_newTab, QSL("tab.new_tab"), QSL("&New Tab"), icon::newTab(), &Actions::newTab, makeKeyCode(Qt::CTRL, Qt::Key_T));
    fn(m_close, QSL("tab.close"), QSL("&Close Tab"), icon::closeTab(), &Actions::close, makeKeyCode(Qt::CTRL, Qt::Key_W));
    fn(m_duplicate, QSL("tab.duplicate"), QSL("&Duplicate Tab"), QIcon(), &Actions::duplicate, makeKeyCode(Qt::CTRL, Qt::Key_U));
    fn(m_moveToNewWindow, QSL("tab.move_to_new_window"), QSL("Move To New &Window"), QIcon(), &Actions::moveToNewWindow, QKeySequence());
    fn(m_closeOthers, QSL("tab.close_others"), QSL("Cl&ose Other Tabs"), QIcon(), &Actions::closeOthers, QKeySequence());
    fn(m_closeToRight, QSL("tab.close_to_right"), QSL("Close Tabs to the R&ight"), QIcon(), &Actions::closeToRight, QKeySequence());
    fn(m_mute, QSL("tab.mute"), QSL("&Mute Tab"), QIcon(), &Actions::mute, QKeySequence());
    fn(m_nextTab, QSL("tab.next"), QSL("Next Tab"), QIcon(), &Actions::nextTab, makeKeyCode(Qt::CTRL, Qt::Key_PageDown));
    fn(m_prevTab, QSL("tab.previous"), QSL("Previous Tab"), QIcon(), &Actions::prevTab, makeKeyCode(Qt::CTRL, Qt::Key_PageUp));
    fn(m_copyTitle, QSL("tab.copy_title"), QSL("Copy Title"), icon::copy(), &Actions::copyTitle, QKeySequence());

    setTarget(-1);
}

Actions::~Actions() = default;

void Actions::
setTarget(int const target)
{
    m_target = target;

    if (m_target < 0) return;

    auto const onlyOneTab = m_tabBrowser.count() == 1;

    m_closeOthers.setEnabled(!onlyOneTab);
    m_moveToNewWindow.setEnabled(!onlyOneTab);

    auto const rightMostTab = m_target == m_tabBrowser.count() - 1;

    m_closeToRight.setEnabled(!rightMostTab);

    auto& tab = m_tabBrowser.tabAt(m_target);
    auto& page = get::webPage(tab);

    if (page.isAudioMuted()) {
        m_mute.setText(QSL("Un&mute Tab"));
    }
    else {
        m_mute.setText(QSL("&Mute Tab"));
    }
}

void Actions::
onCurrentChanged(int const index)
{
    setTarget(index);
}

void Actions::
newTab()
{
    auto& tab = m_tabBrowser.newTab();
    tab.navigationBar().setFocus();
}

void Actions::
close() const
{
    assert(m_target >= 0);
    m_tabBrowser.closeTab(m_tabBrowser.tabAt(m_target));
}

void Actions::
duplicate() const
{
    assert(m_target >= 0);

    auto& tab = m_tabBrowser.tabAt(m_target);
    auto& page = get::webPage(tab);
    auto& browser = get::tabBrowser(tab);

    auto& newTab = browser.newTab();
    newTab.load(page.url());
    //TODO scroll position
}

void Actions::
moveToNewWindow() const
{
    assert(m_target >= 0);
    auto& tab = m_tabBrowser.tabAt(m_target);
    auto& page = get::webPage(tab);
    auto& app = get::application(tab);

    auto& newWin = app.createWindow();
    auto& newTab = get::activeTab(newWin);
    newTab.load(page.url());
    //TODO scroll position

    m_tabBrowser.closeTab(tab);
}

void Actions::
closeOthers() const
{
    assert(m_target >= 0);
    auto const count = m_tabBrowser.count();

    for (auto i = count - 1; i >= 0; --i) {
        if (i == m_target) continue;

        m_tabBrowser.closeTab(m_tabBrowser.tabAt(i));
    }
}

void Actions::
closeToRight() const
{
    assert(m_target >= 0);
    auto const count = m_tabBrowser.count();

    for (auto i = count - 1; i > m_target; --i) {
        m_tabBrowser.closeTab(m_tabBrowser.tabAt(i));
    }
}

void Actions::
mute() const
{
    auto& tab = m_tabBrowser.tabAt(m_target);
    auto& page = get::webPage(tab);

    page.setAudioMuted(!page.isAudioMuted());
}

void Actions::
nextTab() const
{
    auto const next = m_tabBrowser.currentIndex() + 1;
    m_tabBrowser.setCurrentIndex(next == m_tabBrowser.count() ? 0 : next);
}

void Actions::
prevTab() const
{
    auto const next = m_tabBrowser.currentIndex() - 1;
    m_tabBrowser.setCurrentIndex(next < 0 ? m_tabBrowser.count() - 1 : next);
}

void Actions::
copyTitle() const
{
    assert(m_target >= 0);

    auto& tab = m_tabBrowser.tabAt(m_target);
    auto& page = get::webPage(tab);

    auto& clipboard = to_ref(QGuiApplication::clipboard());

    clipboard.setText(page.title());
}

} // namespace natsu::tab_browser
