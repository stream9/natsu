#include "context_menu.hpp"

#include "actions.hpp"
#include "browser/web_view.hpp"
#include "browser/web_view/actions.hpp"
#include "browser/tab_browser.hpp"

#include "core/get.hpp"

namespace natsu::tab_browser {

ContextMenu::
ContextMenu(TabBrowser& browser, BrowserTab& tab)
{
    auto& actions = browser.actions();

    auto& view = get::webView(tab);

    this->addAction(&actions.newTabAction());
    this->addAction(&view.actions().reloadAction());
    this->addAction(&actions.muteAction());
    this->addSeparator();
    this->addAction(&actions.copyTitleAction());
    this->addSeparator();
    this->addAction(&actions.duplicateAction());
    this->addAction(&actions.moveToNewWindowAction());
    this->addSeparator();
    this->addAction(&actions.closeOthersAction());
    this->addAction(&actions.closeToRightAction());
    this->addSeparator();
    this->addAction(&actions.closeAction());
}

ContextMenu::~ContextMenu() = default;

} // namespace natsu::tab_browser
