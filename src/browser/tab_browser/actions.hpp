#ifndef NATSU_BROWSER_TAB_BROWSER_ACTIONS_HPP
#define NATSU_BROWSER_TAB_BROWSER_ACTIONS_HPP

#include "../fwd/tab_browser.hpp"
#include "misc/integer.hpp"

#include <QAction>
#include <QObject>

namespace natsu::tab_browser {

class Actions : public QObject
{
    Q_OBJECT
public:
    Actions(TabBrowser&);
    ~Actions() override;

    // accessor
    TabBrowser& tabBrowser() const { return m_tabBrowser; }

    QAction& newTabAction() { return m_newTab; }
    QAction& closeAction() { return m_close; }
    QAction& duplicateAction() { return m_duplicate; }
    QAction& moveToNewWindowAction() { return m_moveToNewWindow; }
    QAction& closeOthersAction() { return m_closeOthers; }
    QAction& closeToRightAction() { return m_closeToRight; }
    QAction& muteAction() { return m_mute; }
    QAction& nextTabAction() { return m_nextTab; }
    QAction& prevTabAction() { return m_prevTab; }
    QAction& copyTitleAction() { return m_copyTitle; }

    // modifier
    void setTarget(int index);

private:
    Q_SLOT void onCurrentChanged(int index);

    Q_SLOT void newTab();
    Q_SLOT void close() const;
    Q_SLOT void duplicate() const;
    Q_SLOT void moveToNewWindow() const;
    Q_SLOT void closeOthers() const;
    Q_SLOT void closeToRight() const;
    Q_SLOT void mute() const;
    Q_SLOT void nextTab() const;
    Q_SLOT void prevTab() const;
    Q_SLOT void copyTitle() const;

private:
    TabBrowser& m_tabBrowser;

    QAction m_newTab;
    QAction m_close;
    QAction m_duplicate;
    QAction m_moveToNewWindow;
    QAction m_closeOthers;
    QAction m_closeToRight;
    QAction m_mute;
    QAction m_nextTab;
    QAction m_prevTab;
    QAction m_copyTitle;

    int m_target = -1;
};

} // namespace natsu::tab_browser

#endif // NATSU_BROWSER_TAB_BROWSER_ACTIONS_HPP
