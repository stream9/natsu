#ifndef NATSU_BROWSER_TAB_BROWSER_CONTEXT_MENU_HPP
#define NATSU_BROWSER_TAB_BROWSER_CONTEXT_MENU_HPP

#include <QMenu>

namespace natsu {

class BrowserTab;
class TabBrowser;

} // namespace natsu

namespace natsu::tab_browser {

class ContextMenu : public QMenu
{
public:
    ContextMenu(TabBrowser&, BrowserTab&);
    ~ContextMenu() override;
};

} // namespace natsu::tab_browser

#endif // NATSU_BROWSER_TAB_BROWSER_CONTEXT_MENU_HPP
