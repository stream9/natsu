#ifndef NATSU_BROWSER_TAB_MANAGER_HPP
#define NATSU_BROWSER_TAB_MANAGER_HPP

namespace natsu {

class BrowserTab;
class TabBrowser;

class TabManager
{
public:
    TabManager(TabBrowser&);
    virtual ~TabManager();

    // accessor
    TabBrowser& tabBrowser() const { return m_tabBrowser; }

    // command
    //! @detail emit Profile::tabCreated
    BrowserTab& newTab();
    BrowserTab& newTab(int index);

    void closeTab(BrowserTab& tab) { doCloseTab(tab); }

private:
    virtual BrowserTab& doNewTab();
    virtual BrowserTab& doNewTab(int index);

    virtual void doCloseTab(BrowserTab&);

private:
    TabBrowser& m_tabBrowser;
};

} // namespace natsu

#endif // NATSU_BROWSER_TAB_MANAGER_HPP
