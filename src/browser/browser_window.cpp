#include "browser_window.hpp"

#include "browser_window/menu_bar.hpp"

#include "browser_tab.hpp"
#include "tab_browser.hpp"

#include "core/action_manager.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"
#include "misc/url.hpp"

#include <QEvent>
#include <QSettings>
#include <QUrl>

namespace natsu {

BrowserWindow::
BrowserWindow(Application& app, Profile& profile)
    : m_application { app }
    , m_profile { profile }
    , m_tabBrowser { make_qmanaged<TabBrowser>(*this) }
{
    this->setAttribute(Qt::WA_DeleteOnClose, true);

    this->setCentralWidget(m_tabBrowser.get());

    auto menuBar = make_qmanaged<browser_window::MenuBar>(*this);
    this->setMenuBar(menuBar.get());

    this->resize(800, 600);
    loadSettings();

    get::actionManager(*this).installTo(*this);

    Q_EMIT profile.windowCreated(*this);

    m_tabBrowser->newTab(); // this emits tabCreated
}

BrowserWindow::~BrowserWindow() = default;

TabBrowser& BrowserWindow::
tabBrowser() const
{
    return *m_tabBrowser;
}

browser_window::MenuBar& BrowserWindow::
menuBar()
{
    return *dynamic_cast<browser_window::MenuBar*>(QMainWindow::menuBar());
}

void BrowserWindow::
setTitle(QString const& title)
{
    this->setWindowTitle(QSL("natsu - %1").arg(title));
}

void BrowserWindow::
closeEvent(QCloseEvent*)
{
    saveSettings();
}

void BrowserWindow::
changeEvent(QEvent* const ev)
{
    auto& event = to_ref(ev);

    if (event.type() == QEvent::ActivationChange) {
        if (this->isActiveWindow()) {
            m_application.setActiveWindow(*this);
        }
    }
}

void BrowserWindow::
loadSettings()
{
    auto& settings = m_profile.settings();
    settings.beginGroup(QSL("BrowserWindow"));
    this->restoreGeometry(settings.value(QSL("geometry")).toByteArray());
    this->restoreState(settings.value(QSL("windowState")).toByteArray());
    settings.endGroup();
}

void BrowserWindow::
saveSettings()
{
    auto& settings = m_profile.settings();
    settings.beginGroup(QSL("BrowserWindow"));
    settings.setValue(QSL("geometry"), this->saveGeometry());
    settings.setValue(QSL("windowState"), this->saveState());
    settings.endGroup();
}

/* free function */
void
openUrlInActiveTab(BrowserWindow& window, QUrl const& url_)
{
    auto const& url = normalizeUrl(url_);

    if (url.isEmpty()) {
        qWarning() << "invalid URL:" << url_.toDisplayString();
        return;
    }

    auto& tab = get::activeTab(window);

    tab.load(url);
}

void
openUrlInNewTab(BrowserWindow& window, QUrl const& url_)
{
    auto const& url = normalizeUrl(url_);

    if (url.isEmpty()) {
        qWarning() << "invalid URL:" << url_.toDisplayString();
        return;
    }

    auto& activeTab = get::activeTab(window);
    if (activeTab.url().isEmpty()) {
        activeTab.load(url);
    }
    else {
        auto& browser = get::tabBrowser(window);
        auto& tab = browser.newTab();

        tab.load(url);
    }
}

} // namespace natsu
