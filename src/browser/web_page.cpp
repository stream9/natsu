#include "web_page.hpp"

#include "browser_tab.hpp"
#include "browser_window.hpp"
#include "dialog_web_view.hpp"
#include "tab_browser.hpp"
#include "web_dialog.hpp"
#include "web_view.hpp"

#include "core/application.hpp"
#include "core/get.hpp"
#include "core/history_manager.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <algorithm>
#include <cmath>

#include <QIcon>
#include <QUrl>
#include <QWebEngineNewWindowRequest>

namespace natsu {

// WebPage

WebPage::
WebPage(WebView& view)
    : QWebEnginePage(&get::profile(view), &view)
{
    this->connect(this, &QWebEnginePage::loadFinished,
                  this, &WebPage::onLoadFinished);
    this->connect(this, &QWebEnginePage::iconChanged,
                  this, &WebPage::onIconChanged);
    this->connect(this, &QWebEnginePage::linkHovered,
                  this, &WebPage::onLinkHovered);
    this->connect(this, &QWebEnginePage::urlChanged,
                  this, &WebPage::onUrlChanged);
    this->connect(this, &QWebEnginePage::newWindowRequested,
                  this, &WebPage::onNewWindowRequested);
}

WebPage::~WebPage() = default;

WebView& WebPage::
webView() const
{
    return to_ref(static_cast<WebView*>(this->parent()));
}

bool WebPage::
isBlankPage() const
{
    return this->url().isEmpty();
}

void WebPage::
reload()
{
    this->triggerAction(WebAction::Reload);
}

bool WebPage::
acceptNavigationRequest(QUrl const& url, WebPage::NavigationType type,
                        bool isMainFrame)
{
    auto accepted =
        QWebEnginePage::acceptNavigationRequest(url, type, isMainFrame);

    if (accepted) {
        Q_EMIT navigationAccepted(url);
    }

    auto& app = get::application(*this);
    if (app.verbose()) {
        qDebug()
            << (accepted ? "[Navigation Accepted]" : "[Navigation Blocked]")
            << url << type
            << (isMainFrame ? "MainFrame" : "");
    }

    return accepted;
}

void WebPage::
javaScriptConsoleMessage(JavaScriptConsoleMessageLevel,
                         QString const&/*message*/,
                         int/*lineNumber*/, QString const& /*sourceID*/)
{
    // Nop
}

void WebPage::
onLoadFinished(bool const ok)
{
    if (!ok) return;

    auto& history = get::history(*this);

    history.addEntry(this->url(), this->title());
}

void WebPage::
onIconChanged(QIcon const& icon)
{
    auto& history = get::history(*this);

    history.addIcon(this->url(), icon);
}

void WebPage::
onLinkHovered(QString const& url)
{
    auto& view = this->webView();

    view.setStatusMessage(url);
}

void WebPage::
onUrlChanged(QUrl const& url)
{
    auto& app = get::application(*this);

    if (app.verbose()) {
        qDebug() << this << "[URL Changed]" << url;
    }
}

void WebPage::
onNewWindowRequested(QWebEngineNewWindowRequest& req) noexcept
{
    auto verbose = get::application(*this).verbose();
    auto url = req.requestedUrl();

    bool handled = false;
    Q_EMIT newWindowRequested(req, handled);
    if (handled) {
        return;
    }

    if (verbose) {
        qInfo() << "[New Window Request Accepted]" << req.destination()
            << url
            << req.requestedGeometry()
            << (req.isUserInitiated() ? "User Initiated" : "");
    }

    switch (req.destination()) {
        using enum QWebEngineNewWindowRequest::DestinationType;
        default:
        case InNewTab: {
            auto& browser = get::tabBrowser(*this);
            auto& tab = browser.newTab();

            req.openIn(tab.webView().page());
            break;
        }
        case InNewBackgroundTab: {
            auto& browser = get::tabBrowser(*this);
            auto& tab = browser.newBackgroundTab();

            req.openIn(tab.webView().page());
            break;
        }
        case InNewDialog: {
            auto& application = get::application(*this);
            auto& dialog = application.createWebDialog(get::browserTab(*this));

            req.openIn(dialog.webView().page());
            break;
        }
        case InNewWindow: {
            auto& application = get::application(*this);
            auto& window = application.createWindow();
            auto& tab = get::activeTab(window);

            req.openIn(tab.webView().page());
            break;
        }
    }
}

} // namespace natsu
