#include "tab_browser.hpp"

#include "tab_browser/actions.hpp"
#include "tab_browser/context_menu.hpp"

#include "browser_tab.hpp"
#include "browser_window.hpp"
#include "tab_manager.hpp"

#include "core/get.hpp"
#include "core/icon.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QIcon>
#include <QMenu>
#include <QString>
#include <QTabBar>
#include <QTimer>

namespace natsu {

// TabBrowser

TabBrowser::
TabBrowser(BrowserWindow& window)
    : m_window { window }
    , m_actions { std::make_unique<tab_browser::Actions>(*this) }
{
    this->connect(this, &QTabWidget::tabCloseRequested,
                  this, &TabBrowser::onTabCloseRequested);
    this->connect(this, &QTabWidget::currentChanged,
                  this, &TabBrowser::onCurrentChanged);

    auto& tabBar = to_ref(this->tabBar());
    tabBar.setContextMenuPolicy(Qt::CustomContextMenu);
    this->connect(&tabBar, &QTabBar::customContextMenuRequested,
                  this,    &TabBrowser::onContextMenuRequested);

    this->setDocumentMode(true);
    this->setTabsClosable(true);
    this->setMovable(true);
    this->setElideMode(Qt::ElideRight);
    this->tabBar()->setSelectionBehaviorOnRemove(QTabBar::SelectLeftTab);
}

TabBrowser::~TabBrowser() = default;

TabManager& TabBrowser::
tabManager()
{
    if (!m_manager) {
        m_manager = std::make_unique<TabManager>(*this);
    }

    return *m_manager;
}

tab_browser::Actions& TabBrowser::
actions() const
{
    return *m_actions;
}

BrowserTab& TabBrowser::
currentTab() const
{
    auto& tab = to_ref(dynamic_cast<BrowserTab*>(this->currentWidget()));

    return tab;
}

BrowserTab& TabBrowser::
tabAt(non_negative<int> const index) const
{
    auto& tab = to_ref(dynamic_cast<BrowserTab*>(this->widget(index)));

    return tab;
}

std::vector<non_null_ptr<BrowserTab>> TabBrowser::
tabs() const
{
    std::vector<non_null_ptr<BrowserTab>> result;

    for (auto i = 0, count = this->count(); i < count; ++i) {
        result.emplace_back(&tabAt(i));
    }

    return result;
}

void TabBrowser::
setCurrentTab(BrowserTab& tab)
{
    this->setCurrentWidget(&tab);
}

BrowserTab& TabBrowser::
newTab()
{
    auto& tab = newBackgroundTab();
    setCurrentTab(tab);

    return tab;
}

BrowserTab& TabBrowser::
newBackgroundTab()
{
    auto& tab = tabManager().newTab();
    setIcon(tab, QIcon());

    return tab;
}

void TabBrowser::
closeTab(BrowserTab& tab)
{
    tabManager().closeTab(tab);
}

void TabBrowser::
setTitle(BrowserTab& tab, QString const& title)
{
    auto const index = this->indexOf(&tab);
    if (index == -1) return;

    this->setTabText(index, title);

    if (index == this->currentIndex()) {
        m_window.setTitle(title);
    }
}

void TabBrowser::
setIcon(BrowserTab& tab, QIcon const& icon)
{
    auto const index = this->indexOf(&tab);
    if (index == -1) return;

    if (icon.isNull()) {
        this->setTabIcon(index, icon::defaultFavicon());
    }
    else {
        this->setTabIcon(index, icon);
    }
}

void TabBrowser::
setTabManager(std::unique_ptr<TabManager>&& manager)
{
    m_manager = std::move(manager);
}

void TabBrowser::
onCurrentChanged(int const index)
{
    m_window.setTitle(this->tabText(index));
    tabAt(index).setFocus();
}

void TabBrowser::
onContextMenuRequested(QPoint const& pos)
{
    auto& tabBar = to_ref(this->tabBar());

    auto const index = tabBar.tabAt(pos);
    if (index != -1) {
        m_actions->setTarget(index); //TODO RAII

        tab_browser::ContextMenu menu { *this, tabAt(index) };

        menu.exec(this->mapToGlobal(pos));

        m_actions->setTarget(this->currentIndex());
    }
}

void TabBrowser::
onTabCloseRequested(int const index)
{
    auto& tab = tabAt(index);
    closeTab(tab);
}

} // namespace natsu
