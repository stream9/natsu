#ifndef NATSU_WEB_PAGE_HPP
#define NATSU_WEB_PAGE_HPP

#include <QWebEnginePage>

class QIcon;
class QString;
class QUrl;

namespace natsu {

class WebView;

class WebPage : public QWebEnginePage
{
    Q_OBJECT
public:
    WebPage(WebView&);
    ~WebPage() override;

    // accessor
    WebView& webView() const;

    // query
    bool isBlankPage() const;

    // command
    void reload();

    // signal
    Q_SIGNAL void navigationAccepted(QUrl const&) const;
    Q_SIGNAL void newWindowRequested(QWebEngineNewWindowRequest&, bool& handled);

protected:
    // override QWebEnginePage
    bool acceptNavigationRequest(QUrl const&, WebPage::NavigationType,
                                 bool isMainFrame) override;
    void javaScriptConsoleMessage(JavaScriptConsoleMessageLevel,
                                  QString const& message,
                                  int lineNumber, QString const& sourceID);
private:
    Q_SLOT void onLoadFinished(bool);
    Q_SLOT void onIconChanged(QIcon const&);
    Q_SLOT void onLinkHovered(QString const&);
    Q_SLOT void onUrlChanged(QUrl const&);
    Q_SLOT void onNewWindowRequested(QWebEngineNewWindowRequest&) noexcept;
};

} // namespace natsu

#endif // NATSU_WEB_PAGE_HPP
