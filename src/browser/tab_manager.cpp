#include "tab_manager.hpp"

#include "browser_tab.hpp"
#include "browser_window.hpp"
#include "tab_browser.hpp"

#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"

namespace natsu {

TabManager::
TabManager(TabBrowser& browser)
    : m_tabBrowser { browser }
{}

TabManager::~TabManager() = default;

BrowserTab& TabManager::
newTab()
{
    auto& tab = doNewTab();

    Q_EMIT get::browserWindow(m_tabBrowser).tabCreated(tab);

    return tab;
}

BrowserTab& TabManager::
newTab(int const index)
{
    assert(index >= 0);

    auto& tab = doNewTab(index);

    Q_EMIT get::browserWindow(m_tabBrowser).tabCreated(tab);

    return tab;
}

BrowserTab& TabManager::
doNewTab()
{
    return doNewTab(m_tabBrowser.currentIndex() + 1);
}

BrowserTab& TabManager::
doNewTab(int const index)
{
    auto const tab = make_qmanaged<BrowserTab>(m_tabBrowser);

    m_tabBrowser.insertTab(index, tab.get(), QSL("New Tab"));

    return *tab;
}

void TabManager::
doCloseTab(BrowserTab& tab)
{
    auto const index = m_tabBrowser.indexOf(&tab);
    if (index == -1) return;

    if (m_tabBrowser.count() == 1) {
        get::browserWindow(m_tabBrowser).close();
    }
    else {
        tab.deleteLater();
        m_tabBrowser.removeTab(index);

        m_tabBrowser.currentTab().setFocus();
    }
}

} // namespace natsu
