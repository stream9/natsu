#include "status_tip.hpp"

#include "web_view.hpp"
#include "web_page.hpp"

#include "core/get.hpp"
#include "global.hpp"

#include <cmath>

#include <QPalette>

namespace natsu {

StatusTip::
StatusTip(WebView& webView)
    : QLabel { &webView }
    , m_webView { webView }
{
    this->setAutoFillBackground(true);
    this->setForegroundRole(QPalette::ToolTipText);
    this->setBackgroundRole(QPalette::ToolTipBase);
}

StatusTip::~StatusTip() = default;

void StatusTip::
setMessage(QString const& message)
{
    if (!message.isEmpty()) {
        this->setText(elideMessage(message));
        this->resize(this->sizeHint());

        auto& page = get::webPage(m_webView);
        auto const zoom = page.zoomFactor();

        page.runJavaScript(
            QSL("document.documentElement.clientHeight; "),
            [this, zoom](auto const& rv) {
                auto const height =
                    static_cast<int>(std::lround(rv.toInt() * zoom));
                QPoint const pos { 0, height - this->height() };

                this->move(pos);
                this->raise();
                this->show();
            }
        );
    }
    else {
        this->hide();
    }
}

QString StatusTip::
elideMessage(QString const& message) const
{
    auto const& fm = this->fontMetrics();
    return fm.elidedText(
        message,
        Qt::ElideMiddle,
        static_cast<int>(m_webView.width() * 0.8)
    );
}

} // namespace natsu
