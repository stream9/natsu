#include "web_view.hpp"

#include "web_view/actions.hpp"

#include "browser_tab.hpp"
#include "browser_window.hpp"
#include "status_tip.hpp"
#include "web_page.hpp"

#include "core/application.hpp"
#include "core/get.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QKeyEvent>
#include <QMessageBox>
#include <QTimer>

namespace natsu {

WebView::
WebView(BrowserTab& tab)
    : m_browserTab { tab }
    , m_statusTip { std::make_unique<StatusTip>(*this) }
{
    auto webPage = make_qmanaged<WebPage>(*this);
    this->setPage(webPage);

    m_actions = std::make_unique<web_view::Actions>(*this);

    this->connect(this, &QWebEngineView::renderProcessTerminated,
                  this, &WebView::onRenderProcessTerminated);
}

WebView::~WebView() = default;

WebPage& WebView::
webPage() const
{
    return to_ref(dynamic_cast<WebPage*>(this->page()));
}

StatusTip& WebView::
statusTip() const
{
    return *m_statusTip;
}

web_view::Actions& WebView::
actions() const
{
    return *m_actions;
}

void WebView::
setStatusMessage(QString const& message)
{
    m_statusTip->setMessage(message);
}

void WebView::
addAction(QAction& action)
{
    m_actions->addAction(action);
}

bool WebView::
handleKeyPress(QKeyEvent const& ev)
{
    return m_actions->handleKeyPress(ev);
}

void WebView::
onRenderProcessTerminated(
        QWebEnginePage::RenderProcessTerminationStatus const statusCode,
        int const exitCode)
{
    auto const msg = QSL(
        "Do you want to reload the page?\n"
        "Status Code: %1, Exit Code: %2"
    ).arg(statusCode).arg(exitCode);

    auto const response = QMessageBox::question(
        this->window(),
        QSL("Render process terminated."),
        msg
    );

    if (response == QMessageBox::Yes) {
        QTimer::singleShot(0, [this] { this->reload(); });
    }
}

} // namespace natsu
