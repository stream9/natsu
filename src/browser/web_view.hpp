#ifndef NATSU_WEB_VIEW_HPP
#define NATSU_WEB_VIEW_HPP

#include <memory>

#include <QWebEngineView>

class QKeyEvent;

namespace natsu {

namespace web_view { class Actions; } // namespace web_view

class BrowserTab;
class WebPage;
class StatusTip;

class WebView : public QWebEngineView
{
    Q_OBJECT
public:
    WebView(BrowserTab&);
    ~WebView() override;

    // accessor
    BrowserTab& browserTab() const { return m_browserTab; }
    WebPage& webPage() const;
    StatusTip& statusTip() const;
    web_view::Actions& actions() const;

    // modifier
    void setStatusMessage(QString const&);
    void addAction(QAction&);

    // command
    bool handleKeyPress(QKeyEvent const&);

private:
    Q_SLOT void onRenderProcessTerminated(
            QWebEnginePage::RenderProcessTerminationStatus, int exitCode);

private:
    BrowserTab& m_browserTab;

    std::unique_ptr<StatusTip> const m_statusTip; // non-null
    std::unique_ptr<web_view::Actions> m_actions; // non-null
};

} // namespace natsu

#endif // NATSU_WEB_VIEW_HPP
