#ifndef NATSU_CORE_STATUS_TIP_HPP
#define NATSU_CORE_STATUS_TIP_HPP

#include <QLabel>

class QString;

namespace natsu {

class WebView;

class StatusTip : public QLabel
{
public:
    StatusTip(WebView&);
    ~StatusTip();

    // accessor
    WebView& webView() const { return m_webView; }

    // modifier
    void setMessage(QString const&);

private:
    QString elideMessage(QString const&) const;

private:
    WebView& m_webView;
};

} // namespace natsu

#endif // NATSU_CORE_STATUS_TIP_HPP
