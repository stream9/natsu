#include "navigation_bar.hpp"

#include "browser_tab.hpp"
#include "web_view.hpp"
#include "web_view/actions.hpp"

#include "amaze_box/amaze_box.hpp"
#include "core/get.hpp"

#include <QLayout>

namespace natsu {

NavigationBar::
NavigationBar(BrowserTab& tab) //TODO parent
    : m_browserTab { tab }
    , m_amazeBox { make_qmanaged<AmazeBox>(tab) }
{
    auto& actions = get::webView(m_browserTab).actions();

    this->addAction(&actions.backAction());
    this->addAction(&actions.forwardAction());
    this->addAction(&actions.reloadAction());

    this->addWidget(m_amazeBox.get());

    this->setIconSize(QSize(16, 16));
    this->setStyleSheet(QSL("QToolBar { border: 0px; }"));
    this->layout()->setContentsMargins(1, 1, 1, 1);
    this->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);
}

NavigationBar::~NavigationBar() = default;

AmazeBox& NavigationBar::
amazeBox() const
{
    return *m_amazeBox;
}

void NavigationBar::
focusInEvent(QFocusEvent*)
{
    m_amazeBox->setFocus();
}

} // namespace natsu
