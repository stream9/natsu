#ifndef NATSU_BROWSER_DIALOG_WEB_VIEW_HPP
#define NATSU_BROWSER_DIALOG_WEB_VIEW_HPP

#include "web_view.hpp"

namespace natsu {

class DialogWebView : public WebView
{
public:
    using WebView::WebView;
};

} // namespace natsu

#endif // NATSU_BROWSER_DIALOG_WEB_VIEW_HPP
