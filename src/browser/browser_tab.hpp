#ifndef NATSU_BROWSER_TAB_HPP
#define NATSU_BROWSER_TAB_HPP

#include "misc/pointer.hpp"

#include <QMovie>
#include <QMainWindow>

class QEvent;
class QIcon;
class QString;
class QUrl;
class QVBoxLayout;

namespace natsu {

class NavigationBar;
class NotificationBar;
class TabBrowser;
class WebView;

class BrowserTab : public QMainWindow
{
    Q_OBJECT
public:
    BrowserTab(TabBrowser&);
    ~BrowserTab() override;

    // accessor
    TabBrowser& tabBrowser()  const { return m_tabBrowser; }

    NavigationBar& navigationBar() const;
    WebView& webView() const;
    QVBoxLayout& layout();

    // query
    QString title() const;
    QUrl url() const;

    // modifier
    void load(QUrl const&);
    void close();

    // command
    NotificationBar& makeNotification(QString const& message);

protected:
    bool event(QEvent*) override;

private:
    Q_SLOT void onLoadStarted();
    Q_SLOT void onLoadProgress(int);
    Q_SLOT void onLoadFinished(bool);
    Q_SLOT void onTitleChanged(QString const&);
    Q_SLOT void onIconChanged(QIcon const&);
    Q_SLOT void onIconAnimated();
    Q_SLOT void onCloseRequested();

private:
    TabBrowser& m_tabBrowser;

    QMovie m_loadingIcon;
    qmanaged_ptr<WebView> m_webView;
    qmanaged_ptr<NavigationBar> m_navigationBar;
};

} // namespace natsu

#endif // NATSU_BROWSER_TAB_HPP
