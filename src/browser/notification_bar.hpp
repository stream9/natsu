#ifndef NATSU_BROWSER_NOTIFICATION_BAR_HPP
#define NATSU_BROWSER_NOTIFICATION_BAR_HPP

#include "misc/pointer.hpp"

#include <QWidget>

class QLabel;
class QToolButton;

namespace natsu {

class NotificationBar : public QWidget
{
    Q_OBJECT
public:
    NotificationBar(QString const& message, QWidget& parent);
    ~NotificationBar() override;

    // query
    QString message() const;

    // modifier
    void setMessage(QString const& message);
    void addWidget(QWidget&);

    // command
    Q_SLOT void close();

private:
    qmanaged_ptr<QLabel> m_message;
    qmanaged_ptr<QToolButton> m_close;
};

} // namespace natsu

#endif // NATSU_BROWSER_NOTIFICATION_BAR_HPP
