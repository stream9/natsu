#include "browser_tab.hpp"

#include "amaze_box/amaze_box.hpp"
#include "browser_window.hpp"
#include "navigation_bar.hpp"
#include "notification_bar.hpp"
#include "tab_browser.hpp"
#include "web_page.hpp"
#include "web_view.hpp"

#include "core/get.hpp"

#include <QApplication>
#include <QBoxLayout>
#include <QIcon>
#include <QKeyEvent>
#include <QString>
#include <QTimer>
#include <QUrl>

namespace natsu {

BrowserTab::
BrowserTab(TabBrowser& tabBrowser)
    : m_tabBrowser { tabBrowser }
    , m_loadingIcon { QSL(":/icons/loading.gif") }
    , m_webView { make_qmanaged<WebView>(*this) }
    , m_navigationBar { make_qmanaged<NavigationBar>(*this) }
{
    this->setWindowFlags(Qt::Widget);

    auto central = make_qmanaged<QWidget>(this);
    auto layout = make_qmanaged<QVBoxLayout>(central.get());
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    layout->addWidget(m_webView.get(), 1);

    this->setCentralWidget(central.get());

    this->addToolBar(Qt::TopToolBarArea, m_navigationBar.get());

    this->connect(m_webView.get(), &WebView::loadStarted,
                  this,            &BrowserTab::onLoadStarted);
    this->connect(m_webView.get(), &WebView::loadProgress,
                  this,            &BrowserTab::onLoadProgress);
    this->connect(m_webView.get(), &WebView::loadFinished,
                  this,            &BrowserTab::onLoadFinished);
    this->connect(m_webView.get(), &WebView::titleChanged,
                  this,            &BrowserTab::onTitleChanged);
    this->connect(m_webView.get(), &WebView::iconChanged,
                  this,            &BrowserTab::onIconChanged);
    this->connect(&m_loadingIcon, &QMovie::frameChanged,
                  this,           &BrowserTab::onIconAnimated);

    auto& page = get::webPage(*this);
    this->connect(&page, &WebPage::windowCloseRequested,
                  this,  &BrowserTab::onCloseRequested);

    m_loadingIcon.setScaledSize(QSize(16, 16));

    this->setFocusProxy(m_webView.get());
}

BrowserTab::~BrowserTab() = default;

NavigationBar& BrowserTab::
navigationBar() const
{
    return *m_navigationBar;
}

WebView& BrowserTab::
webView() const
{
    return *m_webView;
}

QVBoxLayout& BrowserTab::
layout()
{
    auto& central = to_ref(this->centralWidget());
    return to_ref(static_cast<QVBoxLayout*>(central.layout()));
}

QString BrowserTab::
title() const
{
    return m_webView->title();
}

QUrl BrowserTab::
url() const
{
    return m_webView->url();
}

void BrowserTab::
load(QUrl const& url)
{
    // QWebEngiveView::load() doesn't change URL until enough data is loaded
    m_webView->setUrl(url);
}

void BrowserTab::
close()
{
    m_tabBrowser.closeTab(*this);
}

NotificationBar& BrowserTab::
makeNotification(QString const& message)
{
    auto const bar = make_qmanaged<NotificationBar>(message, *this);

    auto& layout = this->layout();
    auto const index = layout.indexOf(m_webView.get());
    layout.insertWidget(index, bar.get());

    return *bar;
}

bool BrowserTab::
event(QEvent* const ev)
{
    // QWebEngineWebView send unhandled keyevents to here
    assert(ev);

    switch (ev->type()) {
        case QEvent::KeyPress:
            if (this->focusWidget() == m_webView->focusProxy()) {
                auto& key = static_cast<QKeyEvent&>(*ev);
                return m_webView->handleKeyPress(key);
            }
        default:
            break;
    }

    return QMainWindow::event(ev);
}

void BrowserTab::
onLoadStarted()
{
    if (this->isVisible()) {
        // This sometimes work and sometimes doesn't.
        // So to be sure, do the same thing on loadFinished signal.
        if (m_navigationBar->amazeBox().hasFocus()) {
            this->setFocus();
        }
    }
}

void BrowserTab::
onLoadProgress(int const progress)
{
    if (progress == 100) {
        m_loadingIcon.stop();
        onIconChanged(m_webView->icon());
    }
    else if (m_loadingIcon.state() != QMovie::Running) {
        m_loadingIcon.start();
    }
}

void BrowserTab::
onLoadFinished(bool /*ok*/)
{
    if (this->isVisible()) {
        if (m_navigationBar->amazeBox().hasFocus()) {
            this->setFocus();
        }
    }
}

void BrowserTab::
onTitleChanged(QString const& title)
{
    m_tabBrowser.setTitle(*this, title);
}

void BrowserTab::
onIconChanged(QIcon const& icon)
{
    m_tabBrowser.setIcon(*this, icon);
}

void BrowserTab::
onIconAnimated()
{
    QIcon const icon { m_loadingIcon.currentPixmap() };

    onIconChanged(icon);
}

void BrowserTab::
onCloseRequested()
{
    close();
}

} // namespace natsu
