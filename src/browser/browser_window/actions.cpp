#include "actions.hpp"

#include "../browser_tab.hpp"
#include "../browser_window.hpp"
#include "../navigation_bar.hpp"
#include "../web_page.hpp"

#include "core/action_manager.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"
#include "global.hpp"

#include <QTimer>

namespace natsu::browser_window {

Actions::
Actions(BrowserWindow& window)
    : m_browserWindow { window }
{
    m_newTabAction.setObjectName(QSL("new_tab"));
    m_newTabAction.setText(QSL("New Tab"));
    m_newTabAction.setIcon(icon::newTab());
    this->connect(&m_newTabAction, &QAction::triggered,
                  this,            &Actions::onNewTab);

    window.addAction(&m_newTabAction);

    configure();
}

Actions::~Actions() = default;

void Actions::
configure()
{
    auto const& category = QSL("Browser Window");
    auto& manager = get::actionManager(m_browserWindow);

    //manager.configure(category, newTabAction(), makeKeyCode(Qt::CTRL, Qt::Key_T));
}

void Actions::
onNewTab()
{
    auto& tab = m_browserWindow.newTab();

    QTimer::singleShot(10, [&]() {
        tab.navigationBar().setFocus();
    });
}

} // namespace natsu::browser_window
