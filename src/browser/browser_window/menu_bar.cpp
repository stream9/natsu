#include "menu_bar.hpp"

#include "actions.hpp"

#include "../browser_tab.hpp"
#include "../browser_window.hpp"
#include "../tab_browser.hpp"
#include "../tab_browser/actions.hpp"
#include "../web_view.hpp"
#include "../web_view/actions.hpp"

#include "core/application.hpp"
#include "core/application_actions.hpp"
#include "core/get.hpp"
#include "core/history_manager.hpp"
#include "global.hpp"
#include "misc/link_action.hpp"

#include <QEvent>
#include <QKeyEvent>
#include <QMenu>
#include <QObject>
#include <QIcon>

namespace natsu::browser_window {

MenuBar::
MenuBar(BrowserWindow& browserWindow)
    : m_browserWindow { browserWindow }
    , m_fileMenu { make_qmanaged<QMenu>("&File", this) }
    , m_editMenu { make_qmanaged<QMenu>("&Edit", this) }
    , m_viewMenu { make_qmanaged<QMenu>("&View", this) }
    , m_historyMenu { make_qmanaged<QMenu>("&History", this) }
    , m_toolMenu { make_qmanaged<QMenu>("&Tool", this) }
    , m_helpMenu { make_qmanaged<QMenu>("&Help", this) }
{
    this->setVisible(false);
    m_historyMenu->setToolTipsVisible(true);

    auto setName = [](auto& menu, auto const& name) {
        auto& action = to_ref(menu->menuAction());
        action.setObjectName(name);
    };

    setName(m_fileMenu, QSL("menu_bar.file"));
    setName(m_editMenu, QSL("menu_bar.edit"));
    setName(m_viewMenu, QSL("menu_bar.view"));
    setName(m_historyMenu, QSL("menu_bar.history"));
    setName(m_toolMenu, QSL("menu_bar.tool"));
    setName(m_helpMenu, QSL("menu_bar.help"));

    this->addMenu(m_fileMenu.get());
    this->addMenu(m_editMenu.get());
    this->addMenu(m_viewMenu.get());
    this->addMenu(m_historyMenu.get());

    this->addMenu(m_toolMenu.get());
    this->addMenu(m_helpMenu.get());

    this->connect(m_fileMenu.get(), &QMenu::aboutToShow,
                  this,             &MenuBar::populateFileMenu);
    this->connect(m_editMenu.get(), &QMenu::aboutToShow,
                  this,             &MenuBar::populateEditMenu);
    this->connect(m_historyMenu.get(), &QMenu::aboutToShow,
                  this,                &MenuBar::populateHistoryMenu);

    m_toolMenu->addAction(
        &get::application(m_browserWindow).applicationActions().verboseAction());

    m_browserWindow.installEventFilter(this);
    m_time.start();
}

MenuBar::~MenuBar() = default;

void MenuBar::
populateFileMenu()
{
    auto& appActions = get::application(m_browserWindow).applicationActions();
    auto& tabsActions = get::tabBrowser(m_browserWindow).actions();

    m_fileMenu->clear();
    m_fileMenu->addAction(&tabsActions.newTabAction());
    m_fileMenu->addAction(&appActions.newWindowAction());
    m_fileMenu->addAction(&tabsActions.closeAction());
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(&appActions.quitAction());
}

void MenuBar::
populateEditMenu()
{
    auto& tab = get::activeTab(m_browserWindow);
    auto& actions = get::webView(tab).actions();

    m_editMenu->clear();
    m_editMenu->addAction(&actions.undoAction());
    m_editMenu->addAction(&actions.redoAction());
    m_editMenu->addSeparator();
    m_editMenu->addAction(&actions.cutAction());
    m_editMenu->addAction(&actions.copyAction());
    m_editMenu->addAction(&actions.pasteAction());
    m_editMenu->addSeparator();
    m_editMenu->addAction(&actions.selectAllAction());
}

void MenuBar::
populateHistoryMenu()
{
    auto& history = get::history(m_browserWindow);

    auto const& entries = history.recentEntries(15);

    m_historyMenu->clear();
    for (auto const& entry: entries) {
        auto const action = make_qmanaged<LinkAction>(
            entry.title,
            entry.url,
            entry.icon,
            m_browserWindow,
            *m_historyMenu
        );
        m_historyMenu->addAction(action.get());
    }
}

bool MenuBar::
eventFilter(QObject* const obj, QEvent* const ev)
{
    assert(ev);

    if (obj == &m_browserWindow) {
        if (ev->type() == QEvent::KeyPress) {
            auto& event = to_ref(static_cast<QKeyEvent*>(ev));

            if (event.key() == Qt::Key_Alt) {
                m_altKeyPressed = true;
            }
        }
        else if (ev->type() == QEvent::KeyRelease) {
            auto& event = to_ref(static_cast<QKeyEvent*>(ev));

            if (m_altKeyPressed && event.key() == Qt::Key_Alt) {
                if (m_time.restart() <= 500) { //TODO make it configurable
                    this->setVisible(!this->isVisible());
                    if (!this->isVisible()) {
                        auto& tab = get::activeTab(m_browserWindow);
                        tab.setFocus();
                    }
                }
            }
            else if (event.modifiers() & Qt::AltModifier) {
                m_altKeyPressed = false;
            }
        }
    }

    return QMenuBar::eventFilter(obj, ev);
}

} // namespace natsu::browser_window
