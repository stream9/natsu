#ifndef NATSU_BROWSER_BROWSER_WINDOW_ACTIONS_HPP
#define NATSU_BROWSER_BROWSER_WINDOW_ACTIONS_HPP

#include "../fwd/browser_tab.hpp"
#include "../fwd/browser_window.hpp"

#include <QObject>
#include <QAction>

namespace natsu::browser_window {

class Actions : public QObject
{
    Q_OBJECT
public:
    Actions(BrowserWindow&);
    ~Actions() override;

    // accessor
    BrowserWindow& browserWindow() const { return m_browserWindow; }

    QAction& newTabAction() { return m_newTabAction; }

private:
    void configure();

    Q_SLOT void onNewTab();

private:
    BrowserWindow& m_browserWindow;

    QAction m_newTabAction;
};

} // namespace natsu::browser_window

#endif // NATSU_BROWSER_BROWSER_WINDOW_ACTIONS_HPP
