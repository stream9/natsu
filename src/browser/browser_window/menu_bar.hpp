#ifndef NATSU_BROWSER_WINDOW_MENU_BAR_HPP
#define NATSU_BROWSER_WINDOW_MENU_BAR_HPP

#include "../fwd/browser_window.hpp"
#include "misc/pointer.hpp"

#include <QMenuBar>
#include <QElapsedTimer>

class QEvent;
class QMenu;
class QObject;

namespace natsu::browser_window {

class MenuBar : public QMenuBar
{
    Q_OBJECT
public:
    MenuBar(BrowserWindow&);
    ~MenuBar() override;

    // accessor
    BrowserWindow& browserWindow() const { return m_browserWindow; }

    QMenu& viewMenu() const { return *m_viewMenu; }
    QMenu& toolMenu() const { return *m_toolMenu; }

private:
    Q_SLOT void populateFileMenu();
    Q_SLOT void populateEditMenu();
    Q_SLOT void populateHistoryMenu();

    // override QObject
    bool eventFilter(QObject*, QEvent*);

private:
    BrowserWindow& m_browserWindow;

    qmanaged_ptr<QMenu> const m_fileMenu;
    qmanaged_ptr<QMenu> const m_editMenu;
    qmanaged_ptr<QMenu> const m_viewMenu;
    qmanaged_ptr<QMenu> const m_historyMenu;
    qmanaged_ptr<QMenu> const m_toolMenu;
    qmanaged_ptr<QMenu> const m_helpMenu;

    QElapsedTimer m_time;
    bool m_altKeyPressed = false;
};

} // namespace natsu::browser_window

#endif // NATSU_BROWSER_WINDOW_MENU_BAR_HPP
