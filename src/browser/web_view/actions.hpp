#ifndef NATSU_BROWSER_WEB_VIEW_ACTIONS_HPP
#define NATSU_BROWSER_WEB_VIEW_ACTIONS_HPP

#include "../fwd/web_view.hpp"

#include <QObject>

#include <vector>

class QAction;
class QKeyEvent;

namespace natsu::web_view {

class Actions : public QObject
{
    Q_OBJECT
public:
    Actions(WebView&);
    ~Actions() override;

    QAction& backAction() const;
    QAction& forwardAction() const;
    QAction& stopAction() const;
    QAction& reloadAction() const;
    QAction& reloadAndBypassCacheAction() const;

    QAction& cutAction() const;
    QAction& copyAction() const;
    QAction& pasteAction() const;
    QAction& undoAction() const;
    QAction& redoAction() const;
    QAction& selectAllAction() const;

    // modifier
    void addAction(QAction&);

    // command
    bool handleKeyPress(QKeyEvent const&);

private:
    void configure();
    void install();

private:
    WebView& m_view;

    std::vector<QAction*> m_actions;
};

} // namespace natsu::web_view

#endif // NATSU_BROWSER_WEB_VIEW_ACTIONS_HPP
