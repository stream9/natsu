#include "actions.hpp"

#include "../web_page.hpp"
#include "../web_view.hpp"

#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QAction>
#include <QKeyEvent>

namespace natsu::web_view {

Actions::
Actions(WebView& view)
    : m_view { view }
{
    configure();

    install();
}

Actions::~Actions() = default;

QAction& Actions::
backAction() const
{
    return to_ref(m_view.pageAction(WebPage::Back));
}

QAction& Actions::
forwardAction() const
{
    return to_ref(m_view.pageAction(WebPage::Forward));
}

QAction& Actions::
stopAction() const
{
    return to_ref(m_view.pageAction(WebPage::Stop));
}

QAction& Actions::
reloadAction() const
{
    return to_ref(m_view.pageAction(WebPage::Reload));
}

QAction& Actions::
reloadAndBypassCacheAction() const
{
    return to_ref(m_view.pageAction(WebPage::ReloadAndBypassCache));
}

QAction& Actions::
cutAction() const
{
    return to_ref(m_view.pageAction(WebPage::Cut));
}

QAction& Actions::
copyAction() const
{
    return to_ref(m_view.pageAction(WebPage::Copy));
}

QAction& Actions::
pasteAction() const
{
    return to_ref(m_view.pageAction(WebPage::Paste));
}

QAction& Actions::
undoAction() const
{
    return to_ref(m_view.pageAction(WebPage::Undo));
}

QAction& Actions::
redoAction() const
{
    return to_ref(m_view.pageAction(WebPage::Redo));
}

QAction& Actions::
selectAllAction() const
{
    return to_ref(m_view.pageAction(WebPage::SelectAll));
}

void Actions::
install()
{
    addAction(backAction());
    addAction(forwardAction());
    addAction(stopAction());
    addAction(reloadAction());
    addAction(reloadAndBypassCacheAction());

    addAction(cutAction());
    addAction(copyAction());
    addAction(pasteAction());
    addAction(undoAction());
    addAction(redoAction());
    addAction(selectAllAction());
}

void Actions::
configure()
{
    auto const& category = QSL("Web View");
    auto& manager = get::actionManager(m_view);

    auto f = [&](auto& action, auto const& name,
                 auto const& icon, QKeySequence const& shortCut)
    {
        action.setIcon(icon);
        action.setObjectName(name);
        manager.configure(category, action, shortCut);
    };

    f(backAction(), QSL("browser.back"), icon::back(), makeKeyCode(Qt::ALT, Qt::Key_Left));
    f(forwardAction(), QSL("browser.forward"), icon::forward(), makeKeyCode(Qt::ALT, Qt::Key_Right));
    f(stopAction(), QSL("browser.stop"), icon::stop(), Qt::Key_Escape);
    f(reloadAction(), QSL("browser.reload"), icon::reload(), makeKeyCode(Qt::CTRL, Qt::Key_R));
    f(reloadAndBypassCacheAction(), QSL("browser.reload_and_bypass_cache"), icon::reloadAndBypassCache(), makeKeyCode(Qt::SHIFT, Qt::CTRL, Qt::Key_R));

    f(cutAction(), QSL("browser.cut"), icon::cut(), makeKeyCode(Qt::CTRL, Qt::Key_X));
    f(copyAction(), QSL("browser.copy"), icon::copy(), makeKeyCode(Qt::CTRL, Qt::Key_C));
    f(pasteAction(), QSL("browser.paste"), icon::paste(), makeKeyCode(Qt::CTRL, Qt::Key_V));
    f(undoAction(), QSL("browser.undo"), icon::undo(), makeKeyCode(Qt::CTRL, Qt::Key_Z));
    f(redoAction(), QSL("browser.redo"), icon::redo(), makeKeyCode(Qt::CTRL, Qt::SHIFT, Qt::Key_Z));
    f(selectAllAction(), QSL("browser.select_all"), icon::selectAll(), makeKeyCode(Qt::CTRL, Qt::Key_A));
}

void Actions::
addAction(QAction& action)
{
    m_actions.push_back(&action);
}

bool Actions::
handleKeyPress(QKeyEvent const& ev)
{
    auto const mask = Qt::ShiftModifier | Qt::MetaModifier
                    | Qt::ControlModifier | Qt::AltModifier;
    auto const mod = ev.modifiers() & mask;

    QKeySequence const seq { static_cast<int>(mod) + ev.key() };

    for (auto* const action: m_actions) {
        assert(action);

        if (action->shortcut().matches(seq) == QKeySequence::ExactMatch) {
            action->trigger();
            return true;
        }
    }

    return false;
}

} // namespace natsu::web_view
