#include "web_dialog.hpp"

#include "browser_tab.hpp"
#include "dialog_web_view.hpp"
#include "web_page.hpp"

#include <QBoxLayout>
#include <QIcon>
#include <QLineEdit>
#include <QRect>
#include <QString>
#include <QUrl>

namespace natsu {

WebDialog::
WebDialog(BrowserTab& tab)
    : QWidget { &tab, Qt::Dialog }
    , m_tab { tab }
    , m_webView { make_qmanaged<DialogWebView>(tab) }
    , m_location { make_qmanaged<QLineEdit>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    // add WebView first so it has first tab order
    layout->addWidget(m_webView.get());

    m_location->setReadOnly(true);
    layout->insertWidget(0, m_location.get());

    this->connect(m_webView.get(), &WebView::urlChanged,
                  this,            &WebDialog::onUrlChanged);
    this->connect(m_webView.get(), &WebView::titleChanged,
                  this,            &WebDialog::onTitleChanged);
    this->connect(m_webView.get(), &WebView::iconChanged,
                  this,            &WebDialog::onIconChanged);

    auto& page = m_webView->webPage();
    this->connect(&page, &WebPage::geometryChangeRequested,
                  this,  &WebDialog::onGeometryChangeRequested);
    this->connect(&page, &WebPage::windowCloseRequested,
                  this,  &WebDialog::onCloseRequested);
    this->connect(&page, &WebPage::navigationAccepted,
                  this,  &WebDialog::onNavigationAccepted);

    this->setWindowModality(Qt::WindowModal);
}

WebDialog::~WebDialog() = default;

WebView& WebDialog::
webView() const
{
    return *m_webView;
}

QLineEdit& WebDialog::
location() const
{
    return *m_location;
}

void WebDialog::
onUrlChanged(QUrl const& url)
{
    m_location->setText(url.toDisplayString());
}

void WebDialog::
onTitleChanged(QString const& title)
{
    this->setWindowTitle(title);
}

void WebDialog::
onIconChanged(QIcon const& icon)
{
    this->setWindowIcon(icon);
}

void WebDialog::
onGeometryChangeRequested(QRect const& rect)
{
    this->setGeometry(rect);
}

void WebDialog::
onCloseRequested()
{
    this->close();
}

void WebDialog::
onNavigationAccepted(QUrl const&)
{
    this->show();
}

} // namespace natsu
