#ifndef NATSU_NAVIGATION_BAR_HPP
#define NATSU_NAVIGATION_BAR_HPP

#include "misc/pointer.hpp"

#include <QAction>
#include <QToolBar>
#include <QLineEdit>

class QFocusEvent;

namespace natsu {

class AmazeBox;
class BrowserTab;

class NavigationBar : public QToolBar
{
    Q_OBJECT
public:
    NavigationBar(BrowserTab&);
    ~NavigationBar() override;

    // accessor
    BrowserTab& browserTab() const { return m_browserTab; }
    AmazeBox& amazeBox() const;

    // overide QWidget
    void focusInEvent(QFocusEvent*) override;

private:
    BrowserTab& m_browserTab;

    qmanaged_ptr<AmazeBox> m_amazeBox;
};

} // namespace natsu

#endif // NATSU_NAVIGATION_BAR_HPP
