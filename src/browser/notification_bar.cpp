#include <notification_bar.hpp>

#include "core/icon.hpp"
#include "misc/pointer.hpp"

#include <QAction>
#include <QBoxLayout>
#include <QLabel>
#include <QToolButton>

namespace natsu {

NotificationBar::
NotificationBar(QString const& message, QWidget& parent)
    : QWidget { &parent }
    , m_message { make_qmanaged<QLabel>(message, this) }
    , m_close { make_qmanaged<QToolButton>(this) }
{
    auto layout = make_qmanaged<QHBoxLayout>(this);
    layout->setContentsMargins(20, 0, 0, 0);

    m_close->setAutoRaise(true);
    m_close->setIcon(icon::closeTab());

    layout->addWidget(m_message.get());
    layout->addStretch(1);
    layout->addWidget(m_close.get());

    this->connect(m_close.get(), &QToolButton::clicked,
                  this,          &NotificationBar::close);
}

NotificationBar::~NotificationBar() = default;

QString NotificationBar::
message() const
{
    return m_message->text();
}

void NotificationBar::
setMessage(QString const& message)
{
    m_message->setText(message);
}

void NotificationBar::
addWidget(QWidget& widget)
{
    auto& layout = to_ref(static_cast<QHBoxLayout*>(this->layout()));
    auto const index = layout.indexOf(m_close.get());

    layout.insertWidget(index, &widget);
}

void NotificationBar::
close()
{
    auto& parent = to_ref(this->parentWidget());
    auto& layout = to_ref(parent.layout());

    layout.removeWidget(this);

    this->deleteLater();
}

} // namespace natsu
