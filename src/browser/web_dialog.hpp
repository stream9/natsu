#ifndef NATSU_BROWSER_WEB_DIALOG_HPP
#define NATSU_BROWSER_WEB_DIALOG_HPP

#include "misc/pointer.hpp"

#include <QWidget>

class QIcon;
class QLineEdit;
class QRect;
class QString;
class QUrl;

namespace natsu {

class BrowserTab;
class DialogWebView;
class WebView;

class WebDialog : public QWidget
{
public:
    WebDialog(BrowserTab&);
    ~WebDialog() override;

    // accessor
    BrowserTab& browserTab() const { return m_tab; }
    WebView& webView() const;
    QLineEdit& location() const;

private:
    Q_SLOT void onUrlChanged(QUrl const&);
    Q_SLOT void onTitleChanged(QString const&);
    Q_SLOT void onIconChanged(QIcon const&);
    Q_SLOT void onGeometryChangeRequested(QRect const&);
    Q_SLOT void onCloseRequested();
    virtual Q_SLOT void onNavigationAccepted(QUrl const&);

private:
    BrowserTab& m_tab;

    qmanaged_ptr<DialogWebView> m_webView;
    qmanaged_ptr<QLineEdit> m_location;
};

} // namespace natsu

#endif // NATSU_BROWSER_WEB_DIALOG_HPP
