#ifndef NATSU_TAB_BROWSER_HPP
#define NATSU_TAB_BROWSER_HPP

#include "misc/integer.hpp"
#include "misc/pointer.hpp"

#include <memory>
#include <vector>

#include <QTabWidget>

class QIcon;
class QPoint;
class QString;
class QUrl;

namespace natsu::tab_browser { class Actions; } // namespace natsu::tab_browser

namespace natsu {

class BrowserWindow;
class BrowserTab;
class TabManager;

class TabBrowser : public QTabWidget
{
    Q_OBJECT
public:
    TabBrowser(BrowserWindow&);
    ~TabBrowser() override;

    // accessor
    BrowserWindow& browserWindow() const { return m_window; }
    TabManager& tabManager();
    tab_browser::Actions& actions() const;

    // query
    BrowserTab& currentTab() const;
    BrowserTab& tabAt(non_negative<int> index) const;
    std::vector<non_null_ptr<BrowserTab>> tabs() const;

    // modifier
    void setCurrentTab(BrowserTab&);

    BrowserTab& newTab();
    BrowserTab& newBackgroundTab();

    void closeTab(BrowserTab&);
    void restoreTab();

    void setTitle(BrowserTab&, QString const&);
    void setIcon(BrowserTab&, QIcon const&);

    void setTabManager(std::unique_ptr<TabManager>&&);

private:
    Q_SLOT void onCurrentChanged(int index);
    Q_SLOT void onContextMenuRequested(QPoint const& pos);
    Q_SLOT void onTabCloseRequested(int index);

private:
    BrowserWindow& m_window;

    std::unique_ptr<TabManager> m_manager; // non-null
    std::unique_ptr<tab_browser::Actions> const m_actions; // non-null
};

} // namespace natsu

#endif // NATSU_TAB_BROWSER_HPP
