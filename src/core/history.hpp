#ifndef NATSU_HISTORY_HPP
#define NATSU_HISTORY_HPP

#include "misc/integer.hpp"

#include <cstdint>
#include <ctime>

#include <QIcon>
#include <QString>

namespace natsu::history {

using Id = non_negative<int64_t>;
using Size = non_negative<int64_t>;

struct Entry {
    Id id;
    QString url;
    QString title;
    QIcon icon;
    non_negative<int64_t> count;
    time_t lastVisitTime;
};

} // namespace natsu::history

#endif // NATSU_HISTORY_HPP
