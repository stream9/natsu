#include "application_actions.hpp"

#include "action_manager.hpp"
#include "application.hpp"
#include "get.hpp"
#include "icon.hpp"

#include "browser/browser_window.hpp"
#include "global.hpp"

namespace natsu {

ApplicationActions::
ApplicationActions(Application& app)
    : m_application { app }
{
    m_newWindowAction.setObjectName(QSL("new_window"));
    m_newWindowAction.setText(QSL("New Window"));
    m_newWindowAction.setIcon(icon::newWindow());
    m_newWindowAction.setShortcutContext(Qt::ApplicationShortcut);
    this->connect(&m_newWindowAction, &QAction::triggered,
                  this,               &ApplicationActions::onNewWindow);

    m_quitAction.setObjectName(QSL("quit"));
    m_quitAction.setText(QSL("Quit"));
    m_quitAction.setIcon(icon::quit());
    m_newWindowAction.setShortcutContext(Qt::ApplicationShortcut);
    this->connect(&m_quitAction, &QAction::triggered,
                  this,          &ApplicationActions::onQuit);

    m_verboseAction.setObjectName(QSL("verbose"));
    m_verboseAction.setText(QSL("Verbose"));
    m_verboseAction.setCheckable(true);
    m_verboseAction.setChecked(false);

    configure();
}

void ApplicationActions::
installTo(QWidget& widget)
{
    widget.addAction(&m_newWindowAction);
    widget.addAction(&m_quitAction);
}

void ApplicationActions::
configure()
{
    auto& manager = get::actionManager(m_application);
    auto const& category = QSL("Application");

    manager.configure(category, newWindowAction(), makeKeyCode(Qt::CTRL, Qt::Key_N));
    manager.configure(category, quitAction(), makeKeyCode(Qt::CTRL, Qt::Key_Q));
}

void ApplicationActions::
onNewWindow()
{
    m_application.createWindow();
}

void ApplicationActions::
onQuit()
{
    m_application.exit();
}

} // namespace natsu
