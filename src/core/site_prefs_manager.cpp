#include "site_prefs_manager.hpp"

namespace natsu {

QVariant SitePrefsManager::
doGetValue(QUrl const&, QString const&/*key*/) const
{
    return {};
}

void SitePrefsManager::
doSetValue(QUrl const&, QString const&/*key*/, QVariant const&/*value*/)
{
}

void SitePrefsManager::
doRemoveValue(QUrl const&, QString const&/*key*/)
{
}

} // namespace natsu
