#ifndef NATSU_CORE_GET_HPP
#define NATSU_CORE_GET_HPP

namespace natsu {

class ActionManager;
class Application;
class BookmarkManager;
class BrowserTab;
class BrowserWindow;
class HistoryManager;
class Profile;
class SearchEngineManager;
class SitePrefsManager;
class TabBrowser;
class WebPage;
class WebView;

} // namespace natsu

namespace natsu::get {

Application& application(Profile const&);
Application& application(BookmarkManager const&);
Application& application(HistoryManager const&);
Application& application(BrowserWindow const&);
Application& application(TabBrowser const&);
Application& application(BrowserTab const&);
Application& application(WebView const&);
Application& application(WebPage const&);

ActionManager& actionManager(Application const&);
ActionManager& actionManager(Profile const&);
ActionManager& actionManager(BookmarkManager const&);
ActionManager& actionManager(HistoryManager const&);
ActionManager& actionManager(BrowserWindow const&);
ActionManager& actionManager(TabBrowser const&);
ActionManager& actionManager(BrowserTab const&);
ActionManager& actionManager(WebView const&);
ActionManager& actionManager(WebPage const&);

BrowserWindow& activeWindow(Application const&);
BrowserWindow& activeWindow(Profile const&);
BrowserWindow& activeWindow(BookmarkManager const&);
BrowserWindow& activeWindow(HistoryManager const&);
BrowserWindow& browserWindow(TabBrowser const&);
BrowserWindow& browserWindow(BrowserTab const&);
BrowserWindow& browserWindow(WebView const&);
BrowserWindow& browserWindow(WebPage const&);

TabBrowser& tabBrowser(BrowserWindow const&);
TabBrowser& tabBrowser(BrowserTab const&);
TabBrowser& tabBrowser(WebView const&);
TabBrowser& tabBrowser(WebPage const&);

BrowserTab& activeTab(Application const&);
BrowserTab& activeTab(Profile const&);
BrowserTab& activeTab(BookmarkManager const&);
BrowserTab& activeTab(HistoryManager const&);
BrowserTab& activeTab(BrowserWindow const&);
BrowserTab& activeTab(TabBrowser const&);
BrowserTab& browserTab(WebView const&);
BrowserTab& browserTab(WebPage const&);

WebView& webView(BrowserTab const&);
WebView& webView(WebPage const&);

WebPage& webPage(WebView const&);
WebPage& webPage(BrowserTab const&);

Profile& profile(BrowserWindow const&);
Profile& profile(TabBrowser const&);
Profile& profile(BrowserTab const&);
Profile& profile(BookmarkManager const&);
Profile& profile(HistoryManager const&);
Profile& profile(WebView const&);
Profile& profile(WebPage const&);

HistoryManager& history(BrowserWindow const&);
HistoryManager& history(TabBrowser const&);
HistoryManager& history(BrowserTab const&);
HistoryManager& history(Profile const&);
HistoryManager& history(BookmarkManager const&);
HistoryManager& history(WebView const&);
HistoryManager& history(WebPage const&);

SitePrefsManager& sitePrefs(Profile const&);
SitePrefsManager& sitePrefs(HistoryManager const&);
SitePrefsManager& sitePrefs(BookmarkManager const&);
SitePrefsManager& sitePrefs(BrowserWindow const&);
SitePrefsManager& sitePrefs(TabBrowser const&);
SitePrefsManager& sitePrefs(BrowserTab const&);
SitePrefsManager& sitePrefs(WebView const&);
SitePrefsManager& sitePrefs(WebPage const&);

SearchEngineManager& searchEngine(Profile const&);
SearchEngineManager& searchEngine(HistoryManager const&);
SearchEngineManager& searchEngine(BookmarkManager const&);
SearchEngineManager& searchEngine(BrowserWindow const&);
SearchEngineManager& searchEngine(TabBrowser const&);
SearchEngineManager& searchEngine(BrowserTab const&);
SearchEngineManager& searchEngine(WebView const&);
SearchEngineManager& searchEngine(WebPage const&);

BookmarkManager& bookmark(Profile const&);
BookmarkManager& bookmark(HistoryManager const&);
BookmarkManager& bookmark(BrowserWindow const&);
BookmarkManager& bookmark(TabBrowser const&);
BookmarkManager& bookmark(BrowserTab const&);
BookmarkManager& bookmark(WebView const&);
BookmarkManager& bookmark(WebPage const&);

} // namespace natsu::get

#endif // NATSU_CORE_GET_HPP
