#ifndef NATSU_APPLICATION_ACTIONS_HPP
#define NATSU_APPLICATION_ACTIONS_HPP

#include <QAction>
#include <QObject>
#include <QSettings>

namespace natsu {

class Application;
class ActionManager;

class ApplicationActions : public QObject
{
    Q_OBJECT
public:
    ApplicationActions(Application&);

    // accessor
    QAction& newWindowAction() { return m_newWindowAction; }
    QAction& quitAction() { return m_quitAction; }
    QAction& verboseAction() { return m_verboseAction; }

    // command
    void installTo(QWidget&);

private:
    void configure();

    Q_SLOT void onNewWindow();
    Q_SLOT void onQuit();

private:
    Application& m_application;

    QAction m_newWindowAction;
    QAction m_quitAction;
    QAction m_verboseAction;
};

} // namespace natsu

#endif // NATSU_APPLICATION_ACTIONS_HPP
