#include "profile.hpp"

#include "application.hpp"
#include "bookmark_manager.hpp"
#include "history_manager.hpp"
#include "mime/scheme_handler.hpp"
#include "search_engine_manager.hpp"
#include "site_prefs_manager.hpp"

#include "global.hpp"

#include <QDir>
#include <QString>
#include <QWebEngineSettings>

namespace natsu {

Profile::
Profile(Application& app, QString const& name)
    : QWebEngineProfile { name }
    , m_application { app }
    , m_settings { settingPath(), QSettings::IniFormat }
    , m_sitePrefs { std::make_unique<SitePrefsManager>() }
    , m_searchEngineManager { std::make_unique<SearchEngineManager>() }
    , m_historyManager { std::make_unique<HistoryManager>(*this) }
    , m_bookmarkManager { std::make_unique<BookmarkManager>(*this) }
    , m_schemeHandler { std::make_unique<mime::SchemeHandler>(*this) }
{
    this->setPersistentStoragePath(path());
    this->setSpellCheckEnabled(true);
    this->setSpellCheckLanguages({ QSL("en_US") });
    this->setHttpAcceptLanguage(QSL("en-US,en;q=0.9,ja;q=0.8"));
    QWebEngineProfile::settings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
}

Profile::~Profile() = default;

void Profile::
setSitePrefsManager(std::unique_ptr<SitePrefsManager> sitePrefs)
{
    m_sitePrefs = sitePrefs ? std::move(sitePrefs)
                            : std::make_unique<SitePrefsManager>();
}

void Profile::
setSearchEngineManager(std::unique_ptr<SearchEngineManager> manager)
{
    m_searchEngineManager = manager ? std::move(manager)
                                    : std::make_unique<SearchEngineManager>();
}

void Profile::
setHistoryManager(std::unique_ptr<HistoryManager> manager)
{
    m_historyManager = manager ? std::move(manager)
                               : std::make_unique<HistoryManager>(*this);
}

void Profile::
setBookmarkManager(std::unique_ptr<BookmarkManager> manager)
{
    m_bookmarkManager = manager ? std::move(manager)
                                : std::make_unique<BookmarkManager>(*this);
}

QString Profile::
path() const
{
    return m_application.configPath() + QDir::separator() + this->storageName();
}

QString Profile::
settingPath() const
{
    return path() + QDir::separator() + QSL("settings.ini");
}

QDir Profile::
pluginDir() const
{
    QDir dir { path() + QDir::separator() + QSL("plugin") };

    if (!dir.exists()) {
        dir.mkpath(".");
    }

    assert(dir.exists()); //TODO handle error
    return dir;
}

} // namespace natsu
