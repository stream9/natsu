#ifndef NATSU_CORE_FWD_HISTORY_HPP
#define NATSU_CORE_FWD_HISTORY_HPP

namespace natsu::history {

class Entry;

} // namespace natsu::history

#endif // NATSU_CORE_FWD_HISTORY_HPP
