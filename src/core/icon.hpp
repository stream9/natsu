#ifndef NATSU_ICON_HPP
#define NATSU_ICON_HPP

class QIcon;
class QString;

namespace natsu::icon {

// Application
QIcon newWindow();
QIcon quit();

// BrowserWindow
QIcon newTab();

// TabBrowser
QIcon defaultFavicon();

// BrowserTab
QIcon closeTab();

// WebPage
QIcon back();
QIcon forward();
QIcon stop();
QIcon reload();
QIcon reloadAndBypassCache();
QIcon cut();
QIcon copy();
QIcon paste();
QIcon undo();
QIcon redo();
QIcon selectAll();
QIcon find();
QIcon zoomIn();
QIcon zoomOut();
QIcon resetZoom();

QIcon forName(QString const&);

} // namespace natsu::icon

namespace natsu::bookmark::icon {

QIcon add();
QIcon showToolBar();
QIcon folder();
QIcon bookmarked();
QIcon notBookmarked();

} // namespace natsu::bookmark::icon

#endif // NATSU_ICON_HPP
