#include "application.hpp"

#include "action_manager.hpp"
#include "application_actions.hpp"
#include "command_line_option.hpp"
#include "global.hpp"
#include "message_handler.hpp"
#include "module_manager.hpp"
#include "profile.hpp"
#include "web_dialog_factory.hpp"

#include "browser/browser_window.hpp"
#include "browser/web_dialog.hpp"
#include "misc/pointer.hpp"

#include <QApplication>
#include <QDir>
#include <QMouseEvent>
#include <QStandardPaths>
#include <QString>

namespace natsu {

struct DefaultWebDialogFactory : WebDialogFactory
{
public:
    DefaultWebDialogFactory() = default;

    WebDialog& operator()(BrowserTab& tab) const
    {
        return *make_qmanaged<WebDialog>(tab);
    }
};

static QString
settingPath(QString const& path)
{
    return path + QDir::separator() + QSL("settings.ini");
}

// Application

Application::
Application()
    : m_settings { settingPath(configPath()), QSettings::IniFormat }
    , m_commandLineOption { std::make_unique<CommandLineOption>() }
    , m_actionManager { std::make_unique<ActionManager>(*this) }
    , m_actions { std::make_unique<ApplicationActions>(*this) }
    , m_moduleManager { std::make_unique<ModuleManager>(*this) }
    , m_webDialogFactory { std::make_unique<DefaultWebDialogFactory>() }
{
    m_moduleManager->loadModules();

    qApp->installEventFilter(this);
    //qInstallMessageHandler(natsu::messageHandler);

    m_profiles.push_back(std::make_unique<Profile>(*this, QSL("Default")));
    Q_EMIT profileCreated(*m_profiles.back());
}

Application::~Application() = default;

Profile& Application::
defaultProfile() const
{
    assert(!m_profiles.empty());

    return *m_profiles.front();
}

bool Application::
verbose() const noexcept
{
    return applicationActions().verboseAction().isChecked();
}

BrowserWindow& Application::
activeWindow() const
{
    if (!m_activeWindow) {
        auto& self = const_cast<Application&>(*this);
        m_activeWindow = &self.createWindow();
    }

    return *m_activeWindow;
}

QString Application::
configPath()
{
    auto const& path =
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    assert(!path.isEmpty());

    return path + QDir::separator() + QSL("natsu");
}

BrowserWindow& Application::
createWindow()
{
    // Qt::WA_DeleteOnClose will take care of deletion
    m_windows.push_back(
        new BrowserWindow { *this, defaultProfile() });

    auto& window = *m_windows.back();
    window.show();

    return window;
}

WebDialog& Application::
createWebDialog(BrowserTab& tab)
{
    return (*m_webDialogFactory)(tab);
}

void Application::
setActiveWindow(BrowserWindow& window)
{
    m_activeWindow = &window;
}

void Application::
setRequestInterceptor(QWebEngineUrlRequestInterceptor& interceptor)
{
    for (auto const& profile: m_profiles) {
        profile->setUrlRequestInterceptor(&interceptor);
    }
}

void Application::
setWebDialogFactory(std::unique_ptr<WebDialogFactory> factory)
{
    if (factory) {
        m_webDialogFactory = std::move(factory);
    }
    else {
        m_webDialogFactory = std::make_unique<DefaultWebDialogFactory>();
    }
}

void Application::
exit()
{
    assert(qApp);

    qApp->closeAllWindows();
}

bool Application::
eventFilter(QObject* const obj, QEvent* const ev)
{
    auto& event = to_ref(ev);

    if (event.type() == QEvent::MouseButtonPress) {
        auto& mouseEvent = static_cast<QMouseEvent&>(event);

        m_lastButtons = mouseEvent.buttons();
    }
    else if (event.type() == QEvent::Close) {
        auto* const window = dynamic_cast<BrowserWindow*>(obj);
        if (!window) return false;

        handleWindowCloseEvent(*window, *ev);
    }

    return false;
}

void Application::
handleWindowCloseEvent(BrowserWindow& window, QEvent& ev)
{
    if (m_windows.size() == 1) {
        bool cancel = false;
        Q_EMIT queryClose(cancel);

        if (cancel) {
            ev.ignore();
            return;
        }
    }

    m_windows.erase(
        std::remove(m_windows.begin(), m_windows.end(), &window),
        m_windows.end()
    );

    if (!m_windows.empty() && &window == m_activeWindow) {
        m_activeWindow = m_windows.front();
    }
}

} // namespace natsu
