#ifndef NATSU_CORE_BOOKMARK_HPP
#define NATSU_CORE_BOOKMARK_HPP

#include "misc/integer.hpp"

#include <cstdint>
#include <variant>

#include <QDateTime>
#include <QString>

namespace natsu::bookmark {

using Index = non_negative<int64_t>;
using Size = non_negative<int64_t>;
using Id = non_negative<int64_t>;

struct ReservedId
{
    static constexpr Id Root = 0;
    static constexpr Id BookmarkMenu = 1;
    static constexpr Id BookmarkToolBar = 2;
    static constexpr Id OtherBookmark = 3;
};

struct Bookmark
{
    Id id;
    Id parent;
    Index row;
    QString title;
    QString url;
    QString keyword;
    QString description;
    QString createdTime;
    QString accessedTime;
    QString modifiedTime;
    bool editable;
};

struct Folder
{
    Id id;
    Id parent;
    Index row;
    QString title;
    QString description;
    bool editable;
};

struct Separator
{
    Id id;
    Id parent;
    Index row;
};

using Item = std::variant<Bookmark, Folder, Separator>;

inline bool
isBookmark(Item const& item)
{
    return std::holds_alternative<Bookmark>(item);
}

inline bool
isFolder(Item const& item)
{
    return std::holds_alternative<Folder>(item);
}

inline bool
isSeparator(Item const& item)
{
    return std::holds_alternative<Separator>(item);
}

inline Id
getId(Item const& item)
{
    return std::visit([](auto const& item) { return item.id; }, item);
}

inline void
setId(Item& item, Id const id)
{
    std::visit([&](auto& item) { item.id = id; }, item);
}

inline Id
getParent(Item const& item)
{
    return std::visit([](auto const& item) { return item.parent; }, item);
}

inline void
setParent(Item& item, Id const parent)
{
    std::visit([&](auto& item) { item.parent = parent; }, item);
}

inline Index
getRow(Item const& item)
{
    return std::visit([](auto const& item) { return item.row; }, item);
}

inline void
setRow(Item& item, Index const row)
{
    std::visit([&](auto& item) { item.row = row; }, item);
}

} // namespace natsu::bookmark

#endif // NATSU_CORE_BOOKMARK_HPP
