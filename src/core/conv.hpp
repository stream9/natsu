#ifndef NATSU_CORE_CONV_HPP
#define NATSU_CORE_CONV_HPP

#include <span>
#include <string>
#include <string_view>

#include <boost/numeric/conversion/cast.hpp>

#include <QByteArray>
#include <QString>
#include <QUrl>

namespace natsu {

inline QString
toQString(std::string_view const s)
{
    return QString::fromUtf8(s.data(), boost::numeric_cast<int>(s.size()));
}

inline std::string
toString(QString const& s)
{
    return s.toUtf8().constData();
}

inline std::string
toString(QUrl const& url, QUrl::FormattingOptions const opts = QUrl::FullyEncoded)
{
    auto const bytes = url.toEncoded(opts);
    return { bytes.constData(), boost::numeric_cast<size_t>(bytes.size()) };
}

inline QByteArray
toByteArray(std::span<char const> const data)
{
    return {
        data.data(),
        boost::numeric_cast<int>(data.size())
    };
}

inline std::span<char const>
toByteSpan(QByteArray const& a)
{
    return std::span {
        reinterpret_cast<char const*>(a.constData()),
        boost::numeric_cast<size_t>(a.size())
    };
}

inline int
toInt(qsizetype x)
{
    if (x > INT_MAX) {
        return INT_MAX;
    }
    else if (x < INT_MIN) {
        return INT_MIN;
    }
    else {
        return static_cast<int>(x);
    }
}

} // namespace natsu

#endif // NATSU_CORE_CONV_HPP
