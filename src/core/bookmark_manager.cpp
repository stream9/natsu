#include "bookmark_manager.hpp"

namespace natsu {

using namespace natsu::bookmark;

BookmarkManager::
BookmarkManager(Profile& profile)
    : m_profile { profile }
{}

BookmarkManager::~BookmarkManager() = default;

std::optional<Item> BookmarkManager::
item(Id) const
{
    return std::nullopt;
}

std::vector<Item> BookmarkManager::
items(Id /*parent*/) const
{
    return {};
}

Size BookmarkManager::
count(Id/*parent*/) const
{
    return 0;
}

std::vector<Bookmark> BookmarkManager::
search(QString const&/*keyword*/) const
{
    return {};
}

std::vector<Bookmark> BookmarkManager::
search(QUrl const&) const
{
    return {};
}

bool BookmarkManager::
contain(QUrl const&) const
{
    return false;
}

void BookmarkManager::
addBookmark(Id/*parent*/, QString const&/*title*/, QUrl const&/*url*/,
            QString const&/*keyword*/, QString const&/*description*/)
{}

Id BookmarkManager::
addFolder(Id/*parent*/, QString const&/*title*/, QString const&/*description*/)
{
    return ReservedId::Root;
}

void BookmarkManager::
addSeparator(Id/*parent*/)
{}

void BookmarkManager::
updateItem(Item const&)
{}

void BookmarkManager::
moveItemsBefore(st9::array_view<Id const>, Id) {}

void BookmarkManager::
moveItemsAfter(st9::array_view<Id const>, Id) {}

void BookmarkManager::
moveItemsInto(st9::array_view<Id const>, Id) {}

void BookmarkManager::
removeItem(Id)
{}

} // namespace natsu
