#ifndef NATSU_CORE_WEB_DIALOG_FACTORY_HPP
#define NATSU_CORE_WEB_DIALOG_FACTORY_HPP

namespace natsu {

class BrowserTab;
class WebDialog;

struct WebDialogFactory
{
public:
    virtual ~WebDialogFactory() = default;

    virtual WebDialog& operator()(BrowserTab&) const = 0;
};

} // namespace natsu

#endif // NATSU_CORE_WEB_DIALOG_FACTORY_HPP
