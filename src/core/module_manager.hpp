#ifndef NATSU_MODULE_MANAGER_HPP
#define NATSU_MODULE_MANAGER_HPP

#include <memory>
#include <vector>

#include <QString>

class QPluginLoader;
class QVariant;

namespace natsu {

class Application;

class ModuleManager
{
public:
    ModuleManager(Application&);
    ~ModuleManager();

    // accessor
    Application& application() const { return m_application; }

    // query
    QString modulePath() const;
    QStringList enabledModules() const;

    // command
    void loadModules();

private:
    void loadModule(QString const& name);
    QVariant setting(QString const& key) const;

private:
    Application& m_application;

    std::vector<std::unique_ptr<QPluginLoader>> m_plugins;
};

} // namespace natsu

#endif // NATSU_MODULE_MANAGER_HPP
