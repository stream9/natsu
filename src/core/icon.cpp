#include "icon.hpp"

#include "global.hpp"

#include <QIcon>
#include <QString>

namespace natsu::icon {

QIcon newWindow()
{
    return QIcon::fromTheme(QSL("window-new"));
}

QIcon quit()
{
    return QIcon::fromTheme(QSL("application-exit"));
}

QIcon newTab()
{
    return QIcon::fromTheme(QSL("tab-new"));
}

QIcon defaultFavicon()
{
    return QIcon::fromTheme(QSL("document-new"));
}

QIcon closeTab()
{
    return QIcon::fromTheme(QSL("tab-close"));
}

QIcon back()
{
    return QIcon::fromTheme(QSL("go-previous"));
}

QIcon forward()
{
    return QIcon::fromTheme(QSL("go-next"));
}

QIcon stop()
{
    return QIcon::fromTheme(QSL("dialog-cancel"));
}

QIcon reload()
{
    return QIcon::fromTheme(QSL("view-refresh"));
}

QIcon reloadAndBypassCache()
{
    return QIcon::fromTheme(QSL("reload"));
}

QIcon find()
{
    return QIcon::fromTheme(QSL("edit-find"));
}

QIcon cut()
{
    return QIcon::fromTheme(QSL("edit-cut"));
}

QIcon copy()
{
    return QIcon::fromTheme(QSL("edit-copy"));
}

QIcon paste()
{
    return QIcon::fromTheme(QSL("edit-paste"));
}

QIcon undo()
{
    return QIcon::fromTheme(QSL("edit-undo"));
}

QIcon redo()
{
    return QIcon::fromTheme(QSL("edit-redo"));
}

QIcon selectAll()
{
    return QIcon::fromTheme(QSL("edit-select-all"));
}

QIcon zoomIn()
{
    return QIcon::fromTheme(QSL("zoom-in"));
}

QIcon zoomOut()
{
    return QIcon::fromTheme(QSL("zoom-out"));
}

QIcon resetZoom()
{
    return QIcon::fromTheme(QSL("zoom-original"));
}

QIcon forName(QString const& name)
{
    return QIcon::fromTheme(name);
}

} // namespace natsu::icon

namespace natsu::bookmark::icon {

QIcon add()
{
    return QIcon::fromTheme(QSL("bookmark-new"));
}

QIcon showToolBar()
{
    return QIcon::fromTheme(QSL("bookmark-toolbar"));
}

QIcon folder()
{
    return QIcon::fromTheme(QSL("folder"));
}

QIcon bookmarked()
{
    return QIcon::fromTheme(QSL("rating"));
}

QIcon notBookmarked()
{
    return QIcon::fromTheme(QSL("rating-unrated"));
}

} // namespace natsu::bookmark::icon
