#include "history_manager.hpp"

namespace natsu {

using Entry = history::Entry;

HistoryManager::
HistoryManager(Profile& profile)
    : m_profile { profile }
{}

HistoryManager::~HistoryManager() = default;

std::vector<Entry> HistoryManager::
recentEntries(non_negative<int64_t>/*count*/) const
{
    return {};
}

std::vector<Entry> HistoryManager::
search(QString const&/*keyword*/) const
{
    return {};
}

QIcon HistoryManager::
icon(QUrl const&) const
{
    return {};
}

QString HistoryManager::
completeHost(QString const&/*prefix*/) const
{
    return {};
}

void HistoryManager::
addEntry(QUrl const&, QString const&/*title*/) {}

void HistoryManager::
addIcon(QUrl const&, QIcon const&) {}

void HistoryManager::
removeEntry(Id) {}

} // namespace natsu
