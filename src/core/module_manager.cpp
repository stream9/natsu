#include "module_manager.hpp"

#include "module.hpp"
#include "application.hpp"

#include "global.hpp"

#include <QCoreApplication>
#include <QDir>
#include <QPluginLoader>
#include <QString>
#include <QStringList>
#include <QVariant>

#include <boost/preprocessor/stringize.hpp>

namespace natsu {

static QString
defaultModulePath()
{
    return BOOST_PP_STRINGIZE(MODULE_DIR);
}

ModuleManager::
ModuleManager(Application& app)
    : m_application { app }
{
    auto const& path = modulePath();
    if (!path.isEmpty()) {
        QCoreApplication::addLibraryPath(path);
    }

    QCoreApplication::addLibraryPath(defaultModulePath());
}

ModuleManager::~ModuleManager() = default;

QString ModuleManager::
modulePath() const
{
    return setting(QSL("modulePath")).toString();
}

QStringList ModuleManager::
enabledModules() const
{
    return setting(QSL("enabledModules")).toStringList();
}

void ModuleManager::
loadModules()
{
    for (auto const& name: enabledModules()) {
        loadModule(name);
    }
}

void ModuleManager::
loadModule(QString const& name)
{
    auto loader = std::make_unique<QPluginLoader>(name);

    if (!loader->load()) {
        qWarning() << QSL("Module load error on %1: %2")
            .arg(name)
            .arg(loader->errorString());
        return;
    }

    auto* const plugin = qobject_cast<Module*>(loader->instance());
    if (!plugin) {
        qWarning() << QSL("Module instantiation error on %1: %2")
            .arg(name)
            .arg(loader->errorString());
        return;
    }

    auto const& compiledVersion = plugin->compiledVersion();
    if (compiledVersion != NATSU_VERSION) {
        qWarning() << QSL("Module version mismatch: core %1 vs %2 %3")
            .arg(NATSU_VERSION)
            .arg(name)
            .arg(compiledVersion);
        return;
    }

    if (!plugin->initialize(m_application)) {
        qWarning() << QSL("Module initialize error:") << name;
        return;
    }

    m_plugins.push_back(std::move(loader));
}

QVariant ModuleManager::
setting(QString const& key) const
{
    auto& settings = m_application.settings();
    settings.beginGroup(QSL("ModuleManager"));

    auto const& value = settings.value(key);

    settings.endGroup();

    return value;
}

} // namespace natsu

