#include "get.hpp"

#include "action_manager.hpp"
#include "application.hpp"
#include "bookmark_manager.hpp"
#include "history_manager.hpp"
#include "profile.hpp"
#include "search_engine_manager.hpp"
#include "site_prefs_manager.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "browser/tab_browser.hpp"
#include "browser/web_page.hpp"
#include "browser/web_view.hpp"

namespace natsu::get {

// Application

Application&
application(Profile const& profile)
{
    return profile.application();
}

Application&
application(BookmarkManager const& bookmark)
{
    return application(profile(bookmark));
}

Application&
application(HistoryManager const& history)
{
    return application(profile(history));
}

Application&
application(BrowserWindow const& window)
{
    return window.application();
}

Application&
application(TabBrowser const& browser)
{
    return application(browserWindow(browser));
}

Application&
application(BrowserTab const& tab)
{
    return application(browserWindow(tab));
}

Application&
application(WebView const& view)
{
    return application(browserWindow(view));
}

Application&
application(WebPage const& page)
{
    return application(browserWindow(page));
}

// ActionManager

ActionManager&
actionManager(Application const& app)
{
    return app.actionManager();
}

ActionManager&
actionManager(Profile const& profile)
{
    return actionManager(application(profile));
}

ActionManager&
actionManager(BookmarkManager const& bookmark)
{
    return actionManager(application(bookmark));
}

ActionManager&
actionManager(HistoryManager const& history)
{
    return actionManager(application(history));
}

ActionManager&
actionManager(BrowserWindow const& window)
{
    return actionManager(application(window));
}

ActionManager&
actionManager(TabBrowser const& browser)
{
    return actionManager(application(browser));
}

ActionManager&
actionManager(BrowserTab const& tab)
{
    return actionManager(application(tab));
}

ActionManager&
actionManager(WebView const& view)
{
    return actionManager(application(view));
}

ActionManager&
actionManager(WebPage const& page)
{
    return actionManager(application(page));
}

// BrowserWindow

BrowserWindow&
activeWindow(Application const& app)
{
    return app.activeWindow();
}

BrowserWindow&
activeWindow(Profile const& profile)
{
    return activeWindow(application(profile));
}

BrowserWindow&
activeWindow(HistoryManager const& history)
{
    return activeWindow(application(history));
}

BrowserWindow&
activeWindow(BookmarkManager const& bookmark)
{
    return activeWindow(application(bookmark));
}

BrowserWindow&
browserWindow(TabBrowser const& tabBrowser)
{
    return tabBrowser.browserWindow();
}

BrowserWindow&
browserWindow(BrowserTab const& tab)
{
    return browserWindow(tabBrowser(tab));
}

BrowserWindow&
browserWindow(WebView const& view)
{
    return browserWindow(tabBrowser(view));
}

BrowserWindow&
browserWindow(WebPage const& page)
{
    return browserWindow(tabBrowser(page));
}

// TabBrowser

TabBrowser&
tabBrowser(BrowserWindow const& window)
{
    return window.tabBrowser();
}

TabBrowser&
tabBrowser(BrowserTab const& tab)
{
    return tab.tabBrowser();
}

TabBrowser&
tabBrowser(WebView const& view)
{
    return tabBrowser(browserTab(view));
}

TabBrowser&
tabBrowser(WebPage const& page)
{
    return tabBrowser(browserTab(page));
}

// BrowserTab

BrowserTab&
activeTab(Application const& app)
{
    return tabBrowser(activeWindow(app)).currentTab();
}

BrowserTab&
activeTab(Profile const& profile)
{
    return activeTab(application(profile));
}

BrowserTab&
activeTab(BookmarkManager const& bookmark)
{
    return activeTab(application(bookmark));
}

BrowserTab&
activeTab(HistoryManager const& history)
{
    return activeTab(application(history));
}

BrowserTab&
activeTab(BrowserWindow const& window)
{
    return activeTab(tabBrowser(window));
}

BrowserTab&
activeTab(TabBrowser const& tabBrowser)
{
    return tabBrowser.currentTab();
}

BrowserTab&
browserTab(WebView const& view)
{
    return view.browserTab();
}

BrowserTab&
browserTab(WebPage const& page)
{
    return browserTab(webView(page));
}

// WebView

WebView&
webView(BrowserTab const& tab)
{
    return tab.webView();
}

WebView&
webView(WebPage const& page)
{
    return page.webView();
}

// WebPage

WebPage&
webPage(WebView const& view)
{
    return view.webPage();
}

WebPage&
webPage(BrowserTab const& tab)
{
    return webPage(tab.webView());
}

// Profile

Profile&
profile(BrowserWindow const& window)
{
    return window.profile();
}

Profile&
profile(TabBrowser const& tabBrowser)
{
    return profile(browserWindow(tabBrowser));
}

Profile&
profile(BrowserTab const& tab)
{
    return profile(browserWindow(tab));
}

Profile&
profile(BookmarkManager const& bookmark)
{
    return bookmark.profile();
}

Profile&
profile(HistoryManager const& history)
{
    return history.profile();
}

Profile&
profile(WebView const& view)
{
    return profile(browserWindow(view));
}

Profile&
profile(WebPage const& page)
{
    return profile(browserWindow(page));
}

// HistoryManager

HistoryManager&
history(BrowserWindow const& window)
{
    return history(profile(window));
}

HistoryManager&
history(TabBrowser const& tabBrowser)
{
    return history(profile(tabBrowser));
}

HistoryManager&
history(BrowserTab const& tab)
{
    return history(profile(tab));
}

HistoryManager&
history(Profile const& profile)
{
    return profile.historyManager();
}

HistoryManager&
history(BookmarkManager const& bookmark)
{
    return history(profile(bookmark));
}

HistoryManager&
history(WebView const& view)
{
    return history(profile(view));
}

HistoryManager&
history(WebPage const& page)
{
    return history(profile(page));
}

// SitePrefsManager

SitePrefsManager&
sitePrefs(Profile const& profile)
{
    return profile.sitePrefsManager();
}

SitePrefsManager&
sitePrefs(HistoryManager const& history)
{
    return sitePrefs(profile(history));
}

SitePrefsManager&
sitePrefs(BookmarkManager const& bookmark)
{
    return sitePrefs(profile(bookmark));
}

SitePrefsManager&
sitePrefs(BrowserWindow const& window)
{
    return sitePrefs(profile(window));
}

SitePrefsManager&
sitePrefs(TabBrowser const& browser)
{
    return sitePrefs(profile(browser));
}

SitePrefsManager&
sitePrefs(BrowserTab const& tab)
{
    return sitePrefs(profile(tab));
}

SitePrefsManager&
sitePrefs(WebView const& view)
{
    return sitePrefs(profile(view));
}

SitePrefsManager&
sitePrefs(WebPage const& page)
{
    return sitePrefs(profile(page));
}

// SearchEngineManager

SearchEngineManager&
searchEngine(Profile const& profile)
{
    return profile.searchEngineManager();
}

SearchEngineManager&
searchEngine(HistoryManager const& history)
{
    return searchEngine(profile(history));
}

SearchEngineManager&
searchEngine(BookmarkManager const& bookmark)
{
    return searchEngine(profile(bookmark));
}

SearchEngineManager&
searchEngine(BrowserWindow const& window)
{
    return searchEngine(profile(window));
}

SearchEngineManager&
searchEngine(TabBrowser const& browser)
{
    return searchEngine(profile(browser));
}

SearchEngineManager&
searchEngine(BrowserTab const& tab)
{
    return searchEngine(profile(tab));
}

SearchEngineManager&
searchEngine(WebView const& view)
{
    return searchEngine(profile(view));
}

SearchEngineManager&
searchEngine(WebPage const& page)
{
    return searchEngine(profile(page));
}

// Bookmark
BookmarkManager&
bookmark(Profile const& profile)
{
    return profile.bookmarkManager();
}

BookmarkManager&
bookmark(HistoryManager const& history)
{
    return bookmark(profile(history));
}

BookmarkManager&
bookmark(BrowserWindow const& window)
{
    return bookmark(profile(window));
}

BookmarkManager&
bookmark(TabBrowser const& browser)
{
    return bookmark(profile(browser));
}

BookmarkManager&
bookmark(BrowserTab const& tab)
{
    return bookmark(profile(tab));
}

BookmarkManager&
bookmark(WebView const& view)
{
    return bookmark(profile(view));
}

BookmarkManager&
bookmark(WebPage const& page)
{
    return bookmark(profile(page));
}

} // namespace natsu::get
