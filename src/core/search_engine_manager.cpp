#include "search_engine_manager.hpp"

#include "global.hpp"

#include <QString>

namespace natsu {

QUrl SearchEngineManager::
searchUrl(QString const& query) const
{
    auto url = defaultEngine().url;

    return url.replace(QSL("%s"), query);
}

SearchEngine const* SearchEngineManager::
searchEngine(QString const&/*keyword*/) const
{
    return nullptr;
}

SearchEngine const& SearchEngineManager::
defaultEngine() const
{
    static SearchEngine const engine {
        QSL("google"),
        QSL("https://www.google.com/search?q=%s"),
        QSL("g")
    };

    return engine;
}

bool SearchEngineManager::
setDefaultEngine(QString const&)
{
    return false;
}

} // namespace natsu
