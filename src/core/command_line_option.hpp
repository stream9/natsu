#ifndef NATSU_CORE_COMMAND_LINE_OPTION_HPP
#define NATSU_CORE_COMMAND_LINE_OPTION_HPP

#include <cstdint>

#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QStringList>

namespace natsu {

class CommandLineOption
{
public:
    CommandLineOption();
    ~CommandLineOption();

    // query
    uint16_t remoteDebuggingPort() const;
    bool disableSegVHandler() const;
    QStringList urls() const;

private:
    QCommandLineParser m_parser;
    QCommandLineOption m_remoteDebug;
    QCommandLineOption m_disableSegV;
};

} // namespace natsu

#endif // NATSU_CORE_COMMAND_LINE_OPTION_HPP
