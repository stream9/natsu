#include "command_line_option.hpp"

#include "global.hpp"

#include <QApplication>

namespace natsu {

CommandLineOption::
CommandLineOption()
    : m_remoteDebug {
        QStringList { QSL("remote-debugging-port") },
        QSL("Port for remote debugging"),
        QSL("12345")
    }
    , m_disableSegV {
        QStringList { QSL("c"), QSL("dump") },
        QSL("disable default SEGV handler")
    }
{
    auto const& helpOption = m_parser.addHelpOption();
    auto const& versionOption = m_parser.addVersionOption();

    m_parser.addOption(m_disableSegV);
    m_parser.addOption(m_remoteDebug);
    m_parser.addPositionalArgument(
        QSL("urls"), QSL("URLs to open"), QSL("[urls...]"));

    auto args = qApp->arguments();

    m_parser.parse(args);
    if (m_parser.isSet(helpOption)) {
        m_parser.showHelp();
        qApp->exit();
    }
    else if (m_parser.isSet(versionOption)) {
        m_parser.showVersion();
        qApp->exit();
    }
}

CommandLineOption::~CommandLineOption() = default;

uint16_t CommandLineOption::
remoteDebuggingPort() const
{
   return m_parser.value(m_remoteDebug).toUShort();
}

bool CommandLineOption::
disableSegVHandler() const
{
   return m_parser.isSet(m_disableSegV);
}

QStringList CommandLineOption::
urls() const
{
    return m_parser.positionalArguments();
}

} // namespace natsu
