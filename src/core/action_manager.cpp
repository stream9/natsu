#include "action_manager.hpp"

#include "application.hpp"
#include "application_actions.hpp"
#include "get.hpp"

#include "browser/browser_window.hpp"
#include "global.hpp"

#include <QAction>
#include <QKeySequence>
#include <QString>

namespace natsu {

// ActionManager

ActionManager::
ActionManager(Application& app)
    : m_application { app }
{
}

void ActionManager::
installTo(BrowserWindow& window) const
{
    m_application.applicationActions().installTo(window);
}

void ActionManager::
configure(QString const& category, QAction& action,
          QKeySequence const& defaultShortcut)
{
    assert(!category.isEmpty());

    auto& settings = m_application.settings();

    settings.beginGroup(QSL("ShortcutKey"));
    settings.beginGroup(category);

    auto const& name = action.objectName();
    if (name.isEmpty()) {
        qWarning().noquote()
            << QSL("ActionManager: Can't configure unnamed action:") << category;
        return;
    }

    action.setShortcut(
            settings.value(name, defaultShortcut).value<QKeySequence>());

    settings.endGroup(); // category
    settings.endGroup(); // ShortcutKey
}

} // namespace natsu
