#ifndef NATSU_MODULE_HPP
#define NATSU_MODULE_HPP

#include "global.hpp"

#include <QtPlugin>
#include <QObject>

class QString;

namespace natsu {

class Application;

class Module : public QObject
{
public:
    virtual ~Module() = default;

    // query
    QString compiledVersion() const { return NATSU_VERSION; }

    // command
    bool initialize(Application& app) { return doInitialize(app); }

private:
    virtual bool doInitialize(Application&) = 0;
};

} // namespace natsu::plugin

#define MODULE_IID "org.stream9.natsu.Module"
Q_DECLARE_INTERFACE(natsu::Module, MODULE_IID)

#endif // NATSU_MODULE_HPP
