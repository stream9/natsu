#ifndef NATSU_CORE_BOOKMARK_MANAGER_HPP
#define NATSU_CORE_BOOKMARK_MANAGER_HPP

#include "bookmark.hpp"
#include "namespace.hpp"

#include <stream9/array_view.hpp>

#include <optional>
#include <vector>

#include <QObject>

class QString;
class QUrl;

namespace natsu {

class Profile;

class BookmarkManager : public QObject
{
    Q_OBJECT
public:
    using Id = bookmark::Id;
    using Index = bookmark::Index;
    using Size = bookmark::Size;
    using Bookmark = bookmark::Bookmark;
    using Folder = bookmark::Folder;
    using Item = bookmark::Item;

public:
    BookmarkManager(Profile&);
    ~BookmarkManager() override;

    // accessor
    Profile& profile() const { return m_profile; }

    // query
    virtual std::optional<Item> item(Id) const;
    virtual std::vector<Item> items(Id parent) const;
    virtual Size count(Id parent) const;

    virtual std::vector<Bookmark> search(QString const& keyword) const;
    virtual std::vector<Bookmark> search(QUrl const&) const;

    virtual bool contain(QUrl const&) const;

    // modifier
    virtual void addBookmark(Id parent, QString const& title, QUrl const& url,
                     QString const& keyword, QString const& description);
    virtual Id addFolder(Id parent,
                         QString const& title, QString const& description);
    virtual void addSeparator(Id parent);

    virtual void updateItem(Item const&);
    virtual void moveItemsBefore(st9::array_view<Id const> sources, Id target);
    virtual void moveItemsAfter(st9::array_view<Id const> sources, Id target);
    virtual void moveItemsInto(st9::array_view<Id const> sources, Id folder);
    virtual void removeItem(Id);

    // signals
    Q_SIGNAL void itemAdded(Id parent, Id) const;
    Q_SIGNAL void itemUpdated(Id parent, Id) const;
    Q_SIGNAL void itemRemoved(Id parent, Id, Index row) const;
    Q_SIGNAL void itemMovedBefore(Id source, Id target) const;
    Q_SIGNAL void itemMovedAfter(Id source, Id target) const;
    Q_SIGNAL void itemMovedInto(Id source, Id target) const;
    Q_SIGNAL void settingChanged(QString const& key) const;

private:
    Profile& m_profile;
};

} // namespace natsu

#endif // NATSU_CORE_BOOKMARK_MANAGER_HPP
