#ifndef NATSU_HISTORY_MANAGER_HPP
#define NATSU_HISTORY_MANAGER_HPP

#include "misc/integer.hpp"
#include "core/history.hpp"

#include <vector>

class QIcon;
class QString;
class QUrl;

namespace natsu {

class Profile;

class HistoryManager
{
public:
    using Id = history::Id;
    using Entry = history::Entry;

public:
    HistoryManager(Profile&);
    virtual ~HistoryManager();

    // accessor
    Profile& profile() const { return m_profile; }

    // query
    virtual std::vector<Entry>
        recentEntries(non_negative<int64_t> count) const;

    virtual std::vector<Entry> search(QString const& keyword) const;

    virtual QIcon icon(QUrl const&) const;

    virtual QString completeHost(QString const& prefix) const;

    // modifier
    virtual void addEntry(QUrl const&, QString const& title);
    virtual void addIcon(QUrl const&, QIcon const&);

    virtual void removeEntry(Id);

private:
    Profile& m_profile;
};

} // namespace natsu

#endif // NATSU_HISTORY_MANAGER_HPP
