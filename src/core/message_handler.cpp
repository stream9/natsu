#include "message_handler.hpp"

#include <QMessageLogContext>
#include <QString>

#include <cstdio>

namespace natsu {

void
messageHandler(QtMsgType const type,
               QMessageLogContext const& context, QString const& msg)
{
    auto const& message = msg.toLocal8Bit();

    switch (type) {
        case QtDebugMsg:
            fprintf(stderr, "Debug: %s (%s:%u, %s)\n",
                    message.constData(),
                    context.file, context.line, context.function);
            break;
        case QtInfoMsg:
            fprintf(stderr, "Info: %s\n", message.constData());
            break;
        case QtWarningMsg:
            fprintf(stderr, "Warning: %s\n", message.constData());
            break;
        case QtCriticalMsg:
            fprintf(stderr, "Critical: %s\n", message.constData());
            break;
        case QtFatalMsg:
            fprintf(stderr, "Fatal: %s\n", message.constData());
            abort();
    }
}

} // namespace natsu
