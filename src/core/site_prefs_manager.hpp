#ifndef NATSU_SITE_PREFS_MANAGER_HPP
#define NATSU_SITE_PREFS_MANAGER_HPP

#include <QString>
#include <QUrl>
#include <QVariant>

namespace natsu {

class SitePrefsManager
{
public:
    virtual ~SitePrefsManager() = default;

    QVariant getValue(QUrl const& url, QString const& key) const
    {
        return doGetValue(url, key);
    }

    void setValue(QUrl const& url,
                  QString const& key, QVariant const& value)
    {
        doSetValue(url, key, value);
    }

    void removeValue(QUrl const& url, QString const& key)
    {
        doRemoveValue(url, key);
    }

private:
    virtual QVariant doGetValue(QUrl const&, QString const& key) const;

    virtual void doSetValue(QUrl const&,
            QString const& key, QVariant const& value);

    virtual void doRemoveValue(QUrl const&, QString const& key);
};

} // namespace natsu

#endif // NATSU_SITE_PREFS_MANAGER_HPP
