#ifndef NATSU_CORE_MESSAGE_HANDLER_HPP
#define NATSU_CORE_MESSAGE_HANDLER_HPP

#include <QtGlobal>

class QString;
class QMessageLogContext;

namespace natsu {

void messageHandler(
        QtMsgType const, QMessageLogContext const&, QString const& msg);

} // namespace natsu


#endif // NATSU_CORE_MESSAGE_HANDLER_HPP
