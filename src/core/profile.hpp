#ifndef NATSU_PROFILE_HPP
#define NATSU_PROFILE_HPP

#include "mime/fwd/scheme_handler.hpp"

#include <memory>

#include <QSettings>
#include <QWebEngineProfile>

class QDir;
class QString;

namespace natsu {

class Application;
class BookmarkManager;
class BrowserTab;
class BrowserWindow;
class HistoryManager;
class SearchEngineManager;
class SitePrefsManager;

class Profile : public QWebEngineProfile
{
    Q_OBJECT
public:
    Profile(Application&, QString const& name);
    ~Profile();

    // accessor
    Application& application() const { return m_application; }
    QSettings& settings() { return m_settings; }
    BookmarkManager& bookmarkManager() const { return *m_bookmarkManager; }
    SitePrefsManager& sitePrefsManager() const { return *m_sitePrefs; }
    SearchEngineManager& searchEngineManager() const { return *m_searchEngineManager; }
    HistoryManager& historyManager() const { return *m_historyManager; }

    // query
    QString path() const;
    QString settingPath() const;
    QDir pluginDir() const;

    // modifier
    void setSitePrefsManager(std::unique_ptr<SitePrefsManager>);
    void setSearchEngineManager(std::unique_ptr<SearchEngineManager>);
    void setHistoryManager(std::unique_ptr<HistoryManager>);
    void setBookmarkManager(std::unique_ptr<BookmarkManager>);

    // signal
    Q_SIGNAL void windowCreated(BrowserWindow&);

private:
    Application& m_application;

    QSettings m_settings;

    std::unique_ptr<SitePrefsManager> m_sitePrefs; // non-null
    std::unique_ptr<SearchEngineManager> m_searchEngineManager; // non-null
    std::unique_ptr<HistoryManager> m_historyManager; // non-null
    std::unique_ptr<BookmarkManager> m_bookmarkManager; // non-null
    std::unique_ptr<mime::SchemeHandler> m_schemeHandler;
};

} // namespace natsu

#endif // NATSU_PROFILE_HPP
