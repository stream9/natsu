#ifndef NATSU_CORE_ERROR_HPP
#define NATSU_CORE_ERROR_HPP

#include "namespace.hpp"

#include <stream9/errors.hpp>

namespace natsu {

using st9::error;
using st9::print_error;
using st9::rethrow_error;

} // namespace natsu

#endif // NATSU_CORE_ERROR_HPP
