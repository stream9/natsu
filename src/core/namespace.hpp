#ifndef NATSU_CORE_NAMESPACE_HPP
#define NATSU_CORE_NAMESPACE_HPP

namespace boost::container {}
namespace std::filesystem {}
namespace std::ranges {}
namespace stream9::ranges {}
namespace stream9::strings {}

namespace natsu {

namespace st9 = stream9;
namespace con { using namespace boost::container; }
namespace fs { using namespace std::filesystem; }
namespace rng { using namespace std::ranges; }
namespace rng { using namespace stream9::ranges; }
namespace str { using namespace stream9::strings; }

} // namespace natsu

#endif // NATSU_CORE_NAMESPACE_HPP
