#ifndef NATSU_CORE_SEARCH_ENGINE_MANAGER_HPP
#define NATSU_CORE_SEARCH_ENGINE_MANAGER_HPP

#include <QUrl>

class QString;

namespace natsu {

struct SearchEngine {
    QString name;
    QString url;
    QString keyword;
};

class SearchEngineManager
{
public:
    virtual ~SearchEngineManager() = default;

    // query
    virtual QUrl searchUrl(QString const& query) const;

    virtual SearchEngine const* searchEngine(QString const& keyword) const;
    virtual SearchEngine const& defaultEngine() const;

    // modifier
    virtual bool setDefaultEngine(QString const& name);
};

} // namespace natsu

#endif // NATSU_CORE_SEARCH_ENGINE_MANAGER_HPP
