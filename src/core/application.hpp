#ifndef NATSU_APPLICATION_HPP
#define NATSU_APPLICATION_HPP

#include <memory>
#include <vector>

#include <QObject>
#include <QSettings>

class QEvent;
class QString;
class QWebEngineUrlRequestInterceptor;

namespace natsu {

class ActionManager;
class ApplicationActions;
class BrowserTab;
class BrowserWindow;
class CommandLineOption;
class ModuleManager;
class PluginManager;
class Profile;
class WebDialog;
class WebDialogFactory;

class Application : public QObject
{
    Q_OBJECT
public:
    Application();
    ~Application();

    // accessor
    QSettings& settings() { return m_settings; }

    CommandLineOption& commandLineOption() const { return *m_commandLineOption; }
    ActionManager& actionManager() const { return *m_actionManager; }
    ApplicationActions& applicationActions() const { return *m_actions; }
    ModuleManager& moduleManager() const { return *m_moduleManager; }

    Profile& defaultProfile() const;
    auto const& profiles() const { return m_profiles; }

    auto const& windows() const { return m_windows; }
    Qt::MouseButtons lastButtons() const { return m_lastButtons; }

    bool verbose() const noexcept;

    // query
    BrowserWindow& activeWindow() const;
    static QString configPath();

    // modifier
    BrowserWindow& createWindow();
    WebDialog& createWebDialog(BrowserTab&);

    void setActiveWindow(BrowserWindow&);
    void setRequestInterceptor(QWebEngineUrlRequestInterceptor&);
    void setWebDialogFactory(std::unique_ptr<WebDialogFactory>);

    // command
    void exit();

    // signal
    Q_SIGNAL void profileCreated(Profile&);
    Q_SIGNAL void queryClose(bool& cancel) const;

    // override QObject
    bool eventFilter(QObject*, QEvent*) override;

private:
    void handleWindowCloseEvent(BrowserWindow&, QEvent&);

private:
    QSettings m_settings;

    std::unique_ptr<CommandLineOption> const m_commandLineOption; // non null
    std::unique_ptr<ActionManager> const m_actionManager; //non null
    std::unique_ptr<ApplicationActions> const m_actions; // non null
    std::unique_ptr<ModuleManager> const m_moduleManager; // non null

    using ProfilePtr = std::unique_ptr<Profile>;
    std::vector<ProfilePtr> m_profiles;

    std::vector<BrowserWindow*> m_windows;

    mutable BrowserWindow* m_activeWindow;
    Qt::MouseButtons m_lastButtons = Qt::NoButton;

    std::unique_ptr<WebDialogFactory> m_webDialogFactory; // not null, mutable
};

} // namespace natsu

#endif // NATSU_APPLICATION_HPP
