#ifndef NATSU_CORE_ACTION_MANAGER_HPP
#define NATSU_CORE_ACTION_MANAGER_HPP

class QAction;
class QKeySequence;
class QString;

namespace natsu {

class Application;
class BrowserWindow;

class ActionManager
{
public:
    ActionManager(Application&);

    // accessor
    Application& application() const { return m_application; }

    // command
    void installTo(BrowserWindow&) const;

    void configure(QString const& category, QAction&,
                   QKeySequence const& defaultShortcut);
private:
    Application& m_application;
};

template<typename T>
int
makeKeyCode(T key) noexcept
{
    return static_cast<int>(key);
}

template<typename T, typename... Ts>
int
makeKeyCode(T key, Ts... rest) noexcept
{
    return static_cast<int>(key) + makeKeyCode(rest...);
}

} // namespace natsu

#endif // NATSU_CORE_ACTION_MANAGER_HPP
