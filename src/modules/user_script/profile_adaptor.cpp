#include "profile_adaptor.hpp"

#include "grease_monkey_api.hpp"
#include "script_file.hpp"
#include "script_storage.hpp"

#include "browser/browser_window.hpp"
#include "browser/web_page.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QDir>
#include <QSettings>
#include <QWebChannel>
#include <QWebEngineScript>
#include <QWebEngineScriptCollection>

namespace natsu::modules::user_script {

static QString
databasePath(Profile& profile)
{
    auto const& dir = profile.pluginDir();
    assert(dir.exists());

    return dir.filePath(QSL("user_script.db"));
}

ProfileAdaptor::
ProfileAdaptor(Profile& profile)
    : QObject { &profile }
    , m_profile { profile }
    , m_db { std::make_unique<ScriptStorage>(databasePath(profile)) }
    , m_gm { std::make_unique<GreaseMonkeyApi>(*m_db) }
{
    auto const& dir = scriptDir();
    reloadDirectory(dir);

    this->connect(&m_profile, &Profile::windowCreated,
                  this, [&](auto& win) {
                      this->connect(&win, &BrowserWindow::tabCreated,
                                    this, &ProfileAdaptor::onTabCreated);
                  });

    this->connect(&m_fsWatcher, &QFileSystemWatcher::directoryChanged,
                  this,         &ProfileAdaptor::onDirectoryChanged);
    this->connect(&m_fsWatcher, &QFileSystemWatcher::fileChanged,
                  this,         &ProfileAdaptor::onFileChanged);
}

ProfileAdaptor::~ProfileAdaptor() = default;

QSettings ProfileAdaptor::
settings() const
{
    auto const& dir = m_profile.pluginDir();
    assert(dir.exists());

    return { dir.filePath(QSL("user_script.ini")), QSettings::IniFormat };
}

QString ProfileAdaptor::
scriptDir() const
{
    return settings().value(QSL("ScriptDir")).toString();
}

ScriptFile* ProfileAdaptor::
findFile(QString const& path)
{
    auto const it = std::find_if(
        m_files.begin(), m_files.end(),
        [&](auto const& file) {
            return file.path() == path;
        }
    );

    return it != m_files.end() ? &*it : nullptr;
}

void ProfileAdaptor::
addFile(QString const& path)
{
    m_fsWatcher.addPath(path);
    m_files.emplace_back(path);

    auto const& file = m_files.back();
    if (file.isValid()) {
        auto& scripts = to_ref(m_profile.scripts());

        scripts.insert(file.script());
    }
}

void ProfileAdaptor::
removeFile(ScriptFile const& file)
{
    m_fsWatcher.removePath(file.path());

    auto& scripts = to_ref(m_profile.scripts());
    scripts.remove(file.script());

    auto const it = std::remove_if(
        m_files.begin(), m_files.end(),
        [&](auto const& f) {
            return &f == &file;
        }
    );
    m_files.erase(it, m_files.end());
}

void ProfileAdaptor::
clearFiles()
{
    auto& scripts = to_ref(m_profile.scripts());

    for (auto const& file: m_files) {
        scripts.remove(file.script());

        m_fsWatcher.removePath(file.path());
    }

    m_files.clear();
}

void ProfileAdaptor::
reloadDirectory(QString const& path)
{
    clearFiles();

    QDir const dir { path };
    if (!dir.exists()) {
        m_fsWatcher.removePath(path);
        return;
    }

    m_fsWatcher.addPath(path);

    QStringList nameFilter;
    nameFilter << QSL("*.user.js");

    auto const filter = QDir::Files | QDir::Readable;

    for (auto const& fileName: dir.entryList(nameFilter, filter)) {
        auto const& filePath = dir.filePath(fileName);

        addFile(filePath);
    }
}

void ProfileAdaptor::
onTabCreated(BrowserTab& tab)
{
    auto& page = get::webPage(tab);

    auto* channel = page.webChannel();
    if (!channel) {
        channel = new QWebChannel(this);
        page.setWebChannel(channel, QWebEngineScript::MainWorld);
        //page.setWebChannel(channel, QWebEngineScript::ApplicationWorld);
    }

    channel->registerObject("userScript", m_gm.get());
}

void ProfileAdaptor::
onDirectoryChanged(QString const& path)
{
    reloadDirectory(path);
}

void ProfileAdaptor::
onFileChanged(QString const& path)
{
    auto const& file = findFile(path);
    if (!file) {
        qWarning() << QSL("user_script: detect chage from unknown path:")
                   << path;
        return;
    }

    if (!file->exists()) { // removed or renamed
        removeFile(*file);
        return;
    }

    file->reload(); // modified

    auto& scripts = to_ref(m_profile.scripts());
    auto const& script = file->script();

    if (file->isValid()) {
        if (!scripts.contains(script)) {
            scripts.insert(script);
        }
    }
    else {
        scripts.remove(script);
    }
}

} // namespace natsu::modules::user_script
