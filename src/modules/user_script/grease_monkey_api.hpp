#ifndef NATSU_MODULES_USER_SCRIPT_GREASE_MONKEY_API_HPP
#define NATSU_MODULES_USER_SCRIPT_GREASE_MONKEY_API_HPP

#include <QObject>

class QString;

namespace natsu::modules::user_script {

class ScriptStorage;

class GreaseMonkeyApi : public QObject
{
    Q_OBJECT
public:
    GreaseMonkeyApi(ScriptStorage&);
    ~GreaseMonkeyApi() override;

    Q_INVOKABLE QString getValue(QString const& scriptId,
                                 QString const& name);

    Q_INVOKABLE void setValue(QString const& scriptId,
                              QString const& name,
                              QString const& value);

    Q_INVOKABLE QString listValues(QString const& scriptId);

    Q_INVOKABLE void deleteValue(QString const& scriptId,
                                 QString const& name);

private:
    ScriptStorage& m_db;
};

} // namespace natsu::modules::user_script

#endif // NATSU_MODULES_USER_SCRIPT_GREASE_MONKEY_API_HPP
