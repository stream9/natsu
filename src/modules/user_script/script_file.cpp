#include "script_file.hpp"

#include "global.hpp"

#include <QFile>
#include <QRegularExpression>

namespace natsu::modules::user_script {

static QString
webChannelScript()
{
    QFile file { QSL(":/qtwebchannel/qwebchannel.js") };
    auto const ok = file.open(QFile::ReadOnly);
    assert(ok); (void)ok;

    return file.readAll();
}

static QString
gmScript(QString const& name, std::vector<QString> const& grants)
{
    if (grants.empty()) return {};

    QFile file { ":/user_script/gm.js" };
    auto const ok = file.open(QFile::ReadOnly);
    assert(ok); (void)ok;

    QString grantArray = "[ ";
    for (auto const& grant: grants) {
        grantArray += "'" + grant + "', ";
    }
    grantArray += "]";

    auto const& script = QSL(
            "(function(global, scriptName, grants) { "
            "  if (typeof GM !== 'undefined') return; "
            "  %1 %2 "
            "})(this, '%3', %4); "
        )
        .arg(webChannelScript())
        .arg(QString(file.readAll()))
        .arg(name)
        .arg(grantArray);

    return script;
}

// ScriptFile

ScriptFile::
ScriptFile(QString const& path)
    : m_path { path }
    , m_error {}
{
    reload();
}

ScriptFile::~ScriptFile() = default;

bool ScriptFile::
exists() const
{
    return QFile::exists(m_path);
}

QString ScriptFile::
name() const
{
    return metaData(QSL("name"));
}

QString ScriptFile::
metaData(QString const& name) const
{
    auto const it = m_metaData.find(name);

    return it != m_metaData.end() ? it->second : QString();
}

bool ScriptFile::
isValid() const
{
    return m_error.isEmpty();
}

void ScriptFile::
reload()
{
    auto const& content = loadFile();
    if (!isValid()) return;

    extractMetaData(content);

    auto const& wrap = QSL("(function() {\n%1\n})();");

    auto const& text = gmScript(name(), grants()) + wrap.arg(content);

    m_script.setWorldId(QWebEngineScript::MainWorld);
    //m_script.setWorldId(QWebEngineScript::ApplicationWorld); //TODO hide from main world
    m_script.setInjectionPoint(QWebEngineScript::DocumentReady);
    m_script.setSourceCode(text);
}

QString ScriptFile::
loadFile()
{
    QFile file { m_path };
    if (!file.exists()) {
        m_error = QSL("File doesn't exist");
        return {};
    }

    if (!file.open(QFile::ReadOnly)) {
        m_error = QSL("Can't open file");
        return {};
    }

    QString content { file.readAll() };
    content.replace(QSL("\r\n"), QSL("\n"));

    return content;
}

void ScriptFile::
extractMetaData(QString const& content)
{
    assert(isValid());

    using RegExp = QRegularExpression;

    static RegExp const blockRe { QSL(
            "^// ==UserScript==$"
            "(.*)"
            "^// ==/UserScript==$"
        ),
        RegExp::MultilineOption | RegExp::DotMatchesEverythingOption
    };

    auto const& match = blockRe.match(content);
    if (!match.hasMatch()) {
        m_error = QSL("No Metadata");
        return;
    }

    auto const& block = match.captured(1);
    auto const& lines = block.split('\n', Qt::SkipEmptyParts);

    for (auto const& line: lines) {
        static RegExp const re { QSL(
            "^// @([^\\s]+)\\s+(.+)\\s*$"
        )};

        auto const& match = re.match(line);
        if (!match.hasMatch()) continue;

        auto const& key = match.captured(1);
        auto const& value = match.captured(2);

        m_metaData.emplace(key, value);
    }

    if (m_metaData.count(QSL("name")) == 0) {
        m_error = QSL("No name");
    }
}

std::vector<QString> ScriptFile::
grants() const
{
    std::vector<QString> result;

    auto const [lower, upper] = m_metaData.equal_range(QSL("grant"));
    for (auto it = lower; it != upper; ++it) {
        auto const name = it->second;

        if (name == QSL("none")) {
            result.clear();
            break;
        }
        else if (name == QSL("GM.setValue")
         || name == QSL("GM.getValue")
         || name == QSL("GM.listValues")
         || name == QSL("GM.deleteValue") )
        {
            result.push_back(it->second);
        }
    }

    return result;
}

} // namespace natsu::modules::user_script
