#include "script_storage.hpp"

#include "global.hpp"
#include "core/conv.hpp"

namespace natsu::modules::user_script {

// ScriptStorage

ScriptStorage::
ScriptStorage(QString const& path)
    : m_db { toString(path) }
{
    prepareTables();
}

ScriptStorage::~ScriptStorage() = default;

std::optional<std::string_view> ScriptStorage::
getValue(QString const& scriptId, QString const& name)
{
    static auto query = m_db.prepare(
        "SELECT value "
        "FROM storage "
        "WHERE script_id = :script_id "
        "  AND name = :name "
    );

    query.exec(toString(scriptId), toString(name));

    std::optional<std::string_view> result;

    if (!query.done()) {
        result = query.current_row().get_text(0);
    }

    return result;
}

void ScriptStorage::
setValue(QString const& scriptId,
         QString const& name, QString const& value)
{
    static auto stmt = m_db.prepare(
        "INSERT OR REPLACE INTO storage ( "
        "    script_id, name, value, last_update_time "
        ") VALUES ( "
        "    :script_id, :name, :value, strftime('%s', 'now') "
        ") "
    );

    stmt.exec(toString(scriptId), toString(name), toString(value));
}

int ScriptStorage::
deleteValue(QString const& scriptId, QString const& name)
{
    static auto stmt = m_db.prepare(
        "DELETE FROM storage "
        " WHERE script_id = :script_id "
        "   AND name = :name "
    );

    stmt.exec(toString(scriptId), toString(name));

    return m_db.changes();
}

void ScriptStorage::
getNames(QString const& scriptId,
         function_ref<bool(std::string_view)> cb)
{
    static auto query = m_db.prepare(
        "SELECT name "
        "FROM storage "
        "WHERE script_id = :script_id "
    );

    query.exec(toString(scriptId));

    for (auto const row: query) {
        auto const name = row.get_text(0);

        if (!cb(name)) break;
    }
}

void ScriptStorage::
prepareTables()
{
    m_db.exec(
        "CREATE TABLE IF NOT EXISTS storage ( "
        "     id INTEGER PRIMARY KEY "
        "   , script_id TEXT "
        "   , name TEXT "
        "   , value TEXT "
        "   , last_update_time INTEGER "
        ") "
    );
}

} // namespace natsu::modules::user_script
