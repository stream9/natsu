#ifndef NATSU_MODULES_USER_SCRIPT_SCRIPT_STORAGE_HPP
#define NATSU_MODULES_USER_SCRIPT_SCRIPT_STORAGE_HPP

#include <string_view>

#include <QString>

#include <stream9/function_ref.hpp>
#include <stream9/sqlite.hpp>

namespace natsu::modules::user_script {

namespace sqlite = stream9::sqlite;

using stream9::function_ref;

class ScriptStorage
{
public:
    ScriptStorage(QString const& path);
    ~ScriptStorage();

    std::optional<std::string_view>
        getValue(QString const& scriptId, QString const& name);

    void setValue(QString const& scriptId,
                  QString const& name, QString const& value);

    int deleteValue(QString const& scriptId, QString const& name);

    void getNames(QString const& scriptId,
                  function_ref<bool(std::string_view)>);

private:
    void prepareTables();

private:
    sqlite::database m_db;
};

} // namespace natsu::modules::user_script

#endif // NATSU_MODULES_USER_SCRIPT_SCRIPT_STORAGE_HPP
