// @param global global object
// @param QWebChannel
// @param scriptName
// @oaram grants name of functions to export
//
"use strict";

new QWebChannel(qt.webChannelTransport,
  function (channel) {
    const GM = channel.objects.userScript;

    function getValue(name, default_) {
      return new Promise((resolve, reject) => {
        GM.getValue(scriptName, name, (value) => {
          if (value) {
            resolve(JSON.parse(value));
          }
          else if (default_) {
            resolve(default_);
          }
          else {
            resolve(undefined);
          }
        })
      });
    }

    global.GM = {};

    /*
    if (grants.includes("GM.getValue")) {
      global.GM.getValue = getValue;
    }
    */
    global.GM.getValue = getValue;

    function setValue(name, value) {
      GM.setValue(scriptName, name, JSON.stringify(value));
      return Promise.resolve();
    }

    /*
    if (grants.includes("GM.setValue")) {
      global.GM.setValue = setValue;
    }
    */
    global.GM.setValue = setValue;

    function listValues()
    {
      return new Promise((resolve, reject) => {
        GM.listValues(scriptName, (values) => {
          resolve(JSON.parse(values));
        });
      });
    }

    /*
    if (grants.includes("GM.listValues")) {
      global.GM.listValues = listValues;
    }
    */
    global.GM.listValues = listValues;

    function deleteValue(name) {
      GM.deleteValues(scriptName, name);

      return Promise.resolve();
    }

    /*
    if (grants.includes("GM.deleteValue")) {
      global.GM.deleteValue = deleteValue;
    }
    */
    global.GM.deleteValue = deleteValue;

    console.log("GM functions injected");
  }
);
