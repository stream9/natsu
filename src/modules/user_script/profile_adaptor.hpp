#ifndef NATSU_MODULES_USER_SCRIPT_PROFILE_ADAPTOR_HPP
#define NATSU_MODULES_USER_SCRIPT_PROFILE_ADAPTOR_HPP

#include "browser/fwd/browser_tab.hpp"
#include "core/fwd/profile.hpp"

#include <memory>

#include <QObject>
#include <QFileSystemWatcher>

class QDir;
class QSettings;

namespace natsu::modules::user_script {

class GreaseMonkeyApi;
class ScriptFile;
class ScriptStorage;

class ProfileAdaptor : public QObject
{
    Q_OBJECT
public:
    ProfileAdaptor(Profile&);
    ~ProfileAdaptor() override;

    // accessor
    Profile& profile() const { return m_profile; }

private:
    QSettings settings() const;
    QString scriptDir() const;

    ScriptFile* findFile(QString const& path);
    void addFile(QString const& path);
    void removeFile(ScriptFile const&);
    void clearFiles();

    void reloadDirectory(QString const& path);

    Q_SLOT void onTabCreated(BrowserTab&);
    Q_SLOT void onDirectoryChanged(QString const& path);
    Q_SLOT void onFileChanged(QString const& path);

private:
    Profile& m_profile;

    std::vector<ScriptFile> m_files;
    QFileSystemWatcher m_fsWatcher;
    std::unique_ptr<ScriptStorage> m_db; // non-null
    std::unique_ptr<GreaseMonkeyApi> m_gm; // non-null
};

} // namespace natsu::modules::user_script

#endif // NATSU_MODULES_USER_SCRIPT_PROFILE_ADAPTOR_HPP
