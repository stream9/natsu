#ifndef NATSU_MODULES_USER_SCRIPT_SCRIPT_FILE_HPP
#define NATSU_MODULES_USER_SCRIPT_SCRIPT_FILE_HPP

#include <vector>

#include <boost/container/flat_map.hpp>

#include <QString>
#include <QWebEngineScript>

namespace natsu::modules::user_script {

class ScriptFile
{
public:
    ScriptFile(QString const& path);
    ~ScriptFile();

    // accessor
    QWebEngineScript& script() { return m_script; }
    QWebEngineScript const& script() const { return m_script; }
    QString const& path() const { return m_path; }

    // query
    bool exists() const;
    QString name() const;
    QString metaData(QString const&) const;

    bool isValid() const;

    // command
    void reload();

private:
    QString loadFile();
    void extractMetaData(QString const& content);
    std::vector<QString> grants() const;

private:
    QWebEngineScript m_script;
    QString m_path;
    QString m_error;
    boost::container::flat_multimap<QString, QString> m_metaData;
};

} // namespace natsu::modules::user_script

#endif // NATSU_MODULES_USER_SCRIPT_SCRIPT_FILE_HPP
