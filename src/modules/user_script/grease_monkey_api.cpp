#include "grease_monkey_api.hpp"

#include "script_storage.hpp"

#include "global.hpp"
#include "core/conv.hpp"

#include <QVariant>

namespace natsu::modules::user_script {

GreaseMonkeyApi::
GreaseMonkeyApi(ScriptStorage& db)
    : m_db { db }
{}

GreaseMonkeyApi::~GreaseMonkeyApi() = default;

QString GreaseMonkeyApi::
getValue(QString const& scriptId,
         QString const& name)
{
    auto const o_value = m_db.getValue(scriptId, name);
    if (o_value) {
        return toQString(*o_value);
    }
    else {
        return {};
    }
}

void GreaseMonkeyApi::
setValue(QString const& scriptId,
         QString const& name,
         QString const& value)
{
    m_db.setValue(scriptId, name, value);
}

QString GreaseMonkeyApi::
listValues(QString const& scriptId)
{
    QString result = QSL("[ ");
    bool first = true;

    m_db.getNames(scriptId,
        [&](auto name) {
            if (first) {
                result += QSL("\"%1\"").arg(toQString(name));
                first = false;
            }
            else {
                result += QSL(", \"%1\"").arg(toQString(name));
            }

            return true;
        });

    result += QSL(" ]");

    return result;
}

void GreaseMonkeyApi::
deleteValue(QString const& scriptId,
            QString const& name)
{
    m_db.deleteValue(scriptId, name);
}

} // namespace natsu::module
