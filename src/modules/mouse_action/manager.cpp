#include "manager.hpp"

#include "core/application.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QAction>
#include <QDir>
#include <QSettings>
#include <QWidget>

namespace natsu::modules::mouse_action {

static bool
operator<(MouseAction const& lhs, MouseAction const& rhs)
{
    return lhs.modifier == rhs.modifier ? lhs.button < rhs.button
                                        : lhs.modifier < rhs.modifier;
}

static QString
toString(Qt::KeyboardModifiers const modifiers)
{
    QStringList elements;

    if (modifiers & Qt::ShiftModifier) {
        elements += QSL("Shift");
    }
    if (modifiers & Qt::ControlModifier) {
        elements += QSL("Ctrl");
    }
    if (modifiers & Qt::AltModifier) {
        elements += QSL("Alt");
    }
    if (modifiers & Qt::MetaModifier) {
        elements += QSL("Meta");
    }

    return elements.join(QSL(" + "));
}

static Qt::KeyboardModifiers
toModifiers(QString const& str)
{
    Qt::KeyboardModifiers modifiers;

    for (auto& el: str.split(QSL("+"))) {
        auto const& element = el.trimmed().toLower();

        if (element == QSL("shift")) {
            modifiers |= Qt::ShiftModifier;
        }
        else if (element == QSL("ctrl")) {
            modifiers |= Qt::ControlModifier;
        }
        else if (element == QSL("alt")) {
            modifiers |= Qt::AltModifier;
        }
        else if (element == QSL("meta")) {
            modifiers |= Qt::MetaModifier;
        }
        else {
            qWarning() << "unknown modifier in mouse_action.ini:" << el;
        }
    }

    return modifiers;
}

static QString
toString(Qt::MouseButton const button)
{
    switch (button) {
        case Qt::LeftButton:
            return QSL("Left");
        case Qt::RightButton:
            return QSL("Right");
        case Qt::MiddleButton:
            return QSL("Middle");
        case WheelUp:
            return QSL("WheelUp");
        case WheelDown:
            return QSL("WheelDown");
        default:
            qWarning() << "unsupported button:" << button;
            return {};
    }
}

static Qt::MouseButton
toMouseButton(QString const& str)
{
    auto const& string = str.toLower().trimmed();

    if (string == QSL("left")) {
        return Qt::LeftButton;
    }
    else if (string == QSL("right")) {
        return Qt::RightButton;
    }
    else if (string == QSL("middle")) {
        return Qt::MiddleButton;
    }
    else if (string == QSL("wheelup")) {
        return WheelUp;
    }
    else if (string == QSL("wheeldown")) {
        return WheelDown;
    }
    else {
        qWarning() << "unknown button in mouse_action.ini:" << str;
        return Qt::NoButton;
    }
}

// Manager

Manager::
Manager(Application& app)
    : m_app { app }
{
    loadSettings();
}

Manager::~Manager() = default;

QAction* Manager::
findAction(QWidget& widget, Qt::KeyboardModifiers const modifier,
                            Qt::MouseButton const button) const
{
    auto const it = m_actions.find(MouseAction { modifier, button });

    if (it == m_actions.end()) {
        return nullptr;
    }
    else {
        auto const& name = it->second;

        auto const& actions = findActions(widget, name);
        if (actions.empty()) {
            return nullptr;
        }
        else if (actions.size() == 1) {
            return actions.front();
        }
        else {
            qWarning() << "Ambiguous mouse action for" << name << actions.size();
            return nullptr;
        }
    }
}

bool Manager::
addAction(Qt::KeyboardModifiers const modifier,
          Qt::MouseButton const button, QString const& name)
{
    auto const rv =
        m_actions.emplace(MouseAction { modifier, button }, name);

    return rv.second;
}

bool Manager::
removeAction(Qt::KeyboardModifiers const modifier, Qt::MouseButton const button)
{
    auto const count = m_actions.erase(MouseAction { modifier, button });

    return count > 0;
}

QSettings Manager::
settings() const
{
    auto const& path =
        m_app.configPath() + QDir::separator() + QSL("mouse_action.ini");

    return { path, QSettings::IniFormat };
}

void Manager::
loadSettings()
{
    auto settings = this->settings();

    auto const count = settings.beginReadArray(QSL("MouseAction"));
    for (auto i = 0; i < count; ++i) {
        settings.setArrayIndex(i);

        auto const modifier =
            toModifiers(settings.value(QSL("modifier")).toString());
        auto const button =
            toMouseButton(settings.value(QSL("button")).toString());

        auto const& action = settings.value(QSL("action")).toString();

        addAction(modifier, button, action);
    }

    settings.endArray();
}

void Manager::
saveSettings() const
{
    auto settings = this->settings();

    settings.beginWriteArray(QSL("MouseAction"));

    auto i = 0;
    for (auto const& item: m_actions) {
        settings.setArrayIndex(i);

        settings.setValue(QSL("modifier"), toString(item.first.modifier));
        settings.setValue(QSL("button"), toString(item.first.button));
        settings.setValue(QSL("action"), item.second);

        ++i;
    }

    settings.endArray();
}

std::vector<QAction*> Manager::
findActions(QWidget& root, QString const& name) const
{
    std::vector<QAction*> result;

    findActions(root, name, result);

    auto const last = std::unique(result.begin(), result.end());
    result.erase(last, result.end());

    return result;
}

void Manager::
findActions(QWidget& widget,
            QString const& name, std::vector<QAction*>& result) const
{
    if (!widget.isEnabled() || !widget.isVisible()) return;

    auto const& actions = widget.actions();

    std::copy_if(actions.begin(), actions.end(), std::back_inserter(result),
        [&](auto* const action) {
            assert(action);
            return action->objectName() == name;
        });

    auto const& children =
        widget.findChildren<QWidget*>(QString(), Qt::FindDirectChildrenOnly);

    for (auto* const child: children) {
        assert(child);
        findActions(*child, name, result);
    }
}

} // namespace natsu::modules::mouse_action
