#ifndef NATSU_MODULES_MOUSE_ACTION_MANAGER_HPP
#define NATSU_MODULES_MOUSE_ACTION_MANAGER_HPP

#include <memory>
#include <vector>

#include <Qt>

#include <boost/container/flat_map.hpp>

class QAction;
class QSettings;
class QWidget;

namespace natsu { class Application; } // namespace natsu

namespace natsu::modules::mouse_action {

struct MouseAction {
    Qt::KeyboardModifiers modifier;
    Qt::MouseButton button;
};

auto constexpr WheelUp = Qt::ExtraButton23;
auto constexpr WheelDown = Qt::ExtraButton24;

class Manager
{
public:
    Manager(Application&);
    ~Manager();

    // accessor
    Application& application() const { return m_app; }

    // query
    QAction* findAction(QWidget&, Qt::KeyboardModifiers, Qt::MouseButton) const;

    // modifier
    bool addAction(Qt::KeyboardModifiers, Qt::MouseButton, QString const& name);
    bool removeAction(Qt::KeyboardModifiers, Qt::MouseButton);

private:
    QSettings settings() const;
    void loadSettings();
    void saveSettings() const;

    std::vector<QAction*> findActions(QWidget&, QString const& name) const;
    void findActions(QWidget&, QString const& name, std::vector<QAction*>&) const;

private:
    Application& m_app;

    boost::container::flat_map<MouseAction, QString> m_actions;
};

} // namespace natsu::modules::mouse_action

#endif // NATSU_MODULES_MOUSE_ACTION_MANAGER_HPP
