#ifndef NATSU_MODULES_MOUSE_ACTION_EVENT_FILTER_HPP
#define NATSU_MODULES_MOUSE_ACTION_EVENT_FILTER_HPP

#include <QObject>

class QEvent;
class QMouseEvent;
class QWheelEvent;
class QWidget;

namespace natsu::modules::mouse_action {

class Manager;

class EventFilter : public QObject
{
    Q_OBJECT
public:
    EventFilter(Manager&);
    ~EventFilter() override;

    // override QObject
    bool eventFilter(QObject*, QEvent*) override;

private:
    bool handleMouseReleaseEvent(QWidget& widget, QMouseEvent&) const;
    bool handleWheelEvent(QWidget& widget, QWheelEvent&) const;

private:
    Manager& m_manager;
};

} // namespace natsu::modules::mouse_action

#endif // NATSU_MODULES_MOUSE_ACTION_EVENT_FILTER_HPP
