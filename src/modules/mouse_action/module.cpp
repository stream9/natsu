#include "module.hpp"

#include "event_filter.hpp"
#include "manager.hpp"

#include "core/application.hpp"
#include "global.hpp"

#include <QApplication>

namespace natsu::modules::mouse_action {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    m_manager = std::make_unique<Manager>(app);
    m_eventFilter = std::make_unique<EventFilter>(*m_manager);

    qApp->installEventFilter(m_eventFilter.get());

    return true;
}

} // namespace natsu::modules::mouse_action
