#include "event_filter.hpp"

#include "manager.hpp"

#include "browser/browser_window.hpp"
#include "misc/pointer.hpp"

#include <QAction>
#include <QApplication>
#include <QEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QWidget>

namespace natsu::modules::mouse_action {

EventFilter::
EventFilter(Manager& manager)
    : m_manager { manager }
{}

EventFilter::~EventFilter() = default;

bool EventFilter::
eventFilter(QObject* const obj, QEvent* const ev)
{
    assert(obj);
    assert(ev);

    bool rv = false;

    if (ev->type() == QEvent::MouseButtonRelease) {
        auto* widget = dynamic_cast<QWidget*>(obj);
        auto& event = to_ref(static_cast<QMouseEvent*>(ev));
        if (widget) {
            rv = handleMouseReleaseEvent(*widget, event);
        }
    }
    else if (ev->type() == QEvent::Wheel) {
        auto* widget = dynamic_cast<QWidget*>(obj);
        auto& event = to_ref(static_cast<QWheelEvent*>(ev));

        if (widget) {
            rv = handleWheelEvent(*widget, event);
        }
    }

    return rv || QObject::eventFilter(obj, ev);
}

bool EventFilter::
handleMouseReleaseEvent(QWidget& widget, QMouseEvent& event) const
{
    auto& window = to_ref(widget.window());
    if (typeid(window) != typeid(BrowserWindow)) return false;

    auto const modifier = qApp->keyboardModifiers();
    auto const button = event.button();

    auto* const action = m_manager.findAction(window, modifier, button);
    if (action) {
        action->trigger();
        return true;
    }

    return false;
}

bool EventFilter::
handleWheelEvent(QWidget& widget, QWheelEvent& event) const
{
    auto& window = to_ref(widget.window());
    if (typeid(window) != typeid(BrowserWindow)) return false;

    auto const modifier = qApp->keyboardModifiers();
    auto const& delta = event.angleDelta();
    auto const button = (delta.x() > 0 || delta.y() > 0) ? WheelUp : WheelDown;

    auto* const action = m_manager.findAction(window, modifier, button);
    if (action) {
        action->trigger();
        return true;
    }

    return false;
}

} // namespace natsu::modules::mouse_action
