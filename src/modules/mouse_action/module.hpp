#ifndef NATSU_MODULES_MOUSE_ACTION_MODULES_HPP
#define NATSU_MODULES_MOUSE_ACTION_MODULES_HPP

#include "core/module.hpp"

#include <memory>

namespace natsu { class Application; }

namespace natsu::modules::mouse_action {

class EventFilter;
class Manager;

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    // override plugin::Module
    bool doInitialize(Application&) override;

private:
    std::unique_ptr<Manager> m_manager; // non-null
    std::unique_ptr<EventFilter> m_eventFilter; // non-null
};

} // namespace natsu::modules::mouse_action

#endif // NATSU_MODULES_MOUSE_ACTION_MODULES_HPP
