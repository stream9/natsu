#include "module.hpp"

#include "adaptor.hpp"

#include "core/application.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

namespace natsu::modules::dark_mode {

Module::Module() noexcept = default;
Module::~Module() noexcept = default;

bool Module::
doInitialize(Application& a) noexcept
{
    for (auto& p: a.profiles()) {
        onProfileCreated(*p);
    }
    this->connect(&a,   &Application::profileCreated,
                  this, &Module::onProfileCreated);

    return true;
}

void Module::
onProfileCreated(Profile& p) noexcept
{
    make_qmanaged<Adaptor>(p);
}

} // namespace natsu::modules::dark_mode
