#include "adaptor.hpp"

#include "browser/browser_window.hpp"
#include "browser/browser_window/menu_bar.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"

#include <QWebEngineSettings>

namespace natsu::modules::dark_mode {

static bool
loadSetting(Profile& p) noexcept
{
    auto& s = p.settings();
    s.beginGroup(QSL("Dark Mode"));
    auto v = s.value(QSL("enabled"), false).toBool();
    s.endGroup();

    return v;
}

static void
saveSetting(Profile& p, bool x) noexcept
{
    auto& s = p.settings();
    s.beginGroup(QSL("Dark Mode"));
    s.setValue(QSL("enabled"), x);
    s.endGroup();
    s.sync();
}

static void
setDarkMode(QWebEngineProfile& p, bool x) noexcept
{
    auto& s = to_ref(p.settings());

    s.setAttribute(QWebEngineSettings::ForceDarkMode, x);
}

Adaptor::
Adaptor(Profile& p) noexcept
    : QObject { &p }
    , m_profile { p }
    , m_toggle { make_qmanaged<QAction>(QSL("Dark Mode"), this) }
{
    auto x = loadSetting(p);
    m_toggle->setCheckable(true);
    m_toggle->setChecked(x);
    setDarkMode(p, x);

    this->connect(m_toggle, &QAction::toggled,
                  this,     &Adaptor::toggleDarkMode);

    auto& a = get::application(p);

    this->connect(&a,   &Application::profileCreated,
                  this, [&](auto& p) {
                      this->connect(&p,   &Profile::windowCreated,
                                    this, &Adaptor::onWindowCreated);
                  });
}

void Adaptor::
onWindowCreated(BrowserWindow& w) noexcept
{
    auto& m = w.menuBar().toolMenu();
    m.addAction(m_toggle);
}

void Adaptor::
toggleDarkMode(bool x) noexcept
{
    setDarkMode(m_profile, x);
    saveSetting(m_profile, x);
}

} // namespace natsu::modules::dark_mode
