#ifndef NATSU_MODULES_DARK_MODE_ADAPTOR_HPP
#define NATSU_MODULES_DARK_MODE_ADAPTOR_HPP

#include "browser/fwd/browser_window.hpp"
#include "core/fwd/profile.hpp"
#include "misc/pointer.hpp"

#include <QAction>
#include <QObject>

namespace natsu::modules::dark_mode {

class Adaptor : public QObject
{
    Q_OBJECT
public:
    Adaptor(Profile&) noexcept;

private:
    Q_SLOT void onWindowCreated(BrowserWindow&) noexcept;
    Q_SLOT void toggleDarkMode(bool) noexcept;

private:
    Profile& m_profile;
    qmanaged_ptr<QAction> m_toggle;
};

} // namespace natsu::modules::dark_mode

#endif // NATSU_MODULES_DARK_MODE_ADAPTOR_HPP
