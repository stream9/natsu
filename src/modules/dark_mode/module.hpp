#ifndef NATSU_MODULES_DARK_MODE_MODULES_HPP
#define NATSU_MODULES_DARK_MODE_MODULES_HPP

#include "core/fwd/application.hpp"
#include "core/fwd/profile.hpp"
#include "core/module.hpp"

namespace natsu::modules::dark_mode {

class ProfileAdaptor;

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module() noexcept;
    ~Module() noexcept override;

private:
    // override plugin::Module
    bool doInitialize(Application&) noexcept override;

    Q_SLOT void onProfileCreated(Profile&) noexcept;
};

} // namespace natsu::modules::dark_mode

#endif // NATSU_MODULES_DARK_MODE_MODULES_HPP
