#ifndef NATSU_MODULES_HISTORY_MANAGER_HPP
#define NATSU_MODULES_HISTORY_MANAGER_HPP

#include "core/history_manager.hpp"
#include "misc/integer.hpp"

#include <memory>
#include <vector>

class QIcon;
class QString;
class QUrl;

namespace natsu { class Profile; } // namespace natsu

namespace natsu::modules::history {

class Database;

class Manager : public HistoryManager
{
public:
    Manager(Profile&);
    ~Manager();

    // override HistoryManager
    std::vector<Entry>
        recentEntries(non_negative<int64_t> count) const override;

    std::vector<Entry> search(QString const& keyword) const override;

    QIcon icon(QUrl const&) const override;

    QString completeHost(QString const& prefix) const override;

    void addEntry(QUrl const&, QString const& title) override;
    void addIcon(QUrl const&, QIcon const&) override;

    void removeEntry(Id) override;

private:
    void loadSettings();
    void saveSettings() const;

private:
    std::unique_ptr<Database> const m_db; // non-null

    std::vector<QString> m_filters;
};

} // namespace natsu::modules::history

#endif // NATSU_MODULES_HISTORY_MANAGER_HPP
