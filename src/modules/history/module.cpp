#include "module.hpp"

#include "manager.hpp"

#include "core/application.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <memory>

namespace natsu::modules::history {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    for (auto const& profile: app.profiles()) {
        onProfileCreated(*profile);
    }
    this->connect(&app, &Application::profileCreated,
                  this, &Module::onProfileCreated);

    return true;
}

void Module::
onProfileCreated(Profile& profile)
{
    profile.setHistoryManager(std::make_unique<Manager>(profile));
}

} // namespace natsu::modules::history
