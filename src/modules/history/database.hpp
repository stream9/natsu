#ifndef NATSU_MODULES_HISTORY_DATABASE_HPP
#define NATSU_MODULES_HISTORY_DATABASE_HPP

#include "core/history.hpp"

#include <stream9/function_ref.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/sqlite.hpp>

class QString;
class QUrl;

namespace natsu::modules::history {

using Id = natsu::history::Id;

namespace sqlite = stream9::sqlite;
using stream9::function_ref;
using stream9::safe_integer;

class Database
{
public:
    Database(QString const& path);
    ~Database();

    void selectRecent(safe_integer<int64_t, 0> count,
                      function_ref<bool(int64_t/*id*/,
                                         std::string_view/*url*/,
                                         std::string_view/*title*/,
                                         std::span<char const>/*icon*/,
                                         int64_t/*count*/,
                                         time_t/*last_visit_time*/ )>);

    int updateEntry(QUrl const& url, QString const& title);

    int insertEntry(QUrl const& url, QString const& title);

    std::optional<int64_t> selectIconId(std::span<char const> hash);

    std::optional<std::span<char const>>
        getIconForUrl(QUrl const& url);

    std::optional<std::string_view>
        completeHost(QString const& prefix);

    int updateIconId(QUrl const& url, int64_t const id);

    // @return rowid of inserted row
    int64_t insertIconData(std::span<char const> hash,
                           std::span<char const> iconBytes);

    void search(QString const& keyword,
                function_ref<bool(int64_t/*id*/,
                                   std::string_view/*url*/,
                                   std::string_view/*title*/,
                                   std::span<char const>/*icon*/,
                                   int64_t/*count*/,
                                   time_t/*last_visit_time*/ )>);

    int deleteEntry(Id);

    sqlite::transaction beginTransaction();

private:
    void prepareTables();

private:
    sqlite::database m_db;
};

} // namespace natsu::modules::history

#endif // NATSU_MODULES_HISTORY_DATABASE_HPP
