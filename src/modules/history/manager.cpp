#include "manager.hpp"

#include "database.hpp"

#include "core/conv.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <algorithm>

#include <boost/numeric/conversion/cast.hpp>

#include <QBuffer>
#include <QCryptographicHash>
#include <QDir>
#include <QIcon>
#include <QSettings>
#include <QString>
#include <QUrl>

namespace natsu::modules::history {

using Entry = HistoryManager::Entry;

static QString
databasePath(Profile& profile)
{
    auto const& dir = profile.pluginDir();
    assert(dir.exists());

    return dir.filePath(QSL("history.db"));
}

static QString
settingsPath(Profile& profile)
{
    auto const& dir = profile.pluginDir();
    assert(dir.exists());

    return dir.filePath(QSL("history.ini"));
}

// Manager

Manager::
Manager(Profile& profile)
    : HistoryManager { profile }
    , m_db { std::make_unique<Database>(databasePath(profile)) }
{
    loadSettings();
}

Manager::~Manager() = default;

std::vector<Entry> Manager::
recentEntries(non_negative<int64_t> const count) const
{
    std::vector<Entry> results;

    m_db->selectRecent(count,
        [&](auto&& id, auto&& url, auto&& title,
            auto&& icon, auto&& count, auto&& lastVisitTime)
        {
            QPixmap pixmap;
            pixmap.loadFromData(
                reinterpret_cast<uchar const*>(icon.data()),
                boost::numeric_cast<uint>(icon.size()) );

            results.push_back(Entry {
                id, toQString(url), toQString(title),
                QIcon { pixmap },
                count, lastVisitTime
            });

            return true;
        });

    return results;
}

std::vector<Entry> Manager::
search(QString const& keyword) const
{
    std::vector<Entry> result;

    m_db->search(keyword,
        [&](auto id, auto url, auto title, auto icon_,
            auto count, auto lastVisitTime)
        {
            QPixmap pixmap;
            pixmap.loadFromData(
                reinterpret_cast<uchar const*>(icon_.data()),
                boost::numeric_cast<uint>(icon_.size()) );
            QIcon icon { pixmap };

            result.emplace_back(
                id,
                toQString(url),
                toQString(title),
                icon,
                count,
                lastVisitTime
            );

            return true;
        });

    return result;
}

QIcon Manager::
icon(QUrl const& url) const
{
    auto o_data = m_db->getIconForUrl(url);

    if (o_data) {
        QPixmap pixmap;
        pixmap.loadFromData(
            reinterpret_cast<uchar const*>(o_data->data()),
            boost::numeric_cast<uint>(o_data->size()) );

        return { pixmap };
    }
    else {
        return {};
    }
}

QString Manager::
completeHost(QString const& prefix) const
{
    auto const o_url = m_db->completeHost(prefix);
    if (o_url) {
        QUrl const url { toQString(*o_url) };
        auto const& host = url.host();

        if (host.startsWith(prefix)) {
            return host;
        }
        else {
            assert(host.startsWith(QSL("www.")));
            return host.mid(4);
        }
    }
    else {
        return {};
    }
}

void Manager::
addEntry(QUrl const& url, QString const& title)
{
    auto const& urlString = url.toString();

    if (std::any_of(m_filters.begin(), m_filters.end(),
        [&](auto const& filter) {
            return urlString.contains(filter);
        }))
    {
        return;
    }

    auto trans = m_db->beginTransaction();

    auto const changes = m_db->updateEntry(url, title);

    if (changes == 0) {
        m_db->insertEntry(url, title);
    }

    trans.commit();
}

void Manager::
addIcon(QUrl const& url, QIcon const& icon)
{
    if (icon.isNull()) return;

    QByteArray iconBytes;;
    QBuffer buffer { &iconBytes };
    buffer.open(QIODevice::WriteOnly);

    auto const pixmap = icon.pixmap(16, 16);
    pixmap.save(&buffer, "PNG");

    auto const hash_ =
            QCryptographicHash::hash(iconBytes, QCryptographicHash::Md5);

    std::span const hash {
        hash_.constData(),
        boost::numeric_cast<size_t>(hash_.size())
    };

    auto trans = m_db->beginTransaction();
    auto o_id = m_db->selectIconId(hash);

    if (o_id) {
        m_db->updateIconId(url, *o_id);
    }
    else {
        std::span const data {
            iconBytes.constData(),
            boost::numeric_cast<size_t>(iconBytes.size())
        };

        auto const id = m_db->insertIconData(hash, data);
        assert(id != 0);

        m_db->updateIconId(url, id);
    }

    trans.commit();
}

void Manager::
removeEntry(Id const id)
{
    auto trans = m_db->beginTransaction();

    m_db->deleteEntry(id);

    trans.commit();
}

void Manager::
loadSettings()
{
    QSettings settings { settingsPath(this->profile()), QSettings::IniFormat };

    auto const size = settings.beginReadArray(QSL("filter"));
    for (auto i = 0; i < size; ++i) {
        settings.setArrayIndex(i);

        m_filters.push_back(settings.value(QSL("url")).toString());
    }

    settings.endArray();
}

void Manager::
saveSettings() const
{
    QSettings settings { settingsPath(this->profile()), QSettings::IniFormat };

    settings.beginWriteArray(QSL("filter"));

    auto const size = boost::numeric_cast<int>(m_filters.size());
    for (auto i = 0; i < size; ++i) {
        settings.setArrayIndex(i);

        settings.setValue(QSL("url"), m_filters[static_cast<size_t>(i)]);
    }

    settings.endArray();
}

} // namespace natsu::modules::history
