#include <charconv>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <optional>
#include <system_error>

#include <getopt.h>

#include <stream9/sqlite.hpp>

namespace fs = std::filesystem;
namespace sqlite = stream9::sqlite;

class command_line_option
{
public:
    using path_t = fs::path;

public:
    command_line_option(int argc, char* argv[])
        : m_argv0 { argv[0] }
    {
        parse(argc, argv);
    }

    // accessor
    bool help() const { return m_help; }

    std::optional<int64_t> older_than() const { return m_older_than; }

    path_t database_filepath() const
    {
        if (m_filepath.empty()) {
            return default_filepath();
        }

        return m_filepath;
    }

private:
    void parse(int argc, char* argv[])
    {
        static struct option long_options[] = {
            { "help",       no_argument,       nullptr, 'h' },
            { "older_than", required_argument, nullptr, 'o' },
            { "file",       required_argument, nullptr, 'f' },
            { 0, 0, 0, 0 }
        };

        while (true) {
            int idx = 0;
            int val = 0;

            val = getopt_long(argc, argv, "ho:f:", long_options, &idx);

            switch (val) {
            case -1:
                return;
            case 'h':
                m_help = true;
                break;
            case 'o':
                parse_month(optarg);
                break;
            case 'f':
                m_filepath = optarg;
                break;
            }
        }

    }

    void parse_month(std::string_view const arg)
    {
        int64_t result = 0;

        auto const [p, ec] = std::from_chars(
            arg.begin(), arg.end(),
            result, 10
        );
        if (ec != std::errc()) {
            auto const e = std::make_error_code(ec);
            std::cerr << "older_than option's argument is invalid: "
                      << arg << ": " << e.message() << "\n";
        }
        else if (result < 0) {
            std::cerr << "older_than option's argument must be positive\n";
        }
        else {
            m_older_than = result;
        }
    }

    path_t default_filepath() const
    {
        path_t path { std::getenv("XDG_CONFIG_HOME") };
        path = path / "natsu" / "Default" / "plugin" / "history.db";

        return path;
    }

private:
    std::optional<int64_t> m_older_than;
    path_t m_filepath;
    std::string_view m_argv0;
    bool m_help = false;
};

static void
print_usage(char* const argv0)
{
    std::cout << "Usage: " << argv0 << " [-h] [OPTION]\n"
              << "-h, --help            print this message\n"
              << "-f, --file=PATH       path to the database file\n"
              << "-o, --older_than=N    clear history older than N months\n"
              ;
}

class database
{
public:
    database(fs::path path)
        : m_db { path.c_str() }
    {
        m_db.busy_timeout(2000);
    }

    int clear_older_than(int64_t const month)
    {
        auto stmt = m_db.prepare(
            "DELETE "
            "  FROM history "
            " WHERE datetime(last_visit_time, 'unixepoch') < datetime('now', '-' || ? || ' month') "
        );

        stmt.bind_to(1, month);
        stmt.exec();

        return m_db.changes();
    }

    int clear_unused_icons()
    {
        m_db.exec(
            "DELETE "
            "  FROM icon "
            " WHERE id NOT IN (SELECT icon_id FROM history WHERE icon_id IS NOT NULL) "
        );

        return m_db.changes();
    }

    void vacuum()
    {
        m_db.exec("VACUUM");
    }

private:
    sqlite::database m_db;
};

int
main(int argc, char *argv[])
{
    try {
        command_line_option const opt { argc, argv };

        if (opt.help()) {
            print_usage(argv[0]);
            return 1;
        }

        auto const& file_path = opt.database_filepath();
        if (!fs::exists(file_path)) {
            std::cerr << "database file doesn't exists: " << file_path << "\n";
            print_usage(argv[0]);
            return 1;
        }

        database db { file_path };

        if (auto const v = opt.older_than(); v) {
            auto const n = db.clear_older_than(*v);
            std::cout << n << " history entries are deleted\n";
        }

        auto const n = db.clear_unused_icons();
        if (n != 0) {
            std::cout << n << " icons are deleted\n";
        }

        db.vacuum();
        std::cout << "database is vacuumed\n";
    }
    catch (sqlite::error const& e) {
        std::cerr << "database error: " << e.what() << "\n";
        std::cerr << "error code: " << e.why() << "\n";
        return 1;
    }
    catch (std::exception const& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }

    return 0;
}
