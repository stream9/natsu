#include "database.hpp"

#include "global.hpp"

#include "core/conv.hpp"

#include <QIcon>
#include <QString>
#include <QUrl>

namespace natsu::modules::history {

static std::string
toNormalizedString(QUrl const& url)
{
    return toString(url, QUrl::StripTrailingSlash | QUrl::FullyEncoded);
}

// Database

Database::
Database(QString const& path)
    : m_db { toString(path) }
{
    prepareTables();
}

Database::~Database() = default;

void Database::
selectRecent(safe_integer<int64_t, 0> count,
             function_ref<bool(int64_t/*id*/,
                                std::string_view/*url*/,
                                std::string_view/*title*/,
                                std::span<char const>/*icon*/,
                                int64_t/*count*/,
                                time_t/*last_visit_time*/) > cb)
{
    static auto query = m_db.prepare(
        "SELECT history.id, url, title, icon.data as icon, count, last_visit_time "
        "FROM history "
        "LEFT JOIN icon ON history.icon_id = icon.id "
        "ORDER BY last_visit_time DESC "
        "LIMIT :count"
    );

    query.exec(count.value());

    for (auto const& row: query) {
        auto const id = row.get_int64(0);
        auto const url = row.get_text(1);
        auto const title = row.get_text(2);
        auto const icon = row.get_blob(3);
        auto const count = row.get_int64(4);
        auto const last_visit_time = row.get_int64(5);

        if (!cb(id, url, title, icon, count, last_visit_time)) break;
    }
}

int Database::
updateEntry(QUrl const& url, QString const& title)
{
    static auto stmt = m_db.prepare(
        "UPDATE history "
        "SET   title = :title "
        "    , count = count + 1 "
        "    , last_visit_time = strftime('%s', 'now') "
        "WHERE url = :url "
    );

    stmt.exec(toString(title), toNormalizedString(url));

    return m_db.changes();
}

int Database::
insertEntry(QUrl const& url, QString const& title)
{
    static auto stmt = m_db.prepare(
        "INSERT INTO history ( "
        "   url, title, count, last_visit_time "
        ") VALUES ( "
        "   :url, :title, 1, strftime('%s', 'now') "
        ") "
    );

    stmt.exec(toNormalizedString(url), toString(title));

    return m_db.changes();
}

std::optional<int64_t> Database::
selectIconId(std::span<char const> const hash)
{
    static auto query = m_db.prepare(
        "SELECT id "
        "FROM icon "
        "WHERE hash = :hash "
    );

    std::optional<int64_t> result;

    query.exec(hash);
    if (!query.done()) {
        result = query.current_row().get_int64(0);
    }

    return result;
}

std::optional<std::span<char const>> Database::
getIconForUrl(QUrl const& url)
{
    static auto query = m_db.prepare(
        "SELECT icon.data "
        "FROM history "
        "LEFT JOIN icon ON history.icon_id = icon.id "
        "WHERE history.url = :url "
    );

    std::optional<std::span<char const>> result;

    query.exec(toNormalizedString(url));

    if (!query.done()) {
        auto row = query.current_row();
        result = row.get_blob(0);
    }

    return result;
}

std::optional<std::string_view> Database::
completeHost(QString const& prefix)
{
    static auto query = m_db.prepare(
        "SELECT url "
        "  FROM history "
        "WHERE url GLOB 'http://www.' || :prefix1 || '*' "
        "   OR url GLOB 'https://www.' || :prefix2 || '*' "
        "   OR url GLOB 'http://' || :prefix3 || '*' "
        "   OR url GLOB 'https://' || :prefix4 || '*' "
        "ORDER BY count DESC "
        "LIMIT 1 "
    );

    auto const prefix_ = toString(prefix);
    std::optional<std::string_view> result;

    query.exec(prefix_, prefix_, prefix_, prefix_);

    if (!query.done()) {
        result = query.current_row().get_text(0);
    }

    return result;
}

int Database::
updateIconId(QUrl const& url, int64_t const id)
{
    static auto stmt = m_db.prepare(
        "UPDATE history "
        "SET icon_id = :icon_id "
        "WHERE url = :url "
    );

    stmt.exec(id, toNormalizedString(url));

    return m_db.changes();
}

int64_t Database::
insertIconData(std::span<char const> const hash,
               std::span<char const> const iconBytes)
{
    static auto stmt = m_db.prepare(
        "INSERT INTO icon ( "
        "   hash, data, last_update_time "
        ") VALUES ( "
        "   :hash, :data, strftime('%s', 'now') "
        ") "
    );

    stmt.exec(hash, iconBytes);

    return m_db.last_insert_rowid();
}

void Database::
search(QString const& keyword,
       function_ref<bool(int64_t/*id*/,
                          std::string_view/*url*/,
                          std::string_view/*title*/,
                          std::span<char const>/*icon*/,
                          int64_t/*count*/,
                          time_t/*last_visit_time*/ )> cb)
{
    static auto query = m_db.prepare(
        "SELECT history.id, url, title, icon.data as icon, count, last_visit_time "
        "FROM history "
        "LEFT JOIN icon ON history.icon_id = icon.id "
        "WHERE history.url like '%' || :keyword1 || '%' "
        "OR history.title like '%' || :keyword2 || '%' "
        "ORDER BY last_visit_time DESC "
        "LIMIT 20 "
    );

    auto const keyword_ = toString(keyword);
    query.exec(keyword_, keyword_);

    for (auto const row: query) {
        auto const id = row.get_int64(0);
        auto const url = row.get_text(1);
        auto const title = row.get_text(2);
        auto const icon = row.get_blob(3);
        auto const count = row.get_int64(4);
        auto const lastVisitTime = row.get_int64(5);

        if (!cb(id, url, title, icon, count, lastVisitTime)) break;
    }
}

int Database::
deleteEntry(Id const id)
{
    static auto stmt = m_db.prepare(
        "DELETE FROM history "
        "WHERE id = :id "
    );

    stmt.exec(id);

    return m_db.changes();
}

sqlite::transaction Database::
beginTransaction()
{
    return m_db.begin_transaction();
}

void Database::
prepareTables()
{
    auto trans = beginTransaction();

    m_db.exec(
        "CREATE TABLE IF NOT EXISTS history ( "
        "     id INTEGER PRIMARY KEY "
        "   , url TEXT "
        "   , title TEXT "
        "   , count INTEGER "
        "   , icon_id INTEGER "
        "   , last_visit_time INTEGER "
        ") "
    );

    m_db.exec(
        "CREATE INDEX IF NOT EXISTS history_idx1 ON history (url) "
    );

    m_db.exec(
        "CREATE TABLE IF NOT EXISTS icon ( "
        "     id INTEGER PRIMARY KEY "
        "   , hash TEXT "
        "   , data BLOB "
        "   , last_update_time INTEGER "
        ") "
    );

    trans.commit();
}

} // namespace natsu::modules::history
