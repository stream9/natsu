#include "module.hpp"

#include "adaptor.hpp"

#include "core/application.hpp"
#include "core/command_line_option.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"
#include "misc/url.hpp"

#include <QApplication>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QString>
#include <QStringList>
#include <QUrl>

namespace natsu::modules::dbus {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    if (!registerService(app)) {
        auto const& urls = app.commandLineOption().urls();

        if (!openInRemoteInstance(urls)) {
            qCritical() << QSL("fail to register own DBus service and also "
                               "fail to connect to remote service too");
            std::exit(1);
        }
        else {
            std::exit(0);
        }
    }

    return true;
}

bool Module::
registerService(Application& app)
{
    auto const adaptor = make_qmanaged<Adaptor>(app);

    auto dbus = QDBusConnection::sessionBus();
    if (!dbus.registerObject(QSL("/"), &app)) {
        qCritical() << "fail to register object to DBus";
        std::exit(1);
        return false;
    }

    if (!dbus.registerService(Adaptor::serviceName())) {
        return false;
    }

    qInfo() << "dbus: registered service to session bus";

    return true;
}

bool Module::
openInRemoteInstance(QStringList const& urls)
{
    QDBusInterface remote {
        Adaptor::serviceName(),
        QSL("/"),
    };
    if (!remote.isValid()) {
        return false;
    }

    if (urls.isEmpty()) {
        remote.call(QSL("newWindow"));
        if (remote.lastError().isValid()) {
            qCritical() << QSL("fail to call DBus method: newWindow");
            return false;
        }
    }
    else {
        for (auto const& s: urls) {

            auto const& url = normalizeUrl(s);
            if (url.isEmpty()) {
                qWarning() << "invalid URL:" << s;
                continue;
            }

            remote.call(QSL("openUrlInNewTab"), url.toString(QUrl::FullyEncoded));
            if (remote.lastError().isValid()) {
                qCritical() << QSL("fail to call DBus method: openUrlInNewTab");
                qDebug() << remote.lastError();
                return false;
            }
        }
    }

    return true;
}

} // namespace natsu::modules::dbus
