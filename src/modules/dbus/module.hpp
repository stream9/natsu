#ifndef NATSU_MODULES_DBUS_MODULES_HPP
#define NATSU_MODULES_DBUS_MODULES_HPP

#include "core/fwd/application.hpp"
#include "core/module.hpp"

#include <QString>

namespace natsu::modules::dbus {

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    bool registerService(Application&);
    bool openInRemoteInstance(QStringList const&);

    // override plugin::Module
    bool doInitialize(Application&) override;
};

} // namespace natsu::modules::dbus

#endif // NATSU_MODULES_DBUS_MODULES_HPP
