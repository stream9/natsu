#include "adaptor.hpp"

#include "browser/browser_window.hpp"
#include "core/application.hpp"
#include "core/get.hpp"

#include <QApplication>
#include <QString>
#include <QUrl>

namespace natsu::modules::dbus {

Adaptor::
Adaptor(Application& app)
    : QDBusAbstractAdaptor { &app }
    , m_application { app }
{}

Adaptor::~Adaptor() = default;

void Adaptor::
openUrl(QString const& url) const
{
    auto& window = get::activeWindow(m_application);
    natsu::openUrlInActiveTab(window, QUrl(url));
}

void Adaptor::
openUrlInNewTab(QString const& url) const
{
    auto& window = get::activeWindow(m_application);
    natsu::openUrlInNewTab(window, QUrl(url));
}

void Adaptor::
openUrlInNewWindow(QString const& url) const
{
    auto& window = m_application.createWindow();
    natsu::openUrlInActiveTab(window, QUrl(url));
}

void Adaptor::
newWindow() const
{
    m_application.createWindow();
}

} // namespace natsu::modules::dbus
