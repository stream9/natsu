#ifndef NATSU_MODULES_DBUS_ADAPTOR_HPP
#define NATSU_MODULES_DBUS_ADAPTOR_HPP

#include "core/fwd/application.hpp"
#include "global.hpp"

#include <QDBusAbstractAdaptor>

class QString;

namespace natsu::modules::dbus {

class Adaptor : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.stream9.Natsu")

public:
    Adaptor(Application&);
    ~Adaptor() override;

    // query
    static QString serviceName() { return QSL("org.stream9.Natsu"); }

    Q_SLOT void openUrl(QString const&) const;
    Q_SLOT void openUrlInNewTab(QString const&) const;
    Q_SLOT void openUrlInNewWindow(QString const&) const;
    Q_SLOT void newWindow() const;

private:
    Application& m_application;
};

} // namespace natsu::modules::dbus

#endif // NATSU_MODULES_DBUS_ADAPTOR_HPP
