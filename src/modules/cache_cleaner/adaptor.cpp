#include "adaptor.hpp"

#include "browser/browser_window.hpp"
#include "browser/browser_window/menu_bar.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QMessageBox>

namespace natsu::modules::cache_cleaner {

// Adaptor

Adaptor::
Adaptor(Profile& profile)
    : QObject { &profile }
    , m_profile { profile }
    , m_clearAction { QSL("&Clear") }
{
    this->connect(&profile, &Profile::windowCreated,
                  this,     &Adaptor::onWindowCreated);

    this->connect(&m_clearAction, &QAction::triggered,
                  this,           &Adaptor::clear);
}

Adaptor::~Adaptor() = default;

void Adaptor::
onWindowCreated(BrowserWindow& window)
{
    auto& menuBar = window.menuBar();
    auto& toolMenu = menuBar.toolMenu();
    auto& menu = to_ref(toolMenu.addMenu(QSL("&Cache")));
    menu.addAction(&m_clearAction);
}

void Adaptor::
clear()
{
    auto& window = get::activeWindow(m_profile);

    auto const rv = QMessageBox::question(
        &window,
        QSL("Clear cache"),
        QSL("Are you really clear HTTP cache?")
    );

    if (rv == QMessageBox::Yes) {
        m_profile.clearHttpCache();
    }
}

} // namespace natsu::modules::cache_cleaner
