#ifndef NATSU_MODULES_CACHE_CLEANER_ADAPTOR_HPP
#define NATSU_MODULES_CACHE_CLEANER_ADAPTOR_HPP

#include "browser/fwd/browser_window.hpp"
#include "core/fwd/profile.hpp"

#include <QAction>
#include <QObject>

namespace natsu::modules::cache_cleaner {

class Adaptor : public QObject
{
    Q_OBJECT
public:
    Adaptor(Profile&);
    ~Adaptor() override;

    // accessor
    Profile& profile() const { return m_profile; }

private:
    Q_SLOT void onWindowCreated(BrowserWindow&);
    Q_SLOT void clear();

private:
    Profile& m_profile;

    QAction m_clearAction;
};

} // namespace natsu::modules::cache_cleaner

#endif // NATSU_MODULES_CACHE_CLEANER_ADAPTOR_HPP
