#include "adaptor.hpp"

#include "browser/browser_tab.hpp"
#include "browser/web_page.hpp"
#include "browser/web_view.hpp"
#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"
#include "core/site_prefs_manager.hpp"
#include "global.hpp"

#include <algorithm>
#include <array>
#include <cmath>

#include <QUrl>

namespace natsu::modules::zoom {

static auto const&
zoomSteps()
{
    static std::array<int, 17> steps {
        25, 33, 50, 67, 75, 80, 90, 100,
        110, 125, 150, 175, 200, 250, 300, 400, 500
    };

    return steps;
}

static QString
prefKey()
{
    return QSL("zoom factor");
}

static bool
isImage(QUrl const& url)
{
    auto const& path = url.path();

    return path.endsWith(QSL(".jpg"))
        || path.endsWith(QSL(".jpeg"))
        || path.endsWith(QSL(".gif"))
        || path.endsWith(QSL(".png"));
}

// Adaptor

Adaptor::
Adaptor(BrowserTab& tab)
    : QObject { &tab }
    , m_tab { tab }
    , m_zoomInAction { QSL("Zoom In") }
    , m_zoomOutAction { QSL("Zoom Out") }
    , m_resetZoomAction { QSL("Reset") }
{
    auto const& category = QSL("Zoom");
    auto& manager = get::actionManager(m_tab);

    auto f = [&](auto& action, auto const& name,
                 auto const& icon, QKeySequence const& shortCut)
    {
        action.setIcon(icon);
        action.setObjectName(name);
        manager.configure(category, action, shortCut);
    };

    f(zoomInAction(), QSL("zoom.zoom_in"), icon::zoomIn(), makeKeyCode(Qt::CTRL, Qt::Key_Plus));
    f(zoomOutAction(), QSL("zoom.zoom_out"), icon::zoomOut(), makeKeyCode(Qt::CTRL, Qt::Key_Minus));
    f(resetZoomAction(), QSL("zoom.reset_zoom"), icon::resetZoom(), makeKeyCode(Qt::CTRL, Qt::Key_0));

    auto& view = get::webView(m_tab);
    view.addAction(zoomInAction());
    view.addAction(zoomOutAction());
    view.addAction(resetZoomAction());

    this->connect(&m_zoomInAction, &QAction::triggered,
                  this,            &Adaptor::zoomIn);
    this->connect(&m_zoomOutAction, &QAction::triggered,
                  this,            &Adaptor::zoomOut);
    this->connect(&m_resetZoomAction, &QAction::triggered,
                  this,               &Adaptor::resetZoom);

    auto& page = get::webPage(m_tab);

    this->connect(&page, &WebPage::loadFinished,
                  this,  &Adaptor::onLoadFinished);
}

Adaptor::~Adaptor() = default;

bool Adaptor::
isMinimumZoom() const
{
    auto& page = get::webPage(m_tab);
    auto const factor = std::lround(page.zoomFactor() * 100);

    return factor <= zoomSteps().front();
}

bool Adaptor::
isMaximumZoom() const
{
    auto& page = get::webPage(m_tab);
    auto const factor = std::lround(page.zoomFactor() * 100);

    return factor >= zoomSteps().back();
}

void Adaptor::
updateActions()
{
    m_zoomInAction.setEnabled(!isMaximumZoom());
    m_zoomOutAction.setEnabled(!isMinimumZoom());
}

void Adaptor::
loadZoomFactor()
{
    auto& page = get::webPage(m_tab);
    auto& sitePrefs = get::sitePrefs(m_tab);

    auto const& url = page.url();
    if (url.isEmpty()) return;
    if (isImage(url)) return;

    auto const& value = sitePrefs.getValue(url, prefKey());

    if (value.isValid()) {
        page.setZoomFactor(value.toReal());
    }
}

void Adaptor::
saveZoomFactor() const
{
    auto& page = get::webPage(m_tab);
    auto& sitePrefs = get::sitePrefs(m_tab);
    auto const factor = page.zoomFactor();

    auto const& url = page.url();
    if (isImage(url)) return;

    if (factor == 1.0) {
        sitePrefs.removeValue(page.url(), prefKey());
    }
    else {
        sitePrefs.setValue(page.url(), prefKey(), factor);
    }
}

void Adaptor::
onLoadFinished(bool const ok)
{
    if (!ok) return;

    loadZoomFactor();
}

void Adaptor::
zoomIn()
{
    auto& page = get::webPage(m_tab);

    auto const factor = std::lround(page.zoomFactor() * 100);
    auto const& steps = zoomSteps();

    auto it = std::upper_bound(steps.begin(), steps.end(), factor);
    if (it != steps.end()) {
        qreal const newFactor = *it / 100.0;

        page.setZoomFactor(newFactor);
        saveZoomFactor();
        updateActions();
    }
}

void Adaptor::
zoomOut()
{
    auto& page = get::webPage(m_tab);

    auto const factor = std::lround(page.zoomFactor() * 100);
    auto const& steps = zoomSteps();

    auto it = std::lower_bound(steps.begin(), steps.end(), factor);
    if (it != steps.end() && it != steps.begin()) {
        qreal const newFactor = *--it / 100.0;

        page.setZoomFactor(newFactor);
        saveZoomFactor();
        updateActions();
    }
}

void Adaptor::
resetZoom()
{
    auto& page = get::webPage(m_tab);
    auto const factor = 1.0;

    page.setZoomFactor(factor);
    saveZoomFactor();
    updateActions();
}

} // namespace natsu::modules::zoom
