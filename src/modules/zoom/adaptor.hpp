#ifndef NATSU_MODULES_ZOOM_ADAPTOR_HPP
#define NATSU_MODULES_ZOOM_ADAPTOR_HPP

#include "browser/fwd/browser_tab.hpp"

#include <QAction>
#include <QObject>

namespace natsu::modules::zoom {

class Adaptor : public QObject
{
    Q_OBJECT
public:
    Adaptor(BrowserTab&);
    ~Adaptor() override;

    // accessor
    BrowserTab& browserTab() const { return m_tab; }
    QAction& zoomInAction() { return m_zoomInAction; }
    QAction& zoomOutAction() { return m_zoomOutAction; }
    QAction& resetZoomAction() { return m_resetZoomAction; }

private:
    bool isMaximumZoom() const;
    bool isMinimumZoom() const;
    void updateActions();
    void loadZoomFactor();
    void saveZoomFactor() const;

    Q_SLOT void onLoadFinished(bool ok);
    Q_SLOT void zoomIn();
    Q_SLOT void zoomOut();
    Q_SLOT void resetZoom();

private:
    BrowserTab& m_tab;

    QAction m_zoomInAction;
    QAction m_zoomOutAction;
    QAction m_resetZoomAction;
};

} // namespace natsu::modules::zoom

#endif // NATSU_MODULES_ZOOM_ADAPTOR_HPP
