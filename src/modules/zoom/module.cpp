#include "module.hpp"

#include "adaptor.hpp"

#include "browser/browser_window.hpp"
#include "core/application.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

namespace natsu::modules::zoom {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    this->connect(&app, &Application::profileCreated,
                  this, &Module::onProfileCreated);

    return true;
}

void Module::
onProfileCreated(Profile& p)
{
    this->connect(&p,   &Profile::windowCreated,
                  this, &Module::onWindowCreated);
}

void Module::
onWindowCreated(BrowserWindow& w)
{
    this->connect(&w,   &BrowserWindow::tabCreated,
                  this, &Module::onTabCreated);
}

void Module::
onTabCreated(BrowserTab& tab)
{
    make_qmanaged<Adaptor>(tab);
}

} // namespace natsu::modules::zoom
