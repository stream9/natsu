#ifndef NATSU_MODULES_SITE_PREFS_MANAGER_HPP
#define NATSU_MODULES_SITE_PREFS_MANAGER_HPP

#include "core/site_prefs_manager.hpp"

#include <memory>

class QUrl;
class QString;
class QVariant;

namespace natsu { class Profile; }

namespace natsu::modules::site_prefs {

class Manager : public natsu::SitePrefsManager
{
public:
    Manager(Profile&);
    ~Manager() override;

private:
    // override natsu::SitePrefsManager
    QVariant doGetValue(QUrl const&, QString const& key) const override;

    void doSetValue(QUrl const&,
                  QString const& key, QVariant const& value) override;

    void doRemoveValue(QUrl const&, QString const& key) override;

private:
    Profile& m_profile;

    class Database;
    std::unique_ptr<Database> m_db;
};

} // namespace natsu::modules::site_prefs

#endif // NATSU_MODULES_SITE_PREFS_MANAGER_HPP
