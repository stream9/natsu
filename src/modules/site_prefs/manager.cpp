#include "manager.hpp"

#include "global.hpp"

#include "core/conv.hpp"
#include "core/profile.hpp"

#include <string_view>
#include <optional>

#include <QDir>
#include <QString>
#include <QUrl>
#include <QVariant>

#include <stream9/sqlite.hpp>

namespace natsu::modules::site_prefs {

namespace sqlite = stream9::sqlite;

class Manager::Database
{
public:
    Database(QString const& path)
        : m_db { toString(path) }
    {
        prepareTables();
    }

    std::optional<std::string_view>
        selectValue(QString const& site, QString const& key)
    {
        static auto query = m_db.prepare(
            "SELECT a.value "
            "FROM data a "
            "JOIN site b ON a.site = b.id "
            "JOIN key c ON a.key = c.id "
            "WHERE b.name = :site "
            "AND c.name = :key "
        );

        std::optional<std::string_view> result;

        query.exec(toString(site), toString(key));

        if (!query.done()) {
            result = query.current_row().get_text(0);
        }

        return result;
    }

    int insertSite(QString const& site)
    {
        static auto stmt = m_db.prepare(
            "INSERT OR IGNORE INTO site (name) "
            "VALUES ( :name ) "
        );

        stmt.exec(toString(site));

        return m_db.changes();
    }

    int insertKey(QString const& name)
    {
        static auto stmt = m_db.prepare(
            "INSERT OR IGNORE INTO key (name) "
            "VALUES ( :name ) "
        );

        stmt.exec(toString(name));

        return m_db.changes();
    }

    int insertOrReplaceValue(QString const& site,
                             QString const& key, QVariant const& value)
    {
        static auto stmt = m_db.prepare(
            "INSERT OR REPLACE INTO data (site, key, value) "
            "SELECT a.id, b.id, :value "
            "FROM site a "
            "CROSS JOIN key b "
            "WHERE a.name = :site "
            "AND b.name = :key "
        );

        stmt.exec(toString(value.toString()), toString(site), toString(key));

        return m_db.changes();
    }

    int removeValue(QString const& site, QString const& key)
    {
        static auto stmt = m_db.prepare(
            "DELETE FROM data "
            "WHERE site = ( "
            "    SELECT id FROM site WHERE name = :site ) "
            "AND key = ( "
            "    SELECT id FROM key WHERE name = :key ) "
        );

        stmt.exec(toString(site), toString(key));

        return m_db.changes();
    }

    int removeDatalessSite()
    {
        static auto stmt = m_db.prepare(
            "DELETE FROM site "
            "WHERE id NOT IN (SELECT DISTINCT site FROM data) "
        );

        stmt.exec();

        return m_db.changes();
    }

    auto beginTransaction()
    {
        return m_db.begin_transaction();
    }

private:
    void prepareTables()
    {
        m_db.exec(
            "CREATE TABLE IF NOT EXISTS key ( "
            "    id INTEGER PRIMARY KEY, "
            "    name TEXT UNIQUE, "
            "    last_modified DEFAULT CURRENT_TIMESTAMP "
            ") "
        );

        m_db.exec(
            "CREATE TABLE IF NOT EXISTS site ( "
            "    id INTEGER PRIMARY KEY, "
            "    name TEXT UNIQUE, "
            "    last_modified DEFAULT CURRENT_TIMESTAMP "
            ") "
        );

        m_db.exec(
            "CREATE TABLE IF NOT EXISTS data ( "
            "    site INTEGER, "
            "    key INTEGER, "
            "    value TEXT, "
            "    last_modified DEFAULT CURRENT_TIMESTAMP, "
            "    PRIMARY KEY (site, key) "
            ") "
        );
    }

private:
    sqlite::database m_db;
};

static QString
databasePath(Profile& profile)
{
    auto const& dir = profile.pluginDir();
    assert(dir.exists());

    return dir.filePath(QSL("site_prefs.db"));
}

static QString
extractSite(QUrl const& url)
{
    assert(!url.isEmpty());

    auto const& host = url.host();
    if (host.isEmpty()) {
        return url.toEncoded();
    }

    return host;
}

Manager::
Manager(Profile& profile)
    : m_profile { profile }
    , m_db { std::make_unique<Database>(databasePath(m_profile)) }
{}

Manager::~Manager() = default;

QVariant Manager::
doGetValue(QUrl const& url, QString const& key) const
{
    if (url.isEmpty()) return {};
    if (key.isEmpty()) return {};

    auto const& site = extractSite(url);

    auto const o_value = m_db->selectValue(site, key);
    if (o_value) {
        return toQString(*o_value);
    }
    else {
        return {};
    }
}

void Manager::
doSetValue(QUrl const& url,
         QString const& key, QVariant const& value)
{
    assert(!url.isEmpty());
    assert(!key.isEmpty());

    auto const& site = extractSite(url);

    auto trans = m_db->beginTransaction();

    m_db->insertSite(site);
    m_db->insertKey(key);

    m_db->insertOrReplaceValue(site, key, value);

    trans.commit();
}

void Manager::
doRemoveValue(QUrl const& url, QString const& key)
{
    assert(!url.isEmpty());
    assert(!key.isEmpty());

    auto const& site = extractSite(url);

    auto trans = m_db->beginTransaction();

    m_db->removeValue(site, key);
    m_db->removeDatalessSite();

    trans.commit();
}

} // namespace natsu::modules::site_prefs
