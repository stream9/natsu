#include "dock.hpp"

#include "view.hpp"

#include "browser/browser_tab.hpp"
#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "misc/pointer.hpp"

#include <QAction>

namespace natsu::modules::developer_tool {

Dock::
Dock(BrowserTab& tab)
    : QDockWidget { &tab }
    , m_browserTab { tab }
{
    this->setObjectName(QSL("developer_tool.dock"));

    auto& action = to_ref(this->toggleViewAction());
    action.setObjectName(QSL("developer_tool.toggle_dock"));

    auto& actionManager = get::actionManager(m_browserTab);
    actionManager.configure(QSL("DeveloperTool"), action, makeKeyCode(Qt::SHIFT, Qt::CTRL, Qt::Key_I));

    m_browserTab.addAction(&action);

    if (!m_browserTab.restoreDockWidget(this)) {
        m_browserTab.addDockWidget(Qt::BottomDockWidgetArea, this);
    }

    this->connect(this, &QDockWidget::visibilityChanged,
                  this, &Dock::onVisibilityChanged);

    this->hide();
}

Dock::~Dock() = default;

void Dock::
closeEvent(QCloseEvent*)
{
    auto& widget = to_ref(this->widget());

    this->setWidget(nullptr);
    widget.deleteLater();
}

void Dock::
onVisibilityChanged(bool const visible)
{
    if (visible && !this->widget()) {
        auto const view = make_qmanaged<View>(m_browserTab);
        this->setWidget(view.get());
    }
}

} // namespace natsu::modules::developer_tool
