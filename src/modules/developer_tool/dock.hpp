#ifndef NATSU_MODULES_DEVELOPER_TOOL_DOCK_HPP
#define NATSU_MODULES_DEVELOPER_TOOL_DOCK_HPP

#include "browser/fwd/browser_tab.hpp"

#include <QDockWidget>

class QCloseEvent;

namespace natsu::modules::developer_tool {

class Dock : public QDockWidget
{
    Q_OBJECT
public:
    Dock(BrowserTab&);
    ~Dock() override;

    // accessor
    BrowserTab& browserTab() const { return m_browserTab; }

protected:
    void closeEvent(QCloseEvent*) override;

private:
    Q_SLOT void onVisibilityChanged(bool visible);

private:
    BrowserTab& m_browserTab;
};

} // namespace natsu::modules::developer_tool

#endif // NATSU_MODULES_DEVELOPER_TOOL_DOCK_HPP

