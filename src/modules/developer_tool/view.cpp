#include "view.hpp"

#include "browser/browser_tab.hpp"
#include "browser/web_page.hpp"
#include "core/get.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QSize>

namespace natsu::modules::developer_tool {

View::
View(BrowserTab& tab)
    : QWebEngineView { &tab }
    , m_browserTab { tab }
{
    auto& page = to_ref(this->page());
    auto& inspectingPage = get::webPage(tab);
    inspectingPage.setDevToolsPage(&page);
}

View::~View() = default;

QSize View::
sizeHint() const
{
    return { 800, 300 };
}

} // namespace natsu::modules::developer_tool
