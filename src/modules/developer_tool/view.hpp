#ifndef NATSU_MODULES_DEVELOPER_TOOL_VIEW_HPP
#define NATSU_MODULES_DEVELOPER_TOOL_VIEW_HPP

#include "browser/fwd/browser_tab.hpp"

#include <QWebEngineView>

class QSize;

namespace natsu::modules::developer_tool {

class View : public QWebEngineView
{
    Q_OBJECT
public:
    View(BrowserTab&);
    ~View() override;

    // accessor
    BrowserTab& browserTab() const { return m_browserTab; }

    // override QWidget
    QSize sizeHint() const override;

private:
    BrowserTab& m_browserTab;
};

} // namespace natsu::modules::developer_tool

#endif // NATSU_MODULES_DEVELOPER_TOOL_VIEW_HPP
