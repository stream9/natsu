#ifndef NATSU_MODULES_SEARCH_ENGINE_MANAGER_HPP
#define NATSU_MODULES_SEARCH_ENGINE_MANAGER_HPP

#include "misc/integer.hpp"
#include "core/search_engine_manager.hpp"

#include <memory>
#include <optional>
#include <vector>

#include <QString>

class QSettings;
class QUrl;

namespace natsu { class Profile; } // namespace natsu

namespace natsu::modules::search_engine {

class Manager : public SearchEngineManager
{
public:
    Manager(Profile&);
    ~Manager();

    // override SearchEngineManager
    QUrl searchUrl(QString const& query) const override;
    SearchEngine const* searchEngine(QString const& keyword) const override;
    SearchEngine const& defaultEngine() const override;
    bool setDefaultEngine(QString const& name) override;

    // query
    auto begin() const { return m_engines.begin(); }
    auto end() const { return m_engines.end(); }

    non_negative<int> size() const;

private:
    QSettings settings() const;

    void loadSettings();
    void saveSettings() const;

private:
    Profile& m_profile;

    non_negative<int> m_default;
    std::vector<SearchEngine> m_engines;
};

} // namespace natsu::modules::search_engine

#endif // NATSU_MODULES_SEARCH_ENGINE_MANAGER_HPP
