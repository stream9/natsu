#include "manager.hpp"

#include "core/profile.hpp"
#include "global.hpp"

#include <algorithm>

#include <boost/numeric/conversion/cast.hpp>

#include <QDir>
#include <QSettings>
#include <QUrl>

namespace natsu::modules::search_engine {

static QByteArray
encode(QString const& str)
{
    QByteArray buf;

    auto isValid = [](auto ch) {
        return std::isalnum(ch)
            || ch == '*' || ch == '-' || ch == '.' || ch == '*';
    };

    for (char ch: str.toUtf8()) {
        if (isValid(ch)) {
            buf.push_back(ch);
        }
        else if (ch == ' ') {
            buf.push_back('+');
        }
        else {
            auto uch = static_cast<unsigned char>(ch);
            auto const& code = QSL("%%1").arg(static_cast<uint>(uch), 2, 16);
            buf.append(code.toUtf8());
        }
    }

    return buf;
}

static QString
replaceQuery(QString const& url, QString const& query)
{
    auto copy { url };
    copy.replace(QSL("%s"), encode(query));

    return copy;
}

static QString
getKeyword(QString const& query)
{
    auto const index = query.indexOf(' ');
    if (index == -1) {
        return query;
    }
    else {
        return query.left(index);
    }
}

// Manager

Manager::
Manager(Profile& profile)
    : m_profile { profile }
{
    loadSettings();
}

Manager::~Manager() = default;

QUrl Manager::
searchUrl(QString const& query) const
{
    auto const& keyword = getKeyword(query);
    auto const* const engine = this->searchEngine(keyword);

    if (engine) {
        auto const& tail = query.mid(keyword.size()).trimmed();

        return replaceQuery(engine->url, tail);
    }
    else {
        return replaceQuery(defaultEngine().url, query);
    }
}

SearchEngine const* Manager::
searchEngine(QString const& keyword) const
{
    auto const it = std::find_if(
        m_engines.begin(), m_engines.end(),
        [&](auto const& engine) {
            return engine.keyword == keyword;
        }
    );

    if (it == m_engines.end()) {
        return nullptr;
    }
    else {
        return &*it;
    }
}

SearchEngine const& Manager::
defaultEngine() const
{
    return m_engines.at(static_cast<size_t>(m_default));
}

bool Manager::
setDefaultEngine(QString const& name)
{
    auto const it = std::find_if(
        m_engines.begin(), m_engines.end(),
        [&](auto const& engine) {
            return engine.name == name;
        });

    if (it != m_engines.end()) {
        m_default = static_cast<int>(std::distance(m_engines.begin(), it));
        return true;
    }

    return false;
}

QSettings Manager::
settings() const
{
    auto const& dir = m_profile.pluginDir();
    assert(dir.exists());

    return { dir.filePath(QSL("search_engine.ini")), QSettings::IniFormat };
}

void Manager::
loadSettings()
{
    auto settings = this->settings();
    auto const size = settings.beginReadArray(QSL("SearchEngines"));

    for (auto i = 0; i < size; ++i) {
        settings.setArrayIndex(i);

        m_engines.emplace_back(SearchEngine {
            settings.value(QSL("name")).toString(),
            settings.value(QSL("url")).toString(),
            settings.value(QSL("keyword")).toString()
        });
    }

    if (m_engines.empty()) {
        m_engines.emplace_back(SearchEngine {
            QSL("Google"),
            QSL("https://www.google.com/search?q=%s"),
            QSL("g")
        });
    }

    settings.endArray();

    m_default = settings.value(QSL("default"), 0).toInt();
}

void Manager::
saveSettings() const
{
    auto settings = this->settings();
    settings.beginWriteArray(
        QSL("SearchEngines"), boost::numeric_cast<int>(m_engines.size()));
    int size = boost::numeric_cast<int>(m_engines.size());

    for (auto i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        auto const& engine = m_engines.at(static_cast<size_t>(i));

        settings.setValue(QSL("name"), engine.name);
        settings.setValue(QSL("url"), engine.url);
        settings.setValue(QSL("keyword"), engine.keyword);
    }

    settings.endArray();

    settings.setValue(QSL("default"), m_default);
}

} // namespace natsu::modules::search_engine
