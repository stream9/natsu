#ifndef NATSU_MODULES_TAB_MODULES_HPP
#define NATSU_MODULES_TAB_MODULES_HPP

#include "browser/fwd/browser_window.hpp"
#include "core/fwd/application.hpp"
#include "core/module.hpp"

namespace natsu::modules::tab {

class ProfileAdaptor;

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    // override plugin::Module
    bool doInitialize(Application&) override;

    Q_SLOT void onWindowCreated(BrowserWindow&);
};

} // namespace natsu::modules::tab

#endif // NATSU_MODULES_TAB_MODULES_HPP
