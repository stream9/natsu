#include "history.hpp"

#include "manager.hpp"

#include "browser/browser_tab.hpp"
#include "browser/tab_browser.hpp"
#include "browser/web_page.hpp"
#include "core/get.hpp"

namespace natsu::modules::tab {

class ScrollRestorer : public QObject
{
    Q_OBJECT
public:
    ScrollRestorer(WebPage& page, QPointF const& pos)
        : QObject { &page }
        , m_page { page }
        , m_pos { pos }
    {
        this->connect(&m_page, &WebPage::loadFinished,
                      this,    &ScrollRestorer::run);
    }

private:
    Q_SLOT void run(bool const ok)
    {
        if (!ok) return;

        auto const& script =
            QSL("window.scroll(%1, %2);").arg(m_pos.x()).arg(m_pos.y());

        m_page.runJavaScript(script);

        this->deleteLater();
    }

private:
    WebPage& m_page;
    QPointF m_pos;
};

#include "history.moc"

History::
History(Manager& manager)
    : m_manager { manager }
    , m_history { 10 }
{}

History::~History() = default;

void History::
push(BrowserTab& tab)
{
    auto& page = get::webPage(tab);

    auto const& url = page.url();
    auto const& pos = page.scrollPosition();
    auto const index = m_manager.tabBrowser().indexOf(&tab);
    assert(index >= 0);

    m_history.push_front(Entry { url, pos, index });
}

void History::
pop()
{
    if (m_history.empty()) return;

    auto const& entry = m_history.front();

    auto& tab = m_manager.newTab(entry.tabIndex);
    auto& page = get::webPage(tab);

    make_qmanaged<ScrollRestorer>(page, entry.scrollPosition);

    page.load(entry.url);

    m_history.pop_front();
}

} // namespace natsu::modules::tab
