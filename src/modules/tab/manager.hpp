#ifndef NATSU_TAB_MANAGER_HPP
#define NATSU_TAB_MANAGER_HPP

#include "misc/integer.hpp"
#include "browser/tab_manager.hpp"

#include <memory>

#include <boost/circular_buffer.hpp>

#include <QAction>
#include <QObject>
#include <QPointF>
#include <QUrl>

namespace natsu {

class TabBrowser;
class BrowserTab;

} // namespace natsu

namespace natsu::modules::tab {

class History;

class Manager : public QObject, public TabManager
{
    Q_OBJECT
public:
    Manager(TabBrowser&);
    ~Manager();

    // accessor
    QAction& restoreAction() { return m_restore; }

    // command

private:
    // override TabManager
    BrowserTab& doNewTab() override;
    BrowserTab& doNewTab(int index) override;

    void doCloseTab(BrowserTab&) override;

    Q_SLOT void onCurrentChanged();
    Q_SLOT void restore();

private:
    std::unique_ptr<History> const m_history; // non-null
    int m_lastInsertIndex = -1;
    QAction m_restore;
};

} // namespace natsu::modules::tab

#endif // NATSU_TAB_MANAGER_HPP
