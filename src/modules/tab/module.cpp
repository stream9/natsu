#include "module.hpp"

#include "manager.hpp"

#include "browser/browser_window.hpp"
#include "browser/tab_browser.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <memory>

namespace natsu::modules::tab {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    this->connect(&app, &Application::profileCreated,
                  this, [&](auto& p) {
                      this->connect(&p,   &Profile::windowCreated,
                                    this, &Module::onWindowCreated);
                  });

    return true;
}

void Module::
onWindowCreated(BrowserWindow& win)
{
    auto& tabBrowser = win.tabBrowser();

    tabBrowser.setTabManager(std::make_unique<Manager>(tabBrowser));
}

} // namespace natsu::modules::tab
