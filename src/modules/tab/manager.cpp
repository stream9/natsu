#include "manager.hpp"

#include "history.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "browser/tab_browser.hpp"
#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"

namespace natsu::modules::tab {

Manager::
Manager(TabBrowser& tabBrowser)
    : TabManager { tabBrowser }
    , m_history { std::make_unique<History>(*this) }
    , m_restore { QSL("Restore") }
{
    auto& browser = this->tabBrowser();

    this->connect(&browser, &TabBrowser::currentChanged,
                  this,     &Manager::onCurrentChanged);
    this->connect(&m_restore, &QAction::triggered,
                  this,       &Manager::restore);

    m_restore.setShortcut(makeKeyCode(Qt::SHIFT, Qt::CTRL, Qt::Key_T)); //TODO configure
    browser.addAction(&m_restore);
}

Manager::~Manager() = default;

BrowserTab& Manager::
doNewTab()
{
    auto& tabBrowser = this->tabBrowser();

    auto const current = tabBrowser.currentIndex();
    auto const next =
        m_lastInsertIndex != -1 ? m_lastInsertIndex + 1 : current + 1;

    return doNewTab(next);
}

BrowserTab& Manager::
doNewTab(int const index)
{
    assert(index >= 0);

    auto& tabBrowser = this->tabBrowser();

    auto const tab = make_qmanaged<BrowserTab>(tabBrowser);

    tabBrowser.insertTab(index, tab.get(), QSL("New Tab"));

    m_lastInsertIndex = index;

    return *tab;
}

void Manager::
doCloseTab(BrowserTab& tab)
{
    auto& tabBrowser = this->tabBrowser();

    m_history->push(tab);

    if (tabBrowser.count() == 1) {
        get::browserWindow(tabBrowser).close();
    }
    else {
        tab.deleteLater();
        auto const index = tabBrowser.indexOf(&tab);
        assert(index >= 0);;

        tabBrowser.removeTab(index);
        tabBrowser.currentTab().setFocus();
    }
}

void Manager::
onCurrentChanged()
{
    m_lastInsertIndex = -1;
}

void Manager::
restore()
{
    m_history->pop();
}

} // namespace natsu::modules::tab
