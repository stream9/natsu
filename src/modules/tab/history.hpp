#ifndef NATSU_TAB_HISTORY_HPP
#define NATSU_TAB_HISTORY_HPP

#include "misc/integer.hpp"

#include <boost/circular_buffer.hpp>

#include <QUrl>
#include <QPointF>

namespace natsu {

class BrowserTab;

} // namespace browser

namespace natsu::modules::tab {

class Manager;

class History
{
public:
    History(Manager&);
    ~History();

    void push(BrowserTab&);
    void pop();

private:
    Manager& m_manager;

    struct Entry {
        QUrl url;
        QPointF scrollPosition;
        non_negative<int> tabIndex;
    };
    boost::circular_buffer<Entry> m_history;
};

} // namespace natsu::modules::tab

#endif // NATSU_TAB_HISTORY_HPP
