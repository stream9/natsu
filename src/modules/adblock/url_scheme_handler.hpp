#ifndef NATSU_MODULES_ADBLOCK_URL_SCHEME_HANDLER_HPP
#define NATSU_MODULES_ADBLOCK_URL_SCHEME_HANDLER_HPP

#include <QWebEngineUrlSchemeHandler>

class QObject;
class QWebEngineUrlRequestJob;

//TODO no one seems to be using this file, delete?
namespace natsu::modules::adblock {

class AdBlock;

class UrlSchemeHandler : public QWebEngineUrlSchemeHandler
{
    Q_OBJECT
public:
    UrlSchemeHandler(QObject& parent);
    ~UrlSchemeHandler();

    // override QWebEngineUrlSchemeHandler
    void requestStarted(QWebEngineUrlRequestJob*) override;

private:
    AdBlock const& m_adBlock;
};

} // namespace natsu::modules::adblock

#endif // NATSU_MODULES_ADBLOCK_URL_SCHEME_HANDLER_HPP
