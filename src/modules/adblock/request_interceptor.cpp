#include "request_interceptor.hpp"

#include "adblock.hpp"
#include "log.hpp"

#include "global.hpp"
#include "core/application.hpp"
#include "core/get.hpp"

#include <QWebEngineUrlRequestInfo>

namespace natsu::modules::adblock {

using ResourceType = QWebEngineUrlRequestInfo::ResourceType;

static ContentType
getContentType(ResourceType const type)
{
    switch (type) {
        case ResourceType::ResourceTypeMainFrame:
            return ContentType::Document;
        case ResourceType::ResourceTypeSubFrame:
            return ContentType::SubDocument;
        case ResourceType::ResourceTypeStylesheet:
            return ContentType::StyleSheet;
        case ResourceType::ResourceTypeScript:
            return ContentType::Script;
        case ResourceType::ResourceTypeImage:
            return ContentType::Image;
        case ResourceType::ResourceTypeFontResource:
            return ContentType::Font;
        case ResourceType::ResourceTypeObject:
            return ContentType::Object;
        case ResourceType::ResourceTypeMedia:
            return ContentType::Media;
        case ResourceType::ResourceTypeXhr:
            return ContentType::XmlHttpRequest;
        case ResourceType::ResourceTypePing:
            return ContentType::Ping;
        case ResourceType::ResourceTypeSubResource:
        case ResourceType::ResourceTypeWorker:
        case ResourceType::ResourceTypeSharedWorker:
        case ResourceType::ResourceTypePrefetch:
        case ResourceType::ResourceTypeFavicon:
        case ResourceType::ResourceTypeServiceWorker:
        case ResourceType::ResourceTypeCspReport:
        case ResourceType::ResourceTypePluginResource:
        case ResourceType::ResourceTypeUnknown:
        default:
            return ContentType::Other;
    }
}

RequestInterceptor::
RequestInterceptor(Application& app, AdBlock& adblock)
    : m_app { app }
    , m_adBlock { adblock }
{}

RequestInterceptor::~RequestInterceptor() = default;

void RequestInterceptor::
interceptRequest(QWebEngineUrlRequestInfo& info)
{
    auto const& method = info.requestMethod();
    if (method != "GET" && method != "POST") {
        return;
    }

    auto const& url = info.requestUrl();
    auto const& origin = info.firstPartyUrl();
    auto const contentType = getContentType(info.resourceType());

    if (m_adBlock.shouldBlock(url, origin, contentType, false)) {
        if (m_app.verbose()) {
            DEBUG() << "[Request Intercepted]" << url << origin << info.resourceType();
        }
        info.block(true);
    }
}

} // namespace natsu::modules::adblock
