#ifndef NATSU_MODULES_ADBLOCK_LOG_HPP
#define NATSU_MODULES_ADBLOCK_LOG_HPP

#include <QLoggingCategory>

namespace natsu::modules::adblock {

Q_DECLARE_LOGGING_CATEGORY(log_adblock)

#define INFO(...) qCInfo(log_adblock, __VA_ARGS__)
#define WARNING(...) qCWarning(log_adblock, __VA_ARGS__)
#define DEBUG(...) qCDebug(log_adblock, __VA_ARGS__)
#define CRITICAL(...) qCCritical(log_adblock, __VA_ARGS__)

} // namespace natsu::modules::adblock

#endif // NATSU_MODULES_ADBLOCK_LOG_HPP
