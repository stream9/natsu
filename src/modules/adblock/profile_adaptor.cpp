#include "profile_adaptor.hpp"

#include "adblock.hpp"
#include "log.hpp"
#include "request_interceptor.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "browser/browser_window/menu_bar.hpp"
#include "browser/web_page.hpp"
#include "core/application.hpp"
#include "core/error.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <QDir>

namespace natsu::modules::adblock {

static QSettings
makeSettings(Profile const& profile)
{
    auto const& dir = profile.pluginDir();
    assert(dir.exists());

    return { dir.filePath(QSL("adblock.ini")), QSettings::IniFormat };
}

// ProfileAdaptor

ProfileAdaptor::
ProfileAdaptor(Profile& profile)
    : QObject { &profile }
    , m_profile { profile }
    , m_adBlock { std::make_unique<AdBlock>(get::application(m_profile)) }
    , m_interceptor { std::make_unique<RequestInterceptor>(get::application(m_profile), *m_adBlock) }
    , m_settings { makeSettings(m_profile) }
    , m_enabled { QSL("&Enabled") }
{
    loadFilterSets();

    m_profile.setUrlRequestInterceptor(m_interceptor.get());

    this->connect(&m_profile, &Profile::windowCreated,
                  this,       &ProfileAdaptor::onWindowCreated);

    m_enabled.setCheckable(true);
    m_enabled.setChecked(m_adBlock->isEnabled());

    this->connect(&m_enabled, &QAction::toggled,
                  this,       &ProfileAdaptor::onEnabledToggled);

    this->connect(m_adBlock.get(), &AdBlock::statusChanged,
                  this,            &ProfileAdaptor::onStatusChanged);
}

ProfileAdaptor::~ProfileAdaptor() = default;

void ProfileAdaptor::
loadFilterSets()
{
    auto const size = m_settings.beginReadArray(QSL("FilterSets"));
    for (auto i = 0; i < size; ++i) {
        m_settings.setArrayIndex(i);

        auto const& path = m_settings.value(QSL("path")).toString();
        if (path.isEmpty()) {
            WARNING() << QString("adblock filter: %1 has empty path")
                .arg(m_settings.value(QSL("name")).toString());
            continue;
        }

        m_adBlock->addFilterSet(path);
    }
    m_settings.endArray();
}

void ProfileAdaptor::
injectElementHideCss(WebPage& page) const
{
    auto const css =
        m_adBlock->elementHideCss(page.url()).replace("\"", "\\\"");

    page.runJavaScript(QSL(
        "var style = document.createElement('style'); "
        "style.innerHTML = \"%1\"; "
        "document.head.appendChild(style); "
    ).arg(css));
}

void ProfileAdaptor::
onTabCreated(BrowserTab& tab)
{
    auto& page = get::webPage(tab);

    this->connect(&page, &WebPage::loadFinished,
                  this,  &ProfileAdaptor::onPageLoaded);
    this->connect(&page, &WebPage::navigationRequested,
                  this,  &ProfileAdaptor::onNavigationRequested);
    this->connect(&page, &WebPage::newWindowRequested,
                  this,  &ProfileAdaptor::onNewWindowRequested);
}

void ProfileAdaptor::
onWindowCreated(BrowserWindow& window)
{
    auto& menuBar = window.menuBar();

    auto& toolMenu = menuBar.toolMenu();
    auto& menu = to_ref(toolMenu.addMenu(QSL("&ADBlock")));
    menu.addAction(&m_enabled);

    this->connect(&window, &BrowserWindow::tabCreated,
                  this,    &ProfileAdaptor::onTabCreated);
}

void ProfileAdaptor::
onPageLoaded(bool const ok)
{
    if (!ok) return;

    auto& page = to_ref(static_cast<WebPage*>(this->sender()));

    injectElementHideCss(page);
}

void ProfileAdaptor::
onEnabledToggled(bool const checked)
{
    m_adBlock->setEnabled(checked);
}

void ProfileAdaptor::
onStatusChanged()
{
    m_enabled.setChecked(m_adBlock->isEnabled());
}

void ProfileAdaptor::
onNavigationRequested(QWebEngineNavigationRequest& req) noexcept
{
    try {
        auto* page = dynamic_cast<WebPage*>(this->sender());
        if (!page) {
            qWarning() << __func__ << "precondition violation";
            return;
        }

        auto origin = page->url();

        auto blocked = m_adBlock->shouldBlock(
            req.url(), origin,
            req.isMainFrame() ? ContentType::Document : ContentType::SubDocument,
            false
        );

        if (blocked) {
            if (req.isMainFrame()) { // for example, the page is redirected to a blocked page.
                Q_EMIT page->windowCloseRequested();
            }

            req.reject();
        }
    }
    catch (...) {
        print_error();
    }
}

void ProfileAdaptor::
onNewWindowRequested(QWebEngineNewWindowRequest& req,
                     bool& handled) noexcept
{
    try {
        auto target = req.requestedUrl();
        if (m_adBlock->shouldBlock(target, target, ContentType::Document, true)) {
            handled = true;

            auto verbose = get::application(m_profile).verbose();
            if (verbose) {
                INFO() << "Popup is rejected:" << target;
            }
        }
    }
    catch (...) {
        print_error();
    }
}

} // namespace natsu::modules::adblock
