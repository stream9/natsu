#include "adblock.hpp"

#include "log.hpp"

#include "global.hpp"
#include "core/application.hpp"

#include <mutex>

#include <boost/asio/connect.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/numeric/conversion/cast.hpp>

#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QUrl>

namespace asio = boost::asio;
namespace beast = boost::beast;
namespace websocket = beast::websocket;
using tcp = boost::asio::ip::tcp;

namespace natsu::modules::adblock {

static QString
toString(ContentType type)
{
    switch (type) {
        default:
        case ContentType::Other:
            return QSL("Other");
        case ContentType::Script:
            return QSL("Script");
        case ContentType::Image:
            return QSL("Image");
        case ContentType::StyleSheet:
            return QSL("StyleSheet");
        case ContentType::Object:
            return QSL("Object");
        case ContentType::Document:
            return QSL("Document");
        case ContentType::SubDocument:
            return QSL("SubDocument");
        case ContentType::Ping:
            return QSL("Ping");
        case ContentType::XmlHttpRequest:
            return QSL("XmlHttpRequest");
        case ContentType::ObjectSubRequest:
            return QSL("ObjectSubRequest");
        case ContentType::Font:
            return QSL("Font");
        case ContentType::Media:
            return QSL("Media");
        case ContentType::WebSocket:
            return QSL("WebSocket");
        case ContentType::WebRtc:
            return QSL("WebRtc");
    }
}


QByteArray
toByteArray(beast::multi_buffer const& buffer)
{
    QByteArray result;

    assert(buffer.size() < INT_MAX);
    result.reserve(static_cast<int>(buffer.size()));

    for (auto const buf: buffer.data()) {
        assert(buf.size() < INT_MAX);
        result.append(static_cast<char const*>(buf.data()),
                      static_cast<int>(buf.size()));
    }

    return result;
}

// Impl

struct AdBlock::Impl : QObject
{
    AdBlock& m_adBlock;
    asio::io_context m_ioc;
    tcp::resolver m_resolver;
    websocket::stream<tcp::socket> m_ws;
    std::mutex m_mutex;
    bool m_ready = false;

    Impl(AdBlock& adBlock)
        : QObject { &adBlock }
        , m_adBlock { adBlock }
        , m_resolver { m_ioc }
        , m_ws { m_ioc }
    {
        resolve();
        this->startTimer(100);
    }

    bool isReady() const { return m_ready; }

    void resolve()
    {
        m_resolver.async_resolve("localhost", "8211", //TODO configure host and port
            [&](auto const ec, auto const endpoints) {
                if (ec) {
                    CRITICAL() << "fail to resolve adress of ADBlock service";
                    m_adBlock.setEnabled(false);
                    Q_EMIT m_adBlock.statusChanged();
                    return;
                }
                connect(endpoints);
            });
    }

    void connect(tcp::resolver::results_type const endpoints)
    {
        asio::async_connect(m_ws.next_layer(), endpoints,
            [&](auto ec, auto const&/*endpoint*/) {
                if (ec) {
                    CRITICAL() << "fail to connect to ADBlock service";
                    m_adBlock.setEnabled(false);
                    Q_EMIT m_adBlock.statusChanged();
                    return;
                }
                handshake();
            });
    }

    void handshake()
    {
        m_ws.async_handshake("localhost", "/",
            [&](auto ec) {
                if (ec) {
                    CRITICAL() << "fail to handshake with ADBlock service";
                    m_adBlock.setEnabled(false);
                    Q_EMIT m_adBlock.statusChanged();
                    return;
                }
                m_ready = true;
                Q_EMIT m_adBlock.statusChanged();

                DEBUG() << "connected with ADBlock service";
            });
    }

    QByteArray query(QByteArray const& json)
    {
        if (!m_ready) return {};

        auto const& request =
            asio::buffer(json.data(), static_cast<size_t>(json.size()));
        beast::multi_buffer response;

        m_mutex.lock();

        m_ws.write(request);
        m_ws.read(response);

        m_mutex.unlock();

        return toByteArray(response);
    }

protected:
    void timerEvent(QTimerEvent*) override
    {
        m_ioc.poll_one();
    }
};

// AdBlock

AdBlock::
AdBlock(Application& a)
    : m_impl { new Impl { *this } }
    , m_app { a }
{}

AdBlock::~AdBlock() = default;

bool AdBlock::
isEnabled() const
{
    return m_impl && m_impl->isReady();
}

bool AdBlock::
shouldBlock(QUrl const& url,
            QUrl const& origin,
            ContentType contentType,
            bool const isPopup)
{
    if (!isEnabled()) return false;
    if (!url.isValid()) return false;

    QJsonObject req;
    {
        req["jsonrpc"] = "2.0";
        req["id"] = 1;
        req["method"] = "shouldBlock";

        QJsonObject params;
        auto urlStr = url.toString(QUrl::FullyEncoded);
        params["url"] = urlStr;
        params["origin"] = origin.isValid() ? origin.toString(QUrl::FullyEncoded) : urlStr;
        params["contentType"] = toString(contentType);
        params["isPopup"] = isPopup;

        req["params"] = params;
    }
    auto const& json = QJsonDocument(req).toJson(QJsonDocument::Compact);

    try {
        auto const& res = m_impl->query(json);

        auto const& doc = QJsonDocument::fromJson(res);
        auto const& obj = doc.object();
        auto const& result = obj[QSL("result")].toObject();

        auto const block = result[QSL("block")].toBool();
        auto const& filter = result[QSL("filter")].toString();
        auto const& filterSet = result[QSL("filterSet")].toString();

        if (m_app.verbose() && (!filter.isEmpty() || !filterSet.isEmpty())) {
            auto const& msg = QSL("%1, URL = %2, subscription = %3, reason = %4")
                .arg(block ? QSL("blocked") : QSL("allowed"))
                .arg(url.toDisplayString())
                .arg(filterSet)
                .arg(filter)
                ;
            INFO().noquote() << msg;
        }

        return block;
    }
    catch (boost::system::system_error const& e) {
        DEBUG() << "Connection between adblockd is closed: " << e.what();
        setEnabled(false);

        return false;
    }
}

QString AdBlock::
elementHideCss(QUrl const& url)
{
    if (!isEnabled()) return {};

    QJsonObject req;
    {
        req["jsonrpc"] = "2.0";
        req["id"] = 1;
        req["method"] = "elementHideCss";

        QJsonObject params;
        params["url"] = url.toString(QUrl::FullyEncoded);

        req["params"] = params;
    }
    auto const& json = QJsonDocument(req).toJson(QJsonDocument::Compact);

    try {
        auto const& res = m_impl->query(json);

        auto const& doc = QJsonDocument::fromJson(res);
        auto const& obj = doc.object();
        auto const& result = obj[QSL("result")].toObject();

        auto const& css = result[QSL("css")].toString();

        return css;
    }
    catch (boost::system::system_error const& e) {
        DEBUG() << "Connection between adblockd is closed: " << e.what();
        setEnabled(false);

        return {};
    }
}

void AdBlock::
setEnabled(bool const enabled)
{
    if (enabled == isEnabled()) return;

    if (enabled) {
        m_impl = new Impl { *this };
    }
    else {
        m_impl->deleteLater();
        m_impl = nullptr;

        Q_EMIT statusChanged();
    }
}

void AdBlock::
addFilterSet(QString const&/*filePath*/)
{
}

void AdBlock::
removeFilterSet(QString const&/*filePath*/)
{
}

} // namespace natsu::modules::adblock
