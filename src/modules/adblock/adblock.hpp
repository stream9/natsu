#ifndef NATSU_MODULES_ADBLOCK_ADBLOCK_HPP
#define NATSU_MODULES_ADBLOCK_ADBLOCK_HPP

#include "core/fwd/application.hpp"

#include <QObject>

class QUrl;
class QString;

namespace natsu::modules::adblock {

enum class ContentType;

class AdBlock : public QObject
{
    Q_OBJECT
public:
    AdBlock(Application&);
    ~AdBlock();

    // accessor
    bool isEnabled() const;

    // query
    bool shouldBlock(QUrl const& url,
                     QUrl const& origin,
                     ContentType,
                     bool isPopup);

    QString elementHideCss(QUrl const&);

    // modifier
    void setEnabled(bool);
    void setVerbose(bool);
    void addFilterSet(QString const& filePath);
    void removeFilterSet(QString const& filePath);

    // signal
    Q_SIGNAL void statusChanged();

private:
    struct Impl;
    Impl* m_impl;

    Application& m_app;
};

enum class ContentType
{
    Other = 1,
    Script = 2,
    Image = 3,
    StyleSheet = 4,
    Object = 5,
    Document = 6,
    SubDocument = 7,
    Ping = 10,
    XmlHttpRequest = 11,
    ObjectSubRequest = 12,
    Font = 14,
    Media = 15,
    WebSocket = 16,
    WebRtc = 17,
};

} // namespace natsu::modules::adblock

#endif // NATSU_MODULES_ADBLOCK_ADBLOCK_HPP
