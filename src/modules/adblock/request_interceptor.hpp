#ifndef NATSU_MODULES_ADBLOCK_URL_INTERCEPTOR_HPP
#define NATSU_MODULES_ADBLOCK_URL_INTERCEPTOR_HPP

#include "core/fwd/application.hpp"

#include <QWebEngineUrlRequestInterceptor>

class QWebEngineUrlRequestInfo;

namespace natsu::modules::adblock {

class AdBlock;

class RequestInterceptor : public QWebEngineUrlRequestInterceptor
{
    Q_OBJECT
public:
    RequestInterceptor(Application&, AdBlock&);
    ~RequestInterceptor() override;

    // override QWebEngineUrlRequestInterceptor
    void interceptRequest(QWebEngineUrlRequestInfo&) override;

private:
    Application& m_app;
    AdBlock& m_adBlock;
};

} // namespace natsu::modules::adblock

#endif // NATSU_MODULES_ADBLOCK_URL_INTERCEPTOR_HPP
