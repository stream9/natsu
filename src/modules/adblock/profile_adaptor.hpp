#ifndef NATSU_MODULES_ADBLOCK_PROFILE_ADAPTOR_HPP
#define NATSU_MODULES_ADBLOCK_PROFILE_ADAPTOR_HPP

#include "browser/fwd/browser_tab.hpp"
#include "browser/fwd/browser_window.hpp"
#include "browser/fwd/web_page.hpp"
#include "core/fwd/profile.hpp"

#include <memory>

#include <QAction>
#include <QObject>
#include <QSettings>
#include <QWebEngineNavigationRequest>
#include <QWebEngineNewWindowRequest>

namespace natsu::modules::adblock {

class AdBlock;
class RequestInterceptor;

class ProfileAdaptor : public QObject
{
    Q_OBJECT
public:
    ProfileAdaptor(Profile&);
    ~ProfileAdaptor() override;

private:
    void loadFilterSets();
    void injectElementHideCss(WebPage&) const;

    Q_SLOT void onWindowCreated(BrowserWindow&);
    Q_SLOT void onTabCreated(BrowserTab&);
    Q_SLOT void onPageLoaded(bool);
    Q_SLOT void onEnabledToggled(bool);
    Q_SLOT void onStatusChanged();
    Q_SLOT void onNavigationRequested(QWebEngineNavigationRequest&) noexcept;
    Q_SLOT void onNewWindowRequested(QWebEngineNewWindowRequest&, bool& handled) noexcept;

private:
    Profile& m_profile;

    std::unique_ptr<AdBlock> const m_adBlock; // non-null
    std::unique_ptr<RequestInterceptor> const m_interceptor; // non-null
    QSettings m_settings;
    QAction m_enabled;
};

} // namespace natsu::modules::adblock

#endif // NATSU_MODULES_ADBLOCK_PROFILE_ADAPTOR_HPP
