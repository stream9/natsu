#ifndef NATSU_MODULES_ADBLOCK_MODULES_HPP
#define NATSU_MODULES_ADBLOCK_MODULES_HPP

#include "core/fwd/application.hpp"
#include "core/fwd/profile.hpp"
#include "core/module.hpp"

namespace natsu::modules::adblock {

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    // override plugin::Plugin
    bool doInitialize(Application&) override;

    Q_SLOT void onProfileCreated(Profile&);
};

} // namespace natsu::modules::adblock

#endif // NATSU_MODULES_ADBLOCK_MODULES_HPP
