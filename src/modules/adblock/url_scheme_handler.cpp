#include "url_scheme_handler.hpp"

#include "adblock.hpp"
#include "log.hpp"

#include "global.hpp"

#include <QUrl>
#include <QWebEngineUrlRequestJob>

namespace natsu::modules::adblock {

UrlSchemeHandler::
UrlSchemeHandler(AdBlock const& adBlock, QObject& parent)
    : QWebEngineUrlSchemeHandler { &parent }
    , m_adBlock { adBlock }
{
}

UrlSchemeHandler::~UrlSchemeHandler() = default;

void UrlSchemeHandler::
requestStarted(QWebEngineUrlRequestJob* const request)
{
    assert(request);

    auto const& url = request->requestUrl();
    assert(url.scheme() == QSL("adblock"));

    DEBUG() << url;
}

} // namespace natsu::modules::adblock
