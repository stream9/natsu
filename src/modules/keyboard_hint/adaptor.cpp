#include "adaptor.hpp"

#include "hint_action.hpp"
#include "hint_label.hpp"

#include "browser/web_page.hpp"
#include "browser/web_view.hpp"
#include "browser/browser_window.hpp"
#include "core/get.hpp"
#include "global.hpp"

#include <iterator>

#include <boost/numeric/conversion/cast.hpp>

#include <QApplication>
#include <QKeyEvent>
#include <QRect>
#include <QString>
#include <QVariant>
#include <QWebEngineScript>

namespace natsu::modules::keyboard_hint {

template<typename C>
static int64_t
getSize(C const& container)
{
    return boost::numeric_cast<int64_t>(container.size());
}

static constexpr int64_t
getLabelCount()
{
    return 'Z' - 'A' + 1;
}

// Adaptor

Adaptor::
Adaptor(WebView& view)
    : QObject { &view }
    , m_view { view }
    , m_action { std::make_unique<HintAction>(m_view) }
{
    this->connect(m_action.get(), &QAction::triggered,
                  this,           &Adaptor::onActionTriggered);
}

Adaptor::~Adaptor() = default;

bool Adaptor::
eventFilter(QObject* const obj, QEvent* const ev)
{
    assert(ev);

    if (obj->parent() != &m_view) return false;

    switch (ev->type()) {
        case QEvent::MouseButtonPress:
        case QEvent::Wheel:
            hideHint();
            break;
        case QEvent::ShortcutOverride:
            ev->accept(); // cancel all shortcut keys
            break;
        case QEvent::KeyPress:
            return handleKeyPress(static_cast<QKeyEvent&>(*ev));
        default:
            break;
    }

    return false;
}

bool Adaptor::
handleKeyPress(QKeyEvent const& event)
{
    auto const code = event.key();

    switch (code) {
        case Qt::Key_Escape:
            hideHint();
            return true;
        case Qt::Key_Up:
        case Qt::Key_Down:
        case Qt::Key_Left:
        case Qt::Key_Right:
        case Qt::Key_PageUp:
        case Qt::Key_PageDown:
        case Qt::Key_Home:
        case Qt::Key_End:
            hideHint();
            return false;
        case Qt::Key_Space: {
            auto const modifiers = event.modifiers();

            if (modifiers == Qt::NoModifier) {
                nextPage();
            }
            else if (modifiers & Qt::ShiftModifier) {
                previousPage();
            }

            return true;
        }
        default:
            break;
    }

    if (code < Qt::Key_A || code > Qt::Key_Z) return false;

    auto const& text = QString(static_cast<char>('A' + code - Qt::Key_A));

    auto* const hint = m_view.findChild<HintLabel*>(text);
    if (hint) {
        auto const modifiers = event.modifiers();
        auto const& point = hint->mapToGlobal(hint->rect().center());

        click(point, modifiers);

        hideHint();
        return true;
    }

    return false;
}

void Adaptor::
showHint()
{
    auto const width = m_view.width() / m_view.zoomFactor();
    auto const height = m_view.height() / m_view.zoomFactor();

    auto isVisible = [width, height](auto const& rect) {
        if (rect.x() < 0 || rect.x() > width) return false;
        if (rect.y() < 0 || rect.y() > height) return false;
        if (rect.width() <= 0 || rect.height() <= 0) return false;

        return true;
    };

    auto const& script = QSL("keyboardHint.getElements();");

    auto callback = [&, isVisible](auto const& result) {
        m_elements.clear();

        auto const& list = result.toList();

        for (auto const& val: list) {
            auto const& element = val.toMap();

            auto const& rectMap = element[QSL("rect")].toMap();
            auto const x = rectMap[QSL("x")].toInt();
            auto const y = rectMap[QSL("y")].toInt();
            auto const width = rectMap[QSL("width")].toInt();
            auto const height = rectMap[QSL("height")].toInt();

            QRect rect { x, y, width, height };

            if (isVisible(rect)) {
                m_elements.push_back(std::move(rect));
            }
        }

        m_begin = m_end = 0;

        if (!m_elements.empty()) {
            nextPage();
        }
    };

    auto& page = get::webPage(m_view);
    page.runJavaScript(script, QWebEngineScript::MainWorld, callback);

    qApp->installEventFilter(this);
}

void Adaptor::
hideHint()
{
    removeHints();

    qApp->removeEventFilter(this);
}

void Adaptor::
nextPage()
{
    if (m_elements.empty()) return;

    removeHints();

    m_begin = m_end;
    if (m_begin >= getSize(m_elements)) {
        m_begin = 0;
    }

    m_end = m_begin + getLabelCount();
    if (m_end > getSize(m_elements)) {
        m_end = getSize(m_elements);
    }

    for (auto i = m_begin; i < m_end; ++i) {
        auto const label = static_cast<char>('A' + i - m_begin);
        auto const& rect = m_elements[static_cast<size_t>(i)];

        make_qmanaged<HintLabel>(QString(label), rect, m_view);
    }
}

void Adaptor::
previousPage()
{
    if (m_elements.empty()) return;

    removeHints();

    m_end = m_begin;
    if (m_end <= 0) {
        m_end = getSize(m_elements);
    }

    m_begin = m_end - getLabelCount();
    if (m_begin < 0) {
        m_begin = 0;
    }

    for (auto i = m_end - 1; i >= m_begin; --i) {
        auto const label = static_cast<char>('A' + i - m_begin);
        auto const& rect = m_elements[static_cast<size_t>(i)];

        make_qmanaged<HintLabel>(QString(label), rect, m_view);
    }
}

void Adaptor::
removeHints()
{
    for (auto* const child: m_view.findChildren<HintLabel*>()) {
        child->setParent(nullptr);
        child->deleteLater();
    }
}

void Adaptor::
click(QPoint const& point, Qt::KeyboardModifiers const modifiers) const
{
    auto& target = to_ref(qApp->widgetAt(point));

    QMouseEvent press {
        QEvent::MouseButtonPress,
        target.mapFromGlobal(point),
        point,
        Qt::LeftButton,
        Qt::LeftButton,
        modifiers
    };

    QApplication::sendEvent(&target, &press);

    QMouseEvent release {
        QEvent::MouseButtonRelease,
        target.mapFromGlobal(point),
        point,
        Qt::LeftButton,
        Qt::LeftButton,
        modifiers
    };

    QApplication::sendEvent(&target, &release);
}

void Adaptor::
onActionTriggered()
{
    showHint();
}

} // namespace natsu::modules::keyboard_hint
