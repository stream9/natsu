#include "hint_label.hpp"

#include "browser/web_view.hpp"
#include "global.hpp"

namespace natsu::modules::keyboard_hint {

HintLabel::
HintLabel(QString const& text, QRect const& rect, WebView& view)
    : QLabel(text, &view)
    , m_elementRect { rect }
{
    auto const zoomFactor = view.zoomFactor();

    this->setObjectName(text);
    this->setStyleSheet(
            QSL("color: black; background-color: rgba(255, 255, 0, 170);"));
    this->setFrameStyle(static_cast<int>(QFrame::Box) | static_cast<int>(QFrame::Plain));
    this->setAutoFillBackground(true);
    this->setAttribute(Qt::WA_TransparentForMouseEvents);
    this->adjustSize();

    auto geometry = this->geometry();
    geometry.moveTopLeft(rect.topLeft() * zoomFactor);
    this->setGeometry(geometry);

    this->show();
}

HintLabel::~HintLabel() = default;

} // namespace natsu::modules::keyboard_hint
