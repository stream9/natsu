#ifndef NATSU_MODULES_KEYBOARD_HINT_ADAPTOR_HPP
#define NATSU_MODULES_KEYBOARD_HINT_ADAPTOR_HPP

#include "browser/fwd/web_view.hpp"

#include <memory>
#include <vector>

#include <QObject>

class QAction;
class QEvent;
class QKeyEvent;
class QRect;

namespace natsu::modules::keyboard_hint {

class Adaptor : public QObject
{
    Q_OBJECT
public:
    Adaptor(WebView&);
    ~Adaptor() override;

    // override QObject
    bool eventFilter(QObject*, QEvent*) override;

private:
    bool handleKeyPress(QKeyEvent const&);
    void showHint();
    void hideHint();
    void removeHints();
    void nextPage();
    void previousPage();
    void click(QPoint const&, Qt::KeyboardModifiers) const;

    Q_SLOT void onActionTriggered();

private:
    WebView& m_view;

    std::unique_ptr<QAction> m_action;
    std::vector<QRect> m_elements;
    int64_t m_begin, m_end;
};

} // namespace natsu::modules::keyboard_hint

#endif // NATSU_MODULES_KEYBOARD_HINT_ADAPTOR_HPP
