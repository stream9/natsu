#ifndef NATSU_MODULES_KEYBOARD_HINT_HINT_ACTION_HPP
#define NATSU_MODULES_KEYBOARD_HINT_HINT_ACTION_HPP

#include "browser/fwd/web_view.hpp"

#include <QAction>
#include <QElapsedTimer>

namespace natsu::modules::keyboard_hint {

class HintAction : public QAction
{
    Q_OBJECT
public:
    HintAction(WebView&);
    ~HintAction() override;

    // override QObject
    bool eventFilter(QObject*, QEvent*) override;

private:
    QElapsedTimer m_time;
    int m_keyCode;
    int m_doubleTapInterval;
};

} // namespace natsu::modules::keyboard_hint

#endif // NATSU_MODULES_KEYBOARD_HINT_HINT_ACTION_HPP
