#ifndef NATSU_MODULES_KEYBOARD_HINT_HINT_LABEL_HPP
#define NATSU_MODULES_KEYBOARD_HINT_HINT_LABEL_HPP

#include "browser/fwd/web_view.hpp"

#include <QLabel>
#include <QRect>

class QString;
class QWidget;

namespace natsu::modules::keyboard_hint {

class HintLabel : public QLabel
{
    Q_OBJECT
public:
    HintLabel(QString const& text, QRect const& element, WebView&);

    ~HintLabel() override;

    // accessor
    QRect const& elementRect() const { return m_elementRect; }

private:
    QRect m_elementRect;
};

} // namespace natsu::modules::keyboard_hint

#endif // NATSU_MODULES_KEYBOARD_HINT_HINT_LABEL_HPP
