"use strict";

window.keyboardHint = {
  getElements: function () {

    const result =
      Array.from(document.body.getElementsByTagName("*"))
           .filter(this._isClickable)
           .map(this._makeData)
           ;

    return result;
  },

  click: function (x, y, shift, ctrl, alt, meta) {
    const el = document.elementFromPoint(x, y);
    if (el.tagName === "INPUT" || el.tagName === "TEXTAREA") {
      el.focus();
    }
    else {
      const ev = new MouseEvent("click", {
        clientX: x,
        clientY: y,
        shiftKey: !!shift,
        ctrlKey: !!ctrl,
        altKey: !!alt,
        metaKey: !!meta,
        view: window,
        bubbles: false,
        cancelable: false
      });

      //TODO this work only onece. i dont know why.
      const handled = el.dispatchEvent(ev);
    }
  },

  _isClickable: function (el) {
    return el.tagName === "A"
        || el.tagName === "BUTTON"
        || el.tagName === "INPUT"
        || el.tagName === "TEXTAREA"
        || el.onclick
        ;
  },

  _makeData: function (el) {
    const r = el.getBoundingClientRect();

    return {
      text: el.text,
      rect: {
        x: r.x,
        y: r.y,
        width: r.width,
        height: r.height
      }
    };
  },
};
