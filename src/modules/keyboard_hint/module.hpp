#ifndef NATSU_MODULES_KEYBOARD_HINT_MODULES_HPP
#define NATSU_MODULES_KEYBOARD_HINT_MODULES_HPP

#include "core/module.hpp"

#include "browser/fwd/browser_tab.hpp"
#include "core/fwd/application.hpp"
#include "core/fwd/profile.hpp"

namespace natsu::modules::keyboard_hint {

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    void injectScript(Profile&);

    // override plugin::Module
    bool doInitialize(Application&) override;

    Q_SLOT void onProfileCreated(Profile&);
    Q_SLOT void onTabCreated(BrowserTab&);
};

} // namespace natsu::modules::keyboard_hint

#endif // NATSU_MODULES_KEYBOARD_HINT_MODULES_HPP
