#include "hint_action.hpp"

#include "browser/web_view.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <QDir>
#include <QKeyEvent>
#include <QSettings>

namespace natsu::modules::keyboard_hint {

static QSettings
getSettings(Profile& profile)
{
    auto const& filePath =
                profile.pluginDir().filePath(QSL("keyboard_hint.ini"));

    return { filePath, QSettings::IniFormat };
}

// HintAction

HintAction::
HintAction(WebView& view)
    : QAction { QSL("Hint") }
{
    view.installEventFilter(this);

    auto settings = getSettings(get::profile(view));
    m_keyCode = settings.value(QSL("keyCode"), Qt::Key_Control).toInt();
    m_doubleTapInterval = settings.value(QSL("doubleTapInterval"), 500).toInt();
}

HintAction::~HintAction() = default;

bool HintAction::
eventFilter(QObject* const obj, QEvent* const ev)
{
    assert(ev);

    if (ev->type() == QEvent::ShortcutOverride) {
        auto& event = static_cast<QKeyEvent&>(*ev);

        if (event.key() == m_keyCode) {
            auto const elapsed = m_time.restart();
            if (elapsed > 0 && elapsed <= m_doubleTapInterval) {
                this->trigger();
            }
        }
    }

    return QAction::eventFilter(obj, ev);
}

} // namespace natsu::modules::keyboard_hint
