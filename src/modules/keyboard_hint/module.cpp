#include "module.hpp"

#include "adaptor.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "core/application.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <QFile>
#include <QWebEngineScript>
#include <QWebEngineScriptCollection>

inline void initResource()
{
    Q_INIT_RESOURCE(keyboard_hint);
}

namespace natsu::modules::keyboard_hint {

Module::Module()
{
    initResource();
}

Module::~Module() = default;

void Module::
injectScript(Profile& profile)
{
    QFile file { ":/keyboard_hint/script.js" };
    auto const ok = file.open(QFile::ReadOnly);
    assert(ok); (void)ok;

    QWebEngineScript script;
    script.setSourceCode(file.readAll());
    script.setName(QSL("keyboard_hint"));
    script.setRunsOnSubFrames(true);
    script.setWorldId(QWebEngineScript::MainWorld);
    script.setInjectionPoint(QWebEngineScript::DocumentReady);

    to_ref(profile.scripts()).insert(script);
}

bool Module::
doInitialize(Application& app)
{
    this->connect(&app, &Application::profileCreated,
                  this, &Module::onProfileCreated);

    return true;
}

void Module::
onProfileCreated(Profile& profile)
{
    injectScript(profile);

    this->connect(&profile, &Profile::windowCreated,
                  this, [&](auto& w) {
                      this->connect(&w,  &BrowserWindow::tabCreated,
                                    this, &Module::onTabCreated);
                  });
}

void Module::
onTabCreated(BrowserTab& tab)
{
    make_qmanaged<Adaptor>(tab.webView());
}

} // namespace natsu::modules::keyboard_hint
