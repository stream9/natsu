#include "module.hpp"

#include "browser/browser_window.hpp"
#include "browser/browser_window/menu_bar.hpp"
#include "core/application.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <QActionGroup>
#include <QNetworkProxy>

namespace natsu::modules::proxy {

Module::
Module() noexcept
    : m_group { make_qmanaged<QActionGroup>(this) }
    , m_noProxy { make_qmanaged<QAction>(QSL("None"), m_group) }
    , m_tor { make_qmanaged<QAction>(QSL("Tor"), m_group) }
{
    m_noProxy->setCheckable(true);
    m_noProxy->setChecked(true);
    m_tor->setCheckable(true);

    this->connect(m_group, &QActionGroup::triggered,
                  this,    &Module::onActionTriggered);
}

Module::~Module() noexcept = default;

bool Module::
doInitialize(Application& app) noexcept
{
    this->connect(&app, &Application::profileCreated,
                  this, [&](auto& p) {
                      this->connect(&p,   &Profile::windowCreated,
                                    this, &Module::onWindowCreated);
                  });

    return true;
}

void Module::
onWindowCreated(BrowserWindow& win) noexcept
{
    auto& menuBar = win.menuBar();

    auto& menu = to_ref(menuBar.toolMenu().addMenu(QSL("Proxy")));

    menu.addAction(m_noProxy);
    menu.addAction(m_tor);
}

void Module::
onActionTriggered(QAction* action) noexcept
{
    if (!action) return;

    if (action == m_noProxy) {
        QNetworkProxy proxy { QNetworkProxy::NoProxy };
        QNetworkProxy::setApplicationProxy(proxy);
    }
    else if (action == m_tor) {
        QNetworkProxy proxy { QNetworkProxy::Socks5Proxy, QSL("localhost"), 9050 };
        QNetworkProxy::setApplicationProxy(proxy);
    }
}

} // namespace natsu::modules::proxy
