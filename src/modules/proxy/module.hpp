#ifndef NATSU_MODULES_PROXY_MODULES_HPP
#define NATSU_MODULES_PROXY_MODULES_HPP

#include "core/fwd/application.hpp"
#include "core/module.hpp"
#include "browser/fwd/browser_window.hpp"
#include "misc/qmanaged_ptr.hpp"

class QAction;
class QActionGroup;

namespace natsu::modules::proxy {

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module() noexcept;
    ~Module() noexcept override;

private:
    // override plugin::Module
    bool doInitialize(Application&) noexcept override;

    Q_SLOT void onWindowCreated(BrowserWindow&) noexcept;
    Q_SLOT void onActionTriggered(QAction*) noexcept;

private:
    qmanaged_ptr<QActionGroup> m_group;
    qmanaged_ptr<QAction> m_noProxy;
    qmanaged_ptr<QAction> m_tor;
};

} // namespace natsu::modules::proxy

#endif // NATSU_MODULES_PROXY_MODULES_HPP
