#ifndef NATSU_MODULES_AUTHENTICATION_ADAPTOR_HPP
#define NATSU_MODULES_AUTHENTICATION_ADAPTOR_HPP

#include "core/namespace.hpp"
#include "browser/fwd/browser_tab.hpp"

#include <stream9/node.hpp>

#include <QObject>

class QAuthenticator;
class QUrl;

namespace natsu::modules::authentication {

class Adaptor : public QObject
{
    Q_OBJECT
public:
    Adaptor(BrowserTab&);
    ~Adaptor() override;

    // accessor
    BrowserTab& browserTab() const { return m_tab; }

private:
    Q_SLOT void onAuthenticationRequired(QUrl const&, QAuthenticator*);
    Q_SLOT void onLoadFinished(bool ok);

    void saveBasicAuth(QString const& url, QString const& name, QString const& password);

private:
    BrowserTab& m_tab;

    class PasswordDB;
    st9::node<PasswordDB> m_db;
    QString m_url;
    QString m_lastName;
    QString m_lastPassword;
};

} // namespace natsu::modules::authentication

#endif // NATSU_MODULES_AUTHENTICATION_ADAPTOR_HPP
