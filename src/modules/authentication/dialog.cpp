#include "dialog.hpp"

#include <QBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLineEdit>

namespace natsu::modules::authentication {

Dialog::
Dialog(QWidget& parent)
    : QDialog { &parent }
    , m_userName { make_qmanaged<QLineEdit>(this) }
    , m_password { make_qmanaged<QLineEdit>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    auto form = make_qmanaged<QFormLayout>();

    form->addRow(QSL("User name"), m_userName.get());
    form->addRow(QSL("Password"), m_password.get());

    m_password->setEchoMode(QLineEdit::Password);

    layout->addLayout(form.get(), 1);

    auto buttons = make_qmanaged<QDialogButtonBox>(this);

    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);

    this->connect(buttons.get(), &QDialogButtonBox::accepted,
                  this,          &QDialog::accept);
    this->connect(buttons.get(), &QDialogButtonBox::rejected,
                  this,          &QDialog::reject);

    layout->addWidget(buttons.get());

    this->setWindowTitle(QSL("Authentication Requested"));
    this->resize(400, 0);
}

Dialog::~Dialog() = default;

QString Dialog::
userName() const
{
    return m_userName->text();
}

QString Dialog::
password() const
{
    return m_password->text();
}

void Dialog::
setUserName(QString const& text)
{
    m_userName->setText(text);
}

void Dialog::
setPassword(QString const& text)
{
    m_password->setText(text);
}

} // namespace natsu::modules::authentication
