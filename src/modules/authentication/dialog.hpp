#ifndef NATSU_MODULES_AUTHENTICATION_DIALOG_HPP
#define NATSU_MODULES_AUTHENTICATION_DIALOG_HPP

#include "misc/pointer.hpp"

#include <QDialog>

class QLineEdit;

namespace natsu::modules::authentication {

class Dialog : public QDialog
{
public:
    Dialog(QWidget& parent);
    ~Dialog();

    // accessor
    QString userName() const;
    QString password() const;

    // modifier
    void setUserName(QString const&);
    void setPassword(QString const&);

private:
    qmanaged_ptr<QLineEdit> m_userName;
    qmanaged_ptr<QLineEdit> m_password;
};

} // namespace natsu::modules::authentication

#endif // NATSU_MODULES_AUTHENTICATION_DIALOG_HPP
