#include "adaptor.hpp"

#include "dialog.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "browser/web_page.hpp"
#include "core/conv.hpp"
#include "core/error.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"

#include <stream9/errors.hpp>
#include <stream9/optional.hpp>
#include <stream9/sqlite.hpp>

#include <QAuthenticator>
#include <QDir>
#include <QMessageBox>
#include <QUrl>

namespace natsu::modules::authentication {

namespace sqlite = st9::sqlite;

using st9::opt;

struct BasicAuth { QString name; QString password; };

class Adaptor::PasswordDB
{
public:
    PasswordDB(QString const& path)
        : m_db { toString(path) }
    {
        try {
            prepareTables();
        }
        catch (...) {
            rethrow_error({ { "path", toString(path) } });
        }
    }

    opt<BasicAuth>
    getBasicAuth(QString const& url)
    {
        try {
            static auto query = m_db.prepare(
                "SELECT name, password "
                "  FROM basic_auth "
                " WHERE url = :url "
            );

            query.exec(toString(url));

            opt<BasicAuth> result;

            if (!query.done()) {
                result = {
                    toQString(query.current_row().get_text(0)),
                    toQString(query.current_row().get_text(1)),
                };
            }

            return result;
        }
        catch (...) {
            rethrow_error({ { "url", toString(url) } });
        }
    }

    void
    setBasicAuth(QString const& url,
                 QString const& name, QString const& password)
    {
        try {
            static auto query = m_db.prepare(
                "INSERT OR REPLACE INTO basic_auth (url, name, password) "
                "VALUES ( :url, :name, :password ) "
            );

            query.exec(toString(url), toString(name), toString(password));
        }
        catch (...) {
            rethrow_error({
                { "url", toString(url) },
                { "name", toString(name) },
                { "password", toString(password) }
            });
        }
    }

private:
    void prepareTables()
    {
        try {
            m_db.exec(
                "CREATE TABLE IF NOT EXISTS basic_auth ( "
                "    url TEXT PRIMARY KEY, "
                "    name TEXT, "
                "    password TEXT,"
                "    last_modified DEFAULT CURRENT_TIMESTAMP "
                ") "
            );
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    sqlite::database m_db;
};

static QString
databasePath(Profile& profile)
{
    try {
        auto const& dir = profile.pluginDir();
        assert(dir.exists());

        return dir.filePath(QSL("password.db"));
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class Adaptor
 */
Adaptor::
Adaptor(BrowserTab& tab)
    : QObject { &tab }
    , m_tab { tab }
    , m_db { databasePath(get::profile(tab)) }
{
    try {
        auto& page = get::webPage(m_tab);

        this->connect(&page, &WebPage::authenticationRequired,
                      this,  &Adaptor::onAuthenticationRequired);
    }
    catch (...) {
        rethrow_error();
    }
}

Adaptor::~Adaptor() = default;

static bool
match(BasicAuth const& auth,
      QString const& name, QString const& password) noexcept
{
    return auth.name == name
        && auth.password == password;
}

void Adaptor::
onAuthenticationRequired(QUrl const& url_, QAuthenticator* const auth)
{
    try {
        auto& window = get::browserWindow(m_tab);
        auto url = url_.toString();

        Dialog dialog { window };

        auto savedAuth = m_db->getBasicAuth(url);
        if (savedAuth) {
            dialog.setUserName(savedAuth->name);
            dialog.setPassword(savedAuth->password);
        }

        if (dialog.exec() == QDialog::Accepted) {
            m_url = url;
            m_lastName = dialog.userName();
            m_lastPassword = dialog.password();

            auth->setUser(m_lastName);
            auth->setPassword(m_lastPassword);

            auto& page = get::webPage(m_tab);
            this->connect(&page, &QWebEnginePage::loadFinished,
                          this,  &Adaptor::onLoadFinished);
        }
        else {
            *auth = QAuthenticator();
        }
    }
    catch (...) {
        print_error({
            { "url", toString(url_.toString()) }
        });
    }
}

void Adaptor::
onLoadFinished(bool ok)
{
    try {
        auto& page = get::webPage(m_tab);
        this->disconnect(&page, &QWebEnginePage::loadFinished,
                         this,  &Adaptor::onLoadFinished);
        if (!ok) return;

        auto savedAuth = m_db->getBasicAuth(m_url);

        if (!savedAuth || !match(*savedAuth, m_lastName, m_lastPassword)) {
            saveBasicAuth(m_url, m_lastName, m_lastPassword);
        }
    }
    catch (...) {
        print_error({ { "ok", ok } });
    }
}

void Adaptor::
saveBasicAuth(QString const& url,
              QString const& name, QString const& password)
{
    try {
        auto& window = get::browserWindow(m_tab);
        auto res = QMessageBox::question(&window,
            QSL("Save password"),
            QSL("Do you want to save password?")
        );

        if (res == QMessageBox::Yes) {
            m_db->setBasicAuth(url, name, password);
        }
    }
    catch (...) {
        print_error({
            { "url", toString(url) },
            { "name", toString(name) },
            { "password", toString(password) }
        });
    }
}

} // namespace natsu::modules::authentication
