#ifndef NATSU_MODULES_AUTHENTICATION_MODULES_HPP
#define NATSU_MODULES_AUTHENTICATION_MODULES_HPP

#include "browser/fwd/browser_tab.hpp"
#include "browser/fwd/browser_window.hpp"
#include "core/fwd/application.hpp"
#include "core/fwd/profile.hpp"
#include "core/module.hpp"

namespace natsu::modules::authentication {

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    // override plugin::Module
    bool doInitialize(Application&) override;

    Q_SLOT void onProfileCreated(Profile&);
    Q_SLOT void onWindowCreated(BrowserWindow&);
    Q_SLOT void onTabCreated(BrowserTab&);
};

} // namespace natsu::modules::authentication

#endif // NATSU_MODULES_AUTHENTICATION_MODULES_HPP
