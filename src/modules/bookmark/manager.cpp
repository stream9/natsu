#include "manager.hpp"

#include "adaptor.hpp"
#include "actions.hpp"
#include "database.hpp"

#include "core/conv.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QDir>
#include <QUrl>
#include <QVariant>

namespace natsu::modules::bookmark {

using natsu::bookmark::Bookmark;
using natsu::bookmark::Folder;
using natsu::bookmark::Separator;
using natsu::bookmark::ReservedId;

static Bookmark
makeBookmarkItem(Record const& rec)
{
    return {
        rec.id,
        rec.parent,
        rec.row,
        toQString(rec.title),
        toQString(rec.url),
        toQString(rec.keyword),
        toQString(rec.description),
        toQString(rec.created_time),
        toQString(rec.accessed_time),
        toQString(rec.modified_time),
        rec.editable
    };
}

static Folder
makeFolderItem(Record const& rec)
{
    return {
        rec.id,
        rec.parent,
        rec.row,
        toQString(rec.title),
        toQString(rec.description),
        rec.editable
    };
}

static Separator
makeSeparatorItem(Record const& rec)
{
    return {
        rec.id, rec.parent, rec.row,
    };
}

static Item
makeItem(Record const& rec)
{
    switch (rec.type) {
        case Type::Bookmark:
            return makeBookmarkItem(rec);
        case Type::Folder:
            return makeFolderItem(rec);
        case Type::Separator:
            return makeSeparatorItem(rec);
        default: {
            qWarning() << QSL(
                "Unknown bookmark type in the database: "
                "id = %1, type = %2 "
            ).arg(rec.id)
             .arg(static_cast<int>(rec.type));

            return makeBookmarkItem(rec);
        }
    }
}

static QString
databasePath(Profile& profile)
{
    auto const& dir = profile.pluginDir();
    assert(dir.exists());

    return dir.filePath(QSL("bookmark.db"));
}

// Manager

Manager::
Manager(Profile& profile)
    : BookmarkManager { profile }
    , m_db { std::make_unique<Database>(databasePath(profile)) }
    , m_actions { std::make_unique<Actions>(*this) }
    , m_adaptor { std::make_unique<Adaptor>(*this) }
{}

Manager::~Manager() = default;

std::optional<Item> Manager::
item(Id const id) const
{
    auto o_rec = m_db->selectItemById(id);

    std::optional<Item> result;

    if (o_rec) {
        result = makeItem(*o_rec);
    }

    return result;
}

std::vector<Item> Manager::
items(Id const parent) const
{
    std::vector<Item> result;

    m_db->selectItems(parent,
        [&](auto&& rec) {
            result.push_back(makeItem(rec));
            return true;
        });

    return result;
}

Index Manager::
count(Id const parent) const
{
    return m_db->count(parent);
}

std::vector<Bookmark> Manager::
search(QString const& keyword) const
{
    std::vector<Bookmark> result;

    m_db->search(keyword,
        [&](auto const& rec) {
            result.push_back(makeBookmarkItem(rec));
            return true;
        });

    return result;
}

std::vector<Bookmark> Manager::
search(QUrl const& url) const
{
    std::vector<Bookmark> result;

    m_db->search(url,
        [&](auto const& rec) {
            result.push_back(makeBookmarkItem(rec));
            return true;
        });

    return result;
}

bool Manager::
contain(QUrl const& url) const
{
    bool result = false;

    m_db->search(url,
        [&](auto&&) {
            result = true;
            return true;
        });

    return result;
}

QVariant Manager::
loadSetting(QString const& key) const
{
    auto& settings = this->profile().settings();
    settings.beginGroup(QSL("BookmarkManager"));

    auto const value = settings.value(key);

    settings.endGroup();

    return value;
}

void Manager::
saveSetting(QString const& key, QVariant const& value) const
{
    auto& settings = this->profile().settings();
    settings.beginGroup(QSL("BookmarkManager"));

    settings.setValue(key, value);

    settings.endGroup();

    Q_EMIT settingChanged(key);
}

void Manager::
addBookmark(Id const parent, QString const& title, QUrl const& url,
            QString const& keyword, QString const& description)
{
    auto const row = m_db->newRow(parent);

    auto trans = m_db->beginTransaction();

    auto const id = m_db->insertItem(
        parent == ReservedId::Root ? ReservedId::OtherBookmark : parent,
        row, Type::Bookmark, title, url, keyword, description);

    Q_EMIT itemAdded(parent, id);

    trans.commit();
}

Id Manager::
addFolder(Id const parent, QString const& title, QString const& description)
{
    //TODO check arguments' validity

    auto const row = m_db->newRow(parent);

    auto trans = m_db->beginTransaction();

    auto const id = m_db->insertItem(
        parent == ReservedId::Root ? ReservedId::OtherBookmark : parent,
        row, Type::Folder, title, {}, {}, description);

    Q_EMIT itemAdded(parent, id);

    trans.commit();

    return id;
}

void Manager::
addSeparator(Id parent)
{
    auto const row = m_db->newRow(parent);

    auto trans = m_db->beginTransaction();

    auto const id = m_db->insertItem(
        parent == ReservedId::Root ? ReservedId::OtherBookmark : parent,
        row, Type::Separator, {}, {}, {}, {});

    Q_EMIT itemAdded(parent, id);

    trans.commit();
}

void Manager::
updateItem(Item const& item)
{
    auto trans = m_db->beginTransaction();

    auto const changes = m_db->updateItem(item);

    if (changes > 0) {
        auto const [parent, id] = std::visit(
            [](auto const& item) {
                return std::make_pair(item.parent, item.id);
            },
            item);

        Q_EMIT itemUpdated(parent, id);
    }

    trans.commit();
}

static void
moveItems(auto& db,
          st9::array_view<Id const> sources, Id target,
          auto&& getTargetRow,
          auto&& emitSignal)
{
    if (sources.empty()) return;

    auto oTargetRec = db->selectItemById(target);
    if (!oTargetRec) {
        qCritical() << "unknown before:" << target;
        return;
    }
    auto targetItem = makeItem(*oTargetRec);

    auto parent = getParent(targetItem);
    auto targetRow = getTargetRow(targetItem);

    auto trans = db->beginTransaction();

    db->shiftRow(parent, targetRow, sources.size());

    for (auto src: sources) {
        auto oSrcRec = db->selectItemById(src);
        if (!oSrcRec) {
            qCritical() << "unknown item:" << src;
            continue;
        }
        auto srcItem = makeItem(*oSrcRec);

        natsu::bookmark::setParent(srcItem, parent);
        setRow(srcItem, targetRow);

        auto changes = db->updateItem(srcItem);
        if (changes != 1) {
            qCritical() << "fail to insert:" << src << parent << target;
            return;
        }

        emitSignal(src, target);

        ++targetRow;
        target = src;
    }

    db->adjustRow(parent);

    trans.commit();
}

void Manager::
moveItemsBefore(st9::array_view<Id const> sources, Id target)
{
    moveItems(m_db, sources, target,
        [](auto& item) { return getRow(item); },
        [&](auto src, auto tgt) { itemMovedBefore(src, tgt); } );
}

void Manager::
moveItemsAfter(st9::array_view<Id const> sources, Id target) //TODO make this virtual
{
    moveItems(m_db, sources, target,
        [](auto& item) { return getRow(item) + 1; },
        [&](auto src, auto tgt) { itemMovedAfter(src, tgt); } );
}

static bool
isFolder(auto& db, Id id)
{
    auto oRec = db->selectItemById(id);

    return oRec && (oRec->type == Type::Folder);
}

void Manager::
moveItemsInto(st9::array_view<Id const> sources, Id parent)
{
    if (sources.empty()) return;

    if (!isFolder(m_db, parent)) {
        qCritical() << "parent isn't a folder:" << parent;
    }

    auto trans = m_db->beginTransaction();

    for (auto src: sources) {
        auto oSrcRec = m_db->selectItemById(src);
        if (!oSrcRec) {
            qCritical() << "unknown item:" << src;
            continue;
        }
        auto srcItem = makeItem(*oSrcRec);

        natsu::bookmark::setParent(srcItem, parent);
        setRow(srcItem, m_db->newRow(parent));

        auto changes = m_db->updateItem(srcItem);
        if (changes != 1) {
            qCritical() << "fail to append:" << src << parent;
            return;
        }

        Q_EMIT itemMovedInto(src, parent);
    }

    m_db->adjustRow(parent);

    trans.commit();
}

void Manager::
removeItem(Id const id)
{
    auto const& oRec = m_db->selectItemById(id);
    if (!oRec) {
        qWarning() << QSL("Tried to remove bookmark that doesn't exist. id:")
                   << id;
        return;
    }
    auto const parent = oRec->parent;
    auto const row = oRec->row;

    auto trans = m_db->beginTransaction();

    auto const changes = m_db->deleteItem(id);

    if (changes > 0) {
        Q_EMIT itemRemoved(parent, id, row);
    }

    trans.commit();
}

void Manager::
updateAccessTime(Id id)
{
    m_db->updateAccessTime(id);
}

QString Manager::
mimeType()
{
    return QSL("application/x-natsu-bookmark-data");
}

QString Manager::
setting(QString const& key) const
{
    auto& settings = this->profile().settings();

    settings.beginGroup(QSL("BookmarkManager"));

    auto const& result = settings.value(key).toString();

    settings.endGroup();

    return result;
}

} // namespace natsu::modules::bookmark
