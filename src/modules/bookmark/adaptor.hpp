#ifndef NATSU_MODULES_BOOKMARK_ADAPTOR_HPP
#define NATSU_MODULES_BOOKMARK_ADAPTOR_HPP

#include "browser/fwd/browser_tab.hpp"
#include "browser/fwd/browser_window.hpp"

#include <memory>

#include <QObject>

namespace natsu::modules::bookmark {

class Manager;

class Adaptor : public QObject
{
public:
    Adaptor(Manager&);
    ~Adaptor() override;

private:
    void installAddressBarAction(BrowserTab&) const;
    void installMenuBar(BrowserWindow&) const;
    void installToolBar(BrowserTab&) const;
    void installBrowserDock(BrowserWindow&) const;
    void installWindowActions(BrowserWindow&) const;

    Q_SLOT void onWindowCreated(BrowserWindow&) const;
    Q_SLOT void onTabCreated(BrowserTab&) const;

private:
    Manager& m_manager;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_ADAPTOR_HPP
