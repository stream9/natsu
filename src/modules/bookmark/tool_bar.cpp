#include "tool_bar.hpp"

#include "actions.hpp"
#include "context_menu.hpp"
#include "manager.hpp"
#include "menu.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "browser/web_page.hpp"
#include "core/action_manager.hpp"
#include "core/bookmark.hpp"
#include "core/get.hpp"
#include "core/history_manager.hpp"
#include "core/icon.hpp"
#include "core/namespace.hpp"
#include "global.hpp"
#include "misc/link_action.hpp"
#include "misc/variant.hpp"

#include <stream9/array.hpp>

#include <QAction>
#include <QActionEvent>
#include <QContextMenuEvent>
#include <QLayout>
#include <QSettings>
#include <QToolButton>
#include <QVariant>

namespace natsu::modules::bookmark {

using natsu::bookmark::Bookmark;
using natsu::bookmark::Folder;
using natsu::bookmark::Separator;
using natsu::bookmark::Item;
using natsu::bookmark::ReservedId;

static QString
settingKey()
{
    return QSL("bookmarkBarEnabled");
}

ToolBar::
ToolBar(Manager& manager, BrowserTab& tab)
    : ToolBarBase { &tab }
    , m_manager { manager }
    , m_browserTab { tab }
{
    this->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    auto const fm = this->fontMetrics();
    this->setIconSize(QSize(fm.height(), fm.height()));
    this->setStyleSheet(QSL("QToolBar { border: 0px; }"));
    this->layout()->setContentsMargins(1, 1, 1, 1);
    this->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);

    populateItems();

    this->connect(&m_manager, &Manager::itemAdded,
                  this,       &ToolBar::onItemAdded);
    this->connect(&m_manager, &Manager::itemUpdated,
                  this,       &ToolBar::onItemUpdated);
    this->connect(&m_manager, &Manager::itemRemoved,
                  this,       &ToolBar::onItemRemoved);
    this->connect(&m_manager, &Manager::itemMovedBefore,
                  this,       &ToolBar::onItemMovedBefore);
    this->connect(&m_manager, &Manager::settingChanged,
                  this,       &ToolBar::onSettingChanged);

    auto& page = get::webPage(m_browserTab);
    this->connect(&page, &WebPage::urlChanged,
                  this,  &ToolBar::onUrlChanged);

    auto& toggle = to_ref(this->toggleViewAction());
    toggle.setObjectName(QSL("bookmark.show_tool_bar"));
    toggle.setText(QSL("Show Bookmark Toolbar"));
    toggle.setIcon(natsu::bookmark::icon::showToolBar());

    auto& actionManager = get::actionManager(m_manager);
    actionManager.configure(QSL("Bookmark"), toggle, makeKeyCode(Qt::SHIFT, Qt::CTRL, Qt::Key_B));

    m_browserTab.addAction(&toggle);
}

ToolBar::~ToolBar() = default;

void ToolBar::
setVisible(bool const visible)
{
    auto& page = get::webPage(m_browserTab);

    if (page.isBlankPage()) {
        ToolBarBase::setVisible(true);
    }
    else {
        ToolBarBase::setVisible(visible);

        saveSettings();
    }
}

void ToolBar::
contextMenuEvent(QContextMenuEvent* const ev)
{
    auto& event = to_ref(ev);

    if (!this->rect().contains(event.pos())) return;

    auto* action = this->actionAt(event.pos());
    if (!action) return;

    auto const id = action->data();
    if (id.isNull()) return;

    auto& actions = m_manager.actions();
    actions.setSelection(id.value<Id>());

    ContextMenu menu { actions };
    menu.exec(event.globalPos());
}

void ToolBar::
mouseReleaseEvent(QMouseEvent* const ev)
{
    auto& event = to_ref(ev);

    auto* action = this->actionAt(event.pos());
    if (action && (event.button() == Qt::MiddleButton)) {
        action->trigger();
    }
    else {
        ToolBarBase::mouseReleaseEvent(ev);
    }
}

void ToolBar::
actionEvent(QActionEvent* const event)
{
    assert(event);

    ToolBarBase::actionEvent(event);

    if (event->type() == QEvent::ActionAdded) {
        auto& action = to_ref(event->action());
        adjustButtonStyle(action);
    }
}

void ToolBar::
onDrop(QDropEvent& event)
{
    auto* before = this->dropTarget(event.position().toPoint());

    ToolBarBase::onDrop(event);

    auto const& data = to_ref(event.mimeData());
    auto& action = to_ref(stream9::qt6::mixin::getAction(data));

    st9::array<Id> ids;
    ids.insert(action.data().value<Id>());
    auto parentId = ReservedId::BookmarkToolBar;

    if (before) {
        auto const beforeId = before->data().value<Id>();

        m_manager.moveItemsBefore(ids, beforeId);
    }
    else {
        m_manager.moveItemsInto(ids, parentId);
    }
}

void ToolBar::
populateItems()
{
    auto constexpr id = ReservedId::BookmarkToolBar;

    auto i = 0;
    for (auto const& item: m_manager.items(id)) {
        auto& action = makeAction(item);
        if (i < 8) {
            action.setShortcut(Qt::Key_F1 + i);
            ++i;
        }
        this->addAction(&action);
    }
}

QAction& ToolBar::
makeAction(Item const& item)
{
    auto& window = get::browserWindow(m_browserTab);

    return std::visit(overloaded { //TODO duplicate with Menu
        [&](Bookmark const& item) -> QAction& {
            auto& history = get::history(m_browserTab);
            auto action = make_qmanaged<LinkAction>(
                item.title,
                item.url,
                history.icon(item.url),
                window,
                *this
            );
            action->setData(QVariant::fromValue(item.id));

            return *action;
        },
        [&](Folder const& item) -> QAction& {
            auto menu = make_qmanaged<Menu>(m_manager, item.id, *this);
            menu->setTitle(item.title);
            menu->setIcon(natsu::bookmark::icon::folder());

            auto& action = to_ref(menu->menuAction());
            action.setData(QVariant::fromValue(item.id));

            return action;
        },
        [&](Separator const&) -> QAction& { //TODO Separator cant be in toolbar
            auto action = make_qmanaged<QAction>(this);
            action->setSeparator(true);

            return *action;
        }
    }, item);
}

QAction* ToolBar::
findAction(Id const id)
{
    auto const& actions = this->actions();

    auto const it = std::find_if(
        actions.begin(), actions.end(),
        [&](auto* const action) {
            assert(action);
            QVariant const& data = action->data(); // "auto" cause compile error

            return data.value<Id>() == id;
        }
    );

    return it != actions.end() ? *it : nullptr;
}

void ToolBar::
loadSettings()
{
    ToolBarBase::setVisible(m_manager.loadSetting(settingKey()).toBool());
}

void ToolBar::
saveSettings()
{
    m_manager.saveSetting(settingKey(), this->isVisible());
}

void ToolBar::
adjustButtonStyle(QAction& action)
{
    auto* const button =
        dynamic_cast<QToolButton*>(this->widgetForAction(&action));
    if (button) {
        button->setPopupMode(QToolButton::InstantPopup);

        if (action.text().isEmpty()) {
            button->setToolButtonStyle(Qt::ToolButtonIconOnly);
        }
    }
}

void ToolBar::
onUrlChanged(QUrl const&)
{
    auto& page = get::webPage(m_browserTab);
    if (page.isBlankPage()) {
        ToolBarBase::setVisible(true);
    }
    else {
        loadSettings();
    }
}

void ToolBar::
onSettingChanged()
{
    auto& page = get::webPage(m_browserTab);
    if (!page.isBlankPage()) {
        loadSettings();
    }
}

void ToolBar::
onItemAdded(Id const parent, Id const id)
{
    if (parent != ReservedId::BookmarkToolBar) return;

    auto const& item = m_manager.item(id);
    assert(item);

    auto& action = makeAction(*item);
    auto const& actions = this->actions();

    auto const row = getRow(*item);
    auto const last = actions.size() - 1;

    auto* const before =
        row > last ? nullptr : actions.at(static_cast<int>(row));

    this->insertAction(before, &action);
}

void ToolBar::
onItemUpdated(Id const parent, Id const id)
{
    if (parent != ReservedId::BookmarkToolBar) return;

    auto* const action = findAction(id);
    assert(action);

    this->removeAction(action);

    onItemAdded(parent, id);
}

void ToolBar::
onItemRemoved(Id const parent, Id const id)
{
    if (parent != ReservedId::BookmarkToolBar) return;

    auto* const action = findAction(id);
    assert(action);

    this->removeAction(action);
}

void ToolBar::
onItemMovedBefore(Id src, Id dest)
{
    auto* action = findAction(src);

    auto oDestItem = m_manager.item(dest);
    if (!oDestItem) {
        qCritical() << "unknown dest" << dest;
        return;
    }

    auto parent = getParent(*oDestItem);
    if (parent == ReservedId::BookmarkToolBar) {
        if (!action) {
            auto const& item = m_manager.item(src);
            assert(item);

            action = &makeAction(*item);
        }

        QAction* beforeAction = findAction(dest);
        assert(beforeAction);

        this->insertAction(beforeAction, action);
    }
    else {
        if (action) {
            this->removeAction(action);
        }
        else {
            // irrelevant item, just ignore
        }
    }
}

} // namespace natsu::modules::bookmark
