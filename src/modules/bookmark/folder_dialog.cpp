#include "folder_dialog.hpp"

#include "core/bookmark.hpp"

#include "global.hpp"

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QKeyEvent>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>

namespace natsu::modules::bookmark {

using natsu::bookmark::Folder;

FolderDialog::
FolderDialog(QWidget& parent)
    : QDialog { &parent }
    , m_title { make_qmanaged<QLineEdit>(this) }
    , m_description { make_qmanaged<QPlainTextEdit>(this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    auto form = make_qmanaged<QFormLayout>();
    form->addRow(QSL("Title:"), m_title.get());
    form->addRow(QSL("Description:"), m_description.get());

    layout->addLayout(form);

    m_buttons->addButton(QDialogButtonBox::Ok);
    m_buttons->addButton(QDialogButtonBox::Cancel);
    to_ref(m_buttons->button(QDialogButtonBox::Ok)).setEnabled(false);
    layout->addWidget(m_buttons);

    this->connect(m_title.get(), &QLineEdit::textEdited,
                  this,          &FolderDialog::onEdited);
    this->connect(m_description.get(), &QPlainTextEdit::textChanged,
                  this,                &FolderDialog::onEdited);
    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &QDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &QDialog::reject);

    this->setWindowTitle(QSL("New Folder"));
    this->resize(400, 200);
}

FolderDialog::
FolderDialog(Folder const& folder, QWidget& parent)
    : FolderDialog { parent }
{
    m_title->setText(folder.title);
    m_title->setCursorPosition(0);
    m_description->setPlainText(folder.description);
    setReadOnly(!folder.editable);

    to_ref(m_buttons->button(QDialogButtonBox::Ok)).setEnabled(false);

    this->setWindowTitle(QSL("Properties of %1").arg(folder.title));
    m_description->setFocus();
    m_description->selectAll();
}

QString FolderDialog::
title() const
{
    return m_title->text();
}

QString FolderDialog::
description() const
{
    return m_description->toPlainText();
}

void FolderDialog::
keyPressEvent(QKeyEvent* const e)
{
    auto& event = to_ref(e);

    if (event.modifiers() & Qt::ControlModifier
                            && event.key() == Qt::Key_Return)
    {
        if (to_ref(m_buttons->button(QDialogButtonBox::Ok)).isEnabled()) {
            this->accept();
        }
    }

    QDialog::keyPressEvent(e);
}

void FolderDialog::
setReadOnly(bool const flag)
{
    m_readOnly = flag;

    m_title->setEnabled(!flag);
    m_description->setEnabled(!flag);
}

void FolderDialog::
onEdited()
{
    to_ref(m_buttons->button(QDialogButtonBox::Ok)).setEnabled(true);
}

} // namespace natsu::modules::bookmark
