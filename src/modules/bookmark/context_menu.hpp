#ifndef NATSU_MODULES_BOOKMARK_CONTEXT_MENU_HPP
#define NATSU_MODULES_BOOKMARK_CONTEXT_MENU_HPP

#include <QMenu>

namespace natsu::modules::bookmark {

class Actions;

class ContextMenu : public QMenu
{
    Q_OBJECT
public:
    ContextMenu(Actions&);
    ~ContextMenu() override;

    // accessor
    Actions& actions() const { return m_actions; }

private:
    void populateItems();

    Q_SLOT void onAboutToShow();

private:
    Actions& m_actions;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_CONTEXT_MENU_HPP
