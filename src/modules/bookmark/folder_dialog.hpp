#ifndef NATSU_FOLDER_DIALOG_HPP
#define NATSU_FOLDER_DIALOG_HPP

#include "misc/pointer.hpp"

#include <QDialog>

class QDialogButtonBox;
class QKeyEvent;
class QLineEdit;
class QPlainTextEdit;
class QWidget;

namespace natsu::bookmark { class Folder; } // namespace natsu::bookmark

namespace natsu::modules::bookmark {

using natsu::bookmark::Folder;

class FolderDialog : public QDialog
{
    Q_OBJECT
public:
    FolderDialog(QWidget& parent);
    FolderDialog(Folder const&, QWidget& parent);

    // accessor
    QString title() const;
    QString description() const;
    bool isReadOnly() const { return m_readOnly; }

protected:
    // override QWidget
    void keyPressEvent(QKeyEvent*) override;

private:
    void setReadOnly(bool flag);

    Q_SLOT void onEdited();

private:
    qmanaged_ptr<QLineEdit> m_title;
    qmanaged_ptr<QPlainTextEdit> m_description;
    qmanaged_ptr<QDialogButtonBox> m_buttons;

    bool m_readOnly = false;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_FOLDER_DIALOG_HPP
