#include "actions.hpp"

#include "json.hpp"

#include "core/bookmark.hpp"
#include "core/bookmark_manager.hpp"
#include "core/get.hpp"
#include "core/action_manager.hpp"
#include "global.hpp"

#include <QApplication>
#include <QFileDialog>
#include <QIcon>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QKeySequence>
#include <QSaveFile>
#include <QWidget>

namespace natsu::modules::bookmark::backup {

using natsu::bookmark::Bookmark;
using natsu::bookmark::Folder;
using natsu::bookmark::Separator;
using natsu::bookmark::ReservedId;

Actions::
Actions(BookmarkManager& manager)
    : m_manager { manager }
{
    auto& actionManager = get::actionManager(m_manager);
    auto const& category = QSL("Bookmark");

    auto c = [&](auto& action, auto const& name, auto const& text,
                 auto const& icon, auto const fn, auto const& shortcut)
    {
        action.setObjectName(name);
        action.setText(text);
        action.setIcon(icon);

        this->connect(&action, &QAction::triggered, this, fn);

        actionManager.configure(category, action, shortcut);
    };

    c(m_export, QSL("bookmark.export"), QSL("&Export"), QIcon(), &Actions::onExport, QKeySequence());
    c(m_import, QSL("bookmark.import"), QSL("&Import"), QIcon(), &Actions::onImport, QKeySequence());
}

Actions::~Actions() = default;

void Actions::
installTo(QWidget& target)
{
    target.addAction(&m_export);
    target.addAction(&m_import);
}

void Actions::
onExport() const
{
    auto* const parent = qApp->activeWindow();

    auto const& filename = QFileDialog::getSaveFileName(
        parent,
        QSL("Backup file name"),
        QSL("natsu_bookmark.json"),
        QSL("json (*.json)")
    );
    if (filename.isEmpty()) return;

    QSaveFile file { filename };
    if (!file.open(QSaveFile::WriteOnly)) {
        qWarning() << "fail to open file for writing:" << filename;
        return;
    }

    auto const& document = exportToJson(m_manager);

    file.write(document.toJson());

    file.commit();
}

void Actions::
onImport() const
{
    auto* const parent = qApp->activeWindow();

    auto const& filename = QFileDialog::getOpenFileName(
        parent,
        QSL("Input filename to import from"),
        QSL("natsu_bookmark.json"),
        QSL("json (*.json)")
    );
    if (filename.isEmpty()) return;

    QFile file { filename };
    if (!file.open(QFile::ReadOnly)) {
        qWarning() << "fail to open file for reading:" << filename;
        return;
    }

    auto const id =
        m_manager.addFolder(ReservedId::OtherBookmark, QSL("Imported"), "");

    auto const& bytes = file.readAll();
    QJsonParseError error;
    auto const& document = QJsonDocument::fromJson(bytes, &error);
    if (document.isNull()) {
        qWarning() << "JSON parse error:" << error.errorString();
        return;
    }

    importFromJson(m_manager, document, id);
}

} // namespace natsu::modules::bookmark::backup
