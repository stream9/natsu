#include "adaptor.hpp"

#include "actions.hpp"

#include "browser/browser_window.hpp"
#include "browser/browser_window/menu_bar.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "core/profile.hpp"
#include "misc/action_utility.hpp"

#include <QAction>
#include <QMenuBar>

namespace natsu::modules::bookmark::backup {

// Adaptor

Adaptor::
Adaptor(Profile& profile)
    : m_profile { profile }
    , m_actions { std::make_unique<Actions>(get::bookmark(m_profile)) }
{
    auto& app = get::application(m_profile);

    this->connect(&app, &Application::profileCreated,
                  this, [&](auto& p) {
                      this->connect(&p,   &Profile::windowCreated,
                                    this, &Adaptor::onWindowCreated);
                  });
}

Adaptor::~Adaptor() = default;

void Adaptor::
onWindowCreated(BrowserWindow& window)
{
    auto& menuBar = window.menuBar();

    auto& toolMenu = menuBar.toolMenu();
    auto& menu = to_ref(toolMenu.addMenu(QSL("Bookmark")));
    m_actions->installTo(menu);
}

} // namespace natsu::modules::bookmark::backup
