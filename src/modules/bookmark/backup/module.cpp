#include "module.hpp"

#include "adaptor.hpp"

#include "core/application.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <memory>

namespace natsu::modules::bookmark::backup {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    for (auto const& profile: app.profiles()) {
        m_adaptors.emplace(profile.get(), std::make_unique<Adaptor>(*profile));
    }

    return true;
}

} // namespace natsu::modules::bookmark::backup
