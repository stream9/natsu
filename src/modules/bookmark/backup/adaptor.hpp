#ifndef NATSU_MODULES_BOOKMARK_BACKUP_ADAPTOR_HPP
#define NATSU_MODULES_BOOKMARK_BACKUP_ADAPTOR_HPP

#include <memory>

#include <QObject>

namespace natsu {

class BrowserWindow;
class Profile;

} // namespace natsu

namespace natsu::modules::bookmark::backup {

class Actions;

class Adaptor : public QObject
{
    Q_OBJECT
public:
    Adaptor(Profile&);
    ~Adaptor() override;

    // accessor
    Profile& profile() const { return m_profile; }

private:
    Q_SLOT void onWindowCreated(BrowserWindow&);

private:
    Profile& m_profile;

    std::unique_ptr<Actions> const m_actions; // non-null
};

} // namespace natsu::modules::bookmark::backup

#endif // NATSU_MODULES_BOOKMARK_BACKUP_ADAPTOR_HPP
