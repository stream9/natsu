#ifndef NATSU_MODULES_BOOKMARK_BACKUP_ACTIONS_HPP
#define NATSU_MODULES_BOOKMARK_BACKUP_ACTIONS_HPP

#include <QAction>
#include <QObject>

class QWidget;

namespace natsu { class BookmarkManager; } // namespace natsu

namespace natsu::modules::bookmark::backup {

class Actions : public QObject
{
    Q_OBJECT
public:
    Actions(BookmarkManager&);
    ~Actions() override;

    // accessor
    QAction& exportAction() { return m_export; }
    QAction& importAction() { return m_import; }

    // command
    void installTo(QWidget&);

private:
    Q_SLOT void onExport() const;
    Q_SLOT void onImport() const;

private:
    BookmarkManager& m_manager;

    QAction m_export;
    QAction m_import;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_BACKUP_ACTIONS_HPP
