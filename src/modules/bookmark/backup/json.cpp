#include "json.hpp"

#include "core/bookmark_manager.hpp"
#include "misc/variant.hpp"
#include "global.hpp"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QUrl>

namespace natsu::modules::bookmark::backup {

using namespace natsu::bookmark;

static void
exportFolder(BookmarkManager& manager,
             Id const parent, QJsonArray& array)
{
    for (auto const& child: manager.items(parent)) {
        QJsonObject obj;

        std::visit(overloaded {
            [&](Bookmark const& item) {
                obj.insert(QSL("type"), QSL("bookmark"));
                obj.insert(QSL("id"), static_cast<qint64>(item.id));
                obj.insert(QSL("title"), item.title);
                obj.insert(QSL("url"), item.url);
                obj.insert(QSL("keyword"), item.keyword);
                obj.insert(QSL("description"), item.description);
            },
            [&](Folder const& item) {
                obj.insert(QSL("type"), QSL("folder"));
                obj.insert(QSL("id"), static_cast<qint64>(item.id));
                obj.insert(QSL("title"), item.title);
                obj.insert(QSL("description"), item.description);

                QJsonArray children;
                exportFolder(manager, item.id, children);

                obj.insert(QSL("children"), children);
            },
            [&](Separator const& item) {
                obj.insert(QSL("type"), QSL("separator"));
                obj.insert(QSL("id"), static_cast<qint64>(item.id));
            }
        }, child);

        array.append(obj);
    }
}

QJsonDocument
exportToJson(BookmarkManager& manager)
{
    QJsonArray items;

    exportFolder(manager, ReservedId::Root, items);

    return QJsonDocument { items };
}

static void
importFolder(BookmarkManager& manager,
             QJsonArray const& items, Id const parent)
{
    for (auto const& item: items) {
        assert(item.isObject());
        auto const& obj = item.toObject();

        auto const& type = obj.value(QSL("type")).toString();

        if (type == QSL("bookmark")) {
            auto const& title = obj.value(QSL("title")).toString();
            auto const& url = obj.value(QSL("url")).toString();
            auto const& keyword = obj.value(QSL("keyword")).toString();
            auto const& description = obj.value(QSL("description")).toString();

            manager.addBookmark(parent, title, url, keyword, description);
        }
        else if (type == QSL("folder")) {
            auto const& title = obj.value(QSL("title")).toString();
            auto const& description = obj.value(QSL("description")).toString();
            auto const& children = obj.value(QSL("children")).toArray();

            auto const id = manager.addFolder(parent, title, description);

            importFolder(manager, children, id);
        }
        else if (type == QSL("separator")) {
            manager.addSeparator(parent);

        }
        else {
            assert(false);
        }
    }
}

void
importFromJson(BookmarkManager& manager,
               QJsonDocument const& document, Id const id)
{
    assert(document.isArray());
    importFolder(manager, document.array(), id);
}

} // namespace natsu::modules::bookmark::backup
