#ifndef NATSU_MODULES_BOOKMARK_BACKUP_JSON_HPP
#define NATSU_MODULES_BOOKMARK_BACKUP_JSON_HPP

#include "core/bookmark.hpp"

class QJsonDocument;

namespace natsu { class BookmarkManager; } // namespace natsu

namespace natsu::modules::bookmark::backup {

using natsu::bookmark::Id;

QJsonDocument exportToJson(BookmarkManager&);

void importFromJson(BookmarkManager&, QJsonDocument const&, Id);

} // namespace natsu::modules::bookmark::backup

#endif // NATSU_MODULES_BOOKMARK_BACKUP_JSON_HPP
