#ifndef NATSU_MODULES_BOOKMARK_BACKUP_MODULES_HPP
#define NATSU_MODULES_BOOKMARK_BACKUP_MODULES_HPP

#include "core/module.hpp"

#include <memory>

#include <boost/container/flat_map.hpp>

namespace natsu {

class Application;
class Profile;

} // namespace natsu

namespace natsu::modules::bookmark::backup {

class Adaptor;

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    // override plugin::Module
    bool doInitialize(Application&) override;

private:
    boost::container::flat_map<Profile*, std::unique_ptr<Adaptor>> m_adaptors;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_BACKUP_MODULES_HPP
