#include "menu.hpp"

#include "actions.hpp"
#include "context_menu.hpp"
#include "manager.hpp"

#include "core/bookmark.hpp"
#include "core/get.hpp"
#include "core/history_manager.hpp"
#include "core/icon.hpp"
#include "core/namespace.hpp"
#include "global.hpp"
#include "misc/link_action.hpp"
#include "misc/pointer.hpp"
#include "misc/variant.hpp"

#include <stream9/array.hpp>

#include <QMouseEvent>

namespace natsu::modules::bookmark {

using natsu::bookmark::Bookmark;
using natsu::bookmark::Folder;
using natsu::bookmark::Separator;
using natsu::bookmark::Item;
using natsu::bookmark::ReservedId;

Menu::
Menu(Manager& manager, Id const id, QWidget& parent)
    : MenuBase { &parent }
    , m_manager { manager }
    , m_id { id }
{
    this->connect(this, &QMenu::aboutToShow,
                  this, &Menu::populateItems);

    this->setToolTipsVisible(true);
}

Menu::~Menu() = default;

void Menu::
onDrop(QDropEvent& event)
{
    auto* before = this->dropTarget(event.position().toPoint());

    MenuBase::onDrop(event);

    auto const& data = to_ref(event.mimeData());
    auto& action = to_ref(stream9::qt6::mixin::getAction(data));

    st9::array<Id> ids;
    ids.insert(action.data().value<Id>());
    auto parentId = m_id;

    if (before) {
        auto beforeId = before->data().value<Id>();

        m_manager.moveItemsBefore(ids, beforeId);
    }
    else {
        m_manager.moveItemsInto(ids, parentId);
    }
}

void Menu::
populateItems()
{
    this->clear();

    populateTopItems();

    auto& window = get::activeWindow(m_manager);
    auto& history = get::history(m_manager);
    auto const& fm = this->fontMetrics();
    auto const elideMode = Qt::ElideRight;
    auto constexpr width = 300;

    for (auto const& item: m_manager.items(m_id)) {
        std::visit(overloaded {
            [&](Bookmark const& item) {
                auto action = make_qmanaged<LinkAction>(
                    item.title,
                    item.url,
                    history.icon(item.url),
                    window,
                    *this
                );

                action->setText(fm.elidedText(item.title, elideMode, width));
                action->setData(QVariant::fromValue(item.id));
                this->addAction(action.get());
            },
            [&](Folder const& item) {
                auto menu = make_qmanaged<Menu>(m_manager, item.id, *this);

                auto const& title = fm.elidedText(item.title, elideMode, width);
                menu->setTitle(title);

                menu->setIcon(natsu::bookmark::icon::folder());
                to_ref(menu->menuAction()).setData(QVariant::fromValue(item.id));
                this->addMenu(menu.get());
            },
            [&](Separator const&) {
                this->addSeparator();
            }
        }, item);
    }
}

// QWidget::contextMenuEvent send useless event to QMenu
void Menu::
mousePressEvent(QMouseEvent* const event_)
{
    MenuBase::mousePressEvent(event_);

    auto& event = to_ref(event_);

    if (event.button() != Qt::RightButton) return;
    if (!this->rect().contains(event.pos())) return;

    auto* action = this->actionAt(event.pos());
    if (!action) return;

    auto const id = action->data();
    if (id.isNull()) return;

    auto& actions = m_manager.actions();
    actions.setSelection(id.value<Id>());

    ContextMenu menu { actions };
    menu.exec(event.globalPosition().toPoint());

    // hide top level menu
    auto* ptr = this;
    while (dynamic_cast<Menu*>(ptr->parent())) {
        ptr = static_cast<Menu*>(ptr->parent());
    }
    ptr->hide();
}

// TopLevelMenu

void TopLevelMenu::
populateTopItems()
{
    auto& actions = manager().actions();

    this->addAction(&actions.bookmarkThisPageAction());
    this->addSeparator();
}

} // namespace natsu::modules::bookmark
