#ifndef NATSU_MODULES_BOOKMARK_MENU_HPP
#define NATSU_MODULES_BOOKMARK_MENU_HPP

#include "core/bookmark.hpp"

#include <QMenu>

#include <stream9/qt6/mixin/menu/drag.hpp>
#include <stream9/qt6/mixin/menu/drop.hpp>

class QContextMenuEvent;
class QWidget;

namespace natsu::modules::bookmark {

class Manager;

using natsu::bookmark::Id;

using MenuBase = stream9::qt6::mixin::menu::Drag<
                 stream9::qt6::mixin::menu::Drop<QMenu>>;

class Menu : public MenuBase
{
    Q_OBJECT
public:
    Menu(Manager&, Id, QWidget& parent);
    ~Menu() override;

    // accessor
    Manager& manager() const { return m_manager; }
    Id id() const { return m_id; }

protected:
    // override stream9::qt::mixin::widget::Drop
    void onDrop(QDropEvent&) override;

private:
    Q_SLOT void populateItems();

    virtual void populateTopItems() {}

    // overload QWidget
    void mousePressEvent(QMouseEvent*) override;

private:
    Manager& m_manager;
    Id const m_id;
};


class TopLevelMenu : public Menu
{
public:
    using Menu::Menu;

private:
    // override Menu
    void populateTopItems() override;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_MENU_HPP
