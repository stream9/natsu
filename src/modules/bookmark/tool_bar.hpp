#ifndef NATSU_MODULES_BOOKMARK_TOOL_BAR_HPP
#define NATSU_MODULES_BOOKMARK_TOOL_BAR_HPP

#include "core/bookmark.hpp"

#include <optional>

#include <QAction>
#include <QToolBar>

#include <stream9/qt6/mixin/tool_bar/drag.hpp>
#include <stream9/qt6/mixin/tool_bar/drop.hpp>

class QActionEvent;
class QContextMenuEvent;
class QDropEvent;
class QMouseEvent;
class QWidget;

namespace natsu { class BrowserTab; }

namespace natsu::modules::bookmark {

using natsu::bookmark::Id;
using natsu::bookmark::Item;

using ToolBarBase = stream9::qt6::mixin::tool_bar::Drag<
                    stream9::qt6::mixin::tool_bar::Drop<QToolBar>>;

class Manager;

class ToolBar : public ToolBarBase
{
    Q_OBJECT
public:
    ToolBar(Manager&, BrowserTab&);
    ~ToolBar() override;

    // accessor
    BrowserTab& browserTab() const { return m_browserTab; }

    // override QWidget
    void setVisible(bool visible) override;

protected:
    // override QWidget
    void contextMenuEvent(QContextMenuEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void actionEvent(QActionEvent*) override;

    // override stream9::qt::mixin::widget::Drop
    void onDrop(QDropEvent&) override;

private:
    void populateItems();
    QAction& makeAction(Item const&);
    QAction* findAction(Id);
    void saveSettings();
    void loadSettings();
    void adjustButtonStyle(QAction&);

    Q_SLOT void onUrlChanged(QUrl const&);
    Q_SLOT void onSettingChanged();
    Q_SLOT void onItemAdded(Id parent, Id);
    Q_SLOT void onItemUpdated(Id parent, Id);
    Q_SLOT void onItemRemoved(Id parent, Id);
    Q_SLOT void onItemMovedBefore(Id src, Id dest);

private:
    Manager& m_manager;
    BrowserTab& m_browserTab;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_TOOL_BAR_HPP
