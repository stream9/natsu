#ifndef NATSU_MODULES_BOOKMARK_ADD_DIALOG_HPP
#define NATSU_MODULES_BOOKMARK_ADD_DIALOG_HPP

#include "misc/pointer.hpp"
#include "core/bookmark.hpp"

#include <QDialog>

class QDialogButtonBox;
class QLineEdit;
class QString;
class QSize;

namespace natsu { class BrowserWindow; }

namespace natsu::modules::bookmark::add_dialog { class FolderComboBox; } // namespace natsu::bookmark::add_dialog

namespace natsu::modules::bookmark {

using natsu::bookmark::Id;

class Manager;

class AddDialog : public QDialog
{
public:
    AddDialog(Manager&, BrowserWindow&);
    ~AddDialog() override;

    // accessor
    Manager& manager() const { return m_manager; }

    // query
    QString title() const;
    Id folder() const;

    // override QWidget
    QSize sizeHint() const override;

private:
    void setupValues();

private:
    Manager& m_manager;
    BrowserWindow& m_window;

    qmanaged_ptr<QLineEdit> const m_title;
    qmanaged_ptr<add_dialog::FolderComboBox> const m_folder;
    qmanaged_ptr<QDialogButtonBox> const m_buttons;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_ADD_DIALOG_HPP
