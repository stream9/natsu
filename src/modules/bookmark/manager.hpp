#ifndef NATSU_MODULES_BOOKMARK_MANAGER_HPP
#define NATSU_MODULES_BOOKMARK_MANAGER_HPP

#include "core/bookmark.hpp"
#include "core/bookmark_manager.hpp"
#include "core/namespace.hpp"

#include <stream9/array_view.hpp>

#include <memory>
#include <optional>
#include <vector>

#include <QObject>

class QVariant;

namespace natsu { class Profile; } // namespace natsu

namespace natsu::modules::bookmark {

class Actions;
class Adaptor;
class Database;

class Manager : public BookmarkManager
{
    Q_OBJECT
public:
    Manager(Profile&);
    ~Manager() override;

    // accessor
    Actions& actions() const { return *m_actions; }

    // query
    std::optional<Item> item(Id) const override;
    std::vector<Item> items(Id parent) const override;
    Index count(Id parent) const override;

    std::vector<Bookmark> search(QString const& keyword) const override;
    std::vector<Bookmark> search(QUrl const&) const override;

    bool contain(QUrl const&) const override;

    // modifier
    void addBookmark(Id parent, QString const& title, QUrl const& url,
                     QString const& keyword, QString const& description) override;
    Id addFolder(Id parent, QString const& title, QString const& description) override;
    void addSeparator(Id parent) override;

    void updateItem(Item const&) override;
    void moveItemsBefore(st9::array_view<Id const> sources, Id target) override;
    void moveItemsAfter(st9::array_view<Id const> sources, Id target) override;
    void moveItemsInto(st9::array_view<Id const> sources, Id folder) override;
    void removeItem(Id) override;

    void updateAccessTime(Id);

    // setting
    QVariant loadSetting(QString const& key) const;
    void saveSetting(QString const& key, QVariant const& value) const;

    static QString mimeType();

private:
    QString setting(QString const& key) const;

private:
    std::unique_ptr<Database> const m_db; // non-null
    std::unique_ptr<Actions> const m_actions; // non-null
    std::unique_ptr<Adaptor> const m_adaptor; // non-null
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_MANAGER_HPP
