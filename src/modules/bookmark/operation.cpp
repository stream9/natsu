#include "operation.hpp"

#include "add_dialog.hpp"
#include "folder_dialog.hpp"
#include "item_dialog.hpp"
#include "manager.hpp"

#include "browser/browser.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "browser/tab_browser.hpp"
#include "core/get.hpp"
#include "misc/variant.hpp"

#include <stream9/array.hpp>

#include <ranges>
#include <variant>

#include <QApplication>
#include <QClipboard>
#include <QDockWidget>
#include <QMessageBox>
#include <QMimeData>
#include <QUrl>

namespace natsu::modules::bookmark {

using natsu::bookmark::Separator;

Operation::
Operation(Manager& manager)
    : m_manager { manager }
{}

Operation::~Operation() = default;

void Operation::
bookmarkThisPage() const
{
    auto& window = get::activeWindow(m_manager);
    auto const& url = get::activeTab(window).url();

    auto const& bookmarks = m_manager.search(url);
    if (bookmarks.empty()) {
        AddDialog dialog { m_manager, window };

        if (dialog.exec() != AddDialog::Accepted) return;

        m_manager.addBookmark(dialog.folder(), dialog.title(), url, "", "");
    }
    else {
        auto item = bookmarks.front();

        ItemDialog dialog { item, window };

        if (dialog.exec() == QDialog::Accepted) {
            item.title = dialog.title();
            item.url = dialog.location();
            item.keyword = dialog.keyword();
            item.description = dialog.description();

            m_manager.updateItem(item);
        }
    }
}

void Operation::
removeThisPage() const
{
    auto& window = get::activeWindow(m_manager);
    auto const& url = get::activeTab(window).url();

    auto const& bookmarks = m_manager.search(url);
    if (bookmarks.empty()) return;

    auto& item = bookmarks.front();
    m_manager.removeItem(item.id);
}

void Operation::
showBrowser() const
{
    auto& window = get::activeWindow(m_manager);

    Browser dialog { m_manager, window };
    dialog.exec();
}

void Operation::
openInCurrentTab(Id const id) const
{
    auto const& item = m_manager.item(id);
    assert(item);

    std::visit(overloaded {
        [&](Bookmark const& item) {
            auto& window = get::activeWindow(m_manager);
            natsu::openUrlInActiveTab(window, item.url);
            m_manager.updateAccessTime(item.id);
        },
        [&](Folder const& folder) {
            auto& window = get::activeWindow(m_manager);
            auto items = m_manager.items(folder.id);
            if (items.empty()) return;

            openInCurrentTab(getId(items.front()));

            for (auto const& item: std::ranges::views::drop(items, 1)) {
                openInNewTab(window, getId(item));
            }
        },
        [](auto const&) {}
    }, *item);
}

void Operation::
openInNewTab(BrowserWindow& window, Id const id) const
{
    auto const& item = m_manager.item(id);
    assert(item);

    std::visit(overloaded {
        [&](Bookmark const& item) {
            auto& window = get::activeWindow(m_manager);
            natsu::openUrlInNewTab(window, item.url);
            m_manager.updateAccessTime(item.id);
        },
        [&](Folder const& folder) {
            for (auto const& item: m_manager.items(folder.id)) {
                openInNewTab(window, getId(item));
            }
        },
        [](auto const&) {}
    }, *item);
}

void Operation::
newBookmark(Id const id) const
{
    auto* const window = qApp->activeWindow();
    if (!window) return;

    auto const parent = getFolderId(id);

    ItemDialog dialog { *window };

    if (dialog.exec() == QDialog::Accepted) {
        m_manager.addBookmark(
            parent,
            dialog.title(),
            dialog.location(),
            dialog.keyword(),
            dialog.description()
        );
    }
}

void Operation::
newFolder(Id const id) const
{
    auto* const window = qApp->activeWindow();
    if (!window) return;

    auto const parent = getFolderId(id);

    FolderDialog dialog { *window };

    if (dialog.exec() == QDialog::Accepted) {
        m_manager.addFolder(
            parent,
            dialog.title(),
            dialog.description()
        );
    }
}

void Operation::
newSeparator(Id const id) const
{
    auto const parent = getFolderId(id);

    m_manager.addSeparator(parent);
}

void Operation::
deleteItem(Id const id) const
{
    auto* const window = qApp->activeWindow();
    if (!window) return;

    auto const& item = m_manager.item(id);
    assert(item);

    auto const& text =
        std::visit(overloaded {
            [&](Bookmark const& item) {
                return QSL("Really want to delete bookmark \"%1\"?").arg(item.title);
            },
            [&](Folder const& item) {
                return QSL("Really want to delete folder \"%1\"?").arg(item.title);
            },
            [&](Separator const&) {
                return QSL("Really want to delete separator?");
            }
        }, *item);

    auto const response = QMessageBox::question(
        window,
        QSL("Delete"),
        text
    );
    if (response == QMessageBox::Yes) {
        m_manager.removeItem(id);
    }
}

void Operation::
deleteItems(std::vector<Id> const& ids) const
{
    if (ids.empty()) return;

    auto* const window = qApp->activeWindow();
    if (!window) return;

    auto const response = QMessageBox::question(
        window,
        QSL("Delete"),
        QSL("Really want to delete those items?")
    );
    if (response == QMessageBox::Yes) {
        // copy ids before Manager::removeItem() reset selection
        auto const copy = ids;

        for (auto const id: copy) {
            m_manager.removeItem(id);
        }
    }
}

void Operation::
editItem(Id const id) const
{
    auto* const window = qApp->activeWindow();
    if (!window) return;

    auto&& item = m_manager.item(id);
    assert(item);

    std::visit(overloaded {
        [&](Bookmark& item) {
            ItemDialog dialog { item, *window };

            if (dialog.exec() == QDialog::Accepted) {
                item.title = dialog.title();
                item.url = dialog.location();
                item.keyword = dialog.keyword();
                item.description = dialog.description();

                m_manager.updateItem(item);
            }
        },
        [&](Folder& item) {
            FolderDialog dialog { item, *window };

            if (dialog.exec() == QDialog::Accepted) {
                item.title = dialog.title();
                item.description = dialog.description();

                m_manager.updateItem(item);
            }
        },
        [&](auto&) {}

    }, *item);
}

void Operation::
cutItems(st9::array_view<Id const> ids) const
{
    auto data = make_qmanaged<QMimeData>();
    data->setParent(&m_manager);

    QString x;
    for (auto& id: ids) {
        x += QString::number(id);
        x += ",";
    }

    data->setData(m_manager.mimeType(), x.toLatin1());

    auto& clipboard = to_ref(qApp->clipboard());
    clipboard.setMimeData(data);
}

void Operation::
pasteItems(Id dest) const
{
    auto& clipboard = to_ref(qApp->clipboard());

    auto* data = clipboard.mimeData();
    if (data == nullptr) return;

    st9::array<Id> ids;

    QString s = data->data(m_manager.mimeType());
    for (auto& x: s.split(',', Qt::SkipEmptyParts)) {
        x = x.trimmed();
        if (x.isEmpty()) continue;

        auto src = x.toInt();
        if (src == dest) continue;

        ids.insert(src);
    }

    auto oItem = m_manager.item(dest);
    assert(oItem);
    if (isFolder(*oItem)) {
        m_manager.moveItemsInto(ids, dest);
    }
    else {
        m_manager.moveItemsAfter(ids, dest);
    }
}

Id Operation::
getFolderId(Id const id) const
{
    auto const& item = m_manager.item(id);
    assert(item);

    return std::visit(overloaded {
        [&](Folder const& item) {
            return item.id;
        },
        [&](auto const& item) {
            return getParent(item);
        }
    }, *item);
}

} // namespace natsu::modules::bookmark
