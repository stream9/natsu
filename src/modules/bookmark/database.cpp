#include "database.hpp"

#include "core/conv.hpp"
#include "core/error.hpp"
#include "global.hpp"

#include <QUrl>

namespace natsu::modules::bookmark {

using natsu::bookmark::Bookmark;
using natsu::bookmark::Folder;
using natsu::bookmark::Separator;
using natsu::bookmark::ReservedId;

static Record
makeRecord(sqlite::row const& row)
{
    return {
        .id = boost::numeric_cast<Id>(row.get_int64(0)),
        .parent = boost::numeric_cast<Id>(row.get_int64(1)),
        .row = boost::numeric_cast<Index>(row.get_int64(2)),
        .type = static_cast<Type>(row.get_int64(3)),
        .title = row.get_text(4),
        .url = row.get_text(5),
        .keyword = row.get_text(6),
        .description = row.get_text(7),
        .created_time = row.get_text(8),
        .accessed_time = row.get_text(9),
        .modified_time = row.get_text(10),
        .editable = (row.get_int(11) == 1),
    };
}

// Database

Database::
Database(QString const& path)
    : m_db { toString(path) }
{
    prepareTables();
}

Database::~Database() = default;

void Database::
search(QString const& keyword_, function_ref<bool(Record const&)> cb)
{
    static auto query = m_db.prepare(toString(QSL(
        "SELECT id, parent, row, type, title, url, keyword, description, "
        "       datetime(created_time, 'localtime'), "
        "       datetime(accessed_time, 'localtime'), "
        "       datetime(modified_time, 'localtime'), "
        "       editable "
        "FROM bookmark "
        "WHERE type = %1 "
        "AND (title like '%' || :keyword1 || '%' "
        "     OR url like '%' || :keyword2 || '%') "
    ).arg(toInt(Type::Bookmark)) ));

    auto const keyword = toString(keyword_);
    query.exec(keyword, keyword);

    for (auto const row: query) {
        if (!cb(makeRecord(row))) break;
    }
}

void Database::
search(QUrl const& url, function_ref<bool(Record const&)> cb)
{
    static auto query = m_db.prepare(toString(QSL(
        "SELECT id, parent, row, type, title, url, keyword, description, "
        "       datetime(created_time, 'localtime'), "
        "       datetime(accessed_time, 'localtime'), "
        "       datetime(modified_time, 'localtime'), "
        "       editable "
        "FROM bookmark "
        "WHERE type = %1 "
        "AND url = :url "
    ).arg(toInt(Type::Bookmark)) ));

    query.exec(toString(url));

    for (auto const row: query) {
        if (!cb(makeRecord(row))) break;
    }
}

std::optional<Record> Database::
selectItemById(Id const id)
{
    static auto query = m_db.prepare(
        "SELECT id, parent, row, type, title, url, keyword, description, "
        "       datetime(created_time, 'localtime'), "
        "       datetime(accessed_time, 'localtime'), "
        "       datetime(modified_time, 'localtime'), "
        "       editable "
        "FROM bookmark "
        "WHERE id = :id "
    );

    std::optional<Record> result;

    query.exec(id);

    if (!query.done()) {
        result = makeRecord(query.current_row());
    }

    return result;
}

void Database::
selectItems(Id const parent, function_ref<bool(Record const&)> cb)
{
    static auto query = m_db.prepare(
        "SELECT id, parent, row, type, title, url, keyword, description, "
        "       datetime(created_time, 'localtime'), "
        "       datetime(accessed_time, 'localtime'), "
        "       datetime(modified_time, 'localtime'), "
        "       editable "
        "FROM bookmark "
        "WHERE parent = :parent "
        "ORDER BY row "
    );

    query.exec(parent);

    for (auto const row: query) {
        if (!cb(makeRecord(row))) break;
    }
}

Index Database::
newRow(Id const parent)
{
    static auto query = m_db.prepare(
        "SELECT max(row) + 1 as row "
        "FROM bookmark "
        "WHERE parent = :parent "
    );

    query.exec(parent);

    if (query.done()) {
        return 0;
    }
    else {
        return boost::numeric_cast<Index>(query.current_row().get_int64(0));
    }
}

Index Database::
count(Id const parent)
{
    static auto query = m_db.prepare(
        "SELECT count(*) cnt "
        "FROM bookmark "
        "WHERE parent = :parent "
    );

    query.exec(parent);
    assert(!query.done());

    return boost::numeric_cast<Index>(query.current_row().get_int64(0));
}

Id Database::
insertItem(Id const parent,
           Index const row,
           Type const type,
           QString const& title,
           QUrl const& url,
           QString const& keyword,
           QString const& description)
{
    static auto stmt = m_db.prepare(
        "INSERT INTO bookmark ( "
        "   parent, row, type, title, url, keyword, description "
        ") VALUES ( "
        "   :parent, :row, :type, :title, :url, :keyword, :description "
        ") "
    );

    stmt.exec(parent, row, static_cast<int>(type), toString(title),
              toString(url), toString(keyword), toString(description) );

    return m_db.last_insert_rowid();
}

int Database::
updateItem(Item const& item)
{
    std::visit([&](auto const& item) {
        using T = std::decay_t<decltype(item)>;

        if constexpr (std::is_same_v<T, Bookmark>) {
            static auto stmt = m_db.prepare(
                "UPDATE bookmark "
                "SET   parent = :parent "
                "    , row = :row "
                "    , title = :title "
                "    , url = :url "
                "    , keyword = :keyword "
                "    , description = :description "
                "    , modified_time = CURRENT_TIMESTAMP "
                "WHERE id = :id "
            );

            stmt.exec(item.parent, item.row, toString(item.title),
                      toString(item.url), toString(item.keyword),
                      toString(item.description), item.id );
        }
        else if constexpr (std::is_same_v<T, Folder>) {
            static auto stmt = m_db.prepare(
                "UPDATE bookmark "
                "SET   parent = :parent "
                "    , row = :row "
                "    , title = :title "
                "    , description = :description "
                "    , modified_time = CURRENT_TIMESTAMP "
                "WHERE id = :id "
            );

            stmt.exec(item.parent, item.row, toString(item.title),
                      toString(item.description), item.id );
        }
    }, item);

    return m_db.changes();
}

int Database::
updateAccessTime(Id id)
{
    static auto stmt = m_db.prepare(
        "UPDATE bookmark "
        "SET   accessed_time = CURRENT_TIMESTAMP "
        "WHERE id = :id "
    );

    stmt.exec(id);

    return m_db.changes();
}

int Database::
deleteItem(Id const id)
{
    static auto stmt = m_db.prepare(
        "DELETE FROM bookmark "
        "WHERE id = :id "
    );

    stmt.exec(id);

    return m_db.changes();
}

void Database::
shiftRow(Id const parent, Index const start, Index const count)
{
    static auto stmt = m_db.prepare(
        "UPDATE bookmark "
        "SET row = row + :count "
        "WHERE parent = :parent "
        "AND row >= :start "
    );

    stmt.exec(count, parent, start);
}

void Database::
adjustRow(Id const parent)
{
    static auto select = m_db.prepare(
        "SELECT id "
        "FROM bookmark "
        "WHERE parent = :parent "
        "ORDER BY row "
    );

    static auto update = m_db.prepare(
        "UPDATE bookmark "
        "SET row = :row "
        "WHERE id = :id "
    );

    select.exec(parent);

    Index row = 0;

    for (auto const rec: select) {
        auto const id = boost::numeric_cast<Id>(rec.get_int64(0));

        update.exec(row, id);
        assert(m_db.changes() == 1);

        ++row;
    }
}

static void
createBookmarkTable(auto& db)
{
    db.exec(
        "CREATE TABLE IF NOT EXISTS bookmark ( "
        "     id INTEGER PRIMARY KEY "
        "   , parent INTEGER "
        "   , type INTEGER "
        "   , row INTEGER "
        "   , title TEXT "
        "   , url TEXT "
        "   , keyword TEXT "
        "   , description TEXT "
        "   , editable INTEGER DEFAULT 1 "
        "   , created_time TEXT DEFAULT CURRENT_TIMESTAMP "
        "   , accessed_time TEXT "
        "   , modified_time TEXT DEFAULT CURRENT_TIMESTAMP "
        "   , FOREIGN KEY(parent) REFERENCES bookmark (id) ON DELETE CASCADE "
        ") "
    );
}

static void
insertIntialData(auto& db)
{
    auto insert_stmt = QSL(
        "INSERT OR IGNORE INTO bookmark ( id, parent, type, row, title, editable ) "
        "VALUES (  %1,  %2,  %3,  %4,  %5, 0 ) "
        "     , ( %10, %11, %12, %13, %14, 0 ) "
        "     , ( %20, %21, %22, %23, %24, 0 ) "
        "     , ( %30, %31, %32, %33, %34, 0 ) "
    ).arg(ReservedId::Root).arg("NULL").arg(toInt(Type::Folder)).arg(0).arg(QSL("'Root'"))
     .arg(ReservedId::BookmarkMenu).arg(ReservedId::Root).arg(toInt(Type::Folder)).arg(0).arg(QSL("'Bookmark Menu'"))
     .arg(ReservedId::BookmarkToolBar).arg(ReservedId::Root).arg(toInt(Type::Folder)).arg(1).arg(QSL("'Bookmark Toolbar'"))
     .arg(ReservedId::OtherBookmark).arg(ReservedId::Root).arg(toInt(Type::Folder)).arg(2).arg(QSL("'Other Bookmark'"))
     ;

    db.exec(toString(insert_stmt));
}

static int dbVersion() { return 2; }

static int
getDatabaseVersion(auto& db)
{
    auto stmt = db.exec("PRAGMA user_version");
    return stmt.current_row().get_int(0);
}

static void
setDatabaseVersion(auto& db, int v)
{
    auto stmt = QSL("PRAGMA user_version = %1").arg(v);
    db.exec(toString(stmt));
}

static void
initializeDatabase(auto& db)
{
    auto tr = db.begin_transaction();

    createBookmarkTable(db);

    insertIntialData(db);

    setDatabaseVersion(db, dbVersion());

    tr.commit();
}

static void
migrateDatabaseFromV1(auto& db)
{
    auto tr = db.begin_transaction();

    db.exec("ALTER TABLE bookmark RENAME TO bookmark_old ");

    createBookmarkTable(db);

    db.exec(
        "INSERT INTO bookmark "
        "  (id, parent, type, row, title, url, keyword, description, editable) "
        "  SELECT id, parent, type, row, title, url, keyword, description, editable "
        "  FROM bookmark_old "
    );

    db.exec("DROP TABLE bookmark_old");

    setDatabaseVersion(db, dbVersion());

    tr.commit();
}

void Database::
prepareTables()
{
    auto v = getDatabaseVersion(m_db);
    if (v == dbVersion()) { /*nop*/ }
    else if (v == 0) { // initialize
        initializeDatabase(m_db);
    }
    else if (v == 1) {
        migrateDatabaseFromV1(m_db);
    }
    else {
        throw error {
            st9::errc::invalid_data, {
                { "detail", "unsupported version of bookmark database" },
                { "version", v }
            }
        };
    }

    m_db.exec("PRAGMA foreign_keys = ON");
}

sqlite::transaction Database::
beginTransaction()
{
    return m_db.begin_transaction();
}

} // namespace natsu::modules::bookmark
