#include "actions.hpp"

#include "manager.hpp"
#include "operation.hpp"

#include "backup/json.hpp"
#include "browser/item_view.hpp"

#include "core/action_manager.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"
#include "misc/variant.hpp"

#include <algorithm>
#include <iterator>

#include <QApplication>
#include <QClipboard>

namespace natsu::modules::bookmark {

using natsu::bookmark::Bookmark;
using natsu::bookmark::Folder;
using natsu::bookmark::Separator;

Actions::
Actions(Manager& manager)
    : m_manager { manager }
{
    auto& actionManager = get::actionManager(m_manager);
    auto const& category = QSL("Bookmark");

    auto c = [&](auto& action, auto const& name, auto const& text,
                 auto const& icon, auto const fn, QKeySequence const& shortcut)
    {
        action.setObjectName(name);
        action.setText(text);
        action.setIcon(icon);

        this->connect(&action, &QAction::triggered, this, fn);

        actionManager.configure(category, action, shortcut);
    };

    namespace icon = natsu::bookmark::icon;

    c(m_bookmarkThisPage, QSL("bookmark.bookmark_this_page"), QSL("Bookmark This Page"), icon::add(), &Actions::bookmarkThisPage, makeKeyCode(Qt::CTRL, Qt::Key_D));
    c(m_removeThisPage, QSL("bookmark.remove_this_page"), QSL("Remove This Page"), icon::add(), &Actions::removeThisPage, makeKeyCode(Qt::SHIFT, Qt::CTRL, Qt::Key_D));
    c(m_showBrowser, QSL("bookmark.show_browser"), QSL("Show Bookmark Browser"), QIcon(), &Actions::showBrowser, makeKeyCode(Qt::SHIFT, Qt::CTRL, Qt::Key_O));

    c(m_open, QSL("bookmark.browser.open_in_current_tab"), QSL("&Open"), QIcon(), &Actions::onOpenInCurrentTab, QKeySequence());
    c(m_openInNewTab, QSL("bookmark.browser.open_in_new_tab"), QSL("Open in a New &Tab"), QIcon(), &Actions::onOpenInNewTab, QKeySequence());
    c(m_openInNewWindow, QSL("bookmark.browser.open_in_new_window"), QSL("Open in a New &Window"), QIcon(), &Actions::onOpenInNewWindow, QKeySequence());
    c(m_newBookmark, QSL("bookmark.browser.new_bookmark"), QSL("New &Bookmark"), QIcon(), &Actions::onNewBookmark, QKeySequence());
    c(m_newFolder, QSL("bookmark.browser.new_folder"), QSL("New &Folder"), QIcon(), &Actions::onNewFolder, QKeySequence());
    c(m_newSeparator, QSL("bookmark.browser.new_separator"), QSL("New &Separator"), QIcon(), &Actions::onNewSeparator, QKeySequence());
    c(m_delete, QSL("bookmark.browser.delete"), QSL("&Delete"), QIcon(), &Actions::onDelete, Qt::Key_Delete);
    c(m_edit, QSL("bookmark.browser.edit"), QSL("&Properties"), QIcon(), &Actions::onEdit, makeKeyCode(Qt::ALT, Qt::Key_Return));
    c(m_cut, QSL("bookmark.browser.cut"), QSL("&Cut"), natsu::icon::cut(), &Actions::onCut, makeKeyCode(Qt::CTRL, Qt::Key_X));
    c(m_paste, QSL("bookmark.browser.paste"), QSL("&Cut"), natsu::icon::paste(), &Actions::onPaste, makeKeyCode(Qt::CTRL, Qt::Key_V));

    update();
}

Actions::~Actions() = default;

void Actions::
setSelection(Id const id)
{
    m_selection.clear();
    m_selection.push_back(id);
    update();
}

void Actions::
setSelections(std::vector<Id>&& ids)
{
    m_selection = std::move(ids);
    update();
}

void Actions::
clearSelection()
{
    m_selection.clear();
    update();
}

void Actions::
installWindowActionsTo(QWidget& target)
{
    target.addAction(&m_bookmarkThisPage);
    target.addAction(&m_removeThisPage);
    target.addAction(&m_showBrowser);
}

void Actions::
installBrowserActionsTo(QWidget& target)
{
    target.addAction(&m_open);
    target.addAction(&m_openInNewTab);
    target.addAction(&m_openInNewWindow);
    target.addAction(&m_newBookmark);
    target.addAction(&m_newFolder);
    target.addAction(&m_newSeparator);
    target.addAction(&m_delete);
    target.addAction(&m_edit);
    target.addAction(&m_cut);
    target.addAction(&m_paste);
}

void Actions::
update()
{
    if (m_selection.empty()) {
        m_open.setEnabled(false);
        m_openInNewTab.setEnabled(false);
        m_openInNewWindow.setEnabled(false);
        m_newBookmark.setEnabled(false);
        m_newFolder.setEnabled(false);
        m_newSeparator.setEnabled(false);
        m_delete.setEnabled(false);
        m_edit.setEnabled(false);
        return;
    }
    else {
        m_newBookmark.setEnabled(true);
        m_newFolder.setEnabled(true);
        m_newSeparator.setEnabled(true);
    }

    auto const canOpen = std::any_of(m_selection.begin(), m_selection.end(),
        [&](auto const id) {
            auto const& item = m_manager.item(id);

            return isBookmark(*item) || isFolder(*item);
        });

    m_open.setEnabled(canOpen);
    m_openInNewTab.setEnabled(canOpen);
    m_openInNewWindow.setEnabled(canOpen);

    auto const canDelete = std::any_of(m_selection.begin(), m_selection.end(),
        [&](auto const id) {
            auto const& item = m_manager.item(id);
            assert(item);

            return std::visit(overloaded {
                [](Bookmark const& item) {
                    return item.editable;
                },
                [](Folder const& item) {
                    return item.editable;
                },
                [](Separator const&) {
                    return true;
                }
            }, *item);
        });
    m_delete.setEnabled(canDelete);

    auto canEdit = [&]() {
        if (m_selection.size() != 1) return false;

        auto const& item = m_manager.item(m_selection.front());
        assert(item);

        return std::visit(overloaded {
            [](Bookmark const& item) {
                return item.editable;
            },
            [](Folder const& item) {
                return item.editable;
            },
            [](Separator const&) {
                return false;
            }
        }, *item);
    };
    m_edit.setEnabled(canEdit());
}

void Actions::
bookmarkThisPage() const
{
    Operation const op { m_manager };
    op.bookmarkThisPage();
}

void Actions::
removeThisPage() const
{
    Operation const op { m_manager };
    op.removeThisPage();
}

void Actions::
showBrowser() const
{
    Operation const op { m_manager };
    op.showBrowser();
}

void Actions::
onOpenInCurrentTab() const
{
    if (m_selection.empty()) return;

    Operation const op { m_manager };

    op.openInCurrentTab(m_selection.front());

    auto& window = get::activeWindow(m_manager);
    for (size_t i = 1, len = m_selection.size(); i < len; ++i) {
        op.openInNewTab(window, m_selection[i]);
    }
}

void Actions::
onOpenInNewTab() const
{
    auto& window = get::activeWindow(m_manager);

    Operation const op { m_manager };

    for (auto const id: m_selection) {
        op.openInNewTab(window, id);
    }
}

void Actions::
onOpenInNewWindow() const
{
    auto& window = get::application(m_manager).createWindow();

    Operation const op { m_manager };

    for (auto const id: m_selection) {
        op.openInNewTab(window, id);
    }
}

void Actions::
onNewBookmark() const
{
    if (m_selection.empty()) return;

    auto const id = m_selection.front();

    Operation const op { m_manager };
    op.newBookmark(id);
}

void Actions::
onNewFolder() const
{
    if (m_selection.empty()) return;

    auto const id = m_selection.front();

    Operation const op { m_manager };
    op.newFolder(id);
}

void Actions::
onNewSeparator() const
{
    if (m_selection.empty()) return;

    auto const id = m_selection.front();

    Operation const op { m_manager };
    op.newSeparator(id);
}

void Actions::
onDelete() const
{
    Operation const op { m_manager };
    op.deleteItems(m_selection);
}

void Actions::
onEdit() const
{
    if (m_selection.empty()) return;

    auto const id = m_selection.front();

    Operation const op { m_manager };
    op.editItem(id);
}

void Actions::
onCut() const
{
    if (m_selection.empty()) return;

    Operation op { m_manager };
    op.cutItems(m_selection);
}

void Actions::
onPaste() const
{
    if (m_selection.empty()) return;

    auto id = m_selection.front();

    Operation op { m_manager };
    op.pasteItems(id);
}

} // namespace natsu::modules::bookmark
