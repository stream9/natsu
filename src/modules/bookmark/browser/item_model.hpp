#ifndef NATSU_MODULES_BOOKMARK_BROWSER_ITEM_MODEL_HPP
#define NATSU_MODULES_BOOKMARK_BROWSER_ITEM_MODEL_HPP

#include "core/bookmark.hpp"

#include <optional>

#include <QStandardItemModel>

class QMimeData;
class QModelIndex;
class QStandardItem;

namespace natsu::modules::bookmark { class Manager; } // namespace natsu::bookmark

namespace natsu::modules::bookmark::browser {

using natsu::bookmark::Id;
using natsu::bookmark::Item;
using Columns = QList<QStandardItem*>;

class ItemModel : public QStandardItemModel
{
    Q_OBJECT
public:
    ItemModel(Manager&);
    ~ItemModel() override;

    Id id(QModelIndex const&) const;
    QModelIndex index(Id) const;
    using QStandardItemModel::index;

    // override QAbstractItemModel
    QStringList mimeTypes() const override;
    QMimeData* mimeData(QModelIndexList const& indexes) const override;
    bool dropMimeData(QMimeData const* data, Qt::DropAction action,
                      int row, int column, QModelIndex const& parent) override;
    bool setData(QModelIndex const& index, QVariant const& value, int role = Qt::EditRole) noexcept override;

private:
    Columns makeColumns(Item const&) const;
    void populateItems(QStandardItem& parent, Id parentId) const;

    QStandardItem* findItem(Id) const;

    Q_SLOT void onItemAdded(Id parent, Id);
    Q_SLOT void onItemUpdated(Id parent, Id);
    Q_SLOT void onItemRemoved(Id parent, Id);
    Q_SLOT void onItemMovedBefore(Id src, Id target);
    Q_SLOT void onItemMovedAfter(Id src, Id target);
    Q_SLOT void onItemMovedInto(Id src, Id target);

private:
    Manager& m_manager;
};

} // namespace natsu::modules::bookmark::browser

#endif // NATSU_MODULES_BOOKMARK_BROWSER_ITEM_MODEL_HPP
