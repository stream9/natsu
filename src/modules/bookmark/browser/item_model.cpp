#include "item_model.hpp"

#include "../manager.hpp"

#include "core/error.hpp"
#include "core/get.hpp"
#include "core/history_manager.hpp"
#include "core/icon.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"
#include "misc/variant.hpp"

#include <stream9/array.hpp>

#include <QMimeData>
#include <QModelIndex>
#include <QStandardItem>
#include <QUrl>
#include <QVariant>

namespace natsu::modules::bookmark::browser {

using natsu::bookmark::ReservedId;
using natsu::bookmark::Bookmark;
using natsu::bookmark::Folder;
using natsu::bookmark::Separator;

static QStandardItem&
makeItem(QString const& text = {})
{
    auto& item = to_ref(new QStandardItem { text });
    item.setToolTip(text);
    item.setFlags(Qt::ItemIsSelectable |
                  Qt::ItemIsDragEnabled |
                  Qt::ItemIsEnabled);

    return item;
}

static Id
getId(QStandardItem& item)
{
    return item.data().value<Id>();
}

static Columns
cloneColumns(auto& parent, auto row)
{
    Columns columns;

    for (int col = 0; col < parent.columnCount(); ++col) {
        columns << parent.child(row, col)->clone();
    }

    return columns;
}

// ItemModel

ItemModel::
ItemModel(Manager& manager)
    : m_manager { manager }
{
    this->setHorizontalHeaderItem(0, new QStandardItem { QSL("Title") });
    this->setHorizontalHeaderItem(1, new QStandardItem { QSL("URL") });
    this->setHorizontalHeaderItem(2, new QStandardItem { QSL("Keyword") });
    this->setHorizontalHeaderItem(3, new QStandardItem { QSL("Description") });
    this->setHorizontalHeaderItem(4, new QStandardItem { QSL("Created Time") });
    this->setHorizontalHeaderItem(5, new QStandardItem { QSL("Accessed Time") });
    this->setHorizontalHeaderItem(6, new QStandardItem { QSL("Modified Time") });

    populateItems(to_ref(this->invisibleRootItem()), ReservedId::Root);

    this->connect(&m_manager, &Manager::itemAdded,
                  this,       &ItemModel::onItemAdded);
    this->connect(&m_manager, &Manager::itemRemoved,
                  this,       &ItemModel::onItemRemoved);
    this->connect(&m_manager, &Manager::itemUpdated,
                  this,       &ItemModel::onItemUpdated);
    this->connect(&m_manager, &Manager::itemMovedBefore,
                  this,       &ItemModel::onItemMovedBefore);
    this->connect(&m_manager, &Manager::itemMovedAfter,
                  this,       &ItemModel::onItemMovedAfter);
    this->connect(&m_manager, &Manager::itemMovedInto,
                  this,       &ItemModel::onItemMovedInto);
}

ItemModel::~ItemModel() = default;

Id ItemModel::
id(QModelIndex const& index) const
{
    auto& item = to_ref(this->itemFromIndex(index));

    return getId(item);
}

QModelIndex ItemModel::
index(Id const id) const
{
    auto* const item = findItem(id);

    return item ? item->index() : QModelIndex();
}

QStringList ItemModel::
mimeTypes() const
{
    auto&& types = QStandardItemModel::mimeTypes();
    types << m_manager.mimeType();

    return types;
}

QMimeData* ItemModel::
mimeData(QModelIndexList const& indexes) const
{
    auto* const data = QStandardItemModel::mimeData(indexes);

    if (data) {
        QString ids;

        for (auto const& index: indexes) {
            if (index.column() != 0) continue;

            ids += QString::number(id(index));
            ids += ",";
        }

        data->setData(m_manager.mimeType(), ids.toLatin1());
    }

    return data;
}

bool ItemModel::
dropMimeData(QMimeData const* data, Qt::DropAction action,
             int row, int column, QModelIndex const& parentIndex)
{
    assert(data);
    assert(row >= -1);

    auto& parent = to_ref(this->itemFromIndex(parentIndex));
    auto* before = row == -1 ? nullptr : parent.child(row);

    QSignalBlocker blocker { &m_manager };

    st9::array<Id> ids;
    QString s { data->data(m_manager.mimeType()) };
    for (auto const& id: s.split(',', Qt::SkipEmptyParts)) {
        if (id.isEmpty()) continue;
        ids.insert(id.toInt());
    }

    if (before) {
        m_manager.moveItemsBefore(ids, getId(*before));
    }
    else {
        m_manager.moveItemsInto(ids, getId(parent));
    }

    return QStandardItemModel::dropMimeData(data, action, row, column, parentIndex);
}

bool ItemModel::
setData(QModelIndex const& index, QVariant const& value, int role/*= Qt::EditRole*/) noexcept
{
    try {
        if (role == Qt::EditRole && index.isValid() && value.isValid()) {
            auto id = this->id(index);
            auto o_item = m_manager.item(id);
            if (o_item) {
                std::visit(overloaded {
                    [&](Bookmark& item) {
                        item.title = value.toString();
                        m_manager.updateItem(item);
                    },
                    [&](Folder& item) {
                        item.title = value.toString();
                        m_manager.updateItem(item);
                    },
                    [&](auto&) {}
                }, *o_item);
            }
        }

        return QStandardItemModel::setData(index, value, role);
    }
    catch (...) {
        print_error();
        return false;
    }
}

Columns ItemModel::
makeColumns(Item const& item) const
{
    auto& history = get::history(m_manager);

    auto& title = makeItem();
    title.setData(QVariant::fromValue(getId(item)));

    auto const& columns = std::visit(overloaded {
        [&](Bookmark const& item) {
            title.setText(item.title);
            title.setToolTip(item.title);
            title.setIcon(history.icon(item.url));
            title.setFlags(title.flags() | Qt::ItemIsEditable);

            Columns columns;
            columns << &title
                    << &makeItem(item.url)
                    << &makeItem(item.keyword)
                    << &makeItem(item.description)
                    << &makeItem(item.createdTime)
                    << &makeItem(item.accessedTime)
                    << &makeItem(item.modifiedTime);

            return columns;
        },
        [&](Folder const& item) {
            title.setText(item.title);
            title.setIcon(natsu::bookmark::icon::folder());
            title.setDropEnabled(true);

            populateItems(title, item.id);

            Columns columns;
            columns << &title << &makeItem(item.description);

            return columns;
        },
        [&](Separator const&) {
            title.setText(QSL("---"));

            Columns columns;
            columns << &title;

            return columns;
        }
    }, item);

    return columns;
}

void ItemModel::
populateItems(QStandardItem& parent, Id const parentId) const
{
    auto const& items = m_manager.items(parentId);

    for (auto const& item: items) {
        auto const& columns = makeColumns(item);

        parent.appendRow(columns);
    }
}

QStandardItem* ItemModel::
findItem(Id const id) const
{
    auto const& indexes = this->match(
        this->index(0, 0),
        Qt::UserRole + 1,
        QVariant::fromValue(id),
        1,
        Qt::MatchExactly | Qt::MatchRecursive
    );

    assert(indexes.size() < 2);

    if (indexes.isEmpty()) {
        return nullptr;
    }
    else {
        return this->itemFromIndex(indexes.front());
    }
}

void ItemModel::
onItemAdded(Id const parentId, Id const itemId)
{
    auto& parent = to_ref(findItem(parentId));

    auto const& item = m_manager.item(itemId);
    assert(item);

    auto const& columns = makeColumns(*item);

    parent.appendRow(columns);
}

void ItemModel::
onItemUpdated(Id/*parent*/, Id const itemId)
{
    auto const& item = m_manager.item(itemId);
    assert(item);

    auto& title = to_ref(findItem(itemId));
    auto& parent = to_ref(title.parent());
    auto const row = title.row();

    std::visit(overloaded {
        [&](Bookmark const& item) {
            auto& history = get::history(m_manager);

            title.setText(item.title);
            title.setIcon(history.icon(item.url));

            auto& url = to_ref(parent.child(row, 1));
            url.setText(item.url);

            auto& keyword = to_ref(parent.child(row, 2));
            keyword.setText(item.keyword);

            auto& description = to_ref(parent.child(row, 3));
            description.setText(item.description);
        },
        [&](Folder const& item) {
            title.setText(item.title);
            auto& description = to_ref(parent.child(row, 1));
            description.setText(item.description);
        },
        [&](auto) {}
    }, *item);
}

void ItemModel::
onItemRemoved(Id/*parent*/, Id const itemId)
{
    auto& title = to_ref(findItem(itemId));
    auto& parent = to_ref(title.parent());

    parent.removeRow(title.row());
}

void ItemModel::
onItemMovedBefore(Id src, Id target)
{
    auto& srcItem = to_ref(findItem(src));
    auto& srcParentItem = to_ref(srcItem.parent());
    auto srcRow = srcItem.row();

    auto columns = cloneColumns(srcParentItem, srcRow);

    srcParentItem.removeRow(srcRow);

    auto& targetItem = to_ref(findItem(target));
    auto& targetParentItem = to_ref(targetItem.parent());
    auto targetRow = targetItem.row();
    targetParentItem.insertRow(targetRow, columns);
}

void ItemModel::
onItemMovedAfter(Id src, Id target)
{
    auto& srcItem = to_ref(findItem(src));
    auto& srcParentItem = to_ref(srcItem.parent());
    auto srcRow = srcItem.row();

    auto columns = cloneColumns(srcParentItem, srcRow);

    srcParentItem.removeRow(srcRow);

    auto& targetItem = to_ref(findItem(target));
    auto& targetParentItem = to_ref(targetItem.parent());
    auto targetRow = targetItem.row();
    targetParentItem.insertRow(targetRow + 1, columns);
}

void ItemModel::
onItemMovedInto(Id src, Id target)
{
    auto& srcItem = to_ref(findItem(src));
    auto& srcParentItem = to_ref(srcItem.parent());
    auto srcRow = srcItem.row();

    auto columns = cloneColumns(srcParentItem, srcRow);

    srcParentItem.removeRow(srcRow);

    auto& parent = to_ref(findItem(target));
    parent.appendRow(columns);
}

} // namespace natsu::modules::bookmark::browser
