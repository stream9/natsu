#include "item_view.hpp"

#include "item_model.hpp"

#include "../actions.hpp"
#include "../context_menu.hpp"
#include "../manager.hpp"
#include "../operation.hpp"

#include "global.hpp"
#include "misc/pointer.hpp"
#include "misc/variant.hpp"

#include <QApplication>
#include <QContextMenuEvent>
#include <QHeaderView>
#include <QKeyEvent>
#include <QMimeData>

namespace natsu::modules::bookmark::browser {

static QString
headerStateKey()
{
    return QSL("BrowserHeaderState");
}

static QString
currentIdKey()
{
    return QSL("BrowserCurrentId");
}

ItemView::
ItemView(Manager& manager, QWidget& parent)
    : QTreeView { &parent }
    , m_manager { manager }
    , m_model { std::make_unique<ItemModel>(manager) }
{
    this->setModel(m_model.get());
    this->expandAll();
    this->setSelectionMode(ItemView::ExtendedSelection);
    this->setExpandsOnDoubleClick(true);
    this->setDragDropMode(QTreeView::InternalMove);

    auto& selection = to_ref(this->selectionModel());
    this->connect(&selection, &QItemSelectionModel::selectionChanged,
                  this,       &ItemView::onSelectionChanged);

    m_manager.actions().installBrowserActionsTo(*this);
}

ItemView::~ItemView() = default;

void ItemView::
loadSettings()
{
    auto const& bytes = m_manager.loadSetting(headerStateKey()).toByteArray();

    this->header()->restoreState(bytes);

    auto const id = m_manager.loadSetting(currentIdKey()).value<Id>();
    auto const& index = m_model->index(id);

    this->scrollTo(index, ScrollHint::PositionAtCenter);
    this->setCurrentIndex(index);
}

void ItemView::
saveSettings() const
{
    auto const& bytes = this->header()->saveState();

    m_manager.saveSetting(headerStateKey(), bytes);

    auto const& current = this->currentIndex();
    if (current.isValid()) {
        auto const id = m_model->id(current);

        m_manager.saveSetting(currentIdKey(), QVariant::fromValue(id));
    }
}

void ItemView::
openBookmark(Qt::KeyboardModifiers const modifiers) const
{
    if (modifiers & Qt::ControlModifier) {
        m_manager.actions().openInNewTabAction().trigger();
    }
    else if (modifiers & Qt::ShiftModifier) {
        m_manager.actions().openInNewWindowAction().trigger();
    }
    else {
        m_manager.actions().openInCurrentTabAction().trigger();
    }

    if (auto* const window = this->window()) {
        window->close();
    }
}

void ItemView::
contextMenuEvent(QContextMenuEvent* const event)
{
    assert(event);

    auto const& index = this->indexAt(event->pos());
    if (!index.isValid()) return;

    ContextMenu menu { m_manager.actions() };

    menu.exec(event->globalPos());
}

void ItemView::
mouseDoubleClickEvent(QMouseEvent* const event)
{
    QTreeView::mouseDoubleClickEvent(event);

    auto const modifiers = qApp->keyboardModifiers();

    openBookmark(modifiers);
}

void ItemView::
keyPressEvent(QKeyEvent* const event)
{
    QTreeView::keyPressEvent(event);

    assert(event);

    if (this->state() != QTreeView::EditingState) {
        if (event->key() == Qt::Key_Return) {
            auto const modifiers = event->modifiers();

            openBookmark(modifiers);
        }
    }
}

void ItemView::
onSelectionChanged()
{
    auto& selection = to_ref(this->selectionModel());
    auto const& rows = selection.selectedRows();

    std::vector<Id> ids;
    std::transform(rows.begin(), rows.end(), std::back_inserter(ids),
        [&](auto const& index) {
            return m_model->id(index);
        });

    m_manager.actions().setSelections(std::move(ids));
}

void ItemView::
onCurrentChanged(QModelIndex const& current)
{
    auto* const selection = this->selectionModel();
    if (!selection) return;

    selection->select(current,
        QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
}

} // namespace natsu::modules::bookmark::browser
