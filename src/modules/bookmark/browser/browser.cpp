#include "browser.hpp"

#include "../actions.hpp"
#include "../manager.hpp"

#include "item_view.hpp"

#include <QBoxLayout>

namespace natsu::modules::bookmark {

static QString
settingKey()
{
    return QSL("BrowserGeometry");
}

static void
moveToCenter(QWidget& widget)
{
    auto const& parent = to_ref(widget.parentWidget());
    auto const& rect = QStyle::alignedRect(
        Qt::LeftToRight,
        Qt::AlignCenter,
        widget.size(),
        parent.geometry()
    );

    widget.setGeometry(rect);
}

// Browser

Browser::
Browser(Manager& manager, QWidget& parent)
    : QDialog { &parent }
    , m_manager { manager }
    , m_items { make_qmanaged<browser::ItemView>(m_manager, *this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(m_items.get());

    this->setModal(false);
    this->resize(640, 320);
    this->setWindowTitle(QSL("Bookmark Browser"));

    setupActions();
}

Browser::~Browser() = default;

void Browser::
showEvent(QShowEvent* const event)
{
    QDialog::showEvent(event);

    loadSettings();
}

void Browser::
closeEvent(QCloseEvent* const event)
{
    QDialog::closeEvent(event);

    saveSettings();
}

void Browser::
setupActions()
{
    auto& actions = m_manager.actions();
    auto& showAction = actions.showBrowserAction();
    auto const closeAction = make_qmanaged<QAction>(QSL("Close"), this);
    closeAction->setShortcut(showAction.shortcut());

    this->connect(closeAction.get(), &QAction::triggered,
                  this,              &QDialog::close);

    this->addAction(closeAction.get());
}

void Browser::
loadSettings()
{
    auto const& bytes = m_manager.loadSetting(settingKey()).toByteArray();
    this->restoreGeometry(bytes);

    m_items->loadSettings();

    moveToCenter(*this);
}

void Browser::
saveSettings() const
{
    auto const& bytes = this->saveGeometry();
    m_manager.saveSetting(settingKey(), bytes);

    m_items->saveSettings();
}

} // namespace natsu::modules::bookmark
