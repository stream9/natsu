#ifndef NATSU_MODULES_BOOKMARK_BROWSER_HPP
#define NATSU_MODULES_BOOKMARK_BROWSER_HPP

#include "misc/pointer.hpp"

#include <QDialog>

class QCloseEvent;

namespace natsu::modules::bookmark::browser { class ItemView; } // namespace natsu::bookmark::browser

namespace natsu::modules::bookmark {

class Manager;

class Browser : public QDialog
{
public:
    Browser(Manager&, QWidget& parent);
    ~Browser() override;

protected:
    void showEvent(QShowEvent*) override;
    void closeEvent(QCloseEvent*) override;

private:
    void setupActions();
    void loadSettings();
    void saveSettings() const;

private:
    Manager& m_manager;

    qmanaged_ptr<browser::ItemView> m_items;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_BROWSER_HPP
