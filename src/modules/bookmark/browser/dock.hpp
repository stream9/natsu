#ifndef NATSU_MODULES_BOOKMARK_BROWSER_DOCK_HPP
#define NATSU_MODULES_BOOKMARK_BROWSER_DOCK_HPP

#include <QDockWidget>

class QWidget;

namespace natsu::modules::bookmark { class Manager; } // namespace natsu::bookmark

namespace natsu::modules::bookmark::browser {

class ItemView;

class Dock : public QDockWidget
{
    Q_OBJECT
public:
    Dock(Manager&, QWidget& parent);
    ~Dock() override;

private:
    Q_SLOT void onVisibilityChanged(bool visible);

private:
    Manager& m_manager;
};

} // namespace natsu::modules::bookmark::browser

#endif // NATSU_MODULES_BOOKMARK_BROWSER_DOCK_HPP
