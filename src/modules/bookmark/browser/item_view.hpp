#ifndef NATSU_MODULES_BOOKMARK_BROWSER_ITEM_VIEW_HPP
#define NATSU_MODULES_BOOKMARK_BROWSER_ITEM_VIEW_HPP

#include <memory>

#include <QTreeView>

class QContextMenuEvent;
class QKeyEvent;
class QWidget;

namespace natsu::modules::bookmark {

class Actions;
class Manager;

} // namespace natsu::modules::bookmark

namespace natsu::modules::bookmark::browser {

class ItemModel;

class ItemView : public QTreeView
{
    Q_OBJECT
public:
    ItemView(Manager&, QWidget& parent);
    ~ItemView() override;

    // accessor
    ItemModel& model() const { return *m_model; }

    // settings
    void loadSettings();
    void saveSettings() const;

protected:
    void openBookmark(Qt::KeyboardModifiers) const;

    // override QWidget
    void contextMenuEvent(QContextMenuEvent*) override;
    void mouseDoubleClickEvent(QMouseEvent*) override;
    void keyPressEvent(QKeyEvent*) override;

private:
    Q_SLOT void onSelectionChanged();
    Q_SLOT void onCurrentChanged(QModelIndex const& current);

private:
    Manager &m_manager;

    std::unique_ptr<ItemModel> const m_model; // non-null
};

} // namespace natsu::modules::bookmark::browser

#endif // NATSU_MODULES_BOOKMARK_BROWSER_ITEM_VIEW_HPP
