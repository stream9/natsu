#include "dock.hpp"

#include "item_view.hpp"

#include "../manager.hpp"

#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "misc/pointer.hpp"

#include <QAction>

namespace natsu::modules::bookmark::browser {

Dock::
Dock(Manager& manager, QWidget& parent)
    : QDockWidget { QSL("Bookmark Browser"), &parent }
    , m_manager { manager }
{
    this->setObjectName(QSL("bookmark.dock"));

    auto& action = to_ref(this->toggleViewAction());
    action.setObjectName(QSL("bookmark.toggle_dock"));

    auto& actionManager = get::actionManager(m_manager);
    actionManager.configure(QSL("Bookmark"), action, makeKeyCode(Qt::CTRL, Qt::Key_B));

    this->setVisible(false);

    this->connect(this, &QDockWidget::visibilityChanged,
                  this, &Dock::onVisibilityChanged);
}

Dock::~Dock() = default;

void Dock::
onVisibilityChanged(bool const visible)
{
    if (visible) {
        auto const view = make_qmanaged<ItemView>(m_manager, *this);
        view->loadSettings();
        this->setWidget(view.get());
    }
    else {
        auto& view = to_ref(static_cast<ItemView*>(this->widget()));
        view.saveSettings();

        this->setWidget(nullptr);
        view.deleteLater();
    }
}

} // namespace natsu::modules::bookmark::browser
