#ifndef NATSU_MODULES_BOOKMARK_DATABASE_HPP
#define NATSU_MODULES_BOOKMARK_DATABASE_HPP

#include "core/bookmark.hpp"

#include <optional>

#include <stream9/function_ref.hpp>
#include <stream9/sqlite.hpp>

class QUrl;

namespace natsu::modules::bookmark {

namespace sqlite = stream9::sqlite;
using stream9::function_ref;

using natsu::bookmark::Id;
using natsu::bookmark::Index;
using natsu::bookmark::Item;

enum class Type { Bookmark = 0, Folder = 1, Separator = 2 };

struct Record {
    Id               id;
    Id               parent;
    Index            row;
    Type             type;
    std::string_view title;
    std::string_view url;
    std::string_view keyword;
    std::string_view description;
    std::string_view created_time;
    std::string_view accessed_time;
    std::string_view modified_time;
    bool             editable;
};

inline int toInt(Type const type)
{
    return static_cast<int>(type);
}

class Database
{
public:
    Database(QString const& path);
    ~Database();

    // query
    void search(QString const& keyword, function_ref<bool(Record const&)>);
    void search(QUrl const&, function_ref<bool(Record const&)>);

    std::optional<Record> selectItemById(Id);

    void selectItems(Id parent, function_ref<bool(Record const&)>);

    Index newRow(Id parent);
    Index count(Id parent);

    // modifier
    Id insertItem(Id parent,
                  Index row,
                  Type,
                  QString const& title,
                  QUrl const&,
                  QString const& keyword,
                  QString const& description);

    int updateItem(Item const&);
    int updateAccessTime(Id id);
    int deleteItem(Id id);

    void shiftRow(Id parent, Index start, Index count);
    void adjustRow(Id parent);

    sqlite::transaction beginTransaction();

private:
    void prepareTables();

private:
    sqlite::database m_db;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_DATABASE_HPP
