#ifndef NATSU_MODULES_BOOKMARK_OPERATION_HPP
#define NATSU_MODULES_BOOKMARK_OPERATION_HPP

#include "core/bookmark.hpp"
#include "core/namespace.hpp"

#include <stream9/array_view.hpp>

#include <vector>

namespace natsu { class BrowserWindow; } // namespace natsu

namespace natsu::modules::bookmark {

using natsu::bookmark::Id;

class Manager;

class Operation
{
public:
    Operation(Manager&);
    ~Operation();

    void bookmarkThisPage() const;
    void removeThisPage() const;
    void showBrowser() const;

    void openInCurrentTab(Id) const;
    void openInNewTab(BrowserWindow&, Id) const;
    void newBookmark(Id) const;
    void newFolder(Id) const;
    void newSeparator(Id) const;
    void deleteItem(Id) const;
    void deleteItems(std::vector<Id> const&) const;
    void editItem(Id) const;
    void cutItems(st9::array_view<Id const>) const;
    void pasteItems(Id) const;

private:
    Id getFolderId(Id) const;

private:
    Manager& m_manager;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_OPERATION_HPP
