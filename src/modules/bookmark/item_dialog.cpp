#include "item_dialog.hpp"

#include "core/bookmark.hpp"
#include "global.hpp"

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QKeyEvent>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>

namespace natsu::modules::bookmark {

using natsu::bookmark::Folder;

ItemDialog::
ItemDialog(QWidget& parent)
    : QDialog { &parent }
    , m_title { make_qmanaged<QLineEdit>(this) }
    , m_location { make_qmanaged<QLineEdit>(this) }
    , m_keyword { make_qmanaged<QLineEdit>(this) }
    , m_description { make_qmanaged<QPlainTextEdit>(this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    auto form = make_qmanaged<QFormLayout>();
    form->addRow(QSL("Title:"), m_title.get());
    form->addRow(QSL("Location:"), m_location.get());
    form->addRow(QSL("Keyword:"), m_keyword.get());
    form->addRow(QSL("Description:"), m_description.get());

    layout->addLayout(form);

    m_buttons->addButton(QDialogButtonBox::Ok);
    m_buttons->addButton(QDialogButtonBox::Cancel);
    to_ref(m_buttons->button(QDialogButtonBox::Ok)).setEnabled(false);
    layout->addWidget(m_buttons);

    this->connect(m_title.get(), &QLineEdit::textEdited,
                  this,          &ItemDialog::onEdited);
    this->connect(m_location.get(), &QLineEdit::textEdited,
                  this,             &ItemDialog::onEdited);
    this->connect(m_keyword.get(), &QLineEdit::textEdited,
                  this,            &ItemDialog::onEdited);
    this->connect(m_description.get(), &QPlainTextEdit::textChanged,
                  this,                &ItemDialog::onEdited);
    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &QDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &QDialog::reject);

    this->setWindowTitle(QSL("New Bookmark"));
    this->resize(400, 200);
}

ItemDialog::
ItemDialog(Bookmark const& bookmark, QWidget& parent)
    : ItemDialog { parent }
{
    m_title->setText(bookmark.title);
    m_title->setCursorPosition(0);
    m_location->setText(bookmark.url);
    m_location->setCursorPosition(0);
    m_keyword->setText(bookmark.keyword);
    m_keyword->setCursorPosition(0);
    m_description->setPlainText(bookmark.description);
    setReadOnly(!bookmark.editable);

    to_ref(m_buttons->button(QDialogButtonBox::Ok)).setEnabled(false);

    this->setWindowTitle(QSL("Properties of %1").arg(bookmark.title));
    m_description->setFocus();
    m_description->selectAll();
}

QString ItemDialog::
title() const
{
    return m_title->text();
}

QString ItemDialog::
location() const
{
    return m_location->text();
}

QString ItemDialog::
keyword() const
{
    return m_keyword->text();
}

QString ItemDialog::
description() const
{
    return m_description->toPlainText();
}

void ItemDialog::
keyPressEvent(QKeyEvent* const e)
{
    auto& event = to_ref(e);

    if (event.modifiers() & Qt::ControlModifier
                            && event.key() == Qt::Key_Return)
    {
        if (m_buttons->button(QDialogButtonBox::Ok)->isEnabled()) {
            this->accept();
        }
    }

    QDialog::keyPressEvent(e);
}

void ItemDialog::
setReadOnly(bool const flag)
{
    m_readOnly = flag;

    m_title->setEnabled(!flag);
    m_location->setEnabled(!flag);
    m_keyword->setEnabled(!flag);
    m_description->setEnabled(!flag);
}

void ItemDialog::
onEdited()
{
    auto const hasLocation = !m_location->text().isEmpty();

    to_ref(m_buttons->button(QDialogButtonBox::Ok)).setEnabled(hasLocation);
}

} // namespace natsu::modules::bookmark
