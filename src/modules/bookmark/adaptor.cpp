#include "adaptor.hpp"

#include "actions.hpp"
#include "address_bar_action.hpp"
#include "menu.hpp"
#include "tool_bar.hpp"
#include "manager.hpp"

#include "browser/dock.hpp"

#include "amaze_box/amaze_box.hpp"
#include "browser/browser_tab.hpp"
#include "browser/navigation_bar.hpp"
#include "browser/web_view.hpp"
#include "browser/browser_window.hpp"
#include "browser/browser_window/menu_bar.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"
#include "misc/action_utility.hpp"

#include <QBoxLayout>
#include <QMenuBar>

namespace natsu::modules::bookmark {

using natsu::bookmark::ReservedId;

Adaptor::
Adaptor(Manager& manager)
    : m_manager { manager }
{
    auto& profile = get::profile(m_manager);

    this->connect(&profile, &Profile::windowCreated,
                  this,     &Adaptor::onWindowCreated);
}

Adaptor::~Adaptor() = default;

void Adaptor::
installAddressBarAction(BrowserTab& tab) const
{
    auto& addressBox = tab.navigationBar().amazeBox();

    auto& page = get::webPage(tab);
    auto const& action = make_qmanaged<AddressBarAction>(m_manager, page);

    addressBox.addAction(action.get(), QLineEdit::TrailingPosition);
}

void Adaptor::
installMenuBar(BrowserWindow& window) const
{
    auto const bookmarkMenu = make_qmanaged<TopLevelMenu>(
                    m_manager, ReservedId::BookmarkMenu, window);

    bookmarkMenu->setTitle("&Bookmark");

    auto& menuBar = window.menuBar();
    auto& toolMenu = menuBar.toolMenu();

    menuBar.insertMenu(toolMenu.menuAction(), bookmarkMenu.get());
}

void Adaptor::
installToolBar(BrowserTab& tab) const
{
    auto const toolBar = make_qmanaged<ToolBar>(m_manager, tab);

    tab.addToolBarBreak(Qt::TopToolBarArea);
    tab.addToolBar(Qt::TopToolBarArea, toolBar.get());
}

void Adaptor::
installBrowserDock(BrowserWindow& window) const
{
    auto const widget = make_qmanaged<browser::Dock>(m_manager, window);

    auto& action = to_ref(widget->toggleViewAction());
    window.addAction(&action);

    if (!window.restoreDockWidget(widget.get())) {
        window.addDockWidget(Qt::LeftDockWidgetArea, widget.get());
    }
}

void Adaptor::
installWindowActions(BrowserWindow& window) const
{
    m_manager.actions().installWindowActionsTo(window);
}

void Adaptor::
onWindowCreated(BrowserWindow& window) const
{
    installMenuBar(window);
    installBrowserDock(window);
    installWindowActions(window);

    this->connect(&window, &BrowserWindow::tabCreated,
                  this,    &Adaptor::onTabCreated);
}

void Adaptor::
onTabCreated(BrowserTab& tab) const
{
    installToolBar(tab);
    installAddressBarAction(tab);
}

} // namespace natsu::modules::bookmark
