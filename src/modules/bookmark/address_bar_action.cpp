#include "address_bar_action.hpp"

#include "add_dialog.hpp"
#include "manager.hpp"

#include "browser/browser_window.hpp"
#include "browser/web_page.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"

#include <QUrl>

namespace natsu::modules::bookmark {

AddressBarAction::
AddressBarAction(Manager& manager, WebPage& page)
    : QAction { &page }
    , m_page { page }
    , m_manager { manager }
{
    this->connect(&m_page, &WebPage::urlChanged,
                  this,    &AddressBarAction::update);

    this->connect(&m_manager, &Manager::itemAdded,
                  this,       &AddressBarAction::update);
    this->connect(&m_manager, &Manager::itemRemoved,
                  this,       &AddressBarAction::update);
    this->connect(&m_manager, &Manager::itemUpdated,
                  this,       &AddressBarAction::update);

    update();
}

AddressBarAction::~AddressBarAction() = default;

void AddressBarAction::
update()
{
    auto const& url = m_page.url();

    if (m_manager.contain(url)) {
        this->setIcon(natsu::bookmark::icon::bookmarked());

        this->disconnect();
        this->connect(this, &QAction::triggered,
                      this, &AddressBarAction::removeThisPage);
    }
    else {
        this->setIcon(natsu::bookmark::icon::notBookmarked());

        this->disconnect();
        this->connect(this, &QAction::triggered,
                      this, &AddressBarAction::addThisPage);
    }
}

void AddressBarAction::
addThisPage()
{
    auto const& url = m_page.url();

    if (m_manager.contain(url)) return;

    auto& window = get::browserWindow(m_page);

    AddDialog dialog { m_manager, window };

    if (dialog.exec() != AddDialog::Accepted) return;

    m_manager.addBookmark(dialog.folder(), dialog.title(), url, "", "");

    update();
}

void AddressBarAction::
removeThisPage()
{
    auto const& url = m_page.url();

    auto const& bookmarks = m_manager.search(url);
    if (bookmarks.empty()) return;

    auto& item = bookmarks.front();
    m_manager.removeItem(item.id);

    update();
}

} // namespace natsu::modules::bookmark
