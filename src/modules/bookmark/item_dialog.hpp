#ifndef NATSU_MODULES_BOOKMARK_ITEM_DIALOG_HPP
#define NATSU_MODULES_BOOKMARK_ITEM_DIALOG_HPP

#include "misc/pointer.hpp"

#include <QDialog>

class QDialogButtonBox;
class QKeyEvent;
class QLineEdit;
class QPlainTextEdit;
class QWidget;

namespace natsu::bookmark { class Bookmark; } // namespace natsu::bookmark

namespace natsu::modules::bookmark {

using natsu::bookmark::Bookmark;

class ItemDialog : public QDialog
{
    Q_OBJECT
public:
    ItemDialog(QWidget& parent);
    ItemDialog(Bookmark const&, QWidget& parent);

    // accessor
    QString title() const;
    QString location() const;
    QString keyword() const;
    QString description() const;
    bool isReadOnly() const { return m_readOnly; }

    // override QWidget
    void keyPressEvent(QKeyEvent*) override;

private:
    void setReadOnly(bool flag);

    Q_SLOT void onEdited();

private:
    qmanaged_ptr<QLineEdit> m_title;
    qmanaged_ptr<QLineEdit> m_location;
    qmanaged_ptr<QLineEdit> m_keyword;
    qmanaged_ptr<QPlainTextEdit> m_description;
    qmanaged_ptr<QDialogButtonBox> m_buttons;

    bool m_readOnly = false;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_ITEM_DIALOG_HPP
