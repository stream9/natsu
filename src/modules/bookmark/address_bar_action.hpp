#ifndef NATSU_MODULES_BOOKMARK_ADDRESS_BAR_ACTION_HPP
#define NATSU_MODULES_BOOKMARK_ADDRESS_BAR_ACTION_HPP

#include <QAction>

class QUrl;

namespace natsu { class WebPage; } // namespace natsu

namespace natsu::modules::bookmark {

class Manager;

class AddressBarAction : public QAction
{
    Q_OBJECT
public:
    AddressBarAction(Manager&, WebPage&);
    ~AddressBarAction() override;

private:
    Q_SLOT void update();
    Q_SLOT void addThisPage();
    Q_SLOT void removeThisPage();

private:
    WebPage& m_page;
    Manager& m_manager;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_ADDRESS_BAR_ACTION_HPP
