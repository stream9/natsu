#include "context_menu.hpp"

#include "actions.hpp"

namespace natsu::modules::bookmark {

ContextMenu::
ContextMenu(Actions& actions)
    : m_actions { actions }
{
    this->connect(this, &QMenu::aboutToShow,
                  this, &ContextMenu::onAboutToShow);
}

ContextMenu::~ContextMenu() = default;

void ContextMenu::
populateItems()
{
    this->addAction(&m_actions.openInCurrentTabAction());
    this->addAction(&m_actions.openInNewTabAction());
    this->addAction(&m_actions.openInNewWindowAction());

    this->addSeparator();
    this->addAction(&m_actions.newBookmarkAction());
    this->addAction(&m_actions.newFolderAction());
    this->addAction(&m_actions.newSeparatorAction());

    this->addSeparator();
    this->addAction(&m_actions.deleteAction());

    this->addSeparator();
    this->addAction(&m_actions.editAction());
}

void ContextMenu::
onAboutToShow()
{
    populateItems();
}

} // namespace natsu::modules::bookmark
