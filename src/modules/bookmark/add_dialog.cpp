#include "add_dialog.hpp"

#include "manager.hpp"

#include "add_dialog/folder_combo_box.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "core/get.hpp"
#include "global.hpp"

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QString>
#include <QSize>

namespace natsu::modules::bookmark {

AddDialog::
AddDialog(Manager& manager, BrowserWindow& window)
    : QDialog { &window }
    , m_manager { manager }
    , m_window { window }
    , m_title { make_qmanaged<QLineEdit>(this) }
    , m_folder { make_qmanaged<add_dialog::FolderComboBox>(manager, *this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    auto top = make_qmanaged<QFormLayout>();
    auto label = make_qmanaged<QLabel>(QSL("Title:"), this);
    top->addRow(label.get(), m_title.get());

    label = make_qmanaged<QLabel>(QSL("Folder:"), this);
    top->addRow(label.get(), m_folder.get());

    layout->addLayout(top);

    m_buttons->addButton(QDialogButtonBox::Ok);
    m_buttons->addButton(QDialogButtonBox::Cancel);

    layout->addWidget(m_buttons.get());

    setupValues();

    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &QDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &QDialog::reject);
}

AddDialog::~AddDialog() = default;

QString AddDialog::
title() const
{
    return m_title->text();
}

Id AddDialog::
folder() const
{
    return m_folder->folder();
}

QSize AddDialog::
sizeHint() const
{
    return { 350, 120 };
}

void AddDialog::
setupValues()
{
    m_title->setText(get::activeTab(m_window).title());
    m_title->setCursorPosition(0);
}

} // namespace natsu::modules::bookmark
