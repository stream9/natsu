#ifndef NATSU_MODULES_BOOKMARK_ACTIONS_HPP
#define NATSU_MODULES_BOOKMARK_ACTIONS_HPP

#include "core/bookmark.hpp"

#include <optional>
#include <vector>

#include <QAction>
#include <QObject>

class QWidget;

namespace natsu::modules::bookmark {

class Manager;

using Id = natsu::bookmark::Id;

class Actions : public QObject
{
    Q_OBJECT
public:
    Actions(Manager&);
    ~Actions() override;

    // query
    QAction& bookmarkThisPageAction() { return m_bookmarkThisPage; }
    QAction& removeThisPageAction() { return m_removeThisPage; }
    QAction& showBrowserAction() { return m_showBrowser; }

    QAction& openInCurrentTabAction() { return m_open; }
    QAction& openInNewTabAction() { return m_openInNewTab; }
    QAction& openInNewWindowAction() { return m_openInNewWindow; }
    QAction& newBookmarkAction() { return m_newBookmark; }
    QAction& newFolderAction() { return m_newFolder; }
    QAction& newSeparatorAction() { return m_newSeparator; }
    QAction& deleteAction() { return m_delete; }
    QAction& editAction() { return m_edit; }
    QAction& cutAction() { return m_cut; }
    QAction& pasteAction() { return m_paste; }

    // modifier
    void setSelection(Id);
    void setSelections(std::vector<Id>&&);
    void clearSelection();

    // command
    void installWindowActionsTo(QWidget&);
    void installBrowserActionsTo(QWidget&);

private:
    void update();

    Q_SLOT void bookmarkThisPage() const;
    Q_SLOT void removeThisPage() const;
    Q_SLOT void showBrowser() const;

    Q_SLOT void onOpenInCurrentTab() const;
    Q_SLOT void onOpenInNewTab() const;
    Q_SLOT void onOpenInNewWindow() const;
    Q_SLOT void onNewBookmark() const;
    Q_SLOT void onNewFolder() const;
    Q_SLOT void onNewSeparator() const;
    Q_SLOT void onDelete() const;
    Q_SLOT void onEdit() const;
    Q_SLOT void onCut() const;
    Q_SLOT void onPaste() const;

private:
    Manager& m_manager;

    QAction m_bookmarkThisPage;
    QAction m_removeThisPage;
    QAction m_showBrowser;

    QAction m_open;
    QAction m_openInNewTab;
    QAction m_openInNewWindow;
    QAction m_newBookmark;
    QAction m_newFolder;
    QAction m_newSeparator;
    QAction m_delete;
    QAction m_edit;
    QAction m_cut;
    QAction m_paste;

    std::vector<Id> m_selection;
};

} // namespace natsu::modules::bookmark

#endif // NATSU_MODULES_BOOKMARK_ACTIONS_HPP
