#include "folder_tree.hpp"

#include "../manager.hpp"

#include "misc/pointer.hpp"

#include <QTreeWidgetItem>

namespace natsu::modules::bookmark::add_dialog {

using natsu::bookmark::Folder;
using natsu::bookmark::ReservedId;

FolderTree::
FolderTree(Manager& manager, QWidget& parent)
    : QTreeWidget { &parent }
    , m_manager { manager }
{
    populateItems(to_ref(this->invisibleRootItem()), ReservedId::Root);

    this->connect(this, &QTreeWidget::currentItemChanged,
                  this, &FolderTree::onCurrentItemChanged);
}

FolderTree::~FolderTree() = default;

Id FolderTree::
id(QTreeWidgetItem& item) const
{
    return item.data(0, Qt::UserRole + 1).value<Id>();
}

void FolderTree::
populateItems(QTreeWidgetItem& parent, Id const parentId) const
{
    auto const& items = m_manager.items(parentId);

    for (auto const& item: items) {
        if (!isFolder(item)) continue;

        auto* const child = new QTreeWidgetItem { &parent };
        child->setText(0, std::get<Folder>(item).title);
        child->setData(0, Qt::UserRole + 1,
                       QVariant::fromValue(std::get<Folder>(item).id));

        populateItems(*child, std::get<Folder>(item).id);
    }
}

void FolderTree::
onCurrentItemChanged(QTreeWidgetItem* const item) const
{
    Q_EMIT currentIdChanged(item->data(0, Qt::UserRole + 1).value<Id>());
}

} // namespace natsu::modules::bookmark::add_dialog
