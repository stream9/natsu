#ifndef NATSU_MODULES_BOOKMARK_ADD_DIALOG_FOLDER_TREE_HPP
#define NATSU_MODULES_BOOKMARK_ADD_DIALOG_FOLDER_TREE_HPP

#include "core/bookmark.hpp"

#include <QTreeWidget>

class QTreeWidgetItem;
class QWidget;

namespace natsu::modules::bookmark { class Manager; } // namespace natsu::bookmark

namespace natsu::modules::bookmark::add_dialog {

using natsu::bookmark::Id;

class FolderTree : public QTreeWidget
{
    Q_OBJECT
public:
    FolderTree(Manager&, QWidget& parent);
    ~FolderTree() override;

    // query
    Id id(QTreeWidgetItem&) const;

    // signal
    Q_SIGNAL void currentIdChanged(Id) const;

private:
    void populateItems(QTreeWidgetItem& parent, Id parentId) const;

    Q_SLOT void onCurrentItemChanged(QTreeWidgetItem*) const;

private:
    Manager& m_manager;
};

} // namespace natsu::modules::bookmark::add_dialog

#endif // NATSU_MODULES_BOOKMARK_ADD_DIALOG_FOLDER_TREE_HPP
