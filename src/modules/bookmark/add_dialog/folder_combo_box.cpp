#include "folder_combo_box.hpp"

#include "manager.hpp"
#include "folder_tree.hpp"

#include "global.hpp"
#include "misc/pointer.hpp"

#include <QBoxLayout>
#include <QComboBox>
#include <QIcon>
#include <QStyle>
#include <QToolButton>
#include <QTreeWidgetItemIterator>

namespace natsu::modules::bookmark::add_dialog {

using natsu::bookmark::Folder;

static QString
lastFolderKey()
{
    return QSL("lastFolderId");
}

FolderComboBox::
FolderComboBox(Manager& manager, QWidget& parent)
    : QWidget { &parent }
    , m_manager { manager }
    , m_comboBox { make_qmanaged<QComboBox>(this) }
    , m_hide { make_qmanaged<QToolButton>(this) }
    , m_tree { make_qmanaged<FolderTree>(m_manager, *this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto combo = make_qmanaged<QHBoxLayout>();
    combo->setContentsMargins(0, 0, 0, 0);

    setupComboBox();
    combo->addWidget(m_comboBox.get(), 1);

    m_hide->setArrowType(Qt::UpArrow);
    combo->addWidget(m_hide.get());

    layout->addLayout(combo);

    setupTree();
    layout->addWidget(m_tree.get());

    this->connect(m_tree.get(), &FolderTree::currentIdChanged,
                  this,         &FolderComboBox::onTreeSelected);

    loadSettings();
}

FolderComboBox::
~FolderComboBox()
{
    saveSettings();
}

Id FolderComboBox::
folder() const
{
    return m_comboBox->currentData().value<Id>();
}

void FolderComboBox::
setupComboBox()
{
    QTreeWidgetItemIterator it { m_tree.get() };

    m_numFolders = 0;
    while (auto* const item = *it) {
        m_comboBox->addItem(item->text(0),
                            QVariant::fromValue(m_tree->id(*item)));

        if (++m_numFolders == 5) break;
        ++it;
    }

    m_comboBox->insertSeparator(m_comboBox->count());
    m_comboBox->addItem(QSL("Choose..."));

    this->connect(m_comboBox.get(), QOverload<int>::of(&QComboBox::currentIndexChanged),
                  this,             &FolderComboBox::onComboBoxChanged);
}

void FolderComboBox::
setupTree()
{
    m_tree->setHeaderHidden(true);

    this->connect(m_hide.get(), &QToolButton::clicked,
                  this,         &FolderComboBox::hideTree);

    hideTree();
}

void FolderComboBox::
showTree()
{
    m_hide->show();
    m_tree->show();
}

void FolderComboBox::
hideTree()
{
    m_hide->hide();
    m_tree->hide();

    this->adjustSize();
    auto& window = to_ref(this->window());
    window.adjustSize();
}

void FolderComboBox::
selectFolder(Id const id)
{
    auto i = 0;

    for (; i < m_numFolders; ++i) {
        if (m_comboBox->itemData(i).value<Id>() == id) break;
    }

    if (i < m_numFolders) {
        m_comboBox->setCurrentIndex(i);
    }
    else {
        auto const& item = m_manager.item(id);

        if (item) {
            assert(isFolder(*item));

            m_comboBox->insertItem(m_numFolders,
                    std::get<Folder>(*item).title, QVariant::fromValue(id));
            m_comboBox->setCurrentIndex(m_numFolders);
            ++m_numFolders;
        }
        else {
            m_comboBox->setCurrentIndex(0);
        }
    }
}

void FolderComboBox::
loadSettings()
{
    auto const folderId =
                m_manager.loadSetting(lastFolderKey()).value<Id>();
    selectFolder(folderId);
}

void FolderComboBox::
saveSettings()
{
    auto const folderId = m_comboBox->currentData().value<Id>();
    m_manager.saveSetting(lastFolderKey(), QVariant::fromValue(folderId));
}

void FolderComboBox::
onComboBoxChanged(int const index)
{
    if (index == m_comboBox->count()-1) {
        m_comboBox->setCurrentIndex(m_comboBoxIndex);

        showTree();
    }
    else {
        m_comboBoxIndex = index;
    }
}

void FolderComboBox::
onTreeSelected(Id const id)
{
    selectFolder(id);
}

} // namespace natsu::modules::bookmark::add_dialog
