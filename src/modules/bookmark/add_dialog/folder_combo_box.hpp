#ifndef NATSU_MODULES_BOOKMARK_FOLDER_COMBO_BOX_HPP
#define NATSU_MODULES_BOOKMARK_FOLDER_COMBO_BOX_HPP

#include "misc/pointer.hpp"
#include "core/bookmark.hpp"

#include <memory>

#include <QWidget>

class QComboBox;
class QModelIndex;
class QToolButton;

namespace natsu::modules::bookmark { class Manager; } // namespace natsu::bookmark

namespace natsu::modules::bookmark::add_dialog {

using natsu::bookmark::Id;

class FolderTree;

class FolderComboBox : public QWidget
{
    Q_OBJECT
public:
    FolderComboBox(Manager&, QWidget& parent);
    ~FolderComboBox() override;

    // accessor
    Manager& manager() const { return m_manager; }

    // query
    Id folder() const;

private:
    void setupComboBox();
    void setupTree();
    void showTree();
    void selectFolder(Id);
    void loadSettings();
    void saveSettings();

    Q_SLOT void hideTree();
    Q_SLOT void onComboBoxChanged(int index);
    Q_SLOT void onTreeSelected(Id);

private:
    Manager& m_manager;

    qmanaged_ptr<QComboBox> const m_comboBox;
    qmanaged_ptr<QToolButton> const m_hide;
    qmanaged_ptr<FolderTree> const m_tree;

    int m_numFolders = 0;
    int m_comboBoxIndex = 0;
};

} // namespace natsu::modules::bookmark::add_dialog

#endif // NATSU_MODULES_BOOKMARK_FOLDER_COMBO_BOX_HPP
