#include "style_file.hpp"

#include "global.hpp"

#include <filesystem>
#include <fstream>
#include <sstream>
#include <stdexcept>

namespace natsu::modules::user_style {

namespace usp = user_style_parser;
namespace fs = std::filesystem;

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

static std::string
slurpFile(fs::path const& path)
{
    std::ifstream ifs;
    ifs.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        ifs.open(path);
    }
    catch (std::system_error const& e) {
        std::ostringstream oss;

        oss << "error in " << path.filename()
            << ": " << e.what() << "\n";

        throw std::runtime_error(oss.str());
    }

    std::ostringstream oss;
    oss << ifs.rdbuf();

    return oss.str();
}

static std::string
escape(std::string_view const text)
{
    std::string result;

    for (auto const ch: text) {
        if (ch == '`') {
            result.append("\\`");
        }
        else {
            result.push_back(ch);
        }
    }

    return result;
}

static std::string
toScript(usp::ast::document_rule const& rule)
{
    std::ostringstream oss;

    oss << "// ==UserScript==\n";

    for (auto const& func: rule.functions) {
        std::visit(overloaded {
            [&](usp::ast::url_function const& f) {
                oss << "// @include " << f.parameter << "\n";
            },
            [&](usp::ast::url_prefix_function const& f) {
                oss << "// @include " << f.parameter << "*\n";
            },
            [&](usp::ast::domain_function const& f) {
                oss << "// @match *://*." << f.parameter << "/*\n";
            },
            [&](usp::ast::regexp_function const& f) {
                oss << "// @include " << f.parameter << "\n";
            }
        }, func);
    }

    oss << "// ==/UserScript==\n";

    oss << "(function() {\n"
        << "  var style = document.createElement('style');\n"
        << "  style.innerHTML = `" << escape(rule.body) << "`;\n"
        << "  document.documentElement.appendChild(style);\n"
        << "})();"
        ;

    return oss.str();
}

// StyleFile

StyleFile::
StyleFile(QString const& path)
    : m_path { path }
{
    fs::path path_ { path.toUtf8().constData() };
    m_css = slurpFile(path_);

    try {
        m_rules = usp::parse(m_css);
    }
    catch (usp::parse_error const& e) {
        std::ostringstream oss;

        oss << "parse error in " << path_.filename()
            << ": " << e.what() << "\n"
            << usp::error_position(m_css, e.pos());

        throw std::runtime_error(oss.str());
    }
}

StyleFile::~StyleFile() = default;

bool StyleFile::
exists() const
{
    fs::path path = m_path.toUtf8().constData();
    return fs::exists(path);
}

std::vector<std::string> StyleFile::
scripts() const
{
    std::vector<std::string> result;

    for (auto const& rule: m_rules) {
        result.push_back(toScript(rule));
    }

    return result;
}

} // namespace natsu::modules::user_style
