#include "profile_adaptor.hpp"

#include "style_file.hpp"

#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QDir>
#include <QSettings>
#include <QWebEngineScript>
#include <QWebEngineScriptCollection>

namespace natsu::modules::user_style {

ProfileAdaptor::
ProfileAdaptor(Profile& profile)
    : QObject { &profile }
    , m_profile { profile }
{
    auto const& dir = styleDir();
    reloadDirectory(dir);

    this->connect(&m_fsWatcher, &QFileSystemWatcher::directoryChanged,
                  this,         &ProfileAdaptor::onDirectoryChanged);
    this->connect(&m_fsWatcher, &QFileSystemWatcher::fileChanged,
                  this,         &ProfileAdaptor::onFileChanged);
}

ProfileAdaptor::~ProfileAdaptor() = default;

QSettings ProfileAdaptor::
settings() const
{
    auto const& dir = m_profile.pluginDir();
    assert(dir.exists());

    return { dir.filePath(QSL("user_style.ini")), QSettings::IniFormat };
}

QString ProfileAdaptor::
styleDir() const
{
    return settings().value(QSL("StyleDir")).toString();
}

StyleFile* ProfileAdaptor::
findFile(QString const& path)
{
    auto const it = std::find_if(
        m_files.begin(), m_files.end(),
        [&](auto const& file) {
            return file.path() == path;
        }
    );

    return it != m_files.end() ? &*it : nullptr;
}

void ProfileAdaptor::
addFile(QString const& path)
{
    m_fsWatcher.addPath(path);

    try {
        m_files.emplace_back(path);
    }
    catch (std::runtime_error const& e) {
        qDebug() << e.what();
        return;
    }

    auto const& file = m_files.back();

    auto& scripts = to_ref(m_profile.scripts());

    auto constexpr worldId = QWebEngineScript::UserWorld + 1000;

    assert(m_scripts.count(path) == 0);

    for (auto const& text: file.scripts()) {
        QWebEngineScript script;
        script.setName(path);
        script.setSourceCode(text.c_str());
        script.setInjectionPoint(QWebEngineScript::DocumentReady);
        script.setWorldId(worldId);

        m_scripts.emplace(path, script);

        scripts.insert(script);
    }
}

void ProfileAdaptor::
removeFile(StyleFile const& file)
{
    m_fsWatcher.removePath(file.path());

    auto& scripts = to_ref(m_profile.scripts());

    auto it1 = m_scripts.find(file.path());
    if (it1 != m_scripts.end()) {
        scripts.remove(it1->second);
        m_scripts.erase(it1);
    }

    auto const it2 = std::remove_if(
        m_files.begin(), m_files.end(),
        [&](auto const& f) {
            return &f == &file;
        }
    );
    m_files.erase(it2, m_files.end());
}

void ProfileAdaptor::
clearFiles()
{
    for (auto const& file: m_files) {
        m_fsWatcher.removePath(file.path());
    }

    m_files.clear();

    auto& scripts = to_ref(m_profile.scripts());
    for (auto const& [_, script]: m_scripts) {
        scripts.remove(script);
    }

    m_scripts.clear();
}

void ProfileAdaptor::
reloadDirectory(QString const& path)
{
    if (path.isEmpty()) return;

    clearFiles();

    QDir const dir { path };
    if (!dir.exists()) {
        m_fsWatcher.removePath(path);
        return;
    }

    m_fsWatcher.addPath(path);

    QStringList nameFilter;
    nameFilter << QSL("*.css");

    auto const filter = QDir::Files | QDir::Readable;

    for (auto const& fileName: dir.entryList(nameFilter, filter)) {
        auto const& filePath = dir.filePath(fileName);

        addFile(filePath);
    }

    qInfo().noquote() << QSL("user_style: loaded %1 style sheets")
        .arg(m_scripts.size());
}

void ProfileAdaptor::
onDirectoryChanged(QString const& path)
{
    reloadDirectory(path);
}

void ProfileAdaptor::
onFileChanged(QString const& path)
{
    auto const& file = findFile(path);
    if (!file) {
        qWarning() << QSL("user_style: detect chage from unknown path:")
                   << path;
        return;
    }

    if (!file->exists()) { // removed or renamed
        removeFile(*file);
        return;
    }

    file->reload(); // modified
}

} // namespace natsu::modules::user_style
