#ifndef NATSU_MODULES_USER_STYLE_PROFILE_ADAPTOR_HPP
#define NATSU_MODULES_USER_STYLE_PROFILE_ADAPTOR_HPP

#include <memory>

#include <boost/container/flat_map.hpp>

#include <QObject>
#include <QFileSystemWatcher>

class QDir;
class QSettings;
class QWebEngineScript;

namespace natsu {
    class Profile;
    class BrowserWindow;
    class BrowserTab;
}

namespace natsu::modules::user_style {

class StyleFile;

class ProfileAdaptor : public QObject
{
    Q_OBJECT
public:
    ProfileAdaptor(Profile&);
    ~ProfileAdaptor() override;

    // accessor
    Profile& profile() const { return m_profile; }

private:
    QSettings settings() const;
    QString styleDir() const;

    StyleFile* findFile(QString const& path);
    void addFile(QString const& path);
    void removeFile(StyleFile const&);
    void clearFiles();

    void reloadDirectory(QString const& path);

    Q_SLOT void onDirectoryChanged(QString const& path);
    Q_SLOT void onFileChanged(QString const& path);

private:
    Profile& m_profile;

    std::vector<StyleFile> m_files;
    boost::container::flat_multimap<QString, QWebEngineScript> m_scripts;
    QFileSystemWatcher m_fsWatcher;
};

} // namespace natsu::modules::user_style

#endif // NATSU_MODULES_USER_STYLE_PROFILE_ADAPTOR_HPP
