#include "module.hpp"

#include "profile_adaptor.hpp"

#include "core/application.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

namespace natsu::modules::user_style {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    for (auto const& profile: app.profiles()) {
        onProfileCreated(*profile);
    }
    this->connect(&app, &Application::profileCreated,
                  this, &Module::onProfileCreated);

    return true;
}

void Module::
onProfileCreated(Profile& profile)
{
    make_qmanaged<ProfileAdaptor>(profile);
}

} // namespace natsu::modules::user_style
