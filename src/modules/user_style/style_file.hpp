#ifndef NATSU_MODULES_USER_STYLE_STYLE_FILE_HPP
#define NATSU_MODULES_USER_STYLE_STYLE_FILE_HPP

#include <string>
#include <vector>

#include <QString>

#include <user_style_parser/user_style_parser.hpp>

namespace natsu::modules::user_style {

class StyleFile
{
public:
    StyleFile(QString const& path);
    ~StyleFile();

    QString path() const { return m_path; }
    bool exists() const;
    void reload() {}

    std::vector<std::string> scripts() const;

private:
    QString m_path;
    std::string m_css;
    std::vector<user_style_parser::ast::document_rule> m_rules;
};

} // namespace natsu::modules::user_style

#endif // NATSU_MODULES_USER_STYLE_STYLE_FILE_HPP
