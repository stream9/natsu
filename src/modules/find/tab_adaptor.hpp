#ifndef NATSU_MODULES_FIND_TAB_ADAPTOR_HPP
#define NATSU_MODULES_FIND_TAB_ADAPTOR_HPP

#include <misc/pointer.hpp>

#include <QObject>

namespace natsu {

class BrowserTab;

} // namespace natsu

namespace natsu::modules::find {

class Bar;

class TabAdaptor : public QObject
{
    Q_OBJECT
public:
    TabAdaptor(BrowserTab&);
    ~TabAdaptor() override;

    // accessor
    BrowserTab& browserTab() const { return m_tab; }

private:
    BrowserTab& m_tab;

    qmanaged_ptr<Bar> const m_bar;
};

} // namespace natsu::modules::find

#endif // NATSU_MODULES_FIND_TAB_ADAPTOR_HPP
