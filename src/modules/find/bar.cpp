#include "bar.hpp"

#include "browser/browser_tab.hpp"
#include "browser/web_page.hpp"
#include "browser/web_view.hpp"
#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"

#include <QAction>
#include <QApplication>
#include <QHideEvent>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QShowEvent>
#include <QToolButton>
#include <QWebEngineFindTextResult>

namespace natsu::modules::find {

Bar::
Bar(BrowserTab& tab)
    : QToolBar { &tab }
    , m_tab { tab }
    , m_keyword { make_qmanaged<QLineEdit>(this) }
    , m_forward { make_qmanaged<QAction>(this) }
    , m_backward { make_qmanaged<QAction>(this) }
    , m_matchCase { make_qmanaged<QAction>(this) }
    , m_message { make_qmanaged<QLabel>(this) }
{
    this->setToolButtonStyle(Qt::ToolButtonIconOnly);
    this->setIconSize(QSize(16, 16));
    this->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);
    this->layout()->setContentsMargins(1, 1, 1, 1);
    this->setStyleSheet(QSL("QToolBar { border: 0px; }"));

    this->addWidget(m_keyword.get());

    this->addAction(m_backward.get());
    auto& backwardButton = to_ref(
        static_cast<QToolButton*>(this->widgetForAction(m_backward.get())));
    backwardButton.setArrowType(Qt::UpArrow);

    this->addAction(m_forward.get());
    auto& forwardButton = to_ref(
        static_cast<QToolButton*>(this->widgetForAction(m_forward.get())));
    forwardButton.setArrowType(Qt::DownArrow);

    m_matchCase->setText(QSL("Mat&ch Case"));
    m_matchCase->setCheckable(true);
    this->addAction(m_matchCase.get());

    this->addWidget(m_message.get());

    auto const stretch = make_qmanaged<QWidget>(this);
    stretch->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    this->addWidget(stretch.get());

    this->connect(m_keyword.get(), &QLineEdit::textChanged,
                  this,            &Bar::onTextChanged);
    this->connect(m_keyword.get(), &QLineEdit::returnPressed,
                  this,            &Bar::onReturnPressed);
    this->connect(m_backward.get(), &QAction::triggered,
                  this,             &Bar::findBackward);
    this->connect(m_forward.get(), &QAction::triggered,
                  this,            &Bar::findForward);

    auto& toggleAction = to_ref(this->toggleViewAction());
    toggleAction.setText(QSL("&Find"));
    toggleAction.setObjectName(QSL("find"));
    toggleAction.setIcon(icon::find());

    auto& manager = get::actionManager(tab);
    manager.configure(QSL("Web Page"), toggleAction, QSL("Ctrl+F"));

    get::webView(m_tab).addAction(toggleAction);

    this->hide();
}

Bar::~Bar() = default;

void Bar::
showEvent(QShowEvent* const ev)
{
    auto& event = to_ref(ev);

    if (!event.spontaneous()) {
        m_keyword->setFocus();
    }

    QWidget::showEvent(ev);
}

void Bar::
hideEvent(QHideEvent* const ev)
{
    auto& event = to_ref(ev);

    // tab switch also sends non-spontaneous hide event
    if (!event.spontaneous() && m_tab.isVisible()) {
        m_keyword->clear();
        m_tab.setFocus();
    }

    QWidget::hideEvent(ev);
}

void Bar::
keyPressEvent(QKeyEvent* const ev)
{
    auto& event = to_ref(ev);

    if (event.key() == Qt::Key_Escape) {
        this->hide();
    }

    QWidget::keyPressEvent(ev);
}

bool Bar::
isCaseSensitive() const
{
    return m_matchCase->isChecked();
}

void Bar::
find(QWebEnginePage::FindFlags const flags) const
{
    auto& page = get::webPage(m_tab);

    page.findText(m_keyword->text(), flags,
        [&](auto& result) {
            auto const& keyword = m_keyword->text();

            if (keyword.isEmpty()) {
                m_keyword->setStyleSheet(QSL(""));
            }
            else if (result.numberOfMatches() == 0) {
                m_message->setText(QSL("No Match"));
                m_keyword->setStyleSheet(QSL("QLineEdit { background: rgba(255, 0, 0, 0.2) }"));
            }
            else {
                auto msg = QSL("%1 of %2 matches")
                    .arg(result.activeMatch())
                    .arg(result.numberOfMatches());
                m_message->setText(msg);
                m_keyword->setStyleSheet(QSL(""));
            }
        });
}

void Bar::
findForward() const
{
    auto const flags = isCaseSensitive() ?
            QWebEnginePage::FindCaseSensitively : QWebEnginePage::FindFlag();

    find(flags);
}

void Bar::
findBackward() const
{
    QWebEnginePage::FindFlags flags = QWebEnginePage::FindBackward;
    if (isCaseSensitive()) {
        flags |= QWebEnginePage::FindCaseSensitively;
    }

    find(flags);
}

void Bar::
onTextChanged() const
{
    findForward();
}

void Bar::
onReturnPressed() const
{
    if (qApp->keyboardModifiers() & Qt::ShiftModifier) {
        findBackward();
    }
    else {
        findForward();
    }
}

} // namespace natsu::modules::find
