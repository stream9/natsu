#include "tab_adaptor.hpp"

#include "bar.hpp"

#include "browser/browser_tab.hpp"
#include "global.hpp"

namespace natsu::modules::find {

TabAdaptor::
TabAdaptor(BrowserTab& tab)
    : QObject { &tab }
    , m_tab { tab }
    , m_bar { make_qmanaged<Bar>(m_tab) }
{
    m_tab.addToolBar(Qt::BottomToolBarArea, m_bar.get());
}

TabAdaptor::~TabAdaptor() = default;

} // namespace natsu::modules::find
