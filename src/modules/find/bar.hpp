#ifndef NATSU_MODULES_FIND_BAR_HPP
#define NATSU_MODULES_FIND_BAR_HPP

#include "misc/pointer.hpp"

#include <memory>

#include <QToolBar>
#include <QWebEnginePage>

class QAction;
class QHideEvent;
class QKeyEvent;
class QLabel;
class QLineEdit;
class QShowEvent;

namespace natsu { class BrowserTab; } // namespace natsu

namespace natsu::modules::find {

class Actions;

class Bar : public QToolBar
{
    Q_OBJECT
public:
    Bar(BrowserTab&);
    ~Bar() override;

    // accessor
    BrowserTab& browserTab() const { return m_tab; }

    // override QWidget
    void showEvent(QShowEvent*) override;
    void hideEvent(QHideEvent*) override;
    void keyPressEvent(QKeyEvent*) override;

private:
    bool isCaseSensitive() const;
    void find(QWebEnginePage::FindFlags) const;

    Q_SLOT void findForward() const;
    Q_SLOT void findBackward() const;
    Q_SLOT void onTextChanged() const;
    Q_SLOT void onReturnPressed() const;

private:
    BrowserTab& m_tab;

    qmanaged_ptr<QLineEdit> const m_keyword;
    qmanaged_ptr<QAction> const m_forward;
    qmanaged_ptr<QAction> const m_backward;
    qmanaged_ptr<QAction> const m_matchCase;
    qmanaged_ptr<QLabel> const m_message;
};

} // namespace natsu::modules::find

#endif // NATSU_MODULES_FIND_BAR_HPP
