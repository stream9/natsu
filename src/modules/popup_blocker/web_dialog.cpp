#include "web_dialog.hpp"

#include "browser/browser_tab.hpp"
#include "browser/notification_bar.hpp"
#include "browser/web_dialog.hpp"
#include "browser/web_page.hpp"
#include "browser/web_view.hpp"
#include "core/get.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QFontMetrics>
#include <QToolButton>
#include <QUrl>

namespace natsu::modules::popup_blocker {

WebDialog::
WebDialog(BrowserTab& tab)
    : natsu::WebDialog { tab }
{
    auto& page = this->webView().webPage();

    this->connect(&page, &WebPage::windowCloseRequested,
                  this,  &WebDialog::onCloseRequested);
}

WebDialog::~WebDialog() = default;

void WebDialog::
onCloseRequested()
{
    if (m_notification) {
        m_notification->close();
    }
}

void WebDialog::
onNavigationAccepted(QUrl const& url)
{
    auto& tab = this->browserTab();
    auto const& fm = tab.fontMetrics();
    auto const& message = fm.elidedText(
        QSL("Popup is blocked: %1").arg(url.toString()),
        Qt::ElideRight,
        static_cast<int>(tab.width() * 0.7)
    );

    if (m_notification) {
        m_notification->setMessage(message);
    }
    else {
        m_notification = &tab.makeNotification(message);

        auto const open = make_qmanaged<QToolButton>(this);
        open->setText(QSL("Open"));
        this->connect(open.get(), &QToolButton::clicked,
                      this,       &WebDialog::open);

        m_notification->addWidget(*open);
    }
}

void WebDialog::
open()
{
    if (!m_notification) return;

    m_notification->close();
    this->show();
}

} // namespace natsu::modules::popup_blocker
