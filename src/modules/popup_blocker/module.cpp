#include "module.hpp"

#include "window_adaptor.hpp"
#include "web_dialog.hpp"

#include "core/application.hpp"
#include "core/error.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "core/web_dialog_factory.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <memory>

namespace natsu::modules::popup_blocker {

class WebDialogFactory : public natsu::WebDialogFactory
{
public:
    WebDialogFactory() = default;

    natsu::WebDialog& operator()(BrowserTab& tab) const
    {
        return *make_qmanaged<WebDialog>(tab);
    }
};

// Module

Module::Module() = default;

Module::~Module() noexcept = default;

bool Module::
doInitialize(Application& app) noexcept
{
    try {
        app.setWebDialogFactory(std::make_unique<WebDialogFactory>());

        this->connect(&app, &Application::profileCreated,
                      this, [&](auto& p) {
                          this->connect(&p,   &Profile::windowCreated,
                                        this, &Module::onWindowCreated);
                      });

        return true;
    }
    catch (...) {
        print_error();
        return false;
    }
}

void Module::
onWindowCreated(BrowserWindow& window) noexcept
{
    try {
        make_qmanaged<WindowAdaptor>(window);
    }
    catch (...) {
        print_error();
    }
}

} // namespace natsu::modules::popup_blocker
