#include "page_adaptor.hpp"

#include "window_adaptor.hpp"

#include "browser/browser_tab.hpp"
#include "browser/tab_browser.hpp"
#include "browser/web_page.hpp"
#include "browser/web_view.hpp"
#include "core/application.hpp"
#include "core/error.hpp"
#include "core/get.hpp"
#include "core/site_prefs_manager.hpp"
#include "global.hpp"

#include <QTimer>

namespace natsu::modules::popup_blocker {

static bool
openInBackgroundTab(WebPage& page, QWebEngineNewWindowRequest& req) noexcept
{
    try {
        auto& browser = get::tabBrowser(page);
        auto& tab = browser.newBackgroundTab();

        req.openIn(tab.webView().page());

        using enum QWebEngineNewWindowRequest::DestinationType;
        if (req.destination() == InNewTab) {
            // apparently QtWebEngine takes away focus when it opens a foreground tab.
            QTimer::singleShot(100, [&]{ browser.currentTab().setFocus(); });
        }

        return true;
    }
    catch (...) {
        print_error();
        return false;
    }
}

/*
 * PageAdaptor
 */
PageAdaptor::
PageAdaptor(WebPage& p, WindowAdaptor& w)
    : QObject { &p }
    , m_win { w }
{
    this->connect(&p,   &WebPage::newWindowRequested,
                  this, &PageAdaptor::onNewWindowRequested);
}

PageAdaptor::~PageAdaptor() noexcept = default;

WebPage& PageAdaptor::
page() const noexcept
{
    return static_cast<WebPage&>(*this->parent());
}

void PageAdaptor::
onNewWindowRequested(QWebEngineNewWindowRequest& req,
                     bool& handled) noexcept
{
    try {
        auto& page = this->page();
        auto rejected = false;

        auto p = m_win.sitePref(page.url());

        if (p == "reject") {
            rejected = true;
        }
        else if (p == "site") {
            if (req.requestedUrl().host() != page.url().host()) {
                rejected = true;
            }
        }
        else if (p == "background") {
            if (openInBackgroundTab(page, req)) {
                handled = true;
            }
        }

        if (rejected) {
            handled = true;

            auto verbose = get::application(page).verbose();
            if (verbose) {
                qInfo() << "A new window is rejected by popup blocker:" << page.url();
            }
        }
    }
    catch (...) {
        print_error();
    }
}

} // namespace natsu::modules::popup_blocker
