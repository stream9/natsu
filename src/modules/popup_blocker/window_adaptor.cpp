#include "window_adaptor.hpp"

#include "page_adaptor.hpp"

#include "browser/browser_tab.hpp"
#include "browser/browser_window.hpp"
#include "browser/browser_window/menu_bar.hpp"
#include "browser/web_page.hpp"
#include "core/application.hpp"
#include "core/error.hpp"
#include "core/get.hpp"
#include "core/site_prefs_manager.hpp"
#include "global.hpp"

#include "browser/tab_browser.hpp"

#include <stream9/errors.hpp>

#include <QActionGroup>

namespace natsu::modules::popup_blocker {

WindowAdaptor::
WindowAdaptor(BrowserWindow& w)
    : QObject { &w }
    , m_menu { make_qmanaged<QMenu>(QSL("&Popup"), &w) }
    , m_allow { make_qmanaged<QAction>(QSL("&Allow"), m_menu) }
    , m_site { make_qmanaged<QAction>(QSL("Same &site"), m_menu) }
    , m_background { make_qmanaged<QAction>(QSL("In &background tab"), m_menu) }
    , m_reject { make_qmanaged<QAction>(QSL("&Reject"), m_menu) }
{
    w.menuBar().toolMenu().addMenu(m_menu);

    auto group = make_qmanaged<QActionGroup>(m_menu);

    auto setupAction = [&](auto& a) {
        a->setCheckable(true);
        group->addAction(a);
        m_menu->addAction(a);
    };

    setupAction(m_allow);
    setupAction(m_site);
    setupAction(m_background);
    setupAction(m_reject);

    this->connect(&w,   &BrowserWindow::tabCreated,
                  this, &WindowAdaptor::onTabCreated);
    this->connect(m_menu, &QMenu::aboutToShow,
                  this,   &WindowAdaptor::onMenuAboutToShow);
    this->connect(group, &QActionGroup::triggered,
                  this,  &WindowAdaptor::onActionTriggered);
}

WindowAdaptor::~WindowAdaptor() noexcept = default;

BrowserWindow& WindowAdaptor::
window() const noexcept
{
    return static_cast<BrowserWindow&>(*this->parent());
}

QString WindowAdaptor::
sitePref(QUrl const& u) noexcept
{
    try {
        auto& pref = get::sitePrefs(window());
        auto v = pref.getValue(u, QSL("popup")); //TODO QVariant -> QString

        return v.toString();
    }
    catch (...) {
        print_error(); //TODO log
        return {};
    }
}

void WindowAdaptor::
setSitePref(QUrl const& u, QString const& s) noexcept
{
    try {
        auto key = QSL("popup");
        auto& pref = get::sitePrefs(window());

        if (s == "allow") {
            pref.removeValue(u, key);
        }
        else {
            pref.setValue(u, key, s);
        }
    }
    catch (...) {
        print_error();
    }
}

void WindowAdaptor::
onTabCreated(BrowserTab& tab) noexcept
{
    try {
        make_qmanaged<PageAdaptor>(get::webPage(tab), *this);
    }
    catch (...) {
        print_error();
    }
}

void WindowAdaptor::
onMenuAboutToShow() noexcept
{
    try {
        auto& page = get::webPage(get::activeTab(window()));
        auto p = sitePref(page.url());
        if (p == "reject") {
            m_reject->setChecked(true);
        }
        else if (p == "site") {
            m_site->setChecked(true);
        }
        else if (p == "background") {
            m_background->setChecked(true);
        }
        else {
            m_allow->setChecked(true);
        }
    }
    catch (...) {
        print_error();
    }
}

void WindowAdaptor::
onActionTriggered(QAction* a) noexcept
{
    try {
        auto& page = get::webPage(get::activeTab(window()));

        if (a == m_allow) {
            setSitePref(page.url(), "allow");
        }
        else if (a == m_site) {
            setSitePref(page.url(), "site");
        }
        else if (a == m_background) {
            setSitePref(page.url(), "background");
        }
        else if (a == m_reject) {
            setSitePref(page.url(), "reject");
        }
    }
    catch (...) {
        print_error();
    }
}

} // namespace natsu::modules::popup_blocker
