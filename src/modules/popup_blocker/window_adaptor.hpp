#ifndef NATSU_MODULES_POPUP_BLOCKER_WINDOW_ADAPTOR_HPP
#define NATSU_MODULES_POPUP_BLOCKER_WINDOW_ADAPTOR_HPP

#include "browser/fwd/browser_tab.hpp"
#include "browser/fwd/browser_window.hpp"
#include "misc/pointer.hpp"

#include <QAction>
#include <QObject>
#include <QMenu>

namespace natsu::modules::popup_blocker {

class WindowAdaptor : public QObject
{
    Q_OBJECT
public:
    WindowAdaptor(BrowserWindow&);
    ~WindowAdaptor() noexcept override;

    // accessor
    BrowserWindow& window() const noexcept;

    // query
    QString sitePref(QUrl const&) noexcept;

    // modifier
    void setSitePref(QUrl const&, QString const&) noexcept;

private:
    Q_SLOT void onTabCreated(BrowserTab&) noexcept;
    Q_SLOT void onMenuAboutToShow() noexcept;
    Q_SLOT void onActionTriggered(QAction*) noexcept;

private:
    qmanaged_ptr<QMenu> m_menu;
    qmanaged_ptr<QAction> m_allow;
    qmanaged_ptr<QAction> m_site;
    qmanaged_ptr<QAction> m_background;
    qmanaged_ptr<QAction> m_reject;
};

} // namespace natsu::modules::popup_blocker

#endif // NATSU_MODULES_POPUP_BLOCKER_WINDOW_ADAPTOR_HPP
