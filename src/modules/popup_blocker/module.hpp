#ifndef NATSU_MODULES_POPUP_BLOCKER_MODULES_HPP
#define NATSU_MODULES_POPUP_BLOCKER_MODULES_HPP

#include "browser/fwd/browser_tab.hpp"
#include "browser/fwd/browser_window.hpp"
#include "core/fwd/application.hpp"
#include "core/module.hpp"

namespace natsu::modules::popup_blocker {

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() noexcept override;

private:
    // override plugin::Module
    bool doInitialize(Application&) noexcept override;

    Q_SLOT void onWindowCreated(BrowserWindow&) noexcept;
};

} // namespace natsu::modules::popup_blocker

#endif // NATSU_MODULES_POPUP_BLOCKER_MODULES_HPP
