#ifndef NATSU_MODULES_POPUP_BLOCKER_PAGE_ADAPTOR_HPP
#define NATSU_MODULES_POPUP_BLOCKER_PAGE_ADAPTOR_HPP

#include "browser/fwd/web_page.hpp"

#include <QObject>
#include <QUrl>
#include <QWebEngineNewWindowRequest>

namespace natsu::modules::popup_blocker {

class WindowAdaptor;

class PageAdaptor : public QObject
{
    Q_OBJECT
public:
    PageAdaptor(WebPage&, WindowAdaptor&);
    ~PageAdaptor() noexcept override;

    WebPage& page() const noexcept;

private:
    Q_SLOT void onNewWindowRequested(QWebEngineNewWindowRequest&, bool& handled) noexcept;

private:
    WindowAdaptor& m_win;
};

} // namespace natsu::modules::popup_blocker

#endif // NATSU_MODULES_POPUP_BLOCKER_PAGE_ADAPTOR_HPP
