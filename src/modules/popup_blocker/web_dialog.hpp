#ifndef NATSU_MODULES_POPUP_BLOCKER_WEB_DIALOG_HPP
#define NATSU_MODULES_POPUP_BLOCKER_WEB_DIALOG_HPP

#include "browser/fwd/browser_tab.hpp"
#include "browser/fwd/notification_bar.hpp"
#include "browser/web_dialog.hpp"

namespace natsu::modules::popup_blocker {

class WebDialog : public natsu::WebDialog
{
    Q_OBJECT
public:
    WebDialog(BrowserTab&);
    ~WebDialog() override;

private:
    Q_SLOT void onCloseRequested();
    Q_SLOT void onNavigationAccepted(QUrl const&) override;
    Q_SLOT void open();

private:
    NotificationBar* m_notification = nullptr; // mutable, nullable
};

} // namespace natsu::modules::popup_blocker

#endif // NATSU_MODULES_POPUP_BLOCKER_MODULES_HPP
