#ifndef NATSU_MODULES_DOWNLOAD_ACTION_HPP
#define NATSU_MODULES_DOWNLOAD_ACTION_HPP

namespace natsu::modules::download {

enum class Action : int { Open, Save, Delegate };

} // namespace natsu::modules::download

#endif // NATSU_MODULES_DOWNLOAD_ACTION_HPP
