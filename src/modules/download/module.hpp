#ifndef NATSU_MODULES_DOWNLOAD_MODULES_HPP
#define NATSU_MODULES_DOWNLOAD_MODULES_HPP

#include "core/fwd/application.hpp"
#include "core/fwd/profile.hpp"
#include "core/module.hpp"

#include <memory>

namespace natsu::modules::download {

class Adaptor;

class Module : public natsu::Module
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID MODULE_IID)
    Q_INTERFACES(natsu::Module)

public:
    Module();
    ~Module() override;

private:
    // override plugin::Module
    bool doInitialize(Application&) override;

    Q_SLOT void onProfileCreated(Profile&);
};

} // namespace natsu::modules::download

#endif // NATSU_MODULES_DOWNLOAD_MODULES_HPP
