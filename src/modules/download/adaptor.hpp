#ifndef NATSU_MODULES_DOWNLOAD_ADAPTOR_HPP
#define NATSU_MODULES_DOWNLOAD_ADAPTOR_HPP

#include "browser/fwd/browser_tab.hpp"
#include "browser/fwd/browser_window.hpp"
#include "core/fwd/profile.hpp"

#include <memory>

#include <QObject>

namespace natsu::modules::download {

class Manager;

class Adaptor : public QObject
{
public:
    Adaptor(Profile&);
    ~Adaptor() override;

    // accessor
    Profile& profile() const { return m_profile; }

private:
    void installBrowserDock(BrowserWindow&) const;

private:
    Q_SLOT void onWindowCreated(BrowserWindow&);
    Q_SLOT void onTabCreated(BrowserTab&);

private:
    Profile& m_profile;

    std::unique_ptr<Manager> const m_manager; // non-null
};

} // namespace natsu::modules::download

#endif // NATSU_MODULES_DOWNLOAD_ADAPTOR_HPP
