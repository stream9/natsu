#ifndef NATSU_MODULES_DOWNLOAD_ACTION_OPEN_HPP
#define NATSU_MODULES_DOWNLOAD_ACTION_OPEN_HPP

#include "base.hpp"

#include <QProcess>
#include <QString>

class QJsonObject;
class QTemporaryDir;
class QWebEngineDownloadRequest;

namespace natsu::modules::download::action {

class Open : public Base
{
    Q_OBJECT
public:
    Open(QWebEngineDownloadRequest&,
         QTemporaryDir const&,
         QString const& command, QObject& parent);
    ~Open() override;

    // accessor
    QString const& command() const { return m_command; }

    // override Base
    QJsonObject toJson() const override;
    void run() override;

private:
    Q_SLOT void onDownloadFinished();
    Q_SLOT void onProcessFinished(
                        int exitCode, QProcess::ExitStatus exitStatus);

private:
    QWebEngineDownloadRequest& m_item;

    QString m_command;
    QString m_filePath;
    QProcess m_process;
};

} // namespace natsu::modules::download::action

#endif // NATSU_MODULES_DOWNLOAD_ACTION_OPEN_HPP
