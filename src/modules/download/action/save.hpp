#ifndef NATSU_MODULES_DOWNLOAD_ACTION_SAVE_HPP
#define NATSU_MODULES_DOWNLOAD_ACTION_SAVE_HPP

#include "base.hpp"

#include <QDir>

class QJsonObject;
class QObject;
class QWebEngineDownloadRequest;

namespace natsu::modules::download::action {

class Save : public Base
{
public:
    Save(QWebEngineDownloadRequest&, QString const& filePath, QObject& parent);
    ~Save() override;

    // accessor
    QDir const& dir() const { return m_dir; }

    // override Base
    QJsonObject toJson() const override;
    void run() override;

private:
    Q_SLOT void onDownloadFinished();

private:
    QWebEngineDownloadRequest& m_item;

    QDir m_dir;
};

} // namespace natsu::modules::download::action

#endif // NATSU_MODULES_DOWNLOAD_ACTION_SAVE_HPP
