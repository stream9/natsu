#include "delegate.hpp"

#include "global.hpp"

#include <QApplication>
#include <QJsonObject>
#include <QMessageBox>
#include <QObject>
#include <QStringList>
#include <QUrl>
#include <QWebEngineDownloadRequest>

namespace natsu::modules::download::action {

Delegate::
Delegate(QWebEngineDownloadRequest& item, QString const& command, QObject& parent)
    : Base { &parent }
    , m_url { item.url() }
    , m_command { command }
{
    item.cancel();
}

Delegate::~Delegate() = default;

QJsonObject Delegate::
toJson() const
{
    QJsonObject obj;

    obj[QSL("type")] = QSL("delegate");
    obj[QSL("command")] = m_command;

    return obj;
}

void Delegate::
run()
{
    m_process.setProgram(m_command);

    QStringList arguments;
    arguments << m_url.toString(QUrl::FullyEncoded);

    m_process.setArguments(arguments);

    auto const finished =
                qOverload<int, QProcess::ExitStatus>(&QProcess::finished);
    this->connect(&m_process, finished,
                  this,       &Delegate::onProcessFinished);

    m_process.start();
}

void Delegate::
onProcessFinished(int const exitCode, QProcess::ExitStatus const exitStatus)
{
    if (exitCode != 0) {
        auto* const window = qApp->activeWindow();

        QMessageBox::critical(
            window,
            QSL("Download Error"),
            QSL("External manager exited abnormally."
                " exit code = %1, exit status = %2").arg(exitCode).arg(exitStatus)
        );
    }

    this->deleteLater();
}

} // namespace natsu::modules::download::action
