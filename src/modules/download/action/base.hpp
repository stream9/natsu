#ifndef NATSU_MODULES_DOWNLOAD_ACTION_BASE_HPP
#define NATSU_MODULES_DOWNLOAD_ACTION_BASE_HPP

#include <QObject>

class QJsonObject;

namespace natsu::modules::download::action {

class Base : public QObject
{
public:
    using QObject::QObject;

    // query
    virtual QJsonObject toJson() const = 0;

    // command
    virtual void run() = 0;
};

} // namespace natsu::modules::download::action

#endif // NATSU_MODULES_DOWNLOAD_ACTION_BASE_HPP
