#ifndef NATSU_MODULES_DOWNLOAD_ACTION_DELEGATE_HPP
#define NATSU_MODULES_DOWNLOAD_ACTION_DELEGATE_HPP

#include "base.hpp"

#include <QProcess>
#include <QString>
#include <QUrl>

class QJsonObject;
class QUrl;
class QWebEngineDownloadRequest;

namespace natsu::modules::download::action {

class Delegate : public Base
{
public:
    Delegate(QWebEngineDownloadRequest&, QString const& command, QObject& parent);
    ~Delegate() override;

    // accessor
    QString const& command() const { return m_command; }

    // override Base
    QJsonObject toJson() const override;
    void run() override;

private:
    Q_SLOT void onProcessFinished(
                        int exitCode, QProcess::ExitStatus exitStatus);

private:
    QUrl m_url;

    QProcess m_process;
    QString const m_command;
};

} // namespace natsu::modules::download::action

#endif // NATSU_MODULES_DOWNLOAD_ACTION_DELEGATE_HPP
