#include "save.hpp"

#include "global.hpp"

#include <QFileInfo>
#include <QJsonObject>
#include <QObject>
#include <QWebEngineDownloadRequest>

namespace natsu::modules::download::action {

Save::
Save(QWebEngineDownloadRequest& item, QString const& filePath, QObject& parent)
    : Base { &parent }
    , m_item { item }
{
    QFileInfo const info { filePath };
    m_dir = info.absoluteDir();

    m_item.setDownloadDirectory(info.absolutePath());
    m_item.setDownloadFileName(info.fileName());
}

Save::~Save() = default;

void Save::
run()
{
    m_item.accept();

    this->connect(&m_item, &QWebEngineDownloadRequest::isFinishedChanged,
                  this,    &Save::onDownloadFinished);
}

QJsonObject Save::
toJson() const
{
    QJsonObject obj;

    obj[QSL("type")] = QSL("save");
    obj[QSL("directory")] = m_dir.absolutePath();

    return obj;
}

void Save::
onDownloadFinished()
{
    if (m_item.isFinished()) {
        this->deleteLater();
    }
}

} // namespace natsu::modules::download::action
