#include "open.hpp"

#include "global.hpp"
#include "mime/application.hpp"

#include <QApplication>
#include <QFileInfo>
#include <QJsonObject>
#include <QMessageBox>
#include <QObject>
#include <QStringList>
#include <QTemporaryDir>
#include <QUrl>
#include <QWebEngineDownloadRequest>

namespace natsu::modules::download::action {

Open::
Open(QWebEngineDownloadRequest& item,
     QTemporaryDir const& dir, QString const& command, QObject& parent)
    : Base { &parent }
    , m_item { item }
    , m_command { command }
{
    m_filePath = dir.filePath(item.downloadFileName());
}

Open::~Open() = default;

QJsonObject Open::
toJson() const
{
    //TODO save path to command file instead of command line
    QJsonObject obj;

    obj[QSL("type")] = QSL("open");
    obj[QSL("command")] = m_command;

    return obj;
}

void Open::
run()
{
    this->connect(&m_item, &QWebEngineDownloadRequest::isFinishedChanged,
                  this,    &Open::onDownloadFinished);

    auto const processFinished =
                qOverload<int, QProcess::ExitStatus>(&QProcess::finished);
    this->connect(&m_process, processFinished,
                  this,       &Open::onProcessFinished);

    QFileInfo const info { m_filePath };
    m_item.setDownloadDirectory(info.absolutePath());
    m_item.setDownloadFileName(info.fileName());

    m_item.accept();
}

void Open::
onDownloadFinished()
{
    if (!m_item.isFinished()) return;

    auto command = mime::expandCommandLine(m_command, m_filePath);

    QStringList args;
    args << "/bin/sh" << "-c" << command; //TODO abstract away UNIX specific part

    m_process.setProgram(args.takeFirst());
    m_process.setArguments(args);
    m_process.startDetached();
}

void Open::
onProcessFinished(int const exitCode, QProcess::ExitStatus const exitStatus)
{
    if (exitCode != 0) {
        auto* const window = qApp->activeWindow();

        QMessageBox::critical(
            window,
            QSL("Download Error"),
            QSL("External program exited abnormally. "
                "command line = %1, exit code = %2, exit status = %3")
                .arg(m_process.program()).arg(exitCode).arg(exitStatus)
        );
    }

    this->deleteLater();
}

} // namespace natsu::modules::download::action
