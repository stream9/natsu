#ifndef NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_MODEL_HPP
#define NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_MODEL_HPP

#include "../fwd/manager.hpp"

#include "mime/database.hpp"

#include <boost/container/flat_map.hpp>

#include <QStandardItemModel>
#include <QWebEngineDownloadRequest>

class QIcon;
class QModelIndex;
class QStandardItem;

namespace natsu::modules::download::browser {

class DownloadModel : public QStandardItemModel
{
public:
    enum Role {
        IdRole = Qt::UserRole + 1,
        UrlRole,
        IconRole,
        StateRole,
        PathRole,
        ReceivedBytesRole,
        TotalBytesRole,
        PausedRole,
    };

public:
    DownloadModel(Manager&);
    ~DownloadModel() override;

    // accessor
    Manager& manager() const { return m_manager; }

private:
    QStandardItem& item(QWebEngineDownloadRequest&) const;

    Q_SLOT void addItem(QWebEngineDownloadRequest&);
    Q_SLOT void removeItem(QWebEngineDownloadRequest&);

    Q_SLOT void updateItem();

private:
    Manager& m_manager;
    mime::Database const m_mimeDb;

    using ItemMap =
        boost::container::flat_map<QWebEngineDownloadRequest*, QStandardItem*>;

    ItemMap m_itemMap;
};

} // namespace natsu::modules::download::browser

#endif // NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_MODEL_HPP
