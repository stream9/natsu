#ifndef NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_LIST_HPP
#define NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_LIST_HPP

#include "../fwd/manager.hpp"

#include "misc/abstract_widget_list.hpp"

#include <memory>

class QWidget;

namespace natsu::modules::download::browser {

class DownloadModel;

class DownloadList : public AbstractWidgetList
{
    Q_OBJECT
public:
    DownloadList(Manager&, QWidget& parent);

    ~DownloadList() override;

private:
    // override AbstractWidgetList
    QWidget& createItemWidget(QModelIndex const&) override;

private:
    Manager& m_manager;
    std::unique_ptr<DownloadModel> m_model;
};


} // namespace natsu::modules::download::browser

#endif // NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_LIST_HPP
