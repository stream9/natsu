#ifndef NATSU_MODULES_DOWNLOAD_BROWSER_DOCK_HPP
#define NATSU_MODULES_DOWNLOAD_BROWSER_DOCK_HPP

#include "../fwd/manager.hpp"

#include <QDockWidget>

namespace natsu::modules::download::browser {

class Dock : public QDockWidget
{
    Q_OBJECT
public:
    Dock(Manager&, QWidget& parent);
    ~Dock() override;

private:
    Q_SLOT void onVisibilityChanged(bool visible);

private:
    Manager& m_manager;
};

} // namespace natsu::modules::download::browser

#endif // NATSU_MODULES_DOWNLOAD_BROWSER_DOCK_HPP
