#include "download_list.hpp"

#include "download_model.hpp"
#include "download_widget.hpp"

#include "misc/pointer.hpp"

namespace natsu::modules::download::browser {

DownloadList::
DownloadList(Manager& manager, QWidget& parent)
    : AbstractWidgetList { &parent }
    , m_manager { manager }
    , m_model { std::make_unique<DownloadModel>(manager) }
{
    this->setModel(m_model.get());
}

DownloadList::~DownloadList() = default;

QWidget& DownloadList::
createItemWidget(QModelIndex const& index)
{
    return *make_qmanaged<DownloadWidget>(index, m_manager, *this);
}

} // namespace natsu::modules::download::browser
