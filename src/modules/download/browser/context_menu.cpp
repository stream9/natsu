#include "context_menu.hpp"

#include "actions.hpp"

namespace natsu::modules::download::browser {

ContextMenu::
ContextMenu(Actions& actions)
{
    this->addAction(&actions.pauseAction());
    this->addAction(&actions.stopAction());
    this->addSeparator();
    this->addAction(&actions.copyUrlAction());
    this->addAction(&actions.openFolderAction());
}

ContextMenu::~ContextMenu() = default;

} // namespace natsu::modules::download::browser
