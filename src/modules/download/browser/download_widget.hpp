#ifndef NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_WIDGET_HPP
#define NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_WIDGET_HPP

#include "../fwd/manager.hpp"

#include "misc/pointer.hpp"
#include "misc/time.hpp"

#include <memory>

#include <QWidget>

class QContextMenuEvent;
class QLabel;
class QModelIndex;
class QProgressBar;
class QStackedWidget;
class QToolButton;

namespace natsu::modules::download::browser {

class Actions;

class DownloadWidget : public QWidget
{
public:
    DownloadWidget(QModelIndex const&, Manager&, QWidget& parent);

    ~DownloadWidget() override;

protected:
    void contextMenuEvent(QContextMenuEvent*) override;

private:
    void onDataChanged(QModelIndex const&);

private:
    uint32_t const m_id;
    time_type m_timeStamp;
    int64_t m_received = 0;

    qmanaged_ptr<QLabel> const m_fileName;
    qmanaged_ptr<QLabel> const m_downloadInfo;
    qmanaged_ptr<QLabel> const m_icon;
    qmanaged_ptr<QStackedWidget> const m_stack;
    qmanaged_ptr<QProgressBar> const m_progress;
    qmanaged_ptr<QToolButton> const m_pause;
    qmanaged_ptr<QToolButton> const m_stop;
    qmanaged_ptr<QToolButton> const m_cancel;

    std::unique_ptr<Actions> m_actions;
};

} // namespace natsu::modules::download::browser

#endif // NATSU_MODULES_DOWNLOAD_BROWSER_DOWNLOAD_WIDGET_HPP
