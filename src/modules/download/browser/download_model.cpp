#include "download_model.hpp"

#include "../manager.hpp"

#include "mime/database.hpp"
#include "misc/pointer.hpp"

#include "global.hpp"

#include <QDir>
#include <QIcon>
#include <QStandardItem>
#include <QUrl>
#include <QVariant>
#include <QWebEngineDownloadRequest>

namespace natsu::modules::download::browser {

static QString
getFilePath(QWebEngineDownloadRequest const& item)
{
    QDir const dir { item.downloadDirectory() };
    return dir.absoluteFilePath(item.downloadFileName());
}

class DownloadRequest : public QStandardItem
{
public:
    DownloadRequest(QWebEngineDownloadRequest& item, mime::Database const& mimeDb)
        : m_item { item }
        , m_mimeDb { mimeDb }
    {}

    // accessor
    QWebEngineDownloadRequest& item() const { return m_item; }

    // override QStandardItem
    QVariant data(int const role = Qt::UserRole + 1) const
    {
        using R = DownloadModel::Role;

        switch (role) {
            case R::IdRole:
                return m_item.id();
            case R::UrlRole:
            case Qt::DisplayRole:
                return m_item.url().toDisplayString();
            case R::IconRole:
                return m_mimeDb.icon(m_item.mimeType());
            case R::StateRole:
                return m_item.state();
            case R::PathRole:
                return getFilePath(m_item);
            case R::ReceivedBytesRole:
                return m_item.receivedBytes();
            case R::TotalBytesRole:
                return m_item.totalBytes();
            case R::PausedRole:
                return m_item.isPaused();
            default:
                return QStandardItem::data(role);
        }
    }

private:
    QWebEngineDownloadRequest& m_item;
    mime::Database const& m_mimeDb;
};

DownloadModel::
DownloadModel(Manager& manager)
    : m_manager { manager }
{
    for (auto* const item: m_manager.items()) {
        assert(item);
        addItem(*item);
    }

    this->connect(&m_manager, &Manager::itemAdded,
                  this,       &DownloadModel::addItem);
    this->connect(&m_manager, &Manager::itemRemoved,
                  this,       &DownloadModel::removeItem);
}

DownloadModel::~DownloadModel() = default;

QStandardItem& DownloadModel::
item(QWebEngineDownloadRequest& item) const
{
    auto const it = m_itemMap.find(&item);
    assert(it != m_itemMap.end());

    return to_ref(it->second);
}

void DownloadModel::
addItem(QWebEngineDownloadRequest& item)
{
    auto* const row = new DownloadRequest { item, m_mimeDb };
    m_itemMap.emplace(&item, row);

    this->appendRow(row);

    this->connect(&item, &QWebEngineDownloadRequest::receivedBytesChanged,
                  this,  &DownloadModel::updateItem);

    this->connect(&item, &QWebEngineDownloadRequest::isFinishedChanged,
                  this,  &DownloadModel::updateItem);

    this->connect(&item, &QWebEngineDownloadRequest::isPausedChanged,
                  this,  &DownloadModel::updateItem);

    this->connect(&item, &QWebEngineDownloadRequest::stateChanged,
                  this,  &DownloadModel::updateItem);
}

void DownloadModel::
removeItem(QWebEngineDownloadRequest& item)
{
    auto& modelItem = this->item(item);

    this->takeRow(modelItem.row());
}

void DownloadModel::
updateItem()
{
    auto& downloadItem = to_ref(
        dynamic_cast<QWebEngineDownloadRequest*>(this->sender()));

    auto& modelItem = item(downloadItem);
    auto const index = this->indexFromItem(&modelItem);

    Q_EMIT dataChanged(index, index);
}

} // namespace natsu::modules::download::browser
