#include "download_widget.hpp"

#include "actions.hpp"
#include "context_menu.hpp"
#include "download_model.hpp"

#include "core/icon.hpp"
#include "global.hpp"
#include "misc/elided_label.hpp"
#include "misc/format.hpp"

#include <QBoxLayout>
#include <QContextMenuEvent>
#include <QLabel>
#include <QModelIndex>
#include <QProgressBar>
#include <QStackedWidget>
#include <QToolButton>
#include <QUrl>
#include <QWebEngineDownloadRequest>

namespace natsu::modules::download::browser {

static QUrl
getUrl(QModelIndex const& index)
{
    return index.data(DownloadModel::UrlRole).value<QUrl>();
}

static uint32_t
getId(QModelIndex const& index)
{
    return index.data(DownloadModel::IdRole).value<uint32_t>();
}

static QIcon
getIcon(QModelIndex const& index)
{
    return index.data(DownloadModel::IconRole).value<QIcon>();
}

static QWebEngineDownloadRequest::DownloadState
getState(QModelIndex const& index)
{
    return index.data(DownloadModel::StateRole)
                        .value<QWebEngineDownloadRequest::DownloadState>();
}

static int64_t
getReceivedBytes(QModelIndex const& index)
{
    return index.data(DownloadModel::ReceivedBytesRole).value<int64_t>();
}

static int64_t
getTotalBytes(QModelIndex const& index)
{
    return index.data(DownloadModel::TotalBytesRole).value<int64_t>();
}

static bool
getPaused(QModelIndex const& index)
{
    return index.data(DownloadModel::PausedRole).value<bool>();
}

static QString
speedString(int64_t const bytePerSec)
{
    if (!bytePerSec) return {};

    return QSL("%1/sec").arg(format::binaryBytes(bytePerSec, 1, true));
}

static QString
etaString(int64_t const remain, int64_t const bytePerSec)
{
    if (!bytePerSec) return {};

    std::chrono::seconds const eta { remain / bytePerSec };
    return QSL("- %1 left").arg(format::duration(eta));
}

static QString
getDownloadInfo(QModelIndex const& index)
{
    auto const state = getState(index);

    switch (state) {
        case QWebEngineDownloadRequest::DownloadCompleted: {
            auto const size = getTotalBytes(index);
            auto const& host = getUrl(index).host();

            return QSL("%1 -- %2").arg(format::binaryBytes(size)).arg(host);
        }
        case QWebEngineDownloadRequest::DownloadCancelled:
            return QSL("Canceled");
        case QWebEngineDownloadRequest::DownloadInterrupted:
            return QSL("Interrupted");
        default:
            return {};
    }
}

// DownloadWidget

DownloadWidget::
DownloadWidget(QModelIndex const& index, Manager& manager, QWidget& parent)
    : QWidget { &parent }
    , m_id { getId(index) }
    , m_timeStamp { std::chrono::system_clock::now() }
    , m_fileName { make_qmanaged<ElidedLabel>(this) }
    , m_downloadInfo { make_qmanaged<QLabel>(this) }
    , m_icon { make_qmanaged<QLabel>(this) }
    , m_stack { make_qmanaged<QStackedWidget>(this) }
    , m_progress { make_qmanaged<QProgressBar>(this) }
    , m_pause { make_qmanaged<QToolButton>(this) }
    , m_stop { make_qmanaged<QToolButton>(this) }
    , m_cancel { make_qmanaged<QToolButton>(this) }
    , m_actions { std::make_unique<Actions>(manager, m_id, *this) }
{
    auto layout = make_qmanaged<QHBoxLayout>(this);

    m_icon->setPixmap(getIcon(index).pixmap(32, 32));
    layout->addWidget(m_icon.get());

    auto layout2 = make_qmanaged<QVBoxLayout>();

    auto const& fileName = getUrl(index).fileName();
    m_fileName->setText(fileName);
    layout2->addWidget(m_fileName.get());

    m_stack->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    m_stack->addWidget(m_downloadInfo.get());
    m_stack->addWidget(m_progress.get());

    layout2->addWidget(m_stack.get());

    layout->addLayout(layout2.get(), 1);

    m_pause->setDefaultAction(&m_actions->pauseAction());
    m_pause->setAutoRaise(true);
    layout->addWidget(m_pause.get());

    m_stop->setDefaultAction(&m_actions->stopAction());
    m_stop->setAutoRaise(true);
    layout->addWidget(m_stop.get());

    m_cancel->setDefaultAction(&m_actions->removeAction());
    m_cancel->setAutoRaise(true);
    layout->addWidget(m_cancel.get());

    this->setAutoFillBackground(true);

    this->setContextMenuPolicy(Qt::DefaultContextMenu);

    auto& model = to_ref(index.model());
    this->connect(&model, &QAbstractItemModel::dataChanged,
                  this,   &DownloadWidget::onDataChanged);
}

DownloadWidget::~DownloadWidget() = default;

void DownloadWidget::
contextMenuEvent(QContextMenuEvent* const ev)
{
    assert(ev);

    ContextMenu menu { *m_actions };

    menu.exec(ev->globalPos());
}

void DownloadWidget::
onDataChanged(QModelIndex const& index)
{
    namespace ch = std::chrono;

    assert(index.isValid());
    if (getId(index) != m_id) return;

    auto const currentTime = ch::system_clock::now();
    auto const received = getReceivedBytes(index);

    if (getState(index) == QWebEngineDownloadRequest::DownloadInProgress) {
        m_stack->setCurrentWidget(m_progress.get());

        auto const elapse = currentTime - m_timeStamp;
        auto const elapseMs = ch::duration_cast<ch::milliseconds>(elapse).count();
        auto const total = getTotalBytes(index);
        auto const bytePerMSec = elapseMs ? (received - m_received) / elapseMs : 0;
        auto const bytePerSec = bytePerMSec * 1000;
        auto const remain = total - received;

        auto const ratio = static_cast<double>(received)
                         / static_cast<double>(total) * 100.0;
        m_progress->setValue(static_cast<int>(ratio));

        m_progress->setFormat(
            QSL("%1 %2").arg(speedString(bytePerSec))
                        .arg(etaString(remain, bytePerSec))
        );
    }
    else {
        auto const& info = getDownloadInfo(index);
        m_downloadInfo->setText(info);

        m_stack->setCurrentWidget(m_downloadInfo.get());
    }

    m_actions->update(getState(index), getPaused(index));

    m_timeStamp = currentTime;
    m_received = received;
}

} // namespace natsu::modules::download::browser
