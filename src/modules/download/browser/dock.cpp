#include "dock.hpp"

#include "download_list.hpp"
#include "download_model.hpp"

#include "../manager.hpp"

#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <QAction>
#include <QListView>

namespace natsu::modules::download::browser {

Dock::
Dock(Manager& manager, QWidget& parent)
    : QDockWidget { QSL("Download Browser"), &parent }
    , m_manager { manager }
{
    this->setObjectName(QSL("download.dock"));

    auto& action = to_ref(this->toggleViewAction());
    action.setObjectName(QSL("download.toggle_dock"));

    auto& actionManager = get::actionManager(m_manager.profile());
    actionManager.configure(QSL("Download"), action, makeKeyCode(Qt::SHIFT, Qt::CTRL, Qt::Key_Y));

    this->connect(this, &QDockWidget::visibilityChanged,
                  this, &Dock::onVisibilityChanged);
}

Dock::~Dock() = default;

void Dock::
onVisibilityChanged(bool visible)
{
    if (visible) {
        auto view = make_qmanaged<DownloadList>(m_manager, *this);
        this->setWidget(view.get());
    }
    else {
        auto* o_view = static_cast<DownloadList*>(this->widget());

        if (o_view) {
            this->setWidget(nullptr);
            o_view->deleteLater();
        }
    }
}

} // namespace natsu::modules::download::browser
