#ifndef NATSU_MODULES_DOWNLOAD_BROWSER_ACTIONS_HPP
#define NATSU_MODULES_DOWNLOAD_BROWSER_ACTIONS_HPP

#include "../fwd/manager.hpp"

#include <memory>

#include <QAction>
#include <QObject>
#include <QWebEngineDownloadRequest>

namespace natsu::modules::download::browser {

class Actions : public QObject
{
    Q_OBJECT
public:
    Actions(Manager&, uint32_t id, QWidget&);
    ~Actions() override;

    // query
    QAction& pauseAction() { return *m_pause; }
    QAction& stopAction() { return m_stop; }
    QAction& removeAction() { return m_remove; }
    QAction& openFolderAction() { return m_openFolder; }
    QAction& copyUrlAction() { return m_copyUrl; }

    // modifier
    void update(QWebEngineDownloadRequest::DownloadState, bool isPaused);

private:
    Q_SLOT void pause();
    Q_SLOT void stop();
    Q_SLOT void remove();
    Q_SLOT void openFolder();
    Q_SLOT void copyUrl();

private:
    Manager& m_manager;
    uint32_t m_id;

    std::unique_ptr<QAction> m_pause; // non-null
    QAction m_stop;
    QAction m_remove;
    QAction m_openFolder;
    QAction m_copyUrl;
};

} // namespace natsu::module

#endif // NATSU_MODULES_DOWNLOAD_BROWSER_ACTIONS_HPP
