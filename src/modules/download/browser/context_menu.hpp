#ifndef NATSU_MODULES_DOWNLOAD_BROWSER_CONTEXT_MENU_HPP
#define NATSU_MODULES_DOWNLOAD_BROWSER_CONTEXT_MENU_HPP

#include <QMenu>

namespace natsu::modules::download::browser {

class Actions;

class ContextMenu : public QMenu
{
    Q_OBJECT
public:
    ContextMenu(Actions&);
    ~ContextMenu() override;
};

} // namespace natsu::modules::download::browser

#endif // NATSU_MODULES_DOWNLOAD_BROWSER_CONTEXT_MENU_HPP
