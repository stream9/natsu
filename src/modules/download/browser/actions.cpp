#include "actions.hpp"

#include "../manager.hpp"

#include "core/action_manager.hpp"
#include "core/get.hpp"
#include "core/icon.hpp"
#include "global.hpp"

#include <QApplication>
#include <QMessageBox>

namespace natsu::modules::download::browser {

class PauseAction : public QAction
{
public:
    enum State {
        Stopped, Progress, Paused,
    };

public:
    PauseAction() = default;

    State status() const { return m_state; }

    void setStatus(State const state)
    {
        if (state == m_state) return;

        switch (state) {
            case Progress:
                this->setText(QSL("&Pause"));
                this->setIcon(icon::forName(QSL("media-playback-pause")));
                break;
            case Paused:
                this->setText(QSL("&Resume"));
                this->setIcon(icon::forName(QSL("media-playback-start")));
                break;
            default:
                this->setEnabled(false);
                break;
        }

        m_state = state;
    }

private:
    State m_state = Stopped;
};

// Actions

Actions::
Actions(Manager& manager, uint32_t id, QWidget& widget)
    : m_manager { manager }
    , m_id { id }
    , m_pause { std::make_unique<PauseAction>() }
{
    assert(m_pause);

    auto& actionMgr = get::actionManager(m_manager.profile());
    auto fn = [&](auto& action,
                  auto const& name,
                  auto const& text,
                  auto const& icon,
                  auto const slot,
                  auto const& shortcut)
    {
        action.setObjectName(name);
        action.setText(text);
        action.setIcon(icon);
        action.setShortcut(shortcut);
        this->connect(&action, &QAction::triggered, this, slot);

        actionMgr.configure(QSL("DownloadBrowser"), action, shortcut);

        widget.addAction(&action);
    };

    fn(*m_pause, QSL("download.pause"), QSL("&Pause"), icon::forName(QSL("media-playback-pause")), &Actions::pause, QKeySequence());
    fn(m_stop, QSL("download.stop"), QSL("&Stop"), icon::forName(QSL("media-playback-stop")), &Actions::stop, QKeySequence());
    fn(m_remove, QSL("download.remove"), QSL("&Remove"), icon::closeTab(), &Actions::remove, QKeySequence());
    fn(m_openFolder, QSL("download.open_folder"), QSL("&Open Folder"), icon::forName(QSL("folder-open")), &Actions::openFolder, QKeySequence());
    fn(m_copyUrl, QSL("download.copy_url"), QSL("&Copy URL"), icon::copy(), &Actions::copyUrl, QKeySequence());
}

Actions::~Actions() = default;

static PauseAction&
toPauseAction(QAction& action)
{
    return static_cast<PauseAction&>(action);
}

void Actions::
update(QWebEngineDownloadRequest::DownloadState const state, bool const isPaused)
{
    switch (state) {
        default:
        case QWebEngineDownloadRequest::DownloadRequested:
            toPauseAction(*m_pause).setStatus(PauseAction::Stopped);
            m_stop.setEnabled(false);
            m_remove.setEnabled(true);
            m_openFolder.setEnabled(false);
            m_copyUrl.setEnabled(true);
            break;
        case QWebEngineDownloadRequest::DownloadInProgress:
            toPauseAction(*m_pause).setStatus(
                       isPaused ? PauseAction::Paused : PauseAction::Progress);
            m_stop.setEnabled(true);
            m_remove.setEnabled(true);
            m_openFolder.setEnabled(false);
            m_copyUrl.setEnabled(true);
            break;
        case QWebEngineDownloadRequest::DownloadCompleted:
            toPauseAction(*m_pause).setStatus(PauseAction::Stopped);
            m_stop.setEnabled(false);
            m_remove.setEnabled(true);
            m_openFolder.setEnabled(true);
            m_copyUrl.setEnabled(true);
            break;
        case QWebEngineDownloadRequest::DownloadCancelled:
            toPauseAction(*m_pause).setStatus(PauseAction::Stopped);
            m_stop.setEnabled(false);
            m_remove.setEnabled(true);
            m_openFolder.setEnabled(false);
            m_copyUrl.setEnabled(true);
            break;
        case QWebEngineDownloadRequest::DownloadInterrupted:
            toPauseAction(*m_pause).setStatus(PauseAction::Stopped);
            m_stop.setEnabled(false);
            m_remove.setEnabled(true);
            m_openFolder.setEnabled(true);
            m_copyUrl.setEnabled(true);
            break;
    }
}

void Actions::
pause()
{
    switch (toPauseAction(*m_pause).status()) {
        case PauseAction::Progress:
            m_manager.pause(m_id);
            break;
        case PauseAction::Paused:
            m_manager.resume(m_id);
            break;
        default:
            break;
    }
}

void Actions::
stop()
{
    auto const rc = QMessageBox::question(
        QApplication::activeWindow(),
        QSL("Stop"),
        QSL("Really want to stop download?")
    );
    if (rc == QMessageBox::Yes) {
        m_manager.cancel(m_id);
    }
}

void Actions::
remove()
{
    auto const rc = QMessageBox::question(
        QApplication::activeWindow(),
        QSL("Remove"),
        QSL("Really want to remove download?")
    );
    if (rc == QMessageBox::Yes) {
        m_manager.remove(m_id);
    }
}

void Actions::
openFolder()
{
    m_manager.openFolder(m_id);
}

void Actions::
copyUrl()
{
    m_manager.copyUrl(m_id);
}

} // namespace natsu::modules::download::browser
