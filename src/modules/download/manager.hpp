#ifndef NATSU_MODULES_DOWNLOAD_MANAGER_HPP
#define NATSU_MODULES_DOWNLOAD_MANAGER_HPP

#include "action/base.hpp"

#include "core/fwd/profile.hpp"

#include <QObject>
#include <QTemporaryDir>

class QDir;
class QSettings;
class QWebEngineDownloadRequest;

namespace natsu::modules::download {

class Manager : public QObject
{
    Q_OBJECT
public:
    Manager(Profile&);
    ~Manager() override;

    // accessor
    Profile& profile() const { return m_profile; }

    // query
    QString externalManager() const;
    QDir downloadFolder() const;
    QList<QWebEngineDownloadRequest*> items() const;
    QWebEngineDownloadRequest* item(uint32_t id) const;

    // setting
    QSettings settings() const;

    // singal
    Q_SIGNAL void itemAdded(QWebEngineDownloadRequest&) const;
    Q_SIGNAL void itemRemoved(QWebEngineDownloadRequest&) const;

    // command
    void cancel(uint32_t id);
    void remove(uint32_t id);
    void pause(uint32_t id);
    void resume(uint32_t id);
    void openFolder(uint32_t id);
    void copyUrl(uint32_t id);

private:
    action::Base* loadAutomaticAction(QWebEngineDownloadRequest&);
    void saveAutomaticAction(QWebEngineDownloadRequest&, action::Base&);
    action::Base* askAction(QWebEngineDownloadRequest&);

    action::Base* makeAction(QWebEngineDownloadRequest&, QJsonObject const&);

    Q_SLOT void onRequested(QWebEngineDownloadRequest*);
    Q_SLOT void onQueryClose(bool& cancel);

private:
    Profile& m_profile;

    QTemporaryDir m_tempDir;
};

} // namespace natsu::modules::download

#endif // NATSU_MODULES_DOWNLOAD_MANAGER_HPP
