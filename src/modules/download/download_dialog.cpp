#include "download_dialog.hpp"

#include "manager.hpp"

#include "download_dialog/action_panel.hpp"
#include "download_dialog/information_panel.hpp"

#include "global.hpp"
#include "mime/application.hpp"

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFont>
#include <QLabel>
#include <QUrl>
#include <QWidget>

namespace natsu::modules::download {

static void
bold(QLabel& label)
{
    auto font = label.font();
    font.setWeight(QFont::Bold);
    label.setFont(font);
}

// DownloadDialog

DownloadDialog::
DownloadDialog(Manager& manager,
               QUrl const& url,
               QString const& fileName,
               QString const& mimeType,
               QWidget& parent)
    : QDialog { &parent }
    , m_manager { manager }
    , m_url { url }
    , m_fileName { fileName }
    , m_mimeType { mimeType }
    , m_information { make_qmanaged<download_dialog::InformationPanel>(*this) }
    , m_actionPanel { make_qmanaged<download_dialog::ActionPanel>(*this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
{
    auto const layout = make_qmanaged<QVBoxLayout>(this);

    auto const label1 = make_qmanaged<QLabel>(QSL("You have chosen to open:"), this);
    layout->addWidget(label1.get());

    layout->addWidget(m_information.get());

    auto const label2 = make_qmanaged<QLabel>(QSL("What to do with this file?"), this);
    bold(*label2);
    layout->addWidget(label2.get());

    layout->addWidget(m_actionPanel.get());

    m_buttons->addButton(QDialogButtonBox::Ok);
    m_buttons->addButton(QDialogButtonBox::Cancel);
    layout->addWidget(m_buttons.get());

    this->setWindowTitle(QSL("Opening %1").arg(fileName));

    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &QDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &QDialog::reject);

    this->resize(400, 300);
}

DownloadDialog::~DownloadDialog() = default;

Action DownloadDialog::
action() const
{
    return m_actionPanel->action();
}

mime::Application DownloadDialog::
application() const
{
    return m_actionPanel->application();
}

bool DownloadDialog::
remember() const
{
    return m_actionPanel->remember();
}

} // namespace natsu::modules::download
