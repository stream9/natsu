#include "adaptor.hpp"

#include "manager.hpp"

#include "browser/dock.hpp"

#include "browser/browser_tab.hpp"
#include "browser/navigation_bar.hpp"
#include "browser/browser_window.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "misc/pointer.hpp"

namespace natsu::modules::download {

Adaptor::
Adaptor(Profile& profile)
    : QObject { &profile }
    , m_profile { profile }
    , m_manager { std::make_unique<Manager>(m_profile) }
{
    this->connect(&m_profile, &Profile::windowCreated,
                  this,       &Adaptor::onWindowCreated);
}

Adaptor::~Adaptor() = default;

void Adaptor::
installBrowserDock(BrowserWindow& window) const
{
    auto const widget = make_qmanaged<browser::Dock>(*m_manager, window);

    auto& action = to_ref(widget->toggleViewAction());
    window.addAction(&action);

    if (!window.restoreDockWidget(widget.get())) {
        window.addDockWidget(Qt::LeftDockWidgetArea, widget.get());
    }
}

void Adaptor::
onWindowCreated(BrowserWindow& window)
{
    installBrowserDock(window);
    this->connect(&window, &BrowserWindow::tabCreated,
                  this,    &Adaptor::onTabCreated);
}

void Adaptor::
onTabCreated(BrowserTab& tab)
{
    //TODO tool button on navigation bar
    auto& navigationBar = tab.navigationBar();
    (void)navigationBar;
}

} // namespace natsu::modules::download
