#ifndef NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_HPP
#define NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_HPP

#include "action.hpp"

#include "download_dialog/fwd/action_panel.hpp"
#include "download_dialog/fwd/information_panel.hpp"

#include "misc/pointer.hpp"
#include "mime/fwd/application.hpp"

#include <QDialog>

class QDialogButtonBox;
class QString;
class QUrl;
class QWebEngineDownloadItem;
class QWidget;

namespace natsu::modules::download {

class Manager;

class DownloadDialog : public QDialog
{
    Q_OBJECT
public:
    DownloadDialog(Manager&,
                   QUrl const&,
                   QString const& fileName,
                   QString const& mimeType,
                   QWidget& parent);

    ~DownloadDialog() override;

    // accessor
    Manager& manager() const { return m_manager; }
    QUrl const& url() const { return m_url; }
    QString const& fileName() const { return m_fileName; }
    QString const& mimeType() const { return m_mimeType; }

    // query
    Action action() const;
    mime::Application application() const;
    bool remember() const;

private:
    Manager& m_manager;
    QUrl const& m_url;
    QString const& m_fileName;
    QString const m_mimeType;

    qmanaged_ptr<download_dialog::InformationPanel> m_information;
    qmanaged_ptr<download_dialog::ActionPanel> m_actionPanel;

    qmanaged_ptr<QDialogButtonBox> m_buttons;
};

} // namespace natsu::modules::download

#endif // NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_HPP
