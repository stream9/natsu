#include "module.hpp"

#include "adaptor.hpp"

#include "core/application.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "misc/pointer.hpp"

#include <memory>

namespace natsu::modules::download {

Module::Module() = default;
Module::~Module() = default;

bool Module::
doInitialize(Application& app)
{
    for (auto const& profile: app.profiles()) {
        onProfileCreated(*profile);
    }
    this->connect(&app, &Application::profileCreated,
                  this, &Module::onProfileCreated);

    return true;
}

void Module::
onProfileCreated(Profile& profile)
{
    make_qmanaged<Adaptor>(profile);
}

} // namespace natsu::modules::download
