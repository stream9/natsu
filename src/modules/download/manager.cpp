#include "manager.hpp"

#include "action.hpp"
#include "download_dialog.hpp"

#include "action/delegate.hpp"
#include "action/open.hpp"
#include "action/save.hpp"

#include "browser/browser_window.hpp"
#include "core/application.hpp"
#include "core/get.hpp"
#include "core/profile.hpp"
#include "global.hpp"
#include "mime/application.hpp"
#include "mime/database.hpp"

#include <QClipboard>
#include <QDesktopServices>
#include <QDir>
#include <QFileDialog>
#include <QGuiApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMessageBox>
#include <QMimeDatabase>
#include <QSettings>
#include <QStandardPaths>
#include <QUrl>
#include <QVariant>
#include <QWebEngineDownloadRequest>

namespace natsu::modules::download {

static std::optional<QDir>
mkdir(QString const& path)
{
    QDir const folder { path };
    if (!folder.exists()) {
        if (!folder.mkpath(QSL("."))) {
            return std::nullopt;;
        }
    }

    return folder;
}

static QString
defaultDownloadPath()
{
    return QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
}

static QString
automaticActionGroup()
{
    return QSL("AutomaticAction");
}

static bool
isAltPressed()
{
    auto const modifiers = QGuiApplication::keyboardModifiers();
    return modifiers.testFlag(Qt::AltModifier);
}

static QString
mimeType(QWebEngineDownloadRequest const& item)
{
    auto const& mimeType = item.mimeType();
    auto const& fileName = item.downloadFileName();

    if (mimeType == QSL("application/force-download")) {
        QMimeDatabase db;
        auto const& candidate = db.mimeTypesForFileName(fileName);
        if (candidate.empty()) {
            return mimeType;
        }
        else {
            return candidate.front().name();
        }
    }
    else {
        return mimeType;
    }
}

// Manager

Manager::
Manager(Profile& profile)
    : m_profile { profile }
{
    this->connect(&m_profile, &Profile::downloadRequested,
                  this,       &Manager::onRequested);

    this->connect(&get::application(m_profile), &Application::queryClose,
                  this,                         &Manager::onQueryClose);
}

Manager::~Manager() = default;

// return empty if setting doesn't exist
QString Manager::
externalManager() const
{
    return settings().value(QSL("externalManager")).toString();
}

// return default directory in case of no/wrong setting
QDir Manager::
downloadFolder() const
{
    auto path = settings().value(QSL("downloadFolder")).toString();
    if (path.isEmpty()) {
        path = defaultDownloadPath();
    }

    auto const& dir = mkdir(path);
    if (!dir) {
        auto const& defaultDir = mkdir(defaultDownloadPath());
        assert(defaultDir);

        return *defaultDir;
    }

    return *dir;
}

QList<QWebEngineDownloadRequest*> Manager::
items() const
{
    return m_profile.findChildren<QWebEngineDownloadRequest*>();
}

QWebEngineDownloadRequest* Manager::
item(uint32_t const id) const
{
    for (auto* const ptr: items()) {
        if (ptr->id() == id) return ptr;
    }

    return nullptr;
}

QSettings Manager::
settings() const
{
    auto const& dir = m_profile.pluginDir();
    assert(dir.exists());

    return { dir.filePath(QSL("download.ini")), QSettings::IniFormat };
}

void Manager::
cancel(uint32_t const id)
{
    auto* const item = this->item(id);
    if (!item) return;

    item->cancel();
}

void Manager::
remove(uint32_t const id)
{
    auto* const item = this->item(id);
    if (!item) return;

    item->cancel();
    Q_EMIT itemRemoved(*item);

    item->setParent(nullptr);
    item->deleteLater();
}

void Manager::
pause(uint32_t const id)
{
    auto* const item = this->item(id);
    if (!item) return;

    item->pause();
}

void Manager::
resume(uint32_t const id)
{
    auto* const item = this->item(id);
    if (!item) return;

    item->resume();
}

void Manager::
openFolder(uint32_t const id)
{
    auto* const item = this->item(id);
    if (!item) return;

    QDesktopServices::openUrl(QUrl::fromLocalFile(item->downloadDirectory()));
}

void Manager::
copyUrl(uint32_t const id)
{
    auto* const item = this->item(id);
    if (!item) return;

    auto* const clipboard = QGuiApplication::clipboard();
    assert(clipboard);
    clipboard->setText(item->url().toEncoded());
}

action::Base* Manager::
loadAutomaticAction(QWebEngineDownloadRequest& item)
{
    if (isAltPressed()) return nullptr;

    auto settings = this->settings();

    settings.beginGroup(automaticActionGroup());

    auto const& actionObj = QJsonDocument::fromJson(
        settings.value(mimeType(item)).toString().toUtf8()).object();

    settings.endGroup();

    return makeAction(item, actionObj);
}

void Manager::
saveAutomaticAction(QWebEngineDownloadRequest& item, action::Base& action)
{
    auto settings = this->settings();

    QJsonDocument const doc { action.toJson() };

    settings.beginGroup(automaticActionGroup());

    settings.setValue(mimeType(item), QString(doc.toJson(QJsonDocument::Compact)));

    settings.endGroup();
}

action::Base* Manager::
askAction(QWebEngineDownloadRequest& item)
{
    action::Base* action = nullptr;
    auto& window = get::activeWindow(m_profile);
    auto const& mimeType_ = mimeType(item);

    DownloadDialog dialog {
        *this,
        item.url(),
        item.downloadFileName(),
        mimeType_,
        window
    };

    if (dialog.exec() == QDialog::Accepted) {

        switch (dialog.action()) {
            case Action::Save: {
                auto const& initialPath = QDir::cleanPath(
                    downloadFolder().filePath(item.downloadFileName()));

                mime::Database const mimeDb;
                auto const& filter = mimeDb.filterString(mimeType_);

                auto const& selectedPath = QFileDialog::getSaveFileName(
                    &window,
                    QSL("Save File"),
                    initialPath,
                    filter
                );
                if (!selectedPath.isEmpty()) {
                    action = make_qmanaged<action::Save>(
                                    item, selectedPath, *this).get();
                }
                break;
            }
            case Action::Open: {
                auto const& application = dialog.application();
                action = make_qmanaged<action::Open>(
                        item, m_tempDir, application.commandLine, *this);
                break;
            }
            case Action::Delegate:
                action = make_qmanaged<action::Delegate>(
                                         item, externalManager(), *this);
                break;
            default:
                assert(false);
                break;
        }

        if (action) {
            if (dialog.remember()) {
                saveAutomaticAction(item, *action);
            }
        }
    }
    else {
        item.cancel();
    }

    return action;
}

action::Base* Manager::
makeAction(QWebEngineDownloadRequest& item, QJsonObject const& obj)
{
    if (obj.isEmpty()) {
        return nullptr;
    }

    auto const& type = obj[QSL("type")].toString();

    if (type == QSL("save")) {
        QFileInfo const dirInfo { obj[QSL("directory")].toString() };

        if (!dirInfo.exists() || !dirInfo.isDir() || !dirInfo.isWritable()) {
            qWarning() << "wrong download folder at" << __PRETTY_FUNCTION__
                       << QJsonDocument(obj).toJson();
            return nullptr;
        }

        QDir const dir { dirInfo.canonicalFilePath() };
        QFileInfo const fileInfo { dir.filePath(item.downloadFileName()) };

        if (fileInfo.exists()) {
            auto& window = get::activeWindow(m_profile);

            auto const response = QMessageBox::question(
                &window,
                QSL("Download"),
                QSL("File already exists. Do you want to overwrite?")
            );
            if (response != QMessageBox::Yes) {
                return nullptr;
            }
        }

        auto const action = make_qmanaged<action::Save>(
                            item, fileInfo.canonicalFilePath(), *this);

        return action.get();
    }
    else if (type == QSL("open")) {
        auto const& command = obj[QSL("command")].toString();
        if (command.isNull()) {
            qWarning() << "Invalid action data at" << __PRETTY_FUNCTION__
                       << QJsonDocument(obj).toJson();
            return nullptr;
        }

        auto const action =
            make_qmanaged<action::Open>(item, m_tempDir, command, *this);

        return action.get();
    }
    else if (type == QSL("delegate")) {
        auto const& command = obj[QSL("command")].toString();
        if (command.isNull()) {
            qWarning() << "Invalid action data at" << __PRETTY_FUNCTION__
                       << QJsonDocument(obj).toJson();
            return nullptr;
        }

        auto const action = make_qmanaged<action::Delegate>(item, command, *this);

        return action.get();
    }
    else {
        qWarning() << "Invalid action type at" << __PRETTY_FUNCTION__
                   << QJsonDocument(obj).toJson();
        return nullptr;
    }
}

void Manager::
onRequested(QWebEngineDownloadRequest* req)
{
    assert(req);

    auto* action = loadAutomaticAction(*req);

    if (!action) {
        action = askAction(*req);
    }

    if (action) {
        action->run();
    }

    if (req->state() == QWebEngineDownloadRequest::DownloadCancelled) {
        req->deleteLater();
    }
    else {
        Q_EMIT itemAdded(*req);
    }
}

void Manager::
onQueryClose(bool& cancel)
{
    auto const& items = this->items();

    auto const downloading = std::any_of(
        items.begin(), items.end(),
        [](auto item) {
            return item->state() == QWebEngineDownloadRequest::DownloadInProgress;
        });

    if (downloading) {
        auto const response = QMessageBox::question(
            &get::activeWindow(m_profile),
            QSL("Download"),
            QSL("There is a download still running. Really want to quit?")
        );
        if (response != QMessageBox::Yes) {
            cancel = true;
        }
    }
}

} // namespace natsu::modules::download
