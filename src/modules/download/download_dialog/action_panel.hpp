#ifndef NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_ACTION_PANEL_HPP
#define NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_ACTION_PANEL_HPP

#include "../action.hpp"
#include "../fwd/download_dialog.hpp"

#include "mime/fwd/application.hpp"
#include "misc/pointer.hpp"

#include <QWidget>

class QCheckBox;
class QComboBox;
class QRadioButton;

namespace natsu::modules::download::download_dialog {

using download::Action;

class ActionPanel : public QWidget
{
    Q_OBJECT
public:
    ActionPanel(DownloadDialog&);
    ~ActionPanel() override;

    Action action() const;
    mime::Application application() const;
    bool remember() const;

private:
    void loadSettings();
    void saveSettings();

    Q_SLOT void onApplicationSelected();

private:
    DownloadDialog& m_dialog;

    qmanaged_ptr<QRadioButton> const m_open;
    qmanaged_ptr<QComboBox> const m_with;
    qmanaged_ptr<QRadioButton> const m_save;
    qmanaged_ptr<QRadioButton> const m_external;
    qmanaged_ptr<QCheckBox> const m_remember;
};

} // namespace natsu::modules::download::download_dialog

#endif // NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_ACTION_PANEL_HPP
