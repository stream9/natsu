#ifndef NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_APPLICATION_LIST_HPP
#define NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_APPLICATION_LIST_HPP

#include "../fwd/download_dialog.hpp"

#include <QComboBox>

namespace natsu::modules::download::download_dialog {

class ApplicationList : public QComboBox
{
    Q_OBJECT
public:
    ApplicationList(DownloadDialog&);
    ~ApplicationList() override;

    Q_SLOT void onCurrentChanged(int const index);

private:
    DownloadDialog& m_dialog;
};

} // namespace natsu::modules::download::download_dialog

#endif // NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_APPLICATION_LIST_HPP
