#include "application_list.hpp"


#include "../download_dialog.hpp"

#include "mime/application_dialog.hpp"
#include "mime/application.hpp"
#include "mime/database.hpp"

#include "global.hpp"

#include <QSignalBlocker>

namespace natsu::modules::download::download_dialog {

ApplicationList::
ApplicationList(DownloadDialog& dialog)
    : QComboBox { &dialog }
    , m_dialog { dialog }
{
    mime::Database const mimeDb;
    auto const& defaultApp = mimeDb.defaultApplication(dialog.mimeType());

    if (defaultApp) {
        auto const& defaultAppText =
                            QSL("%1 (default)").arg(defaultApp->name);

        this->addItem(defaultAppText, QVariant::fromValue(*defaultApp));
        this->insertSeparator(INT_MAX);
        this->addItem(QSL("others..."));

        this->connect(this, QOverload<int>::of(&QComboBox::currentIndexChanged),
                      this, &ApplicationList::onCurrentChanged);
    }
}

ApplicationList::~ApplicationList() = default;

void ApplicationList::
onCurrentChanged(int const index)
{
    assert(index >= 0);

    QSignalBlocker const blocker { this };

    if (index == this->count()-1) { // "others..."
        mime::Database const mimeDb;
        auto const& mimeType = m_dialog.mimeType();

        mime::ApplicationDialog dialog { mimeType, m_dialog };

        if (dialog.exec() == QDialog::Accepted) {
            auto const pos = this->count() - 2; // last?
            assert(pos > 0);
            auto const& app = dialog.selected();

            this->insertItem(pos, app.name, QVariant::fromValue(app));
            this->setCurrentIndex(pos);
        }
        else {
            this->setCurrentIndex(0);
        }
    }
}

} // namespace natsu::modules::download::download_dialog
