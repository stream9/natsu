#ifndef NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_FWD_INFORMATION_PANEL_HPP
#define NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_FWD_INFORMATION_PANEL_HPP

namespace natsu::modules::download::download_dialog {

class InformationPanel;

} // namespace natsu::modules::download::download_dialog

#endif // NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_FWD_INFORMATION_PANEL_HPP
