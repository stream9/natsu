#include "action_panel.hpp"

#include "application_list.hpp"

#include "../download_dialog.hpp"
#include "../manager.hpp"

#include "global.hpp"
#include "mime/application.hpp"

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QRadioButton>
#include <QSettings>
#include <QString>

namespace natsu::modules::download::download_dialog {

static QString lastActionKey() { return QSL("lastAction"); }

// ActionPanel

ActionPanel::
ActionPanel(DownloadDialog& dialog)
    : QWidget { &dialog }
    , m_dialog { dialog }
    , m_open { make_qmanaged<QRadioButton>(this) }
    , m_with { make_qmanaged<ApplicationList>(m_dialog) }
    , m_save { make_qmanaged<QRadioButton>(this) }
    , m_external { make_qmanaged<QRadioButton>(this) }
    , m_remember { make_qmanaged<QCheckBox>(this) }
{
    auto layout = make_qmanaged<QHBoxLayout>(this);
    auto right = make_qmanaged<QVBoxLayout>();

    auto open = make_qmanaged<QHBoxLayout>();

    m_open->setText(QSL("Open with"));
    open->addWidget(m_open.get());
    open->addWidget(m_with.get(), 1);
    open->addStretch(1);

    right->addLayout(open.get());

    m_save->setText(QSL("Save File"));
    right->addWidget(m_save.get());

    m_external->setText(QSL("Download with External Manager"));
    right->addWidget(m_external.get());

    m_remember->setText(QSL("Do this automatically for file like this from now on."));
    right->addWidget(m_remember.get());

    layout->addSpacing(32);
    layout->addLayout(right.get());

    if (m_with->count() == 0) {
        m_open->hide();
        m_with->hide();
    }

    if (dialog.manager().externalManager().isEmpty()) {
        m_external->hide();
    }

    this->connect(m_with.get(), QOverload<int>::of(&QComboBox::currentIndexChanged),
                  this,         &ActionPanel::onApplicationSelected);

    loadSettings();
}

ActionPanel::
~ActionPanel()
{
    saveSettings();
}

Action ActionPanel::
action() const
{
    if (m_open->isChecked()) {
        return Action::Open;
    }
    else if (m_save->isChecked()) {
        return Action::Save;
    }
    else if (m_external->isChecked()) {
        return Action::Delegate;
    }
    else {
        assert(false);
        return Action::Open;
    }
}

mime::Application ActionPanel::
application() const
{
    return m_with->currentData().value<mime::Application>();
}

bool ActionPanel::
remember() const
{
    return m_remember->checkState() != Qt::Unchecked;
}

void ActionPanel::
onApplicationSelected()
{
    m_open->setChecked(true);
}

void ActionPanel::
loadSettings()
{
    auto& manager = m_dialog.manager();

    auto const action =
        static_cast<Action>(manager.settings().value(lastActionKey()).toInt());

    switch (action) {
        case Action::Open: {
            if (m_open->isVisible()) {
                m_open->setChecked(true);
                break;
            }
            else {
                [[fallthrough]];
            }
        }
        case Action::Save:
        default:
            m_save->setChecked(true);
            break;
        case Action::Delegate:
            m_external->setChecked(true);
            break;
    }
}

void ActionPanel::
saveSettings()
{
    auto settings = m_dialog.manager().settings();

    settings.setValue(lastActionKey(), static_cast<int>(action()));
}

} // namespace natsu::modules::download::download_dialog
