#include "information_panel.hpp"

#include "../download_dialog.hpp"

#include "misc/format.hpp"
#include "misc/elided_label.hpp"
#include "mime/database.hpp"

#include <QBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>

namespace natsu::modules::download::download_dialog {

static void
bold(QLabel& label)
{
    auto font = label.font();
    font.setWeight(QFont::Bold);
    label.setFont(font);
}

// InformationPanel

InformationPanel::
InformationPanel(DownloadDialog& dialog)
    : QWidget { &dialog }
    , m_fileName { make_qmanaged<ElidedLabel>(this) }
    , m_fileType { make_qmanaged<QLabel>(this) }
    , m_fileSize { make_qmanaged<QLabel>(this) }
    , m_origin { make_qmanaged<QLabel>(this) }
    , m_link { make_qmanaged<QLabel>(this) }
{
    auto layout = make_qmanaged<QHBoxLayout>(this);
    auto right = make_qmanaged<QVBoxLayout>();

    bold(*m_fileName);
    m_fileName->setText(dialog.fileName());
    right->addWidget(m_fileName.get());

    auto status = make_qmanaged<QHBoxLayout>();
    auto label1 = make_qmanaged<QLabel>(QSL("which is a:"), this);
    status->addWidget(label1.get());

    mime::Database const mimeDb;
    auto const& fileType = mimeDb.string(dialog.mimeType());
    m_fileType->setText(fileType.isEmpty() ? dialog.mimeType() : fileType);
    status->addWidget(m_fileType.get());

    status->addWidget(m_fileSize.get());
    status->addStretch();
    right->addLayout(status.get());

    auto location = make_qmanaged<QHBoxLayout>();
    auto label2 = make_qmanaged<QLabel>(QSL("from"), this);
    location->addWidget(label2.get());
    m_origin->setText(dialog.url().host());
    location->addWidget(m_origin.get());
    m_link->setTextFormat(Qt::RichText);
    m_link->setText(QSL("<a href='%1'>Copy download link</a>")
                        .arg(dialog.url().toString(QUrl::FullyEncoded)));
    location->addWidget(m_link.get());
    location->addStretch();
    right->addLayout(location.get());

    auto const icon = mimeDb.icon(dialog.mimeType());
    auto iconLayout = make_qmanaged<QVBoxLayout>();
    auto iconLabel = make_qmanaged<QLabel>(this);
    iconLabel->setPixmap(icon.pixmap(32, 32));
    iconLayout->addWidget(iconLabel);
    iconLayout->addStretch(1);
    layout->addLayout(iconLayout);

    layout->addLayout(right.get());

    QNetworkRequest const req { dialog.url() };
    m_reply = m_network.head(req);
    assert(m_reply);

    this->connect(m_reply, &QNetworkReply::finished,
                  this,    &InformationPanel::onHeadFinished);
}

InformationPanel::~InformationPanel() = default;

void InformationPanel::
onHeadFinished()
{
    auto const contentLength =
        m_reply->header(QNetworkRequest::ContentLengthHeader).value<int64_t>();

    if (contentLength > 0) {
        m_fileSize->setText(format::binaryBytes(contentLength));
    }
}

} // namespace natsu::modules::download::download_dialog
