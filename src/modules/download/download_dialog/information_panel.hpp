#ifndef NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_INFORMATION_PANEL_HPP
#define NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_INFORMATION_PANEL_HPP

#include "../fwd/download_dialog.hpp"

#include "misc/pointer.hpp"

#include <QNetworkAccessManager>
#include <QWidget>

class QLabel;
class QNetworkReply;

namespace natsu::modules::download::download_dialog {

class InformationPanel : public QWidget
{
    Q_OBJECT
public:
    InformationPanel(DownloadDialog&);
    ~InformationPanel() override;

private:
    Q_SLOT void onHeadFinished();

private:
    qmanaged_ptr<QLabel> const m_fileName;
    qmanaged_ptr<QLabel> const m_fileType;
    qmanaged_ptr<QLabel> const m_fileSize;
    qmanaged_ptr<QLabel> const m_origin;
    qmanaged_ptr<QLabel> const m_link;

    QNetworkAccessManager m_network;
    QNetworkReply* m_reply;
};

} // namespace natsu::modules::download::download_dialog

#endif // NATSU_MODULES_DOWNLOAD_DOWNLOAD_DIALOG_INFORMATION_PANEL_HPP
